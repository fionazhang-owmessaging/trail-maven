package synchronoss.com.testcase.utils;

public class ConstantName {

	// Definition for navigate tab
	public static final String MAILTAB = "mail";
	public static final String CONTACTTAB = "contacts";
	public static final String CALENDARTAB = "calendar";
	public static final String TASKTAB = "task";
	public static final String SETTINGTAB = "setting";

	// Definition for top tool bar of email detail
	public static final String BACK = "back";
	public static final String UP = "up";
	public static final String DOWN = "down";
	public static final String REPLY = "reply";
	public static final String DELETE = "delete";
	public static final String MOVE = "move";
	public static final String MORE = "more";
	public static final String REPLY_ALL = "replyall";
	public static final String FORWARD = "forward";
	public static final String FORWARD_AS = "forwardas";
	public static final String CREATEC_CONTACT = "createcontact";
	public static final String ADD_TO_EXIST = "addtoexist";
	public static final String SPAM = "spam";
	public static final String NOTSPAM = "notspam";
	public static final String READ = "read";
	public static final String UNREAD = "unread";
	public static final String FLAG = "flag";
	public static final String UNFLAG = "unflag";

	public static final String CCFIELD = "cc";
	public static final String BCCFIELD = "bcc";
	public static final String TOFIELD = "to";
	
	public static final String LOW = "low";
	public static final String NORMAL = "normal";
	public static final String HIGH = "high";

	public static final String TOP = "top";
	public static final String HEIGHT = "height";
	public static final String WIDTH = "width";
	
    public static final String SHOW = "Show details";
    public static final String HIDE = "Hide details";

	
	// Definition for folder name
	public static final String MYFOLDER = "My Folders";
	public static final String TRASH = "trash";
	public static final String INBOX = "inbox";
	public static final String SENT = "Sent";
	public static final String DRAFT = "Draft";
	
	// Definition for swipe direction
	public static final String RIGHT = "right";
	public static final String LEFT = "left";

	// Definition for confirm action
	public static final String YES = "yes";
	public static final String NO = "no";
	public static final String SAVE = "save";
	public static final String CANCEL = "cancel";
	public static final String OK = "OK";
	public static final String EMPTY = "Empty";
	public static final String DONE = "Done";

	// Contact item component
	public static final String EMAIL = "Email";
	public static final String MOBILE = "Mobile";
	public static final String PHONE = "Phone";
	public static final String ADDRESS = "Address";
	public static final String PERSONAL = "Personal";
	public static final String FAX = "Fax";
	public static final String PAGER = "Pager";
	public static final String WEB = "Website";
	public static final String CHATADDR = "Chat address";
	public static final String NOTES = "Notes";

	// Contact personal option
	public static final String BIRTHDAY = "Birthday";
	public static final String ANNIVERSARY = "Anniversary";

	// Contact type option
	public static final String HOME = "Home";
	public static final String WORK = "Work";
	public static final String OTHER = "Other";
	public static final String GOOGLE = "google";
	public static final String SKYPE = "skype";

	public static final String DATEFORMAT = "yyyy/MM/dd";

	public static final String DEFAULT = "Main";
	public static final String EDIT = "edit";

	public static final String ADDTOGROUP = "group";
	public static final String MAIL = "mail";
	public static final String EVENT = "event";
	// Setting options
	public static final String PROFILE = "profile";
	public static final String LOCALE = "locale";
	public static final String CALENDAR = "calendar";
	public static final String CONTACTS = "contacts";

	// Event detail
	public static final String CALSTARTDATE = "startdate";
	public static final String CALENDDATE = "enddate";
	public static final String CALREPEATENDDATE = "endrepeatdate";
	public static final String CATEGORY = "category";
	public static final String REPEAT = "repeat";
	public static final String DAILY = "daily";
	public static final String WEEKLY = "weekly";
	public static final String MONTHLY = "monthly";
	public static final String YEARLY = "yearly";

	// Event item select field
	public static final String RECURRENCE = "recurrence";
	public static final String REMINDER = "reminder";
	public static final String REPEATENDTYPE="repeatendtype";
	
	// Event end type item values
	public static final String NEVEREND = "Never ends";
	public static final String ENDSAFTERTIMES = "Ends after a number of times" ;
	public static final String ENDSONDATE = "Ends on a specific date";
	
	// Delete Recurring event
	public static final String ONLYTHISEVENT = "Only this instance";
	public static final String ALLEVENT = "All events in the series";

	// Event category items value
	public static final String PET = "pet";
	public static final String GREEN = "green";

	// Event invitation response
	public static final String ACCPET = "accepted";
	public static final String DECLINE = "declined";
	public static final String TENTATIVE = "tentative";

	// Event reminder items values
	public static final String ATTIME = "At time of event";
	public static final String NONE = "None";

	public static final String START = "start";
	public static final String END = "end";
	public static Object NOEVENT = "No events";
	
	// Repeat event on day option
	public static final String MONTHONDAYS = "ondays";
	public static final String THISDAYOFMONTH = "This day of month";
	
	// calendar view model
	public static final String WEEKVIEW = "week";
	public static final String DAYVIEW = "day";
	public static final String LISTVIEW = "agenda";
	public static final String MONTHVIEW = "month";
	public static final String GO = "Go";
	
	
	// settings profile
	public static final String FIRSTNAME = "firstname";
	public static final String LASTNAME = "lastname";
	
	// settings calendar
	public static final String SUNDAY = "Sunday";
	public static final String MONDAY = "Monday";
	public static final String TIMEUNITMIN_30MIN = "30 m";
	public static final String TIMEUNITHOUR = "1 h";
	public static final String TIMEUNITHOUR_3HOUR = "3 h";

	// settings signature
    public static final String ABOVE = "Above the original email";
    public static final String BELOW = "Below the original email(except for rich mail reply)";

	// settings mail
	public static final String ONEMINUE = "every minute";
	public static final String MANUALLY = "manually";
	
	public static final String ACCOUNT = "accounts";
    public static final String AUTOREPLY = "auto reply";
	public static final String AUTOFWD = "auto forward";
	public static final String BLOCKSENDER = "block sender";
    public static final String SAFESENDER = "safe sender";
    public static final String BLOCKIMG = "block image";
    public static final String SIGNATURE = "signature";

    // settings time format
    public static final String HOUR24FORMAT = "24-Hour";
    public static final String HOUR12FORMAT = "12-Hour";

    

	public static final String NEWFOLDER = "newFolder";
	
	// task slide bar action
	public static final String FINISHED = "complete";
	public static final String UNFINISHED = "incomplete";
	public static final String NEEDACTION = "needs action";
	
	public static final String GROUP = "group";
	public static final String ALLTASK = "alltask";
	public static final String PRIORITY = "priority";
	public static final String SPECIFICDATE = "Specified due date";
	public static final String DUEDATE = "due date";
	public static final String DUETIME = "duetime";
	public static final String URL = "url";
	public static final String TITLE = "title";
	public static final String FIVTIEENMINTUESREMINDER = "15 minutes before";
	public static final String FIVEMINTUESREMINDER = "5 minutes before";

	
	// platform
	public static final String ANDROID = "android";
	public static final String IOS = "ios";
    public static final String IPAD = "ipad";

	// external account
	public static final String DESCRIPTION = "desc";
	public static final String NAME = "fromname";
	public static final String ACCOUNTID = "username";
	public static final String PASSWORD = "pwd";
	
	
}
