package synchronoss.com.testcase.utils;

import org.openqa.selenium.WebDriverException;

import synchronoss.com.core.Base;

public class ContactUtils extends Base {

	/**
	 * add a contact
	 *
	 * @author Fiona
	 */
	public void addContact(String firstName) {
		try {
			contact.clickAddContactButton();
			contactDetail.addFirstname(firstName);
			contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * add a contact by email, also add frist name for easy search
	 * 
	 * @param contactInfo
	 *            ContactInof entity
	 *
	 * @author Fiona
	 */

	public void addContactByEmail(ContactInfo contactInfo) {
		try {
			contact.clickAddContactButton();
			contactDetail.addFirstname(contactInfo.getFirstname());
			contactDetail.addFieldValue(contactInfo.getEmail(), contactInfo
					.getEmail().getComponentName());
			contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * add a addressbook
	 * 
	 * @param addressbook
	 *            Address book name
	 *
	 * @author Fiona
	 */
	public void addAddressbook(String addressbook) {
		try {
			navigation.clickMenuButton();
			sidebar.clickAddAddressbookIcon();
			sidebar.addAddressbookName(addressbook);
			sidebar.clickButtonOnMessageBox(ConstantName.OK);
			sidebar.clickCloseButton();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * delete contact from detail view
	 *
	 * @param search
	 *            if true, will search the contact first , then delete from
	 *            detail view. if false, will delete from contact view directly
	 * @param firstname
	 *            firstname of the contact
	 * 
	 * @author Fiona
	 */
	public void deleteContactFromDetail(boolean search, String firstname) {
		try {
			if (search) {
				contact.searchContactbyKeywords(firstname);
				contact.clickContactbyName(firstname);
			}
			contactDetail.clickDeleteButton();
			contact.clickButtonOnMessageBox(ConstantName.YES);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * search the contact first, then delete contact from side bar view
	 *
	 * @param firstname
	 *            firstname of the contact
	 * 
	 * @param deleteOrNot
	 *            if true, will click yes to delete contact directrly, if false,
	 *            will click No .
	 * 
	 * 
	 * @author Fiona
	 */
	public void deleteContactFromSlidebar(String firstname, boolean deleteOrNot) {
		try {
			contact.searchContactbyKeywords(firstname);
			contact.swipeContactByName(firstname);
			contact.clickActionButtonOnSlideToolbar(ConstantName.DELETE);
			if (deleteOrNot) {
				sidebar.clickButtonOnMessageBox(ConstantName.YES);
				sidebar.waitForLoadMaskDismissed();
			} else
				sidebar.clickButtonOnMessageBox(ConstantName.NO);
			contact.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * delete addressbook from sidebar
	 * 
	 * @param addressbookName
	 *            addressbookName of the addressbook
	 * 
	 * @author Fiona
	 */
	public void deleteAddressbook(String addressbookName) {
		try {
			sidebar.swipeAddressbookEditRMForm(addressbookName);
			sidebar.clickAddressbookNameAction(ConstantName.DELETE);
			sidebar.clickButtonOnMessageBox(ConstantName.YES);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * delete contact group from sidebar
	 * 
	 * @param contactGroupName
	 *            contact group name
	 * 
	 * @author Fiona
	 */
	public void deleteContactGroup(String contactGroupName) {
		try {
			sidebar.swipeContactGroupEditForm(contactGroupName);
			sidebar.clickContactGroupAction(ConstantName.DELETE);
			sidebar.clickButtonOnMessageBox(ConstantName.YES);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
     * add contact group from sidebar
     * 
     * @param addressbookName
     *            the address book name
     * @param contactGroup
     *            the contact group name
     * 
     * @author Fiona
     */
	public void addContactGroup(String addressbookName, String contactGroup) {
		try {
			navigation.clickMenuButton();
			sidebar.clickContactGroupEnterButton(addressbookName);
			sidebar.clickAddContactGroupButton();
			sidebar.addContactGroupName(contactGroup);
			sidebar.clickButtonOnMessageBox(ConstantName.OK);
			sidebar.clickBackToAddressbookButton();
			sidebar.clickCloseButton();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
     * add a contact to the group from detail view
     * 
     * @param contactname
     *            the contact name
     *            
     * @param contactGroup
     *            the contact group name
     * 
     * @author Fiona
     */
	public void addContactToGroup(String contactname, String contactGroup) {
		try {
			contact.searchContactbyKeywords(contactname);
			contact.clickContactbyName(contactname);
			contactDetail.clickMoreButton();
			contactDetail.clickPopupMenuLabel(ConstantName.ADDTOGROUP);
			contactDetail.clickContactGroupNameInSelectGroup(contactGroup);
			contactDetail.clickBackButton();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

  /**
   * delete all contacts in the contact list
   * 
   * @author Fiona
   */
  public void deleteAllContactInTheList() {
    try {

      while (!contact.isContactListEmpty()) {
        contact.swipeContactAndDelete();
      }
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

}
