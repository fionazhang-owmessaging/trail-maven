package synchronoss.com.testcase.utils;

import java.net.MalformedURLException;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriverException;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyFiles;
import synchronoss.com.core.Tools;
import synchronoss.com.core.UserPool;

public abstract class TestCase extends Base {
	private String dateFormat;
	private ZoneId timeZone;
	private String timeFormat;
	private boolean useDeviceTimeZone;
	private boolean autoSuggestContact;
	private boolean autoSaveOutGoing;
	private boolean timeShowInMsgList;
	private boolean emptyTrash;
	private boolean replyQuoteMsg;
	private UserPool mxosPool;
	private Map<String, String> testNGParameters;
	public static int passCountNumber = 0;
	public static int failCountNumber = 0;
	public static int skipCountNumber = 0;

	private static final String[] parameterNames = new String[] { "emailid1",
			"username1", "password1", "emailid2", "username2", "password2",
			"emailid3", "username3", "password3", "emailid4", "username4",
			"password4", "userInterface", "url" };

	// #########################################################################
	// TestNG Configuration Methods
	// #########################################################################

  @BeforeSuite(alwaysRun = true)
  @Parameters({"userPool", "userPoolFile", "ifCreateDynamicUser"})
  public void beforeSuite(@Optional("false") String usingPool,
      @Optional("userPool.xls") String userPoolFile, String ifCreateDynamicUser) {
    System.out.println("user pool get the value:" + usingPool);
    System.out.println("user pool file is :" + usingPool);
    System.out.println("If use dynamic user :" + ifCreateDynamicUser);

    if (usingPool.equalsIgnoreCase("true") && !ifCreateDynamicUser.equalsIgnoreCase("true")) {
      Base.usingUserPool = true;
      Base.userPoolFileName = userPoolFile;
      try {
        // Create user pool instance.
        Base.setPool(new UserPool(userPoolFileName));
        Logging.info("@BeforeSuite: after set get user pool" + Base.getPool());
      } catch (NullPointerException ex) {
        ex.printStackTrace();
        throw ex;
      }
      System.out.println("@BeforeSuite: User pool initialized successfully");
    } else if (usingPool.equalsIgnoreCase("true") && ifCreateDynamicUser.equalsIgnoreCase("true")) {
      Base.ifCreateDynamicUser = true;
      Base.usingUserPool = true;
    } else {
      Base.usingUserPool = false;
    }
  }


	@BeforeClass(alwaysRun = true)
	@Parameters({ "selenium.hub", "selenium.platform", "browser.url",
			"timelimit", "SuiteExecutionType", "userInterface",
			"enableCssTracker","platformVersion","deviceName","mxosIp","mxosPort" })
	public void beforeClass(@Optional("selenium.hub") String hub,
			String platform, String url, String timelimit,
			String suiteExecutionType, String userInterface,
			boolean enableTracker,String platformVersion,String deviceName,
			String mxosIp, String mxosPort) {
		long id = Thread.currentThread().getId();
		System.out.println("Before class Thread id is: " + id);
		timeout = timelimit;
		platformName = platform;
		Base.deviceName = deviceName;
		try {
			this.setBaseInstance(suiteExecutionType, hub, platformName,
					enableTracker,platformVersion,deviceName);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Logging.info("@BeforeClass: Base instance set up successfully");
		Base.base.startSession(url);
		
		if(ifCreateDynamicUser){
          mxosPool = new UserPool(mxosIp, mxosPort);
          this.setDynamicPool(mxosPool);
		}
	
		PropertyFiles.setUserInterface(userInterface);
	}
	  
	
	// initialize user pool and login
    @BeforeMethod(alwaysRun = true)
    @Parameters({ "emailid1", "username1", "password1", "emailid2",
            "username2", "password2", "emailid3", "username3", "password3",
            "emailid4", "username4", "password4", "userInterface",
            "browser.url"})
    public void beforeMethod(String emailid1, String username1,
            String password1, String emailid2, String username2,
            String password2, String emailid3, String username3,
            String password3, String emailid4, String username4,
            String password4, String userInterface, String url) {

        // always keep in same order with @Parameters
        Map<String, String> map = this.setupFromArguments(emailid1, username1,
                password1, emailid2, username2, password2, emailid3, username3,
                password3, emailid4, username4, password4, userInterface, url);

        System.out.println("before method +++ ");

        this.loginUser = username1;
        this.loginPass = password1;


        // Use user data in user pool
        Logging.info("usingUserPool: " + usingUserPool);
        if (usingUserPool && !Base.ifCreateDynamicUser) {
            try {
                // obtain a free user
                boolean userObtained = Base.getPool().obtainUserByDriver(Base.base.getDriver());

                if (!userObtained) {
                    Logging.error("@BeforeMethod: Fail to obtain user from user pool");
                    throw new IllegalStateException(
                            "Fail to obtain user from user pool");
                }
            } catch (NullPointerException ex) {
                ex.printStackTrace();
            }
            // start to update parameters in testNG
            this.updateFromUserPoolIfNeeded(map);
            Logging.info("@BeforeMethod: User obtained by session successfully");
            this.loginUser = Base.getPool().getUserName1();
            Logging.info("get user name1 from user Pool" + this.loginUser);
            this.loginPass = Base.getPool().getPassword1();
            this.testNGParameters = map;
            Logging.info("update user pool by local excel");
        }
        else if(usingUserPool&&Base.ifCreateDynamicUser){
          this.updateFromUserPoolIfNeeded(map);
          this.loginUser = this.getDynamicPool().getUserName1();
          this.loginPass = this.getDynamicPool().getPassword1();
          this.testNGParameters = map;
          Logging.info("update user pool by creating user by mxos dynamic");

        }
        else{
          this.testNGParameters = map;
          Logging.info("update user pool by ant parameter");
        }
        
        login.waitForLoginPageToLoad(240);


        try {
            doTestSetup();
        } catch (WebDriverException ex) {
            Tools.captureScreen(this.getClass().getSimpleName()
                    + "Setup failed ");
            Logging.info("Setup failed");
        }
    }
    
	@Test(alwaysRun = true)
	public void run() {
		printTestInfo();
		description();
		try {
			this.doTest();
			// this.markAsPassed();
		} catch (WebDriverException ex) {
			Tools.captureFullStackTrace(ex);
		} 
//		  finally { this.handleTestResult(); }
		 
	}

	@AfterMethod(alwaysRun = true)
	public void afterMethod(ITestResult result) {
		try {

			String testCaseName = this.getClass().getSimpleName() + ": ";
			String testResult = null;
			long id = Thread.currentThread().getId();
			System.out.println("1111111 After Method Thread id is: " + id);

			System.out.println("@AfterMethod: User logged out successfully");

			if (result.getStatus() == ITestResult.SUCCESS) {
				testResult = "PASS";
				passCountNumber++;
			} else if (result.getStatus() == ITestResult.FAILURE) {
				testResult = "FAILED";
				failCountNumber++;
				Tools.captureScreen(this.getClass().getSimpleName()
						+ "-Testcase-FailurePoint");
			} else if (result.getStatus() == ITestResult.SKIP) {
				testResult = "SKIPPED";
				skipCountNumber++;
			}
			StringBuilder sb = new StringBuilder();
			sb.append(testCaseName);
			sb.append(testResult);
			sb.append("\t Totally result : Passed: ");
			sb.append(passCountNumber);
			sb.append(" Failed: ");
			sb.append(failCountNumber);
			sb.append(" Skipped: ");
			sb.append(skipCountNumber);
			Logging.status(sb.toString());
			
			doTestCleanup();

		} catch (WebDriverException ex) {
			Tools.captureScreen(this.getClass().getSimpleName()
					+ "cleanup failed ");
			Logging.warn("@AfterMethod: User failed to log out");
		}
	}

	@AfterClass(alwaysRun = true)
	@Parameters({ "userPool" })
	public void afterClass(Boolean usingUserPool) {
		long id = Thread.currentThread().getId();
		System.out.println("1111111 After Class Thread id is: " + id);

        if (usingUserPool && !Base.ifCreateDynamicUser) {
			// Reset login user/pass
			this.loginUser = "";
			this.loginPass = "";

			// Release session occupied user from user pool.
			Base.getPool().setUserFree(Base.base.getDriver());
			Logging.info("@AfterClass: User reclaimed by user pool successfully");
		}

		Logging.info("@AfterClass: Test case completed .. stopping browser and quit simulator");

		// Quit driver and close the browser.
		Base.base.getDriver().quit();
		System.out.println("------------------------------------------------");
		System.out.println("------------------------------------------------");
	}

	@AfterSuite(alwaysRun = true)
	@Parameters({ "enableCssTracker" })
	public void afterSuite(boolean enableTracker) {
		if (enableTracker) {
			Base.cssTracker.saveRecords();
			Logging.info("@AfterSuite: Xpath-CssClass mapping records saved successfully");
		}
	}

	public void doTestSetup() {
		Logging.info("Do test case set up ");
		login.typeUsername(this.loginUser);
		login.typePassword(this.loginPass);
		login.clickLoginButton();

		useDeviceTimeZone = login.isUseDeviceTimeZone();
		autoSuggestContact = login.isAutoSuggestContact();
		dateFormat = login.getUserDateFormat();
		timeZone = login.getUserTimeZone();
		timeFormat = login.getUserTimeFormat();
		autoSaveOutGoing = login.isAutoSaveOutGoingMsg();
		timeShowInMsgList = login.isTimeDisplayedInMsgList();
		emptyTrash = login.isEmptyTrashWhenLogout();
		replyQuoteMsg = login.isReplyQuoteMessage();
		
		// check if conversation view related bug is displayed
		if(login.isConversionViewPattern()){
			if(login.isErrorPopDisplayed()){
				login.clickButtonOnMessageBox(ConstantName.OK);
				// go to setting - mail view, change conversion view as true
				navigation.clickNavigationButton(ConstantName.SETTINGTAB);
				settings.clickSettingOptionList(settings.toCapitlize(ConstantName.MAIL));
				settings.useConversionView(false);
				settings.clickHeadBarOfSettingDetail(ConstantName.SAVE);
				settings.clickHeadBarOfSettingDetail(ConstantName.BACK);
				navigation.clickNavigationButton(ConstantName.MAILTAB);
			}
		}
		
		

		

		Logging.info("user date format is : " + dateFormat
				+ " user time format is : " + timeZone
				+ " user time zone is : " + timeFormat
				+ "use device time zone :" + useDeviceTimeZone);
	}

	public void doTestCleanup() {
		Logging.info("Do test case clean up ");
		// navigation.clickLogoutButton();
		navigation.clickNavigationButton(ConstantName.CALENDARTAB);
		navigation.clickMenuButton();
		navigation.clickLogoutButton();

	}
/*
	private void handleTestResult(ITestResult result) {
		String status = this.getClass().getSimpleName() + ": ";
		status += isPassed() ? "PASS" : "FAIL";
		Logging.status(status);

		// take a screenshot if the test fails
		if (status.toLowerCase().contains("fail")) {
			Tools.captureScreen(this.getClass().getSimpleName()
					+ "-Testcase-FailurePoint");
		}*/


	public void printTestInfo() {
		Logging.description("Test Started: " + this.getClass().getSimpleName());
	}

	private boolean passed = false;

	private void updateFromUserPoolIfNeeded(Map<String, String> map) {
		if (Base.usingUserPool) {
			this.updateFromUserPool(map);
		}
	}

	/**
	 * Update test date info from user pool
	 * 
	 * @param map
	 */
	private void updateFromUserPool(Map<String, String> map) {
		Logging.info("Start to update user pool");
		UserPool pool = null;
		
		// check if use dynamic user pool
        if (Base.usingUserPool && Base.ifCreateDynamicUser) {
          pool = this.getDynamicPool();
        } else {
          pool = Base.getPool();
        }
		// edmund.maruhn: NOTICE
		// a more generic solution would be better here, but requires UserPool
		// to be restructured

		map.put("emailid1", pool.getEmailId1());
		map.put("username1", pool.getUserName1());
		map.put("password1", pool.getPassword1());

		map.put("emailid2", pool.getEmailId2());
		map.put("username2", pool.getUserName2());
		map.put("password2", pool.getPassword2());

		map.put("emailid3", pool.getEmailId3());
		map.put("username3", pool.getUserName3());
		map.put("password3", pool.getPassword3());

		map.put("emailid4", pool.getEmailId4());
		map.put("username4", pool.getUserName4());
		map.put("password4", pool.getPassword4());
		
		Logging.info("Finished to update user pool");

	}
	
  /**
   * Get the dynamic user pool file
   * 
   * @return UserPool
   */
  private UserPool getDynamicPool() {
    return this.mxosPool;
  }

  /**
   * Set the dynamic user pool file
   * 
   * @param UserPool
   */
  private void setDynamicPool(UserPool pool) {
    this.mxosPool = pool;
  }

  
  /**
	 * @Name setupFromArguments
	 * @Description set all test case parameters into map
	 * @param arguments
	 * @return map
	 */
	private Map<String, String> setupFromArguments(String... arguments) {
		Map<String, String> map = new HashMap<String, String>();

		for (int index = 0; index < TestCase.parameterNames.length; ++index) {
			map.put(TestCase.parameterNames[index], arguments[index]);
		}

		return map;
	}

	public final String getParameter(String key) {
		return testNGParameters != null ? testNGParameters.get(key) : null;
	}

	public final void markAsPassed() {
		this.passed = true;
	}

	public final boolean isPassed() {
		return passed;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public ZoneId getTimeZone() {
		return timeZone;
	}

	public String getTimeFormat() {
		return timeFormat;
	}

	public boolean isUseDeviceTimeZone() {
		return useDeviceTimeZone;
	}

	public boolean isAutoSuggestContact() {
		return autoSuggestContact;
	}

	public boolean isAutoSaveOutGoingMsg() {
		return autoSaveOutGoing;
	}

	public boolean isTimeShowInMsgList() {
		return timeShowInMsgList;
	}

	public boolean isEmptyTrash() {
		return emptyTrash;
	}

	public boolean isReplyQuoteMsg() {
		return replyQuoteMsg;
	}

	abstract protected void doTest();

	abstract protected void description();
}
