package synchronoss.com.testcase.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import synchronoss.com.component.ContactChatAddrField;
import synchronoss.com.component.ContactEmailField;
import synchronoss.com.component.ContactFaxField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.component.ContactMobileField;
import synchronoss.com.component.ContactPagerField;
import synchronoss.com.component.ContactPhoneField;
import synchronoss.com.component.ContactWebsiteField;

public class ContactInfo {
	private String firstname = null;
	private String middlename = null;
	private String lastname = null;
	private String nickname = null;

	private ContactEmailField email;
	private ContactMobileField mobile;
	private ContactPhoneField phone;
	private ArrayList<String> homeAddress;
	private ArrayList<String> workAddress;
	private ArrayList<String> otherAddress;

	private LocalDateTime bitrhday;
	private LocalDateTime anniversary;

	private HashMap<String, String> jobs;
	private ContactFaxField fax;
	private ContactPagerField pager;
	private ContactChatAddrField chatAddress;
	private ContactWebsiteField website;
	private String notes;
	private String others;

	private String webSite_home = "http://www.amazon.com/";
	private String webSite_work = "www.bbc.com";
	private String webSite_other = "https://jira.owmessaging.com";

	public ContactInfo(ContactEmailField email) {
		Random rand = new Random();
		firstname = "uiaFirstName" + rand.nextInt(10000);
		lastname = "uiaLastName" + rand.nextInt(10000);
		middlename = "uiaMiddleName" + rand.nextInt(10000);
		nickname = "uiaNickName" + rand.nextInt(10000);

		this.email = email;
	}

  public ContactInfo(ContactItem item1, ContactMobileField mobileField) {
    Random rand = new Random();
    firstname = "uiaFirstName" + rand.nextInt(10000);
    this.mobile = mobileField;
    mobileField.setItems(item1);
  }

  public ContactInfo(ContactItem item1, ContactPhoneField phoneField) {
    Random rand = new Random();
    firstname = "uiaFirstName" + rand.nextInt(10000);
    this.phone = phoneField;
    phoneField.setItems(item1);
  }

  public ContactInfo(ArrayList<ContactItem> items, ContactEmailField email) {
    this.email = email;
    email.setItems(items);
}
	public ContactInfo(ArrayList<ContactItem> items,
			ContactMobileField mobileField) {
		this.mobile = mobileField;
		mobile.setItems(items);
	}

	public ContactInfo(ArrayList<ContactItem> items,
			ContactPhoneField phoneField) {
		this.phone = phoneField;
		phone.setItems(items);
	}

	public ContactInfo(ArrayList<ContactItem> items, ContactFaxField faxField) {
		this.fax = faxField;
		fax.setItems(items);
	}
	
	public ContactInfo(ArrayList<ContactItem> items,
			ContactPagerField pagerField) {
		this.pager = pagerField;
		pager.setItems(items);
	}

	
	public ContactInfo(ArrayList<ContactItem> items,
			ContactWebsiteField webField) {
		this.website = webField;
		website.setItems(items);
	}


	public ContactInfo(ArrayList<ContactItem> items,
			ContactChatAddrField chatAddrField) {
		this.chatAddress = chatAddrField;
		chatAddress.setItems(items);
	}
	
	
	public ContactInfo(String firstName) {
		this.firstname = firstName;
	}

	public ContactInfo() {
		Random rand = new Random();
		firstname = "uiaFirstName" + rand.nextInt(10000);
		lastname = "uiaLastName" + rand.nextInt(10000);
		middlename = "uiaMiddleName" + rand.nextInt(10000);
		nickname = "uiaNickName" + rand.nextInt(10000);
	}

	public ContactEmailField getEmail() {
		return email;
	}

	public void setEmail(ContactEmailField email) {
		this.email = email;
	}

	public ContactMobileField getMobile() {
		return mobile;
	}

	public void setMobile(ContactMobileField mobile) {
		this.mobile = mobile;
	}

	public ContactPhoneField getPhone() {
		return phone;
	}

	public void setPhone(ContactPhoneField phone) {
		this.phone = phone;
	}

	public ContactFaxField getFax() {
		return fax;
	}

	public void setFax(ContactFaxField fax) {
		this.fax = fax;
	}

	public ContactPagerField getPager() {
		return pager;
	}

	public void setPager(ContactPagerField pager) {
		this.pager = pager;
	}

	public ContactChatAddrField getChatAddress() {
		return chatAddress;
	}

	public void setChatAddress(ContactChatAddrField chatAddress) {
		this.chatAddress = chatAddress;
	}

	public ContactWebsiteField getWebsite() {
		return website;
	}

	public void setWebsite(ContactWebsiteField website) {
		this.website = website;
	}

	public ContactInfo(ContactEmailField emailField,
			ArrayList<ContactItem> items) {
		// TODO Auto-generated constructor stub
	}




	public static void main(String[] args) {
		ContactInfo ci = new ContactInfo();
		System.out.println(ci.getNotes());
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public ArrayList<String> getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(ArrayList<String> homeAddress) {
		this.homeAddress = homeAddress;
	}

	public ArrayList<String> getWorkAddress() {
		return workAddress;
	}

	public void setWorkAddress(ArrayList<String> workAddress) {
		this.workAddress = workAddress;
	}

	public ArrayList<String> getOtherAddress() {
		return otherAddress;
	}

	public void setOtherAddress(ArrayList<String> otherAddress) {
		this.otherAddress = otherAddress;
	}

	public LocalDateTime getBitrhday() {
		return bitrhday;
	}

	public void setBitrhday(LocalDateTime bitrhday) {
		this.bitrhday = bitrhday;
	}

	public LocalDateTime getAnniversary() {
		return anniversary;
	}

	public void setAnniversary(LocalDateTime anniversary) {
		this.anniversary = anniversary;
	}

	public HashMap<String, String> getJobs() {
		return jobs;
	}

	public void setJobs(HashMap<String, String> jobs) {
		this.jobs = jobs;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

}
