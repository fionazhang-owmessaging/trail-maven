package synchronoss.com.testcase.utils;

import org.openqa.selenium.WebDriverException;

import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;

public class EmailUtils extends Base {
	/**
	 * @Name writeEmails
	 * @param EmailMessage
	 *            message
	 * @Author Fiona Zhang
	 * @CreateTime 2015/12/30
	 * @Description This method will write a message with multiple recepient
	 */
	public void writeEmails(EmailMessage message) {
		try {
			// click write button
			email.clickWriteEmailButton();

			// add toRecepient
			//for (String toRecepient : message.getToRecepit()) {
			if(message.getToRecepit().length==1)
				emailComposer.addToReceipent(message.getToRecepit()[0]);
			else
				emailComposer.addToReceipent(message.getToRecepit());

			//	Logging.info("Write email to : " + toRecepient);
		//	}

			// if ccRecepient has value, will add it
			if (message.getCcRecepit() != null) {
				emailComposer.expandOrCollapseOtherField(true);
				for (String ccRecepient : message.getCcRecepit()) {
					emailComposer.addCcReceipent(ccRecepient);
					Logging.info("CC to recepient : " + ccRecepient);
				}
			}
			// if bccRecepient has value, will add it
			if (message.getBccRecepit() != null) {
				emailComposer.expandOrCollapseOtherField(true);
				for (String bccRecepient : message.getBccRecepit()) {
					emailComposer.addBccReceipent(bccRecepient);
					Logging.info("BCC recepient : " + bccRecepient);
				}
			}

			if (message.getPriority() != EmailMessage.PRIORITY.MID)
				Logging.info("Change priority");

			if (message.getSignature() == true)
				Logging.info("Change add signature");

			// add a subject
			emailComposer.addSubject(message.getSubject());

			// add a message body
			emailComposer.addMessageBody(message.getMessageBody());

			// click send mail button
			emailComposer.clickSendEmailButton();

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	
	/***
	 * The function will create email thread
	 * @param threadNumber
	 * 		the thread number, for example, 2, there will be 3 email in the thread
	 * @param subject
	 * 		the subject for the mail that need to be threaded
	 */
	public void createThreadConversation(int threadNumber, String subject){
		for(int i=0 ; i<threadNumber; i ++){
			email.refresh_maillist();
			email.clickEmailBySubject(subject);
			emailDetail.clickButtonOnToolBar(ConstantName.REPLY);
			emailDetail.clickPopupMenuLabel(ConstantName.REPLY);
			emailComposer.addMessageBody(String.valueOf(i));
			emailComposer.clickSendEmailButton();
			emailDetail.clickButtonOnToolBar(ConstantName.BACK);
			email.refresh_maillist();

		}
			
	}
	
	
	/***
	 * The function will create email thread
	 * @param threadNumber
	 * 		the thread number, for example, 2, there will be 3 email in the thread
	 * @param subject
	 * 		the subject for the mail that need to be threaded
	 */
	public void createThreadConversation(int threadNumber, String subject, String body){
		for(int i=0 ; i<threadNumber; i ++){
			email.refresh_maillist();
			email.clickEmailBySubject(subject);
			emailDetail.clickButtonOnToolBar(ConstantName.REPLY);
			emailDetail.clickPopupMenuLabel(ConstantName.REPLY);
			emailComposer.addMessageBody(body);
			emailComposer.clickSendEmailButton();
			emailDetail.clickButtonOnToolBar(ConstantName.BACK);

		}
			
	}
	
  /***
   * The function will create email draft
   * 
   * @param toRecepit - the to receipts
   * @param message1 - emailMessage entity
   */
  public void generateDraftMail(String[] toRecepit, EmailMessage message1) {
    email.clickWriteEmailButton();
    emailComposer.addToReceipent(toRecepit[0]);
    emailComposer.addSubject(message1.getSubject());
    emailComposer.addMessageBody(message1.getMessageBody());
    emailComposer.clickCancelComposeButton();
    emailComposer.clickButtonOnMessageBox(ConstantName.YES);

  }
}
