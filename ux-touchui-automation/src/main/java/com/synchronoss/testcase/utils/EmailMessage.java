package synchronoss.com.testcase.utils;

import java.util.Random;

public class EmailMessage {

	private String[] toRecepit = null;
	private String[] ccRecepit = null;
	private String[] bccRecepit = null;
	private String subject = null;
	private String messageBody = null;
	PRIORITY priority;
	private boolean signature = false;

	public enum PRIORITY {
		LOW, MID, HIGH;
	}

	Random random = new Random();

	public EmailMessage() {
		subject = "TestTouchUIA - " + random.nextInt(10000);
		messageBody = "Test automation for touch \nA new letter is coming";
		signature = false;
		priority = PRIORITY.MID;
	}

	public EmailMessage(String[] toRecepit) {
		this.toRecepit = toRecepit;
		subject = "TestTouchUIA - " + random.nextInt(10000);
		messageBody = "Hello world A new letter is comming";
		signature = false;
		priority = PRIORITY.MID;
	}

	public EmailMessage(String[] toRecepit, String[] ccRecepit) {
		this.toRecepit = toRecepit;
		this.ccRecepit = ccRecepit;
		subject = "TestTouchUIA_with_cc" + random.nextInt(10000);
		messageBody = "Hello world A new letter is comming";
		signature = false;
		priority = PRIORITY.MID;
	}

	public EmailMessage(String[] toRecepit, String[] ccRecepit,
			String[] bccRecepit) {
		this.toRecepit = toRecepit;
		this.ccRecepit = ccRecepit;
		this.bccRecepit = bccRecepit;
		subject = "TestTouchUIA_with_bcc" + random.nextInt(10000);
		messageBody = "Hello world A new letter is comming";
		signature = false;
		priority = PRIORITY.MID;
	}

	public String[] getToRecepit() {
		return toRecepit;
	}

	public void setToRecepit(String[] toRecepit) {
		this.toRecepit = toRecepit;
	}

	public String[] getCcRecepit() {
		return ccRecepit;
	}

	public void setCcRecepit(String[] ccRecepit) {
		this.ccRecepit = ccRecepit;
	}

	public String[] getBccRecepit() {
		return bccRecepit;
	}

	public void setBccRecepit(String[] bccRecepit) {
		this.bccRecepit = bccRecepit;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public PRIORITY getPriority() {
		return priority;
	}

	public void setPriority(PRIORITY priority) {
		this.priority = priority;
	}

	public boolean getSignature() {
		return signature;
	}

	public void setSignature(boolean signature) {
		this.signature = signature;
	}

}
