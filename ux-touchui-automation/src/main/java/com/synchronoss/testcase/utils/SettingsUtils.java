package synchronoss.com.testcase.utils;

import synchronoss.com.core.Base;

public class SettingsUtils extends Base {
  private String description = "uia test external account";
  private String name = "uiatouch";

  /**
   * Create an external account
   *
   * @param emailId - the email address
   * @param passwd - password of external account
   * @param protocol - the protocol for the external account
   * 
   */
  public void createExternalAccount(String emailId, String passwd, String protocol) {
    settings.clickAddAcountButton();
    settings.inputAccountInfoOnNewExternalAccountPage(ConstantName.DESCRIPTION, description);
    settings.inputAccountInfoOnNewExternalAccountPage(ConstantName.NAME, name);
    settings.inputAccountInfoOnNewExternalAccountPage(ConstantName.ACCOUNTID, emailId);
    settings.inputAccountInfoOnNewExternalAccountPage(ConstantName.PASSWORD, passwd);
    settings.setOnExternalAccountProtocolValue(protocol);
    settings.clickCreateExternalAccountHeadButton(ConstantName.SAVE);

  }
  
  
  /**
   * Create an external account
   *
   * @param emailId - the email address
   * @param passwd - password of external account
   * @param protocol - the protocol for the external account
   * @param description - external account description
   *            
   */
  public void createExternalAccount(String emailId, String passwd, String protocol, String description) {
    settings.clickAddAcountButton();
    settings.inputAccountInfoOnNewExternalAccountPage(ConstantName.DESCRIPTION, description);
    settings.inputAccountInfoOnNewExternalAccountPage(ConstantName.NAME, name);
    settings.inputAccountInfoOnNewExternalAccountPage(ConstantName.ACCOUNTID, emailId);
    settings.inputAccountInfoOnNewExternalAccountPage(ConstantName.PASSWORD, passwd);
    settings.setOnExternalAccountProtocolValue(protocol);
    settings.clickCreateExternalAccountHeadButton(ConstantName.SAVE);

  }

}
