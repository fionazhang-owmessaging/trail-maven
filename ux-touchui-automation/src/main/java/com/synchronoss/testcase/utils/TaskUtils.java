package synchronoss.com.testcase.utils;

import org.openqa.selenium.WebDriverException;

import synchronoss.com.core.Base;

public class TaskUtils extends Base {

	/**
	 * add a task with default value
	 * 
	 * @param taskName
	 *            The name of the task
	 *
	 * @author Fiona
	 */
	public void addTask(String taskName) {
		try {
			task.clickAddTaskButton();
			taskDetail.addTaskTitle(taskName);
			taskDetail.clickButtonOnTopBar(ConstantName.SAVE);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * add a task with default value
	 * 
	 * @param taskName
	 *            The name of the task
	 *
	 * @author Fiona
	 */
	public void addTask(TaskInfo taskInfo) {
		try {
			task.clickAddTaskButton();
			taskDetail.addTaskTitle(taskInfo.getTaskTitle());
			if (taskInfo.getTaskGroup() != null)
				taskDetail.selectTaskGroup(taskInfo.getTaskGroup());
			if (taskInfo.getTaskPriority() != -1)
				taskDetail.selectTaskPriority(taskInfo.getTaskPriority());
			if (taskInfo.getTaskStauts() != null)
				taskDetail.selectTaskPriority(taskInfo.getTaskPriority());
			if (taskInfo.getDueDateTime()!=null) {
				taskDetail.selectTaskSetDueDate(ConstantName.SPECIFICDATE);
				if (taskInfo.getDueDateTime() != null) {
					taskDetail.selectTaskDueDate(taskInfo.getDueDateTime()
							.toLocalDate());
					taskDetail.selectTaskDueTime(taskInfo.getDueDateTime());
				}
				if (taskInfo.getTaskReminder() != null)
					taskDetail.selectTaskReminder(taskInfo.getTaskReminder());
			}

			if (taskInfo.getTaskURl() != null)
				taskDetail.addTaskURL(taskInfo.getTaskURl());
			if (taskInfo.getTaskDescription() != null)
				taskDetail.addTaskDescription(taskInfo.getTaskDescription());

			taskDetail.clickButtonOnTopBar(ConstantName.SAVE);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * add a task with group name
	 * 
	 * @param taskName
	 *            The name of the task
	 * 
	 * @param groupName
	 *            The name of the group
	 *
	 * @author Fiona
	 */
	public void addTaskWithGroup(String taskName, String groupName) {
		// create a task, and select task group
		try {
			task.clickAddTaskButton();
			taskDetail.addTaskTitle(taskName);
			taskDetail.selectTaskGroup(groupName);
			taskDetail.clickButtonOnTopBar(ConstantName.SAVE);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Delete the task by name
	 * 
	 * @param taskName
	 *            The name of the task
	 *
	 * @author Fiona
	 */
	public void deleteTask(String taskName) {
		try {
			task.swipeTaskByTaskName(taskName);
			task.clickActionButtonOnSlideToolbar(ConstantName.DELETE);
			task.clickButtonOnMessageBox(ConstantName.YES);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * add a task group
	 * 
	 * @param groupName
	 *            The name of the group
	 *
	 * @author Fiona
	 */
	public void addTaskGroup(String groupName) {
		try {
			sidebar.clickAddTaskGroupButton();
			sidebar.addTaskGroupName(groupName);
			sidebar.clickButtonOnMessageBox(ConstantName.OK);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * delete a task group
	 * 
	 * @param groupName
	 *            The name of the group
	 *
	 * @author Fiona
	 */
	public void deleteTaskGroupByName(String groupName) {
		try {
			sidebar.swipeTaskGroupInEditForm(groupName);
			sidebar.clickTaskGroupNameAction(ConstantName.DELETE);
			sidebar.clickButtonOnMessageBox(ConstantName.YES);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * empty the all task list
	 * 
	 * @author Fiona
	 */
	public void emptyAllTasks() {
		try {
			while (!task.isEmptyTaskList()) {
				for (String taskName : task.getCurrentTaskList())
					this.deleteTask(taskName);
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * empty all task list by multiple click check box in task list view
	 * 
	 * @author Fiona
	 */
	public void emptyAllTasksByMultipleDelete() {

		try {
			while (!task.isEmptyTaskList()) {
				for (String taskName : task.getCurrentTaskList())
					task.clickCheckBoxOfTask(taskName);
				task.clickButtomButtonOfTask(ConstantName.DELETE);
				task.clickButtonOnMessageBox(ConstantName.YES);
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

}
