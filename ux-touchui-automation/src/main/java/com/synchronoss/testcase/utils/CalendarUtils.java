package synchronoss.com.testcase.utils;

import java.util.ArrayList;

import synchronoss.com.core.Base;


public class CalendarUtils extends Base {
  // create a event

  // delete event in current day
  public void deleteCurrentDayEvent(ArrayList<String> currentEventList) {
    for (int i = 0; i < currentEventList.size(); i++) {
      calendar.clickEventTitle(currentEventList.get(i));
      eventDetail.clickDeleteButton();
      eventDetail.clickButtonOnMessageBox(ConstantName.OK);
    }
  }

  // delete single event
  public void deleteCurrentDayEvent(String eventName) {
    calendar.clickEventTitle(eventName);
    eventDetail.clickDeleteButton();
    eventDetail.clickButtonOnMessageBox(ConstantName.OK);
  }
}
