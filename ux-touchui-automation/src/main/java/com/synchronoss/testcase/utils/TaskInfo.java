package synchronoss.com.testcase.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import synchronoss.com.component.TaskDueDateDescComparator;

public class TaskInfo {
	private String taskTitle;
	private String taskGroup;
	private int taskPriority = 1;
	private String taskStauts;
	private LocalDateTime dueDateTime;

	private String taskReminder;
	private String taskURl;
	private String taskDescription;
	private Random random = new Random();

	public TaskInfo() {
		taskTitle = "taskDetail" + random.nextInt(10000);
		taskGroup = "taskGroup" + random.nextInt(10000);
		taskPriority = 1;
	}

	public TaskInfo(String taskTitle) {
		this.taskTitle = taskTitle;

	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public String getTaskGroup() {
		return taskGroup;
	}

	public void setTaskGroup(String taskGroup) {
		this.taskGroup = taskGroup;
	}

	public int getTaskPriority() {
		return taskPriority;
	}

	public void setTaskPriority(int taskPriority) {
		this.taskPriority = taskPriority;
	}

	public String getTaskStauts() {
		return taskStauts;
	}

	public void setTaskStauts(String taskStauts) {
		this.taskStauts = taskStauts;
	}

	public LocalDateTime getDueDateTime() {
		return dueDateTime;
	}

	public void setDueDateTime(LocalDateTime dueDateTime) {
		this.dueDateTime = dueDateTime;
	}

	public String getTaskReminder() {
		return taskReminder;
	}

	public void setTaskReminder(String taskReminder) {
		this.taskReminder = taskReminder;
	}

	public String getTaskURl() {
		return taskURl;
	}

	public void setTaskURl(String taskURl) {
		this.taskURl = taskURl;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}



	public static void main(String args[]) {
		List<TaskInfo> taskList = null;
		LocalDateTime dueDateTime;
		ArrayList<LocalDateTime> taskDueTimes;

		taskDueTimes = new ArrayList<LocalDateTime>();
		dueDateTime = LocalDateTime.now();
		int tempTime = dueDateTime.getMinute() % 5;
		taskDueTimes.add(dueDateTime.plusMinutes((5 - tempTime) + 20));
		taskDueTimes.add(dueDateTime.plusMinutes((5 - tempTime) + 50));
		taskDueTimes.add(dueDateTime.plusMinutes((5 - tempTime) + 10));

		taskList = new ArrayList<TaskInfo>();
		for (int i = 0; i < 3; i++){
			taskList.add(new TaskInfo());
			taskList.get(i).setDueDateTime(taskDueTimes.get(i));
//			taskList.get(i).setDueTime(taskDueTimes.get(i));
		}
		
		taskList.get(1).setTaskTitle("1abc");
		taskList.get(2).setTaskTitle("2bbc");
		taskList.get(0).setTaskTitle("0dbc");
		
		
		taskList.get(1).setTaskPriority(1);
		taskList.get(2).setTaskPriority(0);
		taskList.get(0).setTaskPriority(2);


		for (int i = 0; i < 3; i++){
			System.out.println(taskList.get(i).getTaskTitle());

//		System.out.println(taskList.get(i).getDueDate());
//		System.out.println(taskList.get(i).getDueTime());

		}
		System.out.println();

		Collections.sort(taskList, new TaskDueDateDescComparator());
		for (int i = 0; i < 3; i++){
			System.out.println(taskList.get(i).getTaskTitle());
			System.out.println(taskList.get(i).getDueDateTime());

	}
		System.out.println();

		Collections.reverse(taskList);
		for (int i = 0; i < 3; i++){
			System.out.println(taskList.get(i).getTaskTitle());
			System.out.println(taskList.get(i).getDueDateTime());

		}
	}
}
