package synchronoss.com.testcase.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

import synchronoss.com.component.Repeat;

public class EventInfo {
	
	private String title;
	private String description;
	private boolean alldayEvent;
	private LocalDate startDate;
	private LocalDateTime startTime;
	private LocalDate endDate;
	private LocalDateTime endTime;
	private Repeat repeat;
	private String reminder;
	private String[] attendee;
	private String category;
	private String location;

	public EventInfo(){
		Random random = new Random();
		this.title = "event"+random.nextInt(1000);
		this.description = "uIA to test event, this is \n description part";
		this.location = "china beijing";
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isAlldayEvent() {
		return alldayEvent;
	}
	public void setAlldayEvent(boolean alldayEvent) {
		this.alldayEvent = alldayEvent;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public LocalDateTime getStartTime() {
		return startTime;
	}
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	public LocalDateTime getEndTime() {
		return endTime;
	}
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
	public Repeat getRepeat() {
		return repeat;
	}
	public void setRepeat(Repeat repeat) {
		this.repeat = repeat;
	}
	public String getReminder() {
		return reminder;
	}
	public void setReminder(String reminder) {
		this.reminder = reminder;
	}
	public String[] getAttendee() {
		return attendee;
	}
	public void setAttendee(String[] attendee) {
		this.attendee = attendee;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
}
