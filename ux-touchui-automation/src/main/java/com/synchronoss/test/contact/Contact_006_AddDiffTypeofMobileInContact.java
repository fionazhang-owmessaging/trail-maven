package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactItem;
import synchronoss.com.component.ContactMobileField;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_006_AddDiffTypeofMobileInContact extends TestCase {
	Random rand = new Random();
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;
	private ContactMobileField mobileField;
	private ContactItem item1;
	private ContactItem item2;
	private ContactItem item3;
	private String mobileData1 = "(0896-)" + rand.nextInt(10000);
	private String mobileData2 = "086-" + rand.nextInt(10000);
	private String mobileData3 = "010" + rand.nextInt(10000);
	private String firstName = "savemobile" + rand.nextInt(100);

	@Override
	protected void description() {
		Logging.description("Add three types of mobile to a contact");
	}

	@Test(groups = { "contact", "sanity","zjx" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {

		mobileField = new ContactMobileField();
		item1 = new ContactItem();
		item1.setType(ConstantName.HOME);
		item1.setValue(mobileData1);
		item2 = new ContactItem();
		item2.setType(ConstantName.WORK);
		item2.setValue(mobileData2);
		item3 = new ContactItem();
		item3.setType(ConstantName.OTHER);
		item3.setValue(mobileData3);

		ArrayList<ContactItem> items = new ArrayList<ContactItem>();
		items.add(item1);
		items.add(item2);
		items.add(item3);

		contactInfo = new ContactInfo(items, mobileField);

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// create a contact with first name and add 3 types of mobile into the contact (HOME,WORK,OTHER)
		// test data : home: (0896-)12345, work: 086-12345, other 010123456
		contact.clickAddContactButton();
		contactDetail.addFirstname(firstName);
		contactDetail.addFieldValue(contactInfo.getMobile(), contactInfo
				.getMobile().getComponentName());
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
		
		// search the contact by first name 
		contact.searchContactbyKeywords(firstName);
		
		// go to the detail view of contact
		contact.clickContactbyName(firstName);
		
		// verify the contact in detail view
		// and the detail view contains data: "HOME + mobile" ; "WORK + mobile" ; "EMAIL + mobile " 
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.MOBILE).contains(
				ConstantName.HOME + ":\n" + mobileData1));
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.MOBILE).contains(
				ConstantName.WORK + ":\n" + mobileData2));
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.MOBILE).contains(
				ConstantName.OTHER + ":\n" + mobileData3));
	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
