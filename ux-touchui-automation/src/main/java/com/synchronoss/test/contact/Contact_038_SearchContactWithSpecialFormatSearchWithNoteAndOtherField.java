package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactItem;
import synchronoss.com.component.ContactMobileField;
import synchronoss.com.component.ContactPhoneField;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_038_SearchContactWithSpecialFormatSearchWithNoteAndOtherField extends TestCase {
  private Random rand = new Random();
  private ContactInfo contactInfo1;
  private ContactInfo contactInfo2;
  private ContactInfo contactInfo3;

  private ContactUtils contactUtil;
  private ContactMobileField mobileField1;

  private ContactPhoneField phoneField1;

  private ContactItem item1;
  private ContactItem item2;

  private String mobileData1 = "86 138101234";
  private String phoneData1 = "86-13810392";
  private String note = "86-13810392";
  private String contactShortKeyword = "86";
  private ArrayList<ContactInfo> contacts;

  @Override
  protected void description() {
    Logging
        .description("Add different mobile and phone data to different contacts, add some phone/mobile to note/other "
            + "field, search contact by shorten keyword, and normal lenth of keyword, the search result doesn't contain "
            + "the contact that note/other invloved the phone/mobile number");
  }

  @Test(groups = {"contact", "sanity"})
  public void run() {
    super.run();
  }

  public void doTestSetup() {

    // data preparation
    mobileField1 = new ContactMobileField();
    item1 = new ContactItem();
    item1.setType(ConstantName.HOME);
    item1.setValue(mobileData1);
    contactInfo1 = new ContactInfo(item1, mobileField1);

    phoneField1 = new ContactPhoneField();
    item2 = new ContactItem();
    item2.setType(ConstantName.WORK);
    item2.setValue(phoneData1);
    contactInfo2 = new ContactInfo(item2, phoneField1);

    contactInfo3 = new ContactInfo();

    contacts = new ArrayList<ContactInfo>();
    contacts.add(contactInfo1);
    contacts.add(contactInfo2);
    contacts.add(contactInfo3);

    contactUtil = new ContactUtils();

    super.doTestSetup();
    navigation.clickNavigationButton(ConstantName.CONTACTTAB);
    contactUtil.deleteAllContactInTheList();

  }

  @Override
  protected void doTest() {
    // Add 3 contacts with
    // contact 1 mobile home:86 138101234
    // contact 2 phone work:86-13810392
    // contact 3 note:86-13810392, others field:86-13810392
    for (int i = 0; i < contacts.size() - 1; i++) {
      contact.clickAddContactButton();
      contactDetail.addFirstname(contacts.get(i).getFirstname());
      if (contacts.get(i).getMobile() != null)
        contactDetail.addFieldValue(contacts.get(i).getMobile(), contacts.get(i).getMobile()
            .getComponentName());
      else
        contactDetail.addFieldValue(contacts.get(i).getPhone(), contacts.get(i).getPhone()
            .getComponentName());

      contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
    }

    contact.clickAddContactButton();
    contactDetail.addFirstname(contacts.get(2).getFirstname());
    contactDetail.clickAddFieldbutton();
    contactDetail.clickAddFieldLabel(ConstantName.NOTES, ConstantName.NOTES.toLowerCase());
    contactDetail.clickAddFieldbutton();
    contactDetail.clickAddFieldLabel(ConstantName.OTHER, ConstantName.OTHER.toLowerCase());
    contactDetail.addNotes(note);
    contactDetail.addOthers(note);
    contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);


    // search the contact by a short keyword - "86"
    contact.searchContactbyKeywords(contactShortKeyword);

    ArrayList<String> currentContact = contact.getCurrentContactList();
    // verify the current contact size is equals to 2
    Assert.assertEquals(currentContact.size(), 2);

    // verify contact 1,2 are displayed in the search result list
    Assert.assertTrue(currentContact.toString().contains(contactInfo1.getFirstname()));
    Assert.assertTrue(currentContact.toString().contains(contactInfo2.getFirstname()));

    // verify contact 3 is not displayed in the search result list
    Assert.assertFalse(currentContact.toString().contains(contactInfo3.getFirstname()));

    // search the contact by a whole keyword again - "86 138101234"
    contact.searchContactbyKeywords(mobileData1);
    currentContact = contact.getCurrentContactList();

    // verify the current contact size is equals to 1
    Assert.assertEquals(currentContact.size(), 1);

    // verify contact 1 is displayed in the search result list
    Assert.assertTrue(currentContact.toString().contains(contactInfo1.getFirstname()));

    // verify contact 2,3 is not displayed in the search result list
    Assert.assertFalse(currentContact.toString().contains(contactInfo2.getFirstname()));
    Assert.assertFalse(currentContact.toString().contains(contactInfo3.getFirstname()));

  }

  public void doTestCleanup() {
    // delete the created contact
    for (ContactInfo ci : contacts) {
      contactUtil.deleteContactFromSlidebar(ci.getFirstname(), true);
    }
    super.doTestCleanup();
  }
}
