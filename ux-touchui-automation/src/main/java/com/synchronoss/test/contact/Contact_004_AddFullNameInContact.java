package synchronoss.com.test.contact;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;


public class Contact_004_AddFullNameInContact extends TestCase {
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;

	@Override
	protected void description() {
		Logging.description("Add first name, middle name, last name to a contact, also click cancel "
				+ "button during saving. Search last name in contact list");
	}

	@Test(groups = { "contact", "sanity","zjx" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		contactInfo = new ContactInfo();
		contactUtil = new ContactUtils();

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// click add contact button 
		contact.clickAddContactButton();
		
		// input first name
		contactDetail.addFirstname(contactInfo.getFirstname());

		// click "cancel button" to cancel edit contact
		contactDetail.clickSaveOrCancelButton(ConstantName.CANCEL);

		// click cancel button in pop up ("Do you want to save your changes?") to continue edit contact
		contactDetail.clickButtonOnMessageBox(contact
				.toCapitlize(ConstantName.CANCEL));

		// add middle name and last name of the contact and save
		contactDetail.addMiddlename(contactInfo.getMiddlename());
		contactDetail.addLastname(contactInfo.getLastname());
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);

		// search the contact by last name, and go to detail view of contact
		contact.searchContactbyKeywords(contactInfo.getLastname());
		contact.clickContactbyName(contactInfo.getLastname());

		// verify the contact detail view displayed its first, last and middle name
		Assert.assertEquals(contactDetail.getContactName(),
				contactInfo.getFirstname() + " " + contactInfo.getMiddlename()
						+ " " + contactInfo.getLastname());
	}

	public void doTestCleanup() {
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
