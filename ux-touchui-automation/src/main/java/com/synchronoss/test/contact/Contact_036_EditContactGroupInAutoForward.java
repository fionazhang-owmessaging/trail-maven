package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactEmailField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.EmailMessage;
import synchronoss.com.testcase.utils.EmailUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_036_EditContactGroupInAutoForward extends TestCase {
  private Random rand = new Random();
  private ContactUtils contactUtil;
  private String[] originalEmail = null;
  private EmailMessage message;
  private EmailUtils mailUtils;
  private String[] groupUser;
  private String[] passwd;
  private String[] originalPwd;

  private ContactEmailField emailField1;
  private ContactEmailField emailField2;
  private ContactInfo contactInfo1;
  private ContactInfo contactInfo2;
  private String firstName1 = "firstContact" + rand.nextInt(100);
  private String firstName2 = "secondContact" + rand.nextInt(100);
  private ContactItem item1;
  private ContactItem item2;

  private String contact1;
  private String contact2;

  private String contactGroup = "autoForwardEdit" + rand.nextInt(100);


  @Override
  protected void description() {
    Logging
        .description("Create a contact group in default address book, and add 2 contacts to the "
            + "group, go to auto-forward delete one contact, and save settings, verify only 1 contact could get the mail");
  }

  @Test(groups = {"contact", "sanity"})
  public void run() {
    super.run();
  }

  public void doTestSetup() {
    groupUser = new String[] {this.getParameter("emailid2"), this.getParameter("emailid3")};
    passwd = new String[] {this.getParameter("password2"), this.getParameter("password3")};
    originalEmail = new String[] {this.getParameter("emailid1")};
    originalPwd = new String[] {this.getParameter("password1")};

    message = new EmailMessage(originalEmail);

    Logging.info("get user name [0] as " + groupUser[0]);
    Logging.info("get user name [1] as " + groupUser[1]);
    contact1 = groupUser[0];
    contact2 = groupUser[1];
    emailField1 = new ContactEmailField();
    item1 = new ContactItem();
    item1.setType(ConstantName.HOME);
    item1.setValue(contact1);
    ArrayList<ContactItem> items1 = new ArrayList<ContactItem>();
    items1.add(item1);
    mailUtils = new EmailUtils();
    emailField2 = new ContactEmailField();
    item2 = new ContactItem();
    item2.setType(ConstantName.HOME);
    item2.setValue(contact2);
    ArrayList<ContactItem> items2 = new ArrayList<ContactItem>();
    items2.add(item2);

    contactInfo1 = new ContactInfo(items1, emailField1);
    contactInfo1.setFirstname(firstName1);
    contactInfo2 = new ContactInfo(items2, emailField2);
    contactInfo2.setFirstname(firstName2);

    contactUtil = new ContactUtils();

    super.doTestSetup();

    // go to contact view and add 2 contacts
    navigation.clickNavigationButton(ConstantName.CONTACTTAB);
    contactUtil.addContactByEmail(contactInfo1);
    contactUtil.addContactByEmail(contactInfo2);

    // add a contact group in default
    contactUtil.addContactGroup(ConstantName.DEFAULT, contactGroup);


  }

  @Override
  protected void doTest() {
    // add 2 contacts(user1 and user2) to contact group in default address book
    contactUtil.addContactToGroup(firstName1, contactGroup);
    contactUtil.addContactToGroup(firstName2, contactGroup);

    // go to mail settings view
    navigation.clickNavigationButton(ConstantName.SETTINGTAB);
    settings.clickSettingOptionList(settings.toCapitlize(ConstantName.MAIL));

    // set auto forward as true, and remove all emails in the list
    settings.clickMailSettingOption(ConstantName.AUTOFWD);
    settings.useAutoForward(true);
    settings.removeAllAutoForwardEmailDestinations();

    // input few words of the group, select the group in auto suggest list
    settings.addAutoForwardDestinationEmail(contactGroup, true);

    // remove user 1 from the autoforward list
    settings.removeAutoForwardMail(firstName1);
    settings.getAutoForwardDestinationEmailList();

    // set keep copy in inbox as true
    settings.useKeepCopyInInbox(true);
    settings.clickHeadBarOfSettingDetail(ConstantName.SAVE);

    // go to mail view
    navigation.clickNavigationButton(ConstantName.MAILTAB);

    // go to email view and write an email
    mailUtils.writeEmails(message);

    // refresh the mail list
    email.refresh_maillist();

    // verify the user get the new mail
    Assert.assertTrue(email.getEmailSubjectsInMailList().contains(message.getSubject()));

    // log out for current user
    navigation.clickMenuButton();
    navigation.clickLogoutButton();


    // login to user 1, verify user 1 doesn't get the mail
    login.typeUsername(groupUser[0]);
    login.typePassword(passwd[0]);
    login.clickLoginButton();
    Assert.assertFalse(email.getEmailSubjectsInMailList().contains(message.getSubject()));
    navigation.clickMenuButton();
    navigation.clickLogoutButton();

    // login to user2 , verify user 2 get the mail
    login.typeUsername(groupUser[1]);
    login.typePassword(passwd[1]);
    login.clickLoginButton();
    Assert.assertTrue(email.getEmailSubjectsInMailList().contains(message.getSubject()));
    navigation.clickMenuButton();
    navigation.clickLogoutButton();

  }

  public void doTestCleanup() {
    // login current user again
    login.typeUsername(originalEmail[0]);
    login.typePassword(originalPwd[0]);
    login.clickLoginButton();

    // set auto forward as false and remove all mails
    navigation.clickNavigationButton(ConstantName.SETTINGTAB);
    settings.clickSettingOptionList(settings.toCapitlize(ConstantName.MAIL));
    settings.clickMailSettingOption(ConstantName.AUTOFWD);
    settings.removeAllAutoForwardEmailDestinations();
    settings.useAutoForward(false);
    settings.clickHeadBarOfSettingDetail(ConstantName.SAVE);

    // delete the contact group
    navigation.clickNavigationButton(ConstantName.CONTACTTAB);
    navigation.clickMenuButton();
    sidebar.clickContactGroupEnterButton(ConstantName.DEFAULT);
    contactUtil.deleteContactGroup(contactGroup);
    sidebar.clickBackToAddressbookButton();
    sidebar.clickCloseButton();

    contactUtil.deleteContactFromDetail(true, firstName1);
    contactUtil.deleteContactFromDetail(true, firstName2);


    super.doTestCleanup();
  }
}
