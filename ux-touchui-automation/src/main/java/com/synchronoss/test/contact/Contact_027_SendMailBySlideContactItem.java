package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactEmailField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_027_SendMailBySlideContactItem extends TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private ContactEmailField emailField1;
	private ContactInfo contactInfo1;
	private String firstName1 = "mailContact" + rand.nextInt(100);
	private String subject = "UIAutomation - Contact_027_SendMailBySlideContactItem";

	private ContactItem item1;

	private String contact1;

	@Override
	protected void description() {
		Logging.description("create a contact with email, go back to contact list view, slide the contact and click envelope icon");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		String[] user = { this.getParameter("emailid1"),
				this.getParameter("emailid2") };

		Logging.info("get user name [0] as " + user[0]);
		contact1 = user[0];
		emailField1 = new ContactEmailField();
		item1 = new ContactItem();
		item1.setType(ConstantName.HOME);
		item1.setValue(contact1);
		ArrayList<ContactItem> items1 = new ArrayList<ContactItem>();
		items1.add(item1);

		contactInfo1 = new ContactInfo(items1, emailField1);
		contactInfo1.setFirstname(firstName1);

		contactUtil = new ContactUtils();
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);

	}

	@Override
	protected void doTest() {
		// add contact by email and first name
		contactUtil.addContactByEmail(contactInfo1);
		contact.searchContactbyKeywords(contactInfo1.getFirstname());

		// swipe the contact created just now in contact list view
		contact.swipeContactByName(contactInfo1.getFirstname());

		// click mail icon to send a email (the compose view is open)
		contact.clickActionButtonOnSlideToolbar(ConstantName.MAIL);
		
		// verify the email address saved in to field in compose view
		Assert.assertTrue(emailComposer.getTextInToRecepient().toString()
				.contains(contactInfo1.getFirstname()));

	}

	public void doTestCleanup() {
		emailComposer.addSubject(subject);
		emailComposer.clickSendEmailButton();
		contactUtil.deleteContactFromSlidebar(contactInfo1.getFirstname(),
				false);
		contactUtil
				.deleteContactFromSlidebar(contactInfo1.getFirstname(), true);
		super.doTestCleanup();
	}
}
