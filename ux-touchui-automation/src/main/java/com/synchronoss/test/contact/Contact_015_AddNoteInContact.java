package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_015_AddNoteInContact extends TestCase {
	Random rand = new Random();
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;
	private String note = "hello world \n" + rand.nextInt(10000);

	ArrayList<ContactItem> items;

	@Override
	protected void description() {
		Logging.description("Add note to a contact");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {

		contactInfo = new ContactInfo();

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// click add contact button
		contact.clickAddContactButton();
		
		// add first name of the contact
		contactDetail.addFirstname(contactInfo.getFirstname());
		
		// click add field button
		contactDetail.clickAddFieldbutton();
		
		// select note
		contactDetail.clickAddFieldLabel(ConstantName.NOTES,
				ConstantName.NOTES.toLowerCase());

		// add some note to the contact and save
		contactDetail.addNotes(note);
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
		
		// search the contact by first name
		contact.searchContactbyKeywords(contactInfo.getFirstname());
		
		// go to detail view
		contact.clickContactbyName(contactInfo.getFirstname());
		
		// verify the contact details contains the note
		Assert.assertTrue(contactDetail
				.getContactSectionListValue(ConstantName.NOTES).toString()
				.contains(note.toString()));
	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
