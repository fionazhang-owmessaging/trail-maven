package synchronoss.com.test.contact;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactEmailField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;


public class Contact_003_AddEmailInContact extends TestCase {
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;
	private ContactEmailField emailField;
	private ContactItem item;
	private String emailData = "savecontactemail@abc.com";

	@Override
	protected void description() {
		Logging.description("Add email to a contact to check if it can be saved");
	}

	@Test(groups = { "contact", "sanity","zjx" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {

		emailField = new ContactEmailField();
		item = new ContactItem();
		item.setType(ConstantName.HOME);
		item.setValue(emailData);

		ArrayList<ContactItem> items = new ArrayList<ContactItem>();
		items.add(item);

		contactInfo = new ContactInfo(items, emailField);

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// add a contact with Home, email address
		contact.clickAddContactButton();
		contactDetail.addFieldValue(contactInfo.getEmail(), contactInfo
				.getEmail().getComponentName());
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);

		// search the contact by email address
		contact.searchContactbyKeywords(emailData);

		// go to detail view of contact 
		contact.clickContactbyName(emailData);
		
		// verify the email in detail view is "Home: email address added just now"
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.EMAIL).contains(
				ConstantName.HOME + ":\n" + emailData));
	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
