package synchronoss.com.test.contact;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_028_MoveContactToOtherAddrbookInSlideItem extends TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private String firstName = "moveContact" + rand.nextInt(100);
	private String addressbook = "moveAddrbook" + rand.nextInt(100);

	@Override
	protected void description() {
		Logging.description("Create a contact, create a addressbook , move the contact to new addressbook from contact list slide bar");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		contactUtil = new ContactUtils();
		super.doTestSetup();
		// go to contact view, create a new contact in default address book
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
		contactUtil.addContact(firstName);
		
		// create a new address book
		contactUtil.addAddressbook(addressbook);

	}

	@Override
	protected void doTest() {
		// click menu button to open side bar
		navigation.clickMenuButton();

		// get the indicator of default address book
		int orginalCount = Integer.parseInt(sidebar
				.getContactCountInAddressbook(ConstantName.DEFAULT));

		// click close button('x') on side bar
		sidebar.clickCloseButton();

		// search the contact by first name
		contact.searchContactbyKeywords(firstName);

		// swipe the contact by name
		contact.swipeContactByName(firstName);

		// click "move to address book" button
		contact.clickActionButtonOnSlideToolbar(ConstantName.MOVE);
		
		// select addressbook created just now in "select addressbook" popup
		contactDetail.clickAddressbookSelectPopup(addressbook);

		// open side bar 
		navigation.clickMenuButton();
		
		// verify the indicator of default address book minus 1 and the new created address book indicator changes to 1
		Assert.assertEquals(String.valueOf((orginalCount - 1)),
				sidebar.getContactCountInAddressbook(ConstantName.DEFAULT));
		Assert.assertEquals("1",
				sidebar.getContactCountInAddressbook(addressbook));

		// go to the new create address book view, search the moved contact
		sidebar.clickAddressbookName(addressbook);
		contact.searchContactbyKeywords(firstName);

		// go to the detail view of contact ,verify the contact exists
		contact.clickContactbyName(firstName);
		Assert.assertTrue(contactDetail.getContactName().contains(firstName));

	}

	public void doTestCleanup() {
		contactDetail.clickBackButton();
		navigation.clickMenuButton();
		contactUtil.deleteAddressbook(addressbook);
		sidebar.clickCloseButton();
		super.doTestCleanup();
	}
}
