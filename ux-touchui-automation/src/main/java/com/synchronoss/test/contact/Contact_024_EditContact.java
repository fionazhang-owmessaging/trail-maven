package synchronoss.com.test.contact;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_024_EditContact extends TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private String firstName = "editContact" + rand.nextInt(100);
	private String updateFirstName = "updateName" + rand.nextInt(100);
	private String updateLastName = "lastName" + rand.nextInt(100);

	@Override
	protected void description() {
		Logging.description("Create a contact, and edit the contact");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		contactUtil = new ContactUtils();
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
		// create a contact with first name
		contactUtil.addContact(firstName);
	}

	@Override
	protected void doTest() {
		// go to detail view of the contact , and click edit contact button
		contact.searchContactbyKeywords(firstName);
		contact.clickContactbyName(firstName);
		contactDetail.clickEditButton();

		// update the contact with first/last name, and save changes
		contactDetail.addFirstname(updateFirstName);
		contactDetail.addLastname(updateLastName);
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);

		// go to contact list view
		contactDetail.clickBackButton();

		// search the contact ,and go to the detail view of the contact
		contact.clickClearButtonInSearchBar();
		contact.searchContactbyKeywords(updateLastName);
		contact.clickContactbyName(updateLastName);
		
		// verify the updated first name/last name exist in contact list view
		Assert.assertTrue(contactDetail.getContactName().contains(
				updateFirstName));
		Assert.assertTrue(contactDetail.getContactName().contains(
				updateLastName));

		// click "delete button" in contact detail view
		contactDetail.clickDeleteButton();
		
		// click "cancel" in confirm delete popup view  
		contactDetail.clickButtonOnMessageBox(ConstantName.NO);
		
		// click "delete button" in contact detail view again
		contactDetail.clickDeleteButton();
		
		// click yes to delete the contact
		contactDetail.clickButtonOnMessageBox(ConstantName.YES);

	}

	public void doTestCleanup() {
		super.doTestCleanup();
	}
}
