package synchronoss.com.test.contact;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;


public class Contact_002_AddLastNameInContact extends TestCase {
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;

	@Override
	protected void description() {
		Logging.description("Add last name to a contact to check if it can be saved");
	}

	@Test(groups = { "contact", "sanity","zjx" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		contactInfo = new ContactInfo();
		contactUtil = new ContactUtils();

		super.doTestSetup();
	}

	@Override
	protected void doTest() {
		// create a contact with last name
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
		contact.clickAddContactButton();
		contactDetail.addLastname(contactInfo.getLastname());
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);

		// search the contact by last name
		contact.searchContactbyKeywords(contactInfo.getLastname());

		// go to detail view of contact ,and verify the last name in contact Detail view
		contact.clickContactbyName(contactInfo.getLastname());
		Assert.assertEquals(contactDetail.getContactName(),
				contactInfo.getLastname());
	}

	public void doTestCleanup() {
		contactUtil.deleteContactFromDetail(false, contactInfo.getLastname());
		super.doTestCleanup();
	}
}
