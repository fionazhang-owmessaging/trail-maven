package synchronoss.com.test.contact;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_001_AddFirstNameInContact extends TestCase {
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;

	@Override
	protected void description() {
		Logging.description("Add first name to a contact to check if it can be saved");
	}

	@Test(groups = { "contact", "sanity","zjx" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		contactInfo = new ContactInfo();
		contactUtil = new ContactUtils();

		super.doTestSetup();
		
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// click add contact button
		contact.clickAddContactButton();
		
		// add first name to the contact
		contactDetail.addFirstname(contactInfo.getFirstname());
		
		// click save button to save contact
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
		
		// search the contact, and go to the detail view of contact
		contact.searchContactbyKeywords(contactInfo.getFirstname());
		contact.clickContactbyName(contactInfo.getFirstname());
		
		// verify the first name in contact detail view
		Assert.assertEquals(contactDetail.getContactName(),
				contactInfo.getFirstname());
	}

	public void doTestCleanup() {
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
