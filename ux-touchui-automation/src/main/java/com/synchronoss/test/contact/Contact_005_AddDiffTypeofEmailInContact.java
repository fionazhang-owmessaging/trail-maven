package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactEmailField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_005_AddDiffTypeofEmailInContact extends TestCase {
	Random rand = new Random();
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;
	private ContactEmailField emailField;
	private ContactItem item1;
	private ContactItem item2;
	private ContactItem item3;
	private String emailData1 = "testuserhome" + rand.nextInt(100) + "@abc.com";
	private String emailData2 = "testuserwork" + rand.nextInt(100)
			+ "@software.cn";
	private String emailData3 = "testuserother" + rand.nextInt(100)
			+ "@openwave.com";

	@Override
	protected void description() {
		Logging.description("Add three types of email to a contact, then search any email saved, check if all emails are saved");
	}

	@Test(groups = { "contact", "sanity","zjx" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {

		emailField = new ContactEmailField();
		item1 = new ContactItem();
		item1.setType(ConstantName.HOME);
		item1.setValue(emailData1);
		item2 = new ContactItem();
		item2.setType(ConstantName.WORK);
		item2.setValue(emailData2);
		item3 = new ContactItem();
		item3.setType(ConstantName.OTHER);
		item3.setValue(emailData3);

		ArrayList<ContactItem> items = new ArrayList<ContactItem>();
		items.add(item1);
		items.add(item2);
		items.add(item3);

		contactInfo = new ContactInfo(items, emailField);

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// add 3 types of email address (HOME,WORK,OTHER)
		contact.clickAddContactButton();
		contactDetail.addFieldValue(contactInfo.getEmail(), contactInfo
				.getEmail().getComponentName());
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
		
		// search the contact by one of the mail
		contact.searchContactbyKeywords(emailData2);
		
		// go to the detail view of the contact
		contact.clickContactbyName(emailData1);
		
		// verify 3 types of email saved in contact detail view
		// the detail view contains: "HOME + email address" ; "WORK + email address" ; "EMAIL + email address " 
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.EMAIL).contains(
				ConstantName.HOME + ":\n" + emailData1));
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.EMAIL).contains(
				ConstantName.WORK + ":\n" + emailData2));
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.EMAIL).contains(
				ConstantName.OTHER + ":\n" + emailData3));
	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
