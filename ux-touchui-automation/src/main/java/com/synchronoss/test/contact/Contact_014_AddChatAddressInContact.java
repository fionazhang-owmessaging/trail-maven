package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactChatAddrField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_014_AddChatAddressInContact extends TestCase {
	Random rand = new Random();
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;
	private ContactChatAddrField chatAddrField;
	private ContactItem item1;
	private ContactItem item2;
	private String chatAddr1 = "6731485921";
	private String chatAddr2 = "testauto@gmail.com";
	private String firstName = "saveChatAddr" + rand.nextInt(100);

	ArrayList<ContactItem> items;

	@Override
	protected void description() {
		Logging.description("Add chat address, google, and skype to a contact");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {

		chatAddrField = new ContactChatAddrField();
		item1 = new ContactItem();
		item1.setType(ConstantName.GOOGLE);
		item1.setValue(chatAddr1);
		item2 = new ContactItem();
		item2.setType(ConstantName.SKYPE);
		item2.setValue(chatAddr2);

		items = new ArrayList<ContactItem>();
		items.add(item1);
		items.add(item2);

		contactInfo = new ContactInfo(items, chatAddrField);

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// click add contact button, and add first name
		contact.clickAddContactButton();
		contactDetail.addFirstname(firstName);

		// click "add field" button
		contactDetail.clickAddFieldbutton();

		// select "chat address" with "google" option
		contactDetail.clickAddFieldLabel(ConstantName.CHATADDR,
				ConstantName.GOOGLE.toLowerCase());

		// click "+" button in "chat address" field
		// click the "google" option on left
		// select skype in "select label" view
		contactDetail.addFieldValue(contactInfo.getChatAddress(), contactInfo
				.getChatAddress().getComponentName());
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);

		// search the contact created just now, and go to contact detail view
		contact.searchContactbyKeywords(firstName);
		contact.clickContactbyName(firstName);

		// verify google, and skype label are existed in contact detail view with chat address
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.CHATADDR).contains(
				ConstantName.GOOGLE + ":\n" + chatAddr1));
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.CHATADDR).contains(
				ConstantName.SKYPE + ":\n" + chatAddr2));

	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
