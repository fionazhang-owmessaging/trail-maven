package synchronoss.com.test.contact;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_018_EditAddressbookAndCheckEditTrashButtonInMain extends
		TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private String addressbook = "editAddrbook" + rand.nextInt(10);
	private String addressbook_update = "updateName" + rand.nextInt(10);

	@Override
	protected void description() {
		Logging.description("Edit/empty default addressbook to to check the button is disabled, and edit a customer created addressbook");
	} 

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);

	}

	@Override
	protected void doTest() {
		// open side bar, and swipe the default address book
		navigation.clickMenuButton();
		sidebar.swipeAddressbookEditRMForm(ConstantName.DEFAULT);
		
		// verify the edit and delete button are disabled
		Assert.assertTrue(sidebar.getEditStatusOfAddrbook(ConstantName.EDIT));
		Assert.assertTrue(sidebar.getEditStatusOfAddrbook(ConstantName.DELETE));
		
		sidebar.clickMaskOnSidebar();
		
		if(Base.deviceName.toLowerCase().contains(ConstantName.IPAD)){
	      navigation.clickMenuButton();
		}

		// click add address book button, and add address book
		sidebar.clickAddAddressbookIcon();
		sidebar.addAddressbookName(addressbook);
		sidebar.clickButtonOnMessageBox(ConstantName.OK);
		
		// verify the address book is added sucessfully
		Assert.assertTrue(sidebar.getAddressbookNameList().toString()
				.contains(addressbook));
		
		// swipe the address book, click "edit address book icon"
		sidebar.swipeAddressbookEditRMForm(addressbook);
		sidebar.clickAddressbookNameAction(ConstantName.EDIT);
		
		// click "cancel" button in "input address book" popup 
		sidebar.clickButtonOnMessageBox(ConstantName.CANCEL);
		
		// verify the address book is not updated
		Assert.assertTrue(sidebar.getAddressbookNameList().toString()
				.contains(addressbook));
		
		// swipe the address book again, click edit address book
		sidebar.swipeAddressbookEditRMForm(addressbook);
		sidebar.clickAddressbookNameAction(ConstantName.EDIT);
		
		// click the 'X' button in "input address book" popup to clear up all texts
		sidebar.clickClearButtonOnMessageBox();
		
		// add address book, and click OK button
		sidebar.addNameWithoutClearInPopupDialog(addressbook_update);
		sidebar.clickButtonOnMessageBox(ConstantName.OK);
		
		// verify the address book is updated
		Assert.assertTrue(sidebar.getAddressbookNameList().toString()
				.contains(addressbook_update));
		
		// swipe the address book again, click delete button
		sidebar.swipeAddressbookEditRMForm(addressbook_update);
		sidebar.clickAddressbookNameAction(ConstantName.DELETE);
		
		// click "no button" in delete address book popup
		sidebar.clickButtonOnMessageBox(ConstantName.NO);
		
		// verify the address book is not deleted
		Assert.assertTrue(sidebar.getAddressbookNameList().toString()
				.contains(addressbook_update));

	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		
		// delete the address book
		contactUtil.deleteAddressbook(addressbook_update);
		sidebar.clickCloseButton();
		super.doTestCleanup();
	}
}
