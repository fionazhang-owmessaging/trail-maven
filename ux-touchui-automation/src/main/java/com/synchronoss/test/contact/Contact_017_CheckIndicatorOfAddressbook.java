package synchronoss.com.test.contact;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_017_CheckIndicatorOfAddressbook extends TestCase {
	ContactUtils contactUtil;
	ContactInfo contactInfo;

	@Override
	protected void description() {
		Logging.description("Check indicator of addressbook");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		contactUtil = new ContactUtils();
		contactInfo = new ContactInfo();

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// open side bar of contact, and get contact indicator of default address book
		navigation.clickMenuButton();
		int count = Integer.parseInt(sidebar
				.getContactCountInAddressbook(ConstantName.DEFAULT));
		
		// close the side bar
		sidebar.clickCloseButton();
		
		// add a contact
		contactUtil.addContact(contactInfo.getFirstname());
		
		// open side bar of contact, verify the indicator of default address book plus 1
		navigation.clickMenuButton();
		Assert.assertEquals((count + 1), Integer.parseInt(sidebar
				.getContactCountInAddressbook(ConstantName.DEFAULT)));
		
		// close the side bar
		sidebar.clickCloseButton();
		
		// delete the contact
		contactUtil.deleteContactFromDetail(true, contactInfo.getFirstname());
		
		// open the side bar again
		navigation.clickMenuButton();
		
		// verfiy the indicator of default address book minus 1 
		Assert.assertEquals(count, Integer.parseInt(sidebar
				.getContactCountInAddressbook(ConstantName.DEFAULT)));
	}

	public void doTestCleanup() {
		navigation.clickLogoutButton();
	}
}
