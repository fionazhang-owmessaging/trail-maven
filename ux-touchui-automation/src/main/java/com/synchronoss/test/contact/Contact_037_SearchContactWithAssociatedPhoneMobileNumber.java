package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactItem;
import synchronoss.com.component.ContactMobileField;
import synchronoss.com.component.ContactPhoneField;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;


public class Contact_037_SearchContactWithAssociatedPhoneMobileNumber extends TestCase {
  private Random rand = new Random();
  private ContactInfo contactInfo1;
  private ContactInfo contactInfo2;
  private ContactInfo contactInfo3;
  private ContactInfo contactInfo4;

  private ContactUtils contactUtil;
  private ContactMobileField mobileField1;
  private ContactMobileField mobileField2;

  private ContactPhoneField phoneField1;
  private ContactPhoneField phoneField2;

  private ContactItem item1;
  private ContactItem item2;
  private ContactItem item3;
  private ContactItem item4;

  private String mobileData1 = "123456";
  private String mobileData2 = "123456";
  private String phoneData1 = "123456";
  private String phoneData2 = "09876";

  private ArrayList<ContactInfo> contacts;

  @Override
  protected void description() {
    Logging
        .description("Create four contacts with different mobiles and phones, search the contacts by mobile or phone");
  }

  @Test(groups = {"contact", "sanity"})
  public void run() {
    super.run();
  }

  public void doTestSetup() {

    // data preparation
    mobileField1 = new ContactMobileField();
    item1 = new ContactItem();
    item1.setType(ConstantName.HOME);
    item1.setValue(mobileData1);
    contactInfo1 = new ContactInfo(item1, mobileField1);

    mobileField2 = new ContactMobileField();
    item2 = new ContactItem();
    item2.setType(ConstantName.WORK);
    item2.setValue(mobileData2);
    contactInfo2 = new ContactInfo(item2, mobileField2);


    phoneField1 = new ContactPhoneField();
    item3 = new ContactItem();
    item3.setType(ConstantName.WORK);
    item3.setValue(phoneData1);
    contactInfo3 = new ContactInfo(item3, phoneField1);

    phoneField2 = new ContactPhoneField();
    item4 = new ContactItem();
    item4.setType(ConstantName.OTHER);
    item4.setValue(phoneData2);
    contactInfo4 = new ContactInfo(item4, phoneField2);

    contacts = new ArrayList<ContactInfo>();
    contacts.add(contactInfo1);
    contacts.add(contactInfo2);
    contacts.add(contactInfo3);
    contacts.add(contactInfo4);

    contactUtil = new ContactUtils();

    super.doTestSetup();
    navigation.clickNavigationButton(ConstantName.CONTACTTAB);
    contactUtil.deleteAllContactInTheList();

  }

  @Override
  protected void doTest() {

    // Add 4 contacts with
    // contact 1 mobile home:123456
    // contact 2 mobile work:123456
    // contact 3 phone work:123456
    // contact 4 phone other:09876
    for (ContactInfo ci : contacts) {
      contact.clickAddContactButton();
      contactDetail.addFirstname(ci.getFirstname());
      if (ci.getMobile() != null)
        contactDetail.addFieldValue(ci.getMobile(), ci.getMobile().getComponentName());
      else
        contactDetail.addFieldValue(ci.getPhone(), ci.getPhone().getComponentName());

      contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
    }

    // search the contact by keyword - "123456"
    contact.searchContactbyKeywords(mobileData1);

    ArrayList<String> currentContact = contact.getCurrentContactList();

    // verify the contact search result list equals to 3
    Assert.assertEquals(currentContact.size(), contacts.size() - 1);

    // verify contact 1,2,3 are displayed in the list
    Assert.assertTrue(currentContact.toString().contains(contactInfo1.getFirstname()));
    Assert.assertTrue(currentContact.toString().contains(contactInfo2.getFirstname()));
    Assert.assertTrue(currentContact.toString().contains(contactInfo3.getFirstname()));

    // verify contact 4 is not displayed in the list
    Assert.assertFalse(currentContact.toString().contains(contactInfo4.getFirstname()));
  }

  public void doTestCleanup() {
    // delete the created contact
    for (ContactInfo ci : contacts) {
      contactUtil.deleteContactFromSlidebar(ci.getFirstname(), true);
    }
    super.doTestCleanup();
  }
}
