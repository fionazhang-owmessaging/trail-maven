package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactItem;
import synchronoss.com.component.ContactPhoneField;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_007_AddDiffTypeofPhoneInContact extends TestCase {
	Random rand = new Random();
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;
	private ContactPhoneField phoneField;
	private ContactItem item1;
	private ContactItem item2;
	private ContactItem item3;
	private String phoneData1 = "(110996-)" + rand.nextInt(10000);
	private String phoneData2 = "6973-" + rand.nextInt(1000000);
	private String phoneData3 = "66777" + rand.nextInt(100000);
	private String lastname = "savephone" + rand.nextInt(100);

	@Override
	protected void description() {
		Logging.description("Add three types of mobile to a contact");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {

		phoneField = new ContactPhoneField();
		item1 = new ContactItem();
		item1.setType(ConstantName.HOME);
		item1.setValue(phoneData1);
		item2 = new ContactItem();
		item2.setType(ConstantName.WORK);
		item2.setValue(phoneData2);
		item3 = new ContactItem();
		item3.setType(ConstantName.OTHER);
		item3.setValue(phoneData3);

		ArrayList<ContactItem> items = new ArrayList<ContactItem>();
		items.add(item1);
		items.add(item2);
		items.add(item3);

		contactInfo = new ContactInfo(items, phoneField);

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// create a contact with last name and add 3 types of phone into the contact (HOME,WORK,OTHER)
		// test data : home: (110996-)12345, work: 6973-12345, other 66777123456
		contact.clickAddContactButton();
		contactDetail.addLastname(lastname);
		contactDetail.addFieldValue(contactInfo.getPhone(), contactInfo
				.getPhone().getComponentName());
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
		contact.searchContactbyKeywords(lastname);
		contact.clickContactbyName(lastname);
		
		// verify 3 types of phone in contact detail view
		// and the detail view contains data: "HOME + phone" ; "WORK + phone" ; "EMAIL + phone " 
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.PHONE).contains(
				ConstantName.HOME + ":\n" + phoneData1));
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.PHONE).contains(
				ConstantName.WORK + ":\n" + phoneData2));
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.PHONE).contains(
				ConstantName.OTHER + ":\n" + phoneData3));
	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
