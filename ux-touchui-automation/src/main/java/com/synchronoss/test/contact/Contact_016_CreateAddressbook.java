package synchronoss.com.test.contact;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_016_CreateAddressbook extends TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private String addressbook = "addrbookName" + rand.nextInt(10);

	@Override
	protected void description() {
		Logging.description("Add a addressbook");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);

	}

	@Override
	protected void doTest() {
		// click open side bar button
		navigation.clickMenuButton();
		
		// click add address book button and add the address book name
		sidebar.clickAddAddressbookIcon();
		sidebar.addAddressbookName(addressbook);
		
		// click cancel button
		sidebar.clickButtonOnMessageBox(ConstantName.CANCEL);
		
		// click add address book button and add the address book name again
		sidebar.clickAddAddressbookIcon();
		sidebar.addAddressbookName(addressbook);
		
		// click Ok button, and verify the address book is added sucessfully
		sidebar.clickButtonOnMessageBox(ConstantName.OK);
		Assert.assertTrue(sidebar.getAddressbookNameList().toString()
				.contains(addressbook));

	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteAddressbook(addressbook);
		sidebar.clickCloseButton();
		super.doTestCleanup();
	}
}
