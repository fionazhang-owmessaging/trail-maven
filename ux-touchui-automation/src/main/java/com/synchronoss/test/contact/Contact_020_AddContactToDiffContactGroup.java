package synchronoss.com.test.contact;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_020_AddContactToDiffContactGroup extends TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private String contact1 = "first" + +rand.nextInt(100);

	private String contactGroup1 = "firstGroup" + rand.nextInt(100);
	private String contactGroup2 = "secondGroup" + rand.nextInt(100);

	@Override
	protected void description() {
		Logging.description("Create 1 contact, create 2 contact group, add created "
				+ "contact to 2 different contact group, check the indicator of each"
				+ " group, delete contact from one of the group, check the indicator again "
				+ "for both contact group and address book");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		contactUtil = new ContactUtils();
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
		// create a contact
		contactUtil.addContact(contact1);

		// create 2 contact groups("group A", "group B") against default address book
		contactUtil.addContactGroup(ConstantName.DEFAULT, contactGroup1);
		contactUtil.addContactGroup(ConstantName.DEFAULT, contactGroup2);

	}

	@Override
	protected void doTest() {
		// add one contact to two different contact groups
		contact.searchContactbyKeywords(contact1);
		contact.clickContactbyName(contact1);
		contactDetail.clickMoreButton();
		contactDetail.clickPopupMenuLabel(ConstantName.ADDTOGROUP);
		contactDetail.clickContactGroupNameInSelectGroup(contactGroup1);
		contactDetail.clickMoreButton();
		contactDetail.clickPopupMenuLabel(ConstantName.ADDTOGROUP);
		contactDetail.clickContactGroupNameInSelectGroup(contactGroup2);
		contactDetail.clickBackButton();
		navigation.clickMenuButton();

		// get indicator number of contact in default view
		int count = Integer.parseInt(sidebar
				.getContactCountInAddressbook(ConstantName.DEFAULT));

		// verify the contact indicator in each contact group, all should be 1
		sidebar.clickContactGroupEnterButton(ConstantName.DEFAULT);
		Assert.assertEquals(1, Integer.parseInt(sidebar
				.getContactCountInContactGroup(contactGroup1)));
		Assert.assertEquals(1, Integer.parseInt(sidebar
				.getContactCountInContactGroup(contactGroup2)));

		// delete contact in one contact group "group A"
		sidebar.clickContactGroupName(contactGroup2);
		contactUtil.deleteContactFromDetail(true, contact1);
		navigation.clickMenuButton();
		sidebar.clickContactGroupEnterButton(ConstantName.DEFAULT);

		// verify contact indicator in contact group again, one should be 1 ("group B"), the other should be 0 ("group A")
		Assert.assertEquals(1, Integer.parseInt(sidebar
				.getContactCountInContactGroup(contactGroup1)));
		Assert.assertEquals(0, Integer.parseInt(sidebar
				.getContactCountInContactGroup(contactGroup2)));

		// delete contact group with contact ("group B")
		contactUtil.deleteContactGroup(contactGroup1);
		
		// delete contact group without contact ("group A")
		contactUtil.deleteContactGroup(contactGroup2);
		sidebar.clickBackToAddressbookButton();

		// verify contact indicator in default address book, should not changed
		Assert.assertEquals(count, Integer.parseInt(sidebar
				.getContactCountInAddressbook(ConstantName.DEFAULT)));

	}

	public void doTestCleanup() {
		sidebar.clickCloseButton();
		super.doTestCleanup();
	}
}
