package synchronoss.com.test.contact;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_008_AddDiffTypeofAddressInContact extends TestCase {
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;
	ArrayList<String> address1;
	ArrayList<String> address2;
	ArrayList<String> address3;

	@Override
	protected void description() {
		Logging.description("Add three types of address to a contact");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		contactInfo = new ContactInfo();
		address1 = new ArrayList<String>();
		address1.add("xiaoyun");
		address1.add("chaoyang");
		address1.add("beijing");
		address1.add("100101");
		address1.add("china");
		address2 = new ArrayList<String>();
		address2.add("3330 S Figueroa St.");
		address2.add("Los Angeles");
		address2.add("CA");
		address2.add("90007");
		address2.add("America");
		address3 = new ArrayList<String>();
		address3.add("28 Bridge St.");
		address3.add("Frenchtown");
		address3.add("NJ");
		address3.add("08825");
		address3.add("America");

		contactInfo.setHomeAddress(address1);
		contactInfo.setWorkAddress(address2);
		contactInfo.setOtherAddress(address3);

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// click add contact button, and add last name
		contact.clickAddContactButton();
		contactDetail.addLastname(contactInfo.getLastname());
		
		// click add address plus button twice
		contactDetail.clickPlusButton(ConstantName.ADDRESS);
		contactDetail.clickPlusButton(ConstantName.ADDRESS);
		
		// add address in home/work/other fields
		contactDetail.addAddress(contactInfo.getHomeAddress(),
				ConstantName.HOME);
		contactDetail.addAddress(contactInfo.getWorkAddress(),
				ConstantName.WORK);
		contactDetail.addAddress(contactInfo.getOtherAddress(),
				ConstantName.OTHER);
		
		// save the contact
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
		
		// search the contact by last name, and go to detail view
		contact.searchContactbyKeywords(contactInfo.getLastname());
		contact.clickContactbyName(contactInfo.getLastname());

		// verify the 3 types of addresses are stored sucessufully
		Assert.assertTrue(contactDetail
				.getContactSectionListValue(ConstantName.ADDRESS).toString()
				.contains(address1.get(0)));
		Assert.assertTrue(contactDetail
				.getContactSectionListValue(ConstantName.ADDRESS).toString()
				.contains(address2.get(1)));
		Assert.assertTrue(contactDetail
				.getContactSectionListValue(ConstantName.ADDRESS).toString()
				.contains(address3.get(2)));
	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
