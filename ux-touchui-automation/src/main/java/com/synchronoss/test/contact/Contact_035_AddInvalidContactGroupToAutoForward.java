package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactEmailField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_035_AddInvalidContactGroupToAutoForward extends TestCase {
  private Random rand = new Random();
  private ContactUtils contactUtil;
  private String[
                 ] groupUser;

  private ContactEmailField emailField1;
  private ContactInfo contactInfo1;
  private String firstName1 = "firstContact" + rand.nextInt(100);
  private String firstName2 = "secondContact" + rand.nextInt(100);
  private ContactItem item1;

  private String contact1;

  private String contactGroup1 = "containSelf" + rand.nextInt(100);
  private String contactGroup2 = "noEmail" + rand.nextInt(100);
  private String contactGroup3 = "emptyGroup" + rand.nextInt(100);

  private String addSelfFwdPopup =
      "You cannot put your own address as an auto forward destination as this would cause you mailbox to fill up to the maximum limit";
  private String addEmptyGroupFwdPopup = "Empty group list.";
  private String addEmptyDestionationFwdValidation = "Destination cannot be empty";

  @Override
  protected void description() {
    Logging.description("Create 3 contact groups in default address book, one contains current "
        + "mail self, second one don't have email, third one is empty group");
  }

  @Test(groups = {"contact", "sanity"})
  public void run() {
    super.run();
  }

  public void doTestSetup() {
    groupUser = new String[] {this.getParameter("emailid1")};

    Logging.info("get user name [0] as " + groupUser[0]);
    contact1 = groupUser[0];
    emailField1 = new ContactEmailField();
    item1 = new ContactItem();
    item1.setType(ConstantName.HOME);
    item1.setValue(contact1);
    ArrayList<ContactItem> items1 = new ArrayList<ContactItem>();
    items1.add(item1);

    contactInfo1 = new ContactInfo(items1, emailField1);
    contactInfo1.setFirstname(firstName1);

    contactUtil = new ContactUtils();

    super.doTestSetup();

    // go to contact view and add 2 contacts, first contains current user, second one don't contain
    // email
    navigation.clickNavigationButton(ConstantName.CONTACTTAB);
    contactUtil.addContactByEmail(contactInfo1);
    contactUtil.addContact(firstName2);

    // add 3 contact groups in default, group 1 contains current user, group 2 doesnt have email
    // address, group3 is an empty group
    contactUtil.addContactGroup(ConstantName.DEFAULT, contactGroup1);
    contactUtil.addContactGroup(ConstantName.DEFAULT, contactGroup2);
    contactUtil.addContactGroup(ConstantName.DEFAULT, contactGroup3);


  }

  @Override
  protected void doTest() {
    // add 2 contacts to group 1 and group 2
    contactUtil.addContactToGroup(firstName1, contactGroup1);
    contactUtil.addContactToGroup(firstName2, contactGroup2);

    // go to mail settings view
    navigation.clickNavigationButton(ConstantName.SETTINGTAB);
    settings.clickSettingOptionList(settings.toCapitlize(ConstantName.MAIL));

    // set auto forward as true, and remove all emails in the list
    settings.clickMailSettingOption(ConstantName.AUTOFWD);
    settings.useAutoForward(true);
    settings.removeAllAutoForwardEmailDestinations();

    // add contain contact group that contains current user to auto fwd
    settings.addAutoForwardDestinationEmail(contactGroup1, true);

    // verify 'add cannot self as auto forward' popup displayed
    Assert.assertEquals(settings.getTextOnMessageBox(), addSelfFwdPopup);
    settings.clickButtonOnMessageBox(ConstantName.OK);
    settings.removeAllAutoForwardEmailDestinations();

    // add group that don't contain emails to auto fwd
    settings.addAutoForwardDestinationEmail(contactGroup2, true);

    // verify 'Empty group list.' popup displayed
    Assert.assertEquals(settings.getValidationText(), addEmptyDestionationFwdValidation);

    // add empty group to auto fwd
    settings.addAutoForwardDestinationEmail(contactGroup3, true);

    // verify 'Destination cannot be empty' validation displayed
    Assert.assertEquals(settings.getTextOnMessageBox(), addEmptyGroupFwdPopup);

    settings.clickButtonOnMessageBox(ConstantName.OK);
    settings.useAutoForward(false);
    settings.clickHeadBarOfSettingDetail(ConstantName.SAVE);



  }

  public void doTestCleanup() {

    // delete the contact group and contact
    navigation.clickNavigationButton(ConstantName.CONTACTTAB);
    navigation.clickMenuButton();
    sidebar.clickContactGroupEnterButton(ConstantName.DEFAULT);
    contactUtil.deleteContactGroup(contactGroup1);
    contactUtil.deleteContactGroup(contactGroup2);
    contactUtil.deleteContactGroup(contactGroup3);
    sidebar.clickBackToAddressbookButton();
    sidebar.clickCloseButton();
    contactUtil.deleteContactFromDetail(true, firstName1);
    contactUtil.deleteContactFromDetail(true, firstName2);

    super.doTestCleanup();
  }
}
