package synchronoss.com.test.contact;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.EmailMessage;
import synchronoss.com.testcase.utils.EmailUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_032_MoveAutoSavedContactToDefaultAddressbook extends
		TestCase {
	private ArrayList<EmailMessage> emailMessage;
	private ArrayList<String[]> users;

	private EmailUtils emailUtils;
	private ContactUtils contactUtils;

	
	@Override
	protected void description() {
		Logging.description("Clean up all contacts we use in addressbook and auto-saved-folder. "
				+ "Set auto-save contact as true in settings view, send a mail to a user, check "
				+ "the user in auto-saved-folder, swipe the contact, and select move it to default "
				+ "address book. Go to default addressbook, verify the contact exists");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		users = new ArrayList<String[]>();
		users.add(new String[] { this.getParameter("emailid4") });

		contactUtils = new ContactUtils();
		emailUtils = new EmailUtils();
		emailMessage = new ArrayList<EmailMessage>();
		for (int i = 0; i < 1; i++) {
			emailMessage.add(new EmailMessage(users.get(i)));
		}

		super.doTestSetup();
		boolean isAutoSaveNewContact = login.isAutoSaveNewContact();
		// precondition:
		// go to contact view , delete contact we will use
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);

		for (int i = 0; i < users.size(); i++) {
			contact.searchContactbyKeywords(users.get(i)[0]);
			while (!contact.isContactListEmpty())
				contactUtils.deleteContactFromSlidebar(users.get(i)[0], true);
		}
		
		// delete all address books
		navigation.clickMenuButton();
		ArrayList<String> addressbookNameList = sidebar.getAddressbookNameList();
		if(addressbookNameList.size()!=1){
			for(int i = 1 ; i < addressbookNameList.size() ; i ++ ){
				contactUtils.deleteAddressbook(addressbookNameList.get(i));
			}
		}
		sidebar.clickCloseButton();


		// check auto-compelete folder, and delete auto-complete users
		navigation.clickMenuButton();
		int autoCompleteFolderContacts = sidebar.getContactAutoCompleteCount();
		if (autoCompleteFolderContacts != 0) {
			sidebar.clickAutoCompleteFolder();
			for (int i = 0; i < autoCompleteFolderContacts; i++) {
				WebElement el = contact.getAutoContactList().get(0);
				contact.swipeAutoSuggestContactByName(el.getText());
				contact.clickActionButtonOnSlideToolbar(ConstantName.DELETE);
				contact.clickButtonOnMessageBox(ConstantName.YES);
			}
		} else
			sidebar.clickCloseButton();

		if (!isAutoSaveNewContact) {
			// if auto save new contact is disable
			// go to setting - mail view, change the auto-save contact as true
			navigation.clickNavigationButton(ConstantName.SETTINGTAB);
			settings.clickSettingOptionList(settings
					.toCapitlize(ConstantName.MAIL));
			settings.useAutoSaveNewContacts(true);
			settings.clickHeadBarOfSettingDetail(ConstantName.SAVE);
			settings.clickHeadBarOfSettingDetail(ConstantName.BACK);
		}

	}

	@Override
	protected void doTest() {
		// steps:
		// go to mail list view 
		navigation.clickNavigationButton(ConstantName.MAILTAB);
		
		// send a email to user1
		for (int i = 0; i < users.size(); i++)
			emailUtils.writeEmails(emailMessage.get(i));

		// go to contact view 
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
		navigation.clickMenuButton();
		
		// open sidebar and get auto-complete indicator count
		int autoCompleteCount = sidebar.getContactAutoCompleteCount();
		
		// verify the indicator number is 1
		Assert.assertEquals(autoCompleteCount, 1,
				"verify there's 1 contact stored in auto-complete folder");

		// go into the auto-complete folder
		sidebar.clickAutoCompleteFolder();
		
		// swipe the contact, and select move to default addressbook
		contact.swipeAutoSuggestContactByName(users.get(0)[0]);
		contact.clickActionButtonOnSlideToolbar(ConstantName.MOVE);
		contactDetail.clickAddressbookSelectPopup(ConstantName.DEFAULT);
		contactDetail.clickButtonOnMessageBox(ConstantName.YES);

		// go to default addressbook
		navigation.clickMenuButton();
		sidebar.clickAddressbookName(ConstantName.DEFAULT);

		for (int i = 0; i < users.size(); i++) {
			// search the contact in default addressbook
			contact.searchContactbyKeywords(users.get(i)[0]);
			
			// go to the detail view of the contact
			contact.clickContactbyName(users.get(i)[0]);
			
			// verify the detail of the contact (email) is created just now
			Assert.assertTrue(contactDetail
					.getContactSectionListValue(ConstantName.EMAIL).toString()
					.contains(users.get(i)[0]));
			contactDetail.clickDeleteButton();
			contactDetail.clickButtonOnMessageBox(ConstantName.YES);
		}
	}

	public void doTestCleanup() {
		super.doTestCleanup();
	}

}
