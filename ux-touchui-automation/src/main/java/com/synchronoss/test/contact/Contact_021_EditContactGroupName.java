package synchronoss.com.test.contact;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_021_EditContactGroupName extends TestCase {
	private Random rand = new Random();
	private ContactUtils contactUtil;
	private String addressbook = "addr-cont" + rand.nextInt(100);;

	private String contactGroup = "originalName" + rand.nextInt(100);
	private String updateContactGroupName = "updateName" + rand.nextInt(100);

	@Override
	protected void description() {
		Logging.description("create a contact group in a new create addressbook, "
				+ "edit the name of contact group, and click Cancel , edit name of "
				+ "contact group again, and click ok, check contact group name, delete"
				+ " addressbook with contact");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		contactUtil = new ContactUtils();
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
		
		// create address book
		contactUtil.addAddressbook(addressbook);

		// create contact group
		contactUtil.addContactGroup(addressbook, contactGroup);

	}

	@Override
	protected void doTest() {
		// open the side bar
		navigation.clickMenuButton();
		
		// go to the contact group list view of the address book created just now
		sidebar.clickContactGroupEnterButton(addressbook);

		// swipe the contact group and click "edit button"
		sidebar.swipeContactGroupEditForm(contactGroup);
		sidebar.clickContactGroupAction(ConstantName.EDIT);

		// click the "cancel button" in edit contact group name popup
		sidebar.clickButtonOnMessageBox(ConstantName.CANCEL);

		// verify the contact group name is not updated  
		Assert.assertTrue(sidebar.getContactGroupList().toString()
				.contains(contactGroup));

		// swipe the contact group name again, and click edit button
		sidebar.swipeContactGroupEditForm(contactGroup);
		sidebar.clickContactGroupAction(ConstantName.EDIT);

		// enter a new name to the contact group, and save changes
		sidebar.addContactGroupName(updateContactGroupName);
		sidebar.clickButtonOnMessageBox(ConstantName.OK);

		// verify the contact group name is updated
		Assert.assertTrue(sidebar.getContactGroupList().toString()
				.contains(updateContactGroupName));
		Assert.assertFalse(sidebar.getContactGroupList().toString()
				.contains(contactGroup));

		// go back to address book list, and delete the address book
		sidebar.clickBackToAddressbookButton();
		contactUtil.deleteAddressbook(addressbook);
	}

	public void doTestCleanup() {
		sidebar.clickCloseButton();
		super.doTestCleanup();
	}
}
