package synchronoss.com.test.contact;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_009_AddBirthdayAndAnniversayInContact extends TestCase {
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;
	private LocalDate date1;
	private LocalDate date2;
	private String expectDate1;
	private String expectDate2;
    private String dateFormat = "dd-MM-yyyy";
    private DateTimeFormatter format = DateTimeFormatter
        .ofPattern(dateFormat);

	@Override
	protected void description() {
		Logging.description("Add birthday and anniversay to a contact");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		date1 = LocalDate.of(1990, 6, 1);
		date2 = LocalDate.of(2000, 8, 1);
		expectDate1 = date1.format(format);
		expectDate2 = date2.format(format);
		
        super.doTestSetup();

        // check the date format
		if(!login.getUserDateFormat().equalsIgnoreCase(dateFormat)){
	        // go to setting - locale view
	        navigation.clickNavigationButton(ConstantName.SETTINGTAB);
	        settings.clickSettingOptionList(ConstantName.LOCALE);

	        // select date format(YYYY/MM/DD)
	        settings.selectDateFormat_ByJS(dateFormat);
	        settings.clickHeadBarOfSettingDetail(ConstantName.SAVE);
	        settings.clickHeadBarOfSettingDetail(ConstantName.BACK);
		}
		
		contactInfo = new ContactInfo();

		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// click add contact button, and add last name
		contact.clickAddContactButton();
		contactDetail.addLastname(contactInfo.getLastname());
		
		// click plus button in personal field to add another option
		contactDetail.clickPlusButton(ConstantName.PERSONAL);

		// select birthday
		contactDetail.selectPesonalDate(date1, ConstantName.BIRTHDAY);
		
		// select anniversary
		contactDetail.selectPesonalDate(date2, ConstantName.ANNIVERSARY);

		// save the contact
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
		
		// search the contact and go to detail view of the contact
		contact.searchContactbyKeywords(contactInfo.getLastname());
		contact.clickContactbyName(contactInfo.getLastname());

		// verify the 2 dates are saved as expected
		Assert.assertTrue(contactDetail
				.getContactSectionListValue(ConstantName.PERSONAL).toString()
				.contains(expectDate1));
		Assert.assertTrue(contactDetail
				.getContactSectionListValue(ConstantName.PERSONAL).toString()
				.contains(expectDate2));

	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
