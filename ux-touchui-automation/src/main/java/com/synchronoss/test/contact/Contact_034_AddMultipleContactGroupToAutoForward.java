package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactEmailField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.EmailMessage;
import synchronoss.com.testcase.utils.EmailUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_034_AddMultipleContactGroupToAutoForward extends TestCase {
  private Random rand = new Random();
  private ContactUtils contactUtil;
  private String[] originalEmail = null;
  private EmailMessage message;
  private EmailUtils mailUtils;
  private String[] groupUser;
  private String[] passwd;
  private String[] originalPwd;

  private ContactEmailField emailField1;
  private ContactEmailField emailField2;
  private ContactInfo contactInfo1;
  private ContactInfo contactInfo2;
  private String firstName1 = "firstContact" + rand.nextInt(100);
  private String firstName2 = "secondContact" + rand.nextInt(100);
  private ContactItem item1;
  private ContactItem item2;

  private String contact1;
  private String contact2;

  private String contactGroup1 = "autoForwardWithMutlple1" + rand.nextInt(100);
  private String contactGroup2 = "autoForwardWithMutlple2" + rand.nextInt(100);


  @Override
  protected void description() {
    Logging
        .description("Create 2 contact groups in default address book, and add 2 contacts to the "
            + "group seperatly, set the 2 contacts to the auto-forward field");
  }

  @Test(groups = {"contact", "sanity"})
  public void run() {
    super.run();
  }

  public void doTestSetup() {
    groupUser = new String[] {this.getParameter("emailid2"), this.getParameter("emailid3")};
    passwd = new String[] {this.getParameter("password2"), this.getParameter("password3")};
    originalEmail = new String[] {this.getParameter("emailid1")};
    originalPwd = new String[] {this.getParameter("password1")};

    message = new EmailMessage(originalEmail);

    Logging.info("get user name [0] as " + groupUser[0]);
    Logging.info("get user name [1] as " + groupUser[1]);
    contact1 = groupUser[0];
    contact2 = groupUser[1];
    emailField1 = new ContactEmailField();
    item1 = new ContactItem();
    item1.setType(ConstantName.HOME);
    item1.setValue(contact1);
    ArrayList<ContactItem> items1 = new ArrayList<ContactItem>();
    items1.add(item1);
    mailUtils = new EmailUtils();
    emailField2 = new ContactEmailField();
    item2 = new ContactItem();
    item2.setType(ConstantName.HOME);
    item2.setValue(contact2);
    ArrayList<ContactItem> items2 = new ArrayList<ContactItem>();
    items2.add(item2);

    contactInfo1 = new ContactInfo(items1, emailField1);
    contactInfo1.setFirstname(firstName1);
    contactInfo2 = new ContactInfo(items2, emailField2);
    contactInfo2.setFirstname(firstName2);

    contactUtil = new ContactUtils();

    super.doTestSetup();

    // go to contact view and add 2 contacts
    navigation.clickNavigationButton(ConstantName.CONTACTTAB);
    contactUtil.addContactByEmail(contactInfo1);
    contactUtil.addContactByEmail(contactInfo2);

    // add 2 contact groups in default
    contactUtil.addContactGroup(ConstantName.DEFAULT, contactGroup1);
    contactUtil.addContactGroup(ConstantName.DEFAULT, contactGroup2);


  }

  @Override
  protected void doTest() {
    // add 2 contacts to each contact group separately in default address book
    contactUtil.addContactToGroup(firstName1, contactGroup1);
    contactUtil.addContactToGroup(firstName2, contactGroup2);

    // go to mail settings view
    navigation.clickNavigationButton(ConstantName.SETTINGTAB);
    settings.clickSettingOptionList(settings.toCapitlize(ConstantName.MAIL));

    // set auto forward as true, and remove all emails in the list
    settings.clickMailSettingOption(ConstantName.AUTOFWD);
    settings.useAutoForward(true);
    settings.removeAllAutoForwardEmailDestinations();

    // input few words of the group, select the group1 in auto suggest list
    settings.addAutoForwardDestinationEmail(contactGroup1, false);
    settings.clickAutoSuggestContact(contactGroup1);

    // input few words of the group, select the group2 in auto suggest list
    settings.addAutoForwardDestinationEmail(contactGroup2, false);
    settings.clickAutoSuggestContact(contactGroup2);

    // set keep copy in inbox as false
    settings.useKeepCopyInInbox(false);
    settings.clickHeadBarOfSettingDetail(ConstantName.SAVE);

    // go to email view and write an email
    navigation.clickNavigationButton(ConstantName.MAILTAB);
    mailUtils.writeEmails(message);

    // refresh the mail list
    email.refresh_maillist();

    // verify the user do not get the new mail
    Assert.assertFalse(email.getEmailSubjectsInMailList().contains(message.getSubject()));

    // log out for current user
    navigation.clickMenuButton();
    navigation.clickLogoutButton();


    // login to user1@xxxx and user2@xxxx seperately , and verify user1 and user2 get the new mail
    for (int i = 0; i < groupUser.length; i++) {
      login.typeUsername(groupUser[i]);
      login.typePassword(passwd[i]);
      login.clickLoginButton();
      Assert.assertTrue(email.getEmailSubjectsInMailList().contains(message.getSubject()));
      navigation.clickMenuButton();
      navigation.clickLogoutButton();
    }

  }

  public void doTestCleanup() {
    // login current user again
    login.typeUsername(originalEmail[0]);
    login.typePassword(originalPwd[0]);
    login.clickLoginButton();

    // set auto forward as false and remove all mails
    navigation.clickNavigationButton(ConstantName.SETTINGTAB);
    settings.clickSettingOptionList(settings.toCapitlize(ConstantName.MAIL));
    settings.clickMailSettingOption(ConstantName.AUTOFWD);
    settings.removeAllAutoForwardEmailDestinations();
    settings.useAutoForward(false);
    settings.clickHeadBarOfSettingDetail(ConstantName.SAVE);


    // delete the contact group
    navigation.clickNavigationButton(ConstantName.CONTACTTAB);
    navigation.clickMenuButton();
    sidebar.clickContactGroupEnterButton(ConstantName.DEFAULT);
    contactUtil.deleteContactGroup(contactGroup1);
    contactUtil.deleteContactGroup(contactGroup2);
    sidebar.clickBackToAddressbookButton();
    sidebar.clickCloseButton();
    contactUtil.deleteContactFromDetail(true, firstName1);
    contactUtil.deleteContactFromDetail(true, firstName2);

    super.doTestCleanup();
  }
}
