package synchronoss.com.test.contact;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_029_AddContactToGroupFromSlideContactItem extends TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private String contact1 = "test29" + +rand.nextInt(100);

	private String contactGroup1 = "addGroup" + rand.nextInt(100);

	@Override
	protected void description() {
		Logging.description("Create 1 contact, and a contact group, go to contact list view, slide the contact ,and add it to a group");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		contactUtil = new ContactUtils();
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
		
		// create a contact with first name
		contactUtil.addContact(contact1);

		// create a contact group in default address book
		contactUtil.addContactGroup(ConstantName.DEFAULT, contactGroup1);

	}

	@Override
	protected void doTest() {
		// add the contact to the contact group by swipe the contact
		contact.searchContactbyKeywords(contact1);
		
		// swipe the contact 
		contact.swipeContactByName(contact1);
		
		// select "Add to group" icon
		contact.clickActionButtonOnSlideToolbar(ConstantName.ADDTOGROUP);

		// click cancel button in "select group" view
		contactDetail.clickContactGroupNameSelectCancel();

		// swipe the contact , and click add to group icon again
		contact.swipeContactByName(contact1);
		contact.clickActionButtonOnSlideToolbar(ConstantName.ADDTOGROUP);
		
		// select the group created just now
		contactDetail.clickContactGroupNameInSelectGroup(contactGroup1);

		// click menu button
		navigation.clickMenuButton();

		// get the indicator of contacts in default address book
		int count = Integer.parseInt(sidebar
				.getContactCountInAddressbook(ConstantName.DEFAULT));

		// go to the contact group list view
		sidebar.clickContactGroupEnterButton(ConstantName.DEFAULT);

		// verify the contact group created just now , the indicator count is 1
		Assert.assertEquals(1, Integer.parseInt(sidebar
				.getContactCountInContactGroup(contactGroup1)));

		// enter the contact group view
		sidebar.clickContactGroupName(contactGroup1);

		// swipe the contact 
		// select "delete" icon
		// select "No" in delete contact popup to cancel "delete contact"
		contactUtil.deleteContactFromSlidebar(contact1, false);
		
		// swipe the contact 
		// select "delete" icon
		// select "yes" in delete contact popup to delete contact
		contactUtil.deleteContactFromSlidebar(contact1, true);

		// open contact group side bar list
		navigation.clickMenuButton();
		sidebar.clickContactGroupEnterButton(ConstantName.DEFAULT);

		// verify the contact indicator in contact group again, it changes to 0
		Assert.assertEquals(0, Integer.parseInt(sidebar
				.getContactCountInContactGroup(contactGroup1)));

		// delete contact group without contact
		contactUtil.deleteContactGroup(contactGroup1);
		sidebar.clickBackToAddressbookButton();

		// verify the contact indicator in default address book, the count should be
		// same as before, not changes even delete the contact in contact group
		Assert.assertEquals(count, Integer.parseInt(sidebar
				.getContactCountInAddressbook(ConstantName.DEFAULT)));

	}

	public void doTestCleanup() {
		sidebar.clickAddressbookName(ConstantName.DEFAULT);
		contactUtil.deleteContactFromSlidebar(contact1, true);
		super.doTestCleanup();
	}
}
