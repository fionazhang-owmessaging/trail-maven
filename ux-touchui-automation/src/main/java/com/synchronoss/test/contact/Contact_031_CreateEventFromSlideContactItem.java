package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactEmailField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_031_CreateEventFromSlideContactItem extends TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private ContactEmailField emailField1;
	private ContactInfo contactInfo1;
	private String firstName1 = "event" + rand.nextInt(100);
	private ContactItem item1;
	private String contact1;
	private String eventTitle = "eventFromContact-item" + rand.nextInt(100);;

	@Override
	protected void description() {
		Logging.description("create a contact with email, click create event in slide bar, check Attendee field");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		String user = getParameter("emailid2");

		Logging.info("get user name  as " + user);
		contact1 = user;
		emailField1 = new ContactEmailField();
		item1 = new ContactItem();
		item1.setType(ConstantName.HOME);
		item1.setValue(contact1);
		ArrayList<ContactItem> items1 = new ArrayList<ContactItem>();
		items1.add(item1);

		contactInfo1 = new ContactInfo(items1, emailField1);
		contactInfo1.setFirstname(firstName1);

		contactUtil = new ContactUtils();
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);

	}

	@Override
	protected void doTest() {

		// add contact by email address and first name
		contactUtil.addContactByEmail(contactInfo1);
		
		// search the contact by first name
		contact.searchContactbyKeywords(contactInfo1.getFirstname());
		
		// swipe the contact item created just now in search result list
		contact.swipeContactByName(contactInfo1.getFirstname());
		
		// select "create event" icon on slide bar
		contact.clickActionButtonOnSlideToolbar(ConstantName.EVENT);
		
		// verify the contact displayed in attendee field of the event view
		Assert.assertTrue(eventDetail.getAttendee().toString()
				.contains(contact1));
		
		// add a title of the event and save
		eventDetail.addEventTitle(eventTitle);
		eventDetail.clickSaveButton();

	}

	public void doTestCleanup() {
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
		contactUtil.deleteContactFromSlidebar(contact1, true);
		super.doTestCleanup();
	}
}
