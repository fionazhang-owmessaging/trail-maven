
package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactEmailField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_022_SendMailFromContactGroup extends TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private ContactEmailField emailField1;
	private ContactEmailField emailField2;
	private ContactInfo contactInfo1;
	private ContactInfo contactInfo2;
	private String firstName1 = "firstContact" + rand.nextInt(100);
	private String firstName2 = "secondContact" + rand.nextInt(100);

	private ContactItem item1;
	private ContactItem item2;

	private String contact1;
	private String contact2;

	private String contactGroup = "realUser" + rand.nextInt(100);
	private String addressbook = "SendMailAddrbook" + rand.nextInt(100);

	private String subject = "UIAutomation - Contact_022_SendMailFromContactGroup";

	@Override
	protected void description() {
		Logging.description("Create a addressbook, create 2 contacts and add to a group, send mail from contact group view");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		String[] user = { this.getParameter("emailid1"),
				this.getParameter("emailid2") };

		Logging.info("get user name [0] as " + user[0]);
		Logging.info("get user name [1] as " + user[1]);
		contact1 = user[0];
		contact2 = user[1];
		emailField1 = new ContactEmailField();
		item1 = new ContactItem();
		item1.setType(ConstantName.HOME);
		item1.setValue(contact1);
		ArrayList<ContactItem> items1 = new ArrayList<ContactItem>();
		items1.add(item1);

		emailField2 = new ContactEmailField();
		item2 = new ContactItem();
		item2.setType(ConstantName.HOME);
		item2.setValue(contact2);
		ArrayList<ContactItem> items2 = new ArrayList<ContactItem>();
		items2.add(item2);

		contactInfo1 = new ContactInfo(items1, emailField1);
		contactInfo1.setFirstname(firstName1);
		contactInfo2 = new ContactInfo(items2, emailField2);
		contactInfo2.setFirstname(firstName2);

		contactUtil = new ContactUtils();
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);

	}

	@Override
	protected void doTest() {
		// add a new address book "AAA"
		contactUtil.addAddressbook(addressbook);
		navigation.clickMenuButton();
		sidebar.clickAddressbookName(addressbook);

		// add 2 contacts
		contactUtil.addContactByEmail(contactInfo1);
		contactUtil.addContactByEmail(contactInfo2);

		// add contact group "aaa" in address book "AAA"
		contactUtil.addContactGroup(addressbook, contactGroup);

		// add contact to contact groups "aaa"
		contactUtil.addContactToGroup(firstName1, contactGroup);
		contactUtil.addContactToGroup(firstName2, contactGroup);

		// open side bar
		navigation.clickMenuButton();

		// open contact group list
		sidebar.clickContactGroupEnterButton(addressbook);

		// swipe the contact group
		sidebar.swipeContactGroupEditForm(contactGroup);

		// select "send email icon", and the compose view is open
		sidebar.clickContactGroupAction(ConstantName.MAIL);

		// verify the compose view displayed and the 2 contacts exist in To field
		String tempResult = emailComposer.getTextInToRecepient().toString();
		Assert.assertTrue(tempResult.contains(contact1)
				|| tempResult.contains(contactInfo1.getFirstname()));
		Assert.assertTrue(tempResult.contains(contact2)
				|| tempResult.contains(contactInfo2.getFirstname()));

	}

	public void doTestCleanup() {
		emailComposer.addSubject(subject);
		emailComposer.clickSendEmailButton();
		navigation.clickMenuButton();
		contactUtil.deleteAddressbook(addressbook);
		sidebar.clickCloseButton();
		super.doTestCleanup();
	}
}
