package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactItem;
import synchronoss.com.component.ContactWebsiteField;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_013_AddDiffTypeofWeblinkInContact extends TestCase {
	Random rand = new Random();
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;
	private ContactWebsiteField webField;
	private ContactItem item1;
	private ContactItem item2;
	private ContactItem item3;
	private String webSite_home = "http://www.amazon.com/";
	private String webSite_work = "http://www.bbc.com";
	private String webSite_other = "https://jira.owmessaging.com";
	private String firstName = "webiste" + rand.nextInt(100);
	ArrayList<ContactItem> items;

	@Override
	protected void description() {
		Logging.description("Add three types of website to a contact, and click each website, check the title of it");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {

		webField = new ContactWebsiteField();
		item1 = new ContactItem();
		item1.setType(ConstantName.HOME);
		item1.setValue(webSite_home);
		item2 = new ContactItem();
		item2.setType(ConstantName.WORK);
		item2.setValue(webSite_work);
		item3 = new ContactItem();
		item3.setType(ConstantName.OTHER);
		item3.setValue(webSite_other);

		items = new ArrayList<ContactItem>();
		items.add(item1);
		items.add(item2);
		items.add(item3);

		contactInfo = new ContactInfo(items, webField);

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// click add contact button, and add first name
		contact.clickAddContactButton();
		contactDetail.addFirstname(firstName);

		// click "add field" button
		contactDetail.clickAddFieldbutton();

		// select website with "home" option
		contactDetail.clickAddFieldLabel(ConstantName.WEB,
				ConstantName.HOME.toLowerCase());

		// click "+" button twice in website field
		// click the "home" option on left, select "work" in "select label" view
		// click the "home" option on left again, select "other" in "select label" view
		// save the contact
		contactDetail.addFieldValue(contactInfo.getWebsite(), contactInfo
				.getWebsite().getComponentName());
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);

		// search the contact, and go to detail view
		contact.searchContactbyKeywords(firstName);
		contact.clickContactbyName(firstName);

		// verify the contact detail view contains website label of home/work/others with correct website link
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.WEB).contains(
				ConstantName.HOME + ":\n" + webSite_home));
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.WEB).contains(
				ConstantName.WORK + ":\n" + webSite_work));
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.WEB).contains(
				ConstantName.OTHER + ":\n" + webSite_other));
	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
