package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactEmailField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_025_SendMailFromContactDetailMoreOption extends TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private ContactEmailField emailField1;
	private ContactInfo contactInfo1;
	private String firstName1 = "mailContact" + rand.nextInt(100);
	private String subject = "UIAutomation - Contact_025_SendMailFromContactDetailMoreOption";

	private ContactItem item1;

	private String contact1;

	@Override
	protected void description() {
		Logging.description("create a contact with email, click send email in more button, check To field");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		String[] user = { this.getParameter("emailid1"),
				this.getParameter("emailid2") };

		Logging.info("get user name [0] as " + user[0]);
		contact1 = user[0];
		emailField1 = new ContactEmailField();
		item1 = new ContactItem();
		item1.setType(ConstantName.HOME);
		item1.setValue(contact1);
		ArrayList<ContactItem> items1 = new ArrayList<ContactItem>();
		items1.add(item1);

		contactInfo1 = new ContactInfo(items1, emailField1);
		contactInfo1.setFirstname(firstName1);

		contactUtil = new ContactUtils();
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);

	}

	@Override
	protected void doTest() {

		// add contact with first name and email address
		contactUtil.addContactByEmail(contactInfo1);
		contact.searchContactbyKeywords(contactInfo1.getFirstname());
		
		// go to the detail view of contact
		contact.clickContactbyName(contactInfo1.getFirstname());
		
		// click more button 
		contactDetail.clickMoreButton();
		
		// select "send email" icon, and the compose view is open
		contactDetail.clickPopupMenuLabel(ConstantName.MAIL);
		
		// verify the to recepient saved in compose view in "To field"
		Assert.assertTrue(emailComposer.getTextInToRecepient().toString()
				.contains(contactInfo1.getFirstname()));

	}

	public void doTestCleanup() {
		emailComposer.addSubject(subject);
		emailComposer.clickSendEmailButton();
		contactUtil.deleteContactFromDetail(false, contactInfo1.getFirstname());
		super.doTestCleanup();
	}
}
