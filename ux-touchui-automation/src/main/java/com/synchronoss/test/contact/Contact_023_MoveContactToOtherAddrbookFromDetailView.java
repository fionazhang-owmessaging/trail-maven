package synchronoss.com.test.contact;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_023_MoveContactToOtherAddrbookFromDetailView extends
		TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private String firstName = "moveContact" + rand.nextInt(100);
	private String addressbook = "moveAddrbook" + rand.nextInt(100);

	@Override
	protected void description() {
		Logging.description("Create a contact, create a addressbook , move the contact to new addressbook throw detail view");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		contactUtil = new ContactUtils();
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
		
		// create a contact with first name
		contactUtil.addContact(firstName);
		
		// create an address book
		contactUtil.addAddressbook(addressbook);

	}

	@Override
	protected void doTest() {
		// open side bar and get the indicator of address book
		navigation.clickMenuButton();
		int orginalCount = Integer.parseInt(sidebar
				.getContactCountInAddressbook(ConstantName.DEFAULT));
		
		// close the side bar
		sidebar.clickCloseButton();

		// search by first name of the contact
		contact.searchContactbyKeywords(firstName);

		// go to detail view of contact and click more buton
		contact.clickContactbyName(firstName);
		contactDetail.clickMoreButton();

		// click "move" option
		contactDetail.clickPopupMenuLabel(ConstantName.MOVE);
		
		// select the address book created just now
		contactDetail.clickAddressbookSelectPopup(addressbook);

		// open side bar, and verify the default address book count minus 1 and the address book created just now changes to 1
		navigation.clickMenuButton();
		Assert.assertEquals(String.valueOf((orginalCount - 1)),
				sidebar.getContactCountInAddressbook(ConstantName.DEFAULT));
		Assert.assertEquals("1",
				sidebar.getContactCountInAddressbook(addressbook));

		// go to the address book view created just now, and search the contact
		sidebar.clickAddressbookName(addressbook);
		contact.searchContactbyKeywords(firstName);

		// go to the detail view of contact 
		contact.clickContactbyName(firstName);
		
		// verify the first name in contact detail view
		Assert.assertTrue(contactDetail.getContactName().contains(firstName));

	}

	public void doTestCleanup() {
		contactUtil.deleteContactFromDetail(false, firstName);
		navigation.clickMenuButton();
		contactUtil.deleteAddressbook(addressbook);
		sidebar.clickCloseButton();
		super.doTestCleanup();
	}
}
