package synchronoss.com.test.contact;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_019_AddContactGroupToAddressbook extends TestCase {
	Random rand = new Random();
	ContactUtils contactUtil;
	private String contactGroup = "contactGroup" + rand.nextInt(10);
	private String expectPopupMsg = "Group name cannot be empty.";

	@Override
	protected void description() {
		Logging.description("Add a contact group to a addressbook, click Cancel button on popup message box, check contact "
				+ "group list, then add name again, click 'x' button to clear the text, and click ok, to check warning "
				+ "message, then add name again, check contact group list");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {
		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);

	}

	@Override
	protected void doTest() {
		// open side bar and add a contact group
		navigation.clickMenuButton();
		sidebar.clickContactGroupEnterButton(ConstantName.DEFAULT);
		sidebar.clickAddContactGroupButton();
		sidebar.addContactGroupName(contactGroup);

		// click cancel button in "input group name" popup to cancel "add contact group" 
		sidebar.clickButtonOnMessageBox(ConstantName.CANCEL);

		// verify the contact group is not added in the contact group list
		Assert.assertFalse(sidebar.getContactGroupList().toString()
				.contains(contactGroup));

		// click add contact group button , and input address book name again
		sidebar.clickAddContactGroupButton();
		sidebar.addContactGroupName(contactGroup);

		// click the 'X' button in "input group name" popup to clear the address book name
		sidebar.clickClearButtonOnMessageBox();

		// click ok button in "input group name" popup
		sidebar.clickButtonOnMessageBox(ConstantName.OK);

		// verify address book name cannot be empty warning popup
		Assert.assertEquals(expectPopupMsg, sidebar.getTextOnMessageBox());
		sidebar.clickButtonOnMessageBox(ConstantName.OK);

		// click add address book button again, add normal address book name, and click OK button
		sidebar.clickAddContactGroupButton();
		sidebar.addContactGroupName(contactGroup);
		sidebar.clickButtonOnMessageBox(ConstantName.OK);

		// verify the address book is sucessfully created
		Assert.assertTrue(sidebar.getContactGroupList().toString()
				.contains(contactGroup));

	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactGroup(contactGroup);
		sidebar.clickBackToAddressbookButton();
		sidebar.clickCloseButton();
		super.doTestCleanup();
	}
}
