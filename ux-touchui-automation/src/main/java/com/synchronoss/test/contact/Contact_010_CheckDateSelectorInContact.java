package synchronoss.com.test.contact;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_010_CheckDateSelectorInContact extends TestCase {
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;

	@Override
	protected void description() {
		Logging.description("Check date selector in contact view, click 'Cancel','Empty','Done' in date select, also change birthday to anniversary");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {

		contactInfo = new ContactInfo();

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {

		// click add contact button, and add last name
		contact.clickAddContactButton();
		contactDetail.addLastname(contactInfo.getLastname());

		// click personal date birthday selector
		contactDetail.clickPersonalDateSelect(ConstantName.BIRTHDAY);
		
		// click done button in date picker popup
		contactDetail.clickDateSelectOptionButton(ConstantName.DONE);

		// click personal date birthday selector again
		contactDetail.clickPersonalDateSelect(ConstantName.BIRTHDAY);
		
		// click empty button in date picker popup
		contactDetail.clickDateSelectOptionButton(ConstantName.EMPTY);

		// click personal date birthday selector again
		contactDetail.clickPersonalDateSelect(ConstantName.BIRTHDAY);
		
		// click cancel button in date picker popup
		contactDetail.clickDateSelectOptionButton(ConstantName.CANCEL);

		// click personal date birthday selector, and click done, and save the contact
		contactDetail.clickPersonalDateSelect(ConstantName.BIRTHDAY);
		contactDetail.clickDateSelectOptionButton(ConstantName.DONE);
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);

		// search the contact by last name and go to detail view
		contact.searchContactbyKeywords(contactInfo.getLastname());
		contact.clickContactbyName(contactInfo.getLastname());

		// verify the birthday of today saved in contact detail view
		Assert.assertTrue(contactDetail
				.getContactSectionListValue(ConstantName.PERSONAL).toString()
				.contains(ConstantName.BIRTHDAY));

	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
