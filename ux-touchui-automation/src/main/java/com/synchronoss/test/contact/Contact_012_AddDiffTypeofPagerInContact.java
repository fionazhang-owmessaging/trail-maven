package synchronoss.com.test.contact;

import java.util.ArrayList;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import synchronoss.com.component.ContactItem;
import synchronoss.com.component.ContactPagerField;
import synchronoss.com.core.Logging;
import synchronoss.com.testcase.utils.ConstantName;
import synchronoss.com.testcase.utils.ContactInfo;
import synchronoss.com.testcase.utils.ContactUtils;
import synchronoss.com.testcase.utils.TestCase;

public class Contact_012_AddDiffTypeofPagerInContact extends TestCase {
	Random rand = new Random();
	private ContactInfo contactInfo;
	private ContactUtils contactUtil;
	private ContactPagerField pagerField;
	private ContactItem item1;
	private ContactItem item2;
	private ContactItem item3;
	private String pagerData1 = "123456" + rand.nextInt(10000);
	private String pagerData2 = "086999" + rand.nextInt(10000);
	private String pagerData3 = "0104523" + rand.nextInt(10000);
	private String firstName = "savePager" + rand.nextInt(100);

	@Override
	protected void description() {
		Logging.description("Add three types of pager to a contact");
	}

	@Test(groups = { "contact", "sanity" })
	public void run() {
		super.run();
	}

	public void doTestSetup() {

		pagerField = new ContactPagerField();
		item1 = new ContactItem();
		item1.setType(ConstantName.HOME);
		item1.setValue(pagerData1);
		item2 = new ContactItem();
		item2.setType(ConstantName.WORK);
		item2.setValue(pagerData2);
		item3 = new ContactItem();
		item3.setType(ConstantName.OTHER);
		item3.setValue(pagerData3);

		ArrayList<ContactItem> items = new ArrayList<ContactItem>();
		items.add(item1);
		items.add(item2);
		items.add(item3);

		contactInfo = new ContactInfo(items, pagerField);

		super.doTestSetup();
		navigation.clickNavigationButton(ConstantName.CONTACTTAB);
	}

	@Override
	protected void doTest() {
		// click add contact button, and add first name
		contact.clickAddContactButton();
		contactDetail.addFirstname(firstName);
		
		// click "add field" button
		contactDetail.clickAddFieldbutton();
		
		// select pager with home option
		contactDetail.clickAddFieldLabel(ConstantName.PAGER,
				ConstantName.HOME.toLowerCase());

		// click "+" button twice in pager field
		// click the "home" option on left, select "work" in "select label" view
		// click the "home" option on left again, select "other" in "select label" view
		// save the contact
		contactDetail.addFieldValue(contactInfo.getPager(), contactInfo
				.getPager().getComponentName());
		contactDetail.clickSaveOrCancelButton(ConstantName.SAVE);
		
		// search the contact, and go to detail view
		contact.searchContactbyKeywords(firstName);
		contact.clickContactbyName(firstName);
		
		// verify the contact detail view contain pager label of home/work/others with correct pager number
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.PAGER).contains(
				ConstantName.HOME + ":\n" + pagerData1));
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.PAGER).contains(
				ConstantName.WORK + ":\n" + pagerData2));
		Assert.assertTrue(contactDetail.getContactSectionListValue(
				ConstantName.PAGER).contains(
				ConstantName.OTHER + ":\n" + pagerData3));
	}

	public void doTestCleanup() {
		contactUtil = new ContactUtils();
		contactUtil.deleteContactFromDetail(false, contactInfo.getFirstname());
		super.doTestCleanup();
	}
}
