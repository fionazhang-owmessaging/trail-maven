package synchronoss.com.pageobject.core;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import synchronoss.com.component.ContactField;
import synchronoss.com.component.ContactItem;
import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;

public class CoreContactDetail extends PageObjectBase {

	/**
	 * Click the back to contact list button in contact detail view
	 *
	 * @author Fiona
	 */
	public void clickBackButton() {
		String back_to_contact_list_button = PropertyHelper.coreContactDetail
				.getPropertyValue("back_to_contact_list_button");
		try {
			WebElement backToContactListElment = this.getWebElement(
					back_to_contact_list_button, false);
			this.clickElement(backToContactListElment);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the delete button in contact detail view
	 *
	 * @author Fiona
	 */
	public void clickDeleteButton() {
		Logging.info("Click delete button in contact detail view ");
		String delete_contact_button = PropertyHelper.coreContactDetail
				.getPropertyValue("delete_contact_button");
		try {
			WebElement deleteContactElement = this.getWebElement(
					delete_contact_button, false);
			this.clickElement(deleteContactElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the edit button in contact detail view
	 *
	 * @author Fiona
	 */
	public void clickEditButton() {
		Logging.info("Click edit button in contact detail view ");
		String edit_contact_button = PropertyHelper.coreContactDetail
				.getPropertyValue("edit_contact_button");
		try {
			WebElement editContactElement = this.getWebElement(
					edit_contact_button, false);
			this.clickElement(editContactElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the more button in contact detail view
	 *
	 * @author Fiona
	 */
	public void clickMoreButton() {
		Logging.info("Click more button in contact detail view ");
		String more_contact_button = PropertyHelper.coreContactDetail
				.getPropertyValue("more_contact_button");
		try {
			WebElement moreContactElement = this.getWebElement(
					more_contact_button, false);
			this.clickElement(moreContactElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Add first name of the contact
	 *
	 * @param firstname
	 *            firstname of the contact
	 * 
	 * @author Jason Song
	 */
	public void addFirstname(String firstname) {
		String firstname_ebox = PropertyHelper.coreContactDetail
				.getPropertyValue("firstname_ebox");
		try {
			WebElement firstnameElement = this.getWebElement(firstname_ebox,
					false);
			firstnameElement.clear();
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
			firstnameElement.sendKeys(firstname.trim());
			}
			else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
				this.elementSendKeys(firstnameElement,firstname);
			}
			firstnameElement.sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("cannot add first name");
			throw ex;
		}
	}

	/**
	 * Add middle name of the contact
	 *
	 * @param middlename
	 *            middle name of the contact
	 * 
	 * @author Jason Song
	 */
	public void addMiddlename(String middlename) {
		String middlename_ebox = PropertyHelper.coreContactDetail
				.getPropertyValue("middlename_ebox");
		try {
			WebElement midnameElement = this.getWebElement(middlename_ebox,
					false);
			midnameElement.clear();
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
			midnameElement.sendKeys(middlename.trim());
			}
			else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
				this.elementSendKeys(midnameElement,middlename);
			}
			midnameElement.sendKeys(Keys.ENTER);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("cannot add middle name");
			throw ex;
		}

	}

	/**
	 * Add last name of the contact
	 *
	 * @param lastname
	 *            lastname of the contact
	 * 
	 * @author Jason Song
	 */
	public void addLastname(String lastname) {
		Logging.info("Add a last name " + lastname +"to contact");
		String lastname_ebox = PropertyHelper.coreContactDetail
				.getPropertyValue("lastname_ebox");
		try {
			WebElement lastnameElement = this.getWebElement(lastname_ebox,
					false);
			lastnameElement.clear();
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				lastnameElement.sendKeys(lastname.trim());
			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				this.elementSendKeys(lastnameElement, lastname);
			}
			lastnameElement.sendKeys(Keys.ENTER);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("cannot add last name");
			throw ex;
		}
	}

	/**
	 * Click the save or cancel button in contact detail view
	 *
	 * @param action
	 *            Save or cancel the contact
	 * 
	 * @author Fiona Zhang
	 */
	public void clickSaveOrCancelButton(String action) {
		Logging.info("Click " + action + " button in contact view");
		String contactAction = null;

		if (action.equalsIgnoreCase("save")) {
			contactAction = PropertyHelper.coreContactDetail
					.getPropertyValue("contact_Save_Btn_addview");
		} else if (action.equalsIgnoreCase("cancel")) {
			contactAction = PropertyHelper.coreContactDetail
					.getPropertyValue("contact_Cancel_Btn_addview");
		}

		try {
			Tools.scrollElementIntoViewByXpath(contactAction);
			WebElement saveContact_actions = this.getWebElement(contactAction,
					false);
			this.clickElement(saveContact_actions);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("cannot click save/cancel button");
			throw ex;
		}
	}

	/**
	 * Add website link in add Contact view
	 *
	 * @param website
	 *            Website url
	 * 
	 * @param type
	 *            Type of url, the value could be "Home" , "Other", "Work"
	 * 
	 * @author Fiona Zhang
	 */
	public void addWebsite(String website, ContactItemType type) {
		String website_ebox = PropertyHelper.coreContactDetail
				.getPropertyValue("website_ebox");
		String scroll_action = PropertyHelper.coreContactDetail
				.getPropertyValue("scroll_action");

		website_ebox = String.format(website_ebox, type.toString()
				.toLowerCase());
		Logging.info("xpath of website : " + website_ebox);

		try {
			WebElement webSiteElement = getWebElement(website_ebox, false);
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			executor.executeScript(scroll_action, webSiteElement);

			webSiteElement.clear();
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
			webSiteElement.sendKeys(website);
			}
			else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
				this.elementSendKeys(webSiteElement,website);
			}
			webSiteElement.sendKeys(Keys.ENTER);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("add fax failed");
			throw ex;
		}
	}

	/**
	 * The function will click plus button in different area
	 * 
	 * @param area
	 *            Area of the field, like Email,Mobile,Phone,Fax,Pager etc.
	 * 
	 * @author Fiona Zhang
	 */
	public void clickPlusButton(String area) {
		Logging.info("Click plus button at " + area
				+ " in add contact detail view");
		String plus_button = PropertyHelper.coreContactDetail
				.getPropertyValue("plus_button");
		plus_button = String.format(plus_button, area);

		try {
			Tools.scrollElementIntoViewByXpath(plus_button);
			WebElement plusButtonElement = getWebElement(plus_button, false);

			this.clickElement(plusButtonElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("click Plus Button failed");
			throw ex;
		}
	}

	/**
	 * Start to add field, according to the component you pass, this function
	 * will find the field, and will click the plus button if your item is more
	 * than one.
	 *
	 * @param t
	 *            Class of Field, like ContactEmailField,ContactPhoneField
	 * 
	 * @param componentName
	 *            Area of the field, like Email,Mobile,Phone,Fax,Pager etc.
	 * 
	 * @author Fiona Zhang
	 */
	public <T extends ContactField> void addFieldValue(T t, String componentName) {
		Logging.info("Start to add field : " + componentName);
		ArrayList<ContactItem> items = t.getItems();
		try {
			int count = items.size();
			// if item number =0 , add item value directly
			if (count == 0)
				this.addItemValue(items.get(0), componentName, 0);
			else {
				// if item number >1 , click plus button to add more field item
				while (count - 1 > 0) {
					this.clickPlusButton(componentName);
					count--;
				}

				for (int i = 0; i < items.size(); i++) {
					this.addItemValue(items.get(i), componentName, i);
				}
			}

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the field label Contact add view
	 *
	 * @param item
	 *            contact item, could get type (Home/Work/Other) , and value
	 *            from it.
	 * 
	 * @param componentName
	 *            Component name , like Email,Mobile,Phone,Fax,Pager
	 * 
	 * @param index
	 *            Index of Item, from 0 to 50
	 * 
	 * @author Fiona Zhang
	 */
	private void addItemValue(ContactItem item, String componentName, int index) {
		String type = item.getType();
		String value = item.getValue();
		String field_value = PropertyHelper.coreContactDetail
				.getPropertyValue("field_value");

		if (index < 0 || index > 50)
			throw new IndexOutOfBoundsException();
		else {
			try {
				if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
					this.clickFieldEditLabel(componentName, index);
					this.clickLabelListType(type);
				}
				field_value = String.format(field_value, componentName);
				Tools.scrollElementIntoViewByXpath(field_value);
				if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
				this.clickFieldEditLabel(componentName, index);
				this.clickLabelListType(type);
					}
				List<WebElement> fieldsElement = this.getWebElements(
						field_value, true);
				fieldsElement.get(index).clear();
				fieldsElement.get(index).sendKeys(value);
				fieldsElement.get(index).sendKeys(Keys.ENTER);
			} catch (WebDriverException ex) {
				ex.printStackTrace();
				Logging.error(" Add item failed");
				throw ex;
			}
		}
	}

	/**
	 * Click the field label in Contact add view, means click "Home/Work/Other"
	 * on the left of the field
	 *
	 * @param area
	 *            Area of the field, like Email,Mobile,Phone,Fax,Pager etc.
	 * 
	 * @param index
	 *            Index of Item, from 0 to 50
	 * 
	 * @author Fiona Zhang
	 */
	public void clickFieldEditLabel(String area, int index) {
		Logging.info("Click lable in field type");
		String field_type = PropertyHelper.coreContactDetail
				.getPropertyValue("field_type");
		String field_area = PropertyHelper.coreContactDetail
				.getPropertyValue("field_area");

		field_type = String.format(field_type, area);
		field_area = String.format(field_area, area);

		try {
			Tools.scrollElementIntoViewByXpath(field_area);
			List<WebElement> fieldTypeList = getWebElements(field_type, false);

			this.clickElement(fieldTypeList.get(index));
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("click Field type failed");
			throw ex;
		}
	}

	/**
	 * Click the field label in select label view
	 *
	 * @param name
	 *            the name you want to click in select label view
	 * 
	 * @author Fiona Zhang
	 */
	public void clickLabelListType(String name) {
		Logging.info("Select lable " + name);
		String select_field_label = PropertyHelper.coreContactDetail
				.getPropertyValue("select_field_label");
		select_field_label = String.format(select_field_label, name);
		try {
			WebElement typeLableElement = getWebElement(select_field_label,
					false);
			this.clickElement(typeLableElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("click field label");
			throw ex;
		}

	}

	/**
	 * Click the "Add fields..." button in add contact view
	 * 
	 * @author Jason
	 */
	public void clickAddFieldbutton() {
		Logging.info("Click Add field... button");
		String add_field_button = PropertyHelper.coreContactDetail
				.getPropertyValue("add_field_button");

		try {
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS))
				Tools.scrollElementIntoViewByXpath(add_field_button);
			WebElement addfieldElement = this.getWebElement(add_field_button,
					true);
			if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID))
				Tools.scrollElementIntoViewByXpath(add_field_button);

			this.clickElement(addfieldElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Clicking add field button...");

	}

	/**
	 * Click the add field label in "Add Field" view
	 *
	 * @param name
	 *            the name you want to click in select label view
	 * 
	 * @author Fiona Zhang
	 */
	public void clickAddFieldLabel(String listItem, String type) {
		Logging.info("Select listItem : " + listItem
				+ " in add field view , and type is " + type);
		String add_field_label = PropertyHelper.coreContactDetail
				.getPropertyValue("add_field_label");

		add_field_label = String.format(add_field_label, listItem, type);
		try {
			Tools.scrollElementIntoViewByXpath(add_field_label);
			WebElement typeLableElement = getWebElement(add_field_label, false);
			this.clickElement(typeLableElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("click add field label failed");
			throw ex;
		}

	}

	/**
	 * Click the add field label in "Add Field" view
	 *
	 * @param area
	 *            area of personal, like birthday/anniversary
	 * 
	 * @author Fiona Zhang
	 */
	public void clickPersonalDateSelect(String area) {
		Logging.info("Click " + area + " in personal date select field");
		String personal_ebox = PropertyHelper.coreContactDetail
				.getPropertyValue("personal_ebox");

		personal_ebox = String.format(personal_ebox, area.toLowerCase());
		try {
			Tools.scrollElementIntoViewByXpath(personal_ebox);
			WebElement personalDateElment = getWebElement(personal_ebox, false);
			this.clickElement(personalDateElment);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the button in date select option
	 *
	 * @param buttonName
	 *            button name on date select , like 'Cancel','Empty','Done'
	 * 
	 * @author Fiona Zhang
	 */
	public void clickDateSelectOptionButton(String buttonName) {
		Logging.info("Click " + buttonName + " on date select view");
		String date_selector_button = PropertyHelper.coreContactDetail
				.getPropertyValue("date_selector_button");

		date_selector_button = String.format(date_selector_button,
				toCapitlize(buttonName));
		try {
			WebElement selectorButtonElement = getWebElement(
					date_selector_button, false);
			this.clickElement(selectorButtonElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click user define option in more pop up list
	 *
	 * @param option
	 * 
	 * 
	 * @author Fiona Zhang
	 */

	public void clickPopupMenuLabel(String option) {
		Logging.info("Click " + option + " option in pop up menu ");
		String action = null;
		boolean waitMask = false;
		switch (option.toLowerCase()) {
		case "group":
			action = PropertyHelper.coreContactDetail
					.getPropertyValue("popup_menu_add_to_group");
			break;
		case "mail":
			action = PropertyHelper.coreContactDetail
					.getPropertyValue("popup_menu_send_mail");
			break;
		case "event":
			action = PropertyHelper.coreContactDetail
					.getPropertyValue("popup_menu_invite_event");
			break;
		case "move":
			action = PropertyHelper.coreContactDetail
					.getPropertyValue("popup_menu_move_to_addr_book");
			break;

		}

		try {
			WebElement toolbarIconElement = getWebElement(action, false);
			this.clickElement(toolbarIconElement);
			if (waitMask)
				waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}

	/**
	 * Click address book in popup list
	 *
	 * @param addressbook
	 *            name of addresss book
	 * 
	 * @author Fiona Zhang
	 */
	public void clickAddressbookSelectPopup(String addressbook) {
		Logging.info("Click " + addressbook
				+ " in address book select popup view");
		String move_to_addressbook_popup ;
		if(addressbook.equals(ConstantName.DEFAULT))
			move_to_addressbook_popup = PropertyHelper.coreContactDetail
			.getPropertyValue("move_to_addressbook_popup_default");

		else{
			move_to_addressbook_popup = PropertyHelper.coreContactDetail
				.getPropertyValue("move_to_addressbook_popup");

		move_to_addressbook_popup = String.format(move_to_addressbook_popup,
			addressbook);
		}
		try {
			WebElement addressbookSelectElement = getWebElement(
					move_to_addressbook_popup, false);
			this.clickElement(addressbookSelectElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click contact group in popup list
	 *
	 * @param contactGroup
	 *            name of contact group
	 * 
	 * @author Fiona Zhang
	 */
	public void clickContactGroupNameInSelectGroup(String contactGroup) {
		Logging.info("Click " + contactGroup + " in contact group select view");
		String contact_group_select = PropertyHelper.coreContactDetail
				.getPropertyValue("contact_group_select");

		contact_group_select = String
				.format(contact_group_select, contactGroup);
		try {
		  Tools.scrollElementIntoViewByXpath(contact_group_select);
			WebElement groupSelectElement = getWebElement(contact_group_select,
					false);
			this.clickElement(groupSelectElement);
			Tools.waitFor(2, TimeUnit.SECONDS);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click cancel button in contact group select view
	 *
	 * @author Fiona Zhang
	 */
	public void clickContactGroupNameSelectCancel() {
		Logging.info("Click cancel button in contact group select view");
		String contact_group_select_cancel_button = PropertyHelper.coreContactDetail
				.getPropertyValue("contact_group_select_cancel_button");

		try {
			WebElement groupSelectElement = getWebElement(
					contact_group_select_cancel_button, false);
			this.clickElement(groupSelectElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click email in contact detail view
	 *
	 * @param contactGroup
	 *            name of contact group
	 * 
	 * @author Fiona Zhang
	 */
	public void clickEmailDetail(String email) {
		Logging.info("Click email address in contact detail view");
		String contact_email_detail = PropertyHelper.coreContactDetail
				.getPropertyValue("contact_email_detail");
		contact_email_detail = String.format(contact_email_detail, email);
		try {
			WebElement emailContactElement = getWebElement(
					contact_email_detail, false);
			this.clickElement(emailContactElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Input address of contact, including street, city, state, zip, country
	 *
	 * @param address
	 *            The street, city, state, zip, country of the address
	 * 
	 * @param area
	 *            The area of the field, would be home/work/other
	 * 
	 * @author Fiona Zhang
	 */
	public void addAddress(ArrayList<String> address, String area) {
		Logging.info("Add address " + address.toString() + " to a contact");
		String addr_street_ebox = PropertyHelper.coreContactDetail
				.getPropertyValue("addr_street_ebox");
		String addr_city_ebox = PropertyHelper.coreContactDetail
				.getPropertyValue("addr_city_ebox");
		String addr_state_ebox = PropertyHelper.coreContactDetail
				.getPropertyValue("addr_state_ebox");
		String addr_zip_ebox = PropertyHelper.coreContactDetail
				.getPropertyValue("addr_zip_ebox");
		String addr_country_ebox = PropertyHelper.coreContactDetail
				.getPropertyValue("addr_country_ebox");

		ArrayList<String> addr_input = new ArrayList<String>();
		addr_input.add(addr_street_ebox);
		addr_input.add(addr_city_ebox);
		addr_input.add(addr_state_ebox);
		addr_input.add(addr_zip_ebox);
		addr_input.add(addr_country_ebox);

		try {
			for (int i = 0; i < address.size(); i++) {
				addr_input.set(i,
						String.format(addr_input.get(i), area.toLowerCase()));
				Tools.scrollElementIntoViewByXpath(addr_input.get(i));
				WebElement inputAddElement = getWebElement(addr_input.get(i),
						false);
				inputAddElement.clear();

				if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
				inputAddElement.sendKeys(address.get(i));
				}
				else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
					this.elementSendKeys(inputAddElement,address.get(i));
				}
				inputAddElement.sendKeys(Keys.ENTER);

			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Add notes in contact view
	 *
	 * @param note
	 *            Input contacter notes
	 * 
	 * @author Jason Song
	 */
	public void addNotes(String note) {
		Logging.info("Add note in contact as " + note );
		String note_textarea = PropertyHelper.coreContactDetail
				.getPropertyValue("note_textarea");
		try {
			Tools.scrollElementIntoViewByXpath(note_textarea);
			WebElement addnotes = getWebElement(note_textarea, false);
			addnotes.clear();
			addnotes.sendKeys(note);
			addnotes.sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Add other in contact view
	 *
	 * @param note
	 *            Input other info in Other of contact view
	 * 
	 * @author Jason Song
	 */
	public void addOthers(String other) {

		String scroll_action = PropertyHelper.coreContactDetail
				.getPropertyValue("scroll_action");
		String other_ebox = PropertyHelper.coreContactDetail
				.getPropertyValue("other_ebox");
		String field_area = PropertyHelper.coreContactDetail
				.getPropertyValue("field_area");

		try {
			field_area = String.format(field_area, "Other");
			WebElement fieldElement = getWebElement(field_area, false);
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			executor.executeScript(scroll_action, fieldElement);

			WebElement otherNoteElement = getWebElement(other_ebox, false);
			
			
			otherNoteElement.clear();
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS))
			otherNoteElement.sendKeys(other);
			else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
				this.elementSendKeys(otherNoteElement,other);
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Select personal date for contact
	 *
	 * @param date
	 *            The expected birthday/anniversary date
	 * 
	 * @param position
	 *            Set value in birthday/anniversary area, and the value could be
	 *            birthday/anniversary
	 * 
	 * @author Fiona Zhang
	 */
	public void selectPesonalDate(LocalDate date, String position) {
		Logging.info("select personal " + position + " , and added date : "
				+ date.toString());
		String date_js = null;
		switch (position.toLowerCase()) {

		case "birthday":
			date_js = PropertyHelper.coreContactDetail
					.getPropertyValue("birthday_compose_js");
			break;
		case "anniversary":
			date_js = PropertyHelper.coreContactDetail
					.getPropertyValue("anniversary_compose_js");
			break;

		}
		try {
			selectInDatePicker_ByJS(date, date_js);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("add personal date failed...");
			throw ex;
		}

	}

	/**
	 * Get contact name from detail view
	 *
	 * @author Fiona Zhang
	 */
	public String getContactName() {
		Logging.info("Get the contact name");

		String contact_name = PropertyHelper.coreContactDetail
				.getPropertyValue("contact_name");
		try {
			WebElement typeLableElement = getWebElement(contact_name, false);
			Logging.info("Get text : " + typeLableElement.getText());

			return typeLableElement.getText();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Get contact section list from detail view
	 * 
	 * @param section
	 *            Section name of the detail list , like Email/Fax/Mobile/Phone,
	 *            etc.
	 * 
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getContactSectionListValue(String section) {
		Logging.info("Get the " + section + " list in contact detail view");

		String section_list = PropertyHelper.coreContactDetail
				.getPropertyValue("section_list");
		if (section.equals(ConstantName.CHATADDR))
			section_list = String.format(section_list, section);
		else
			section_list = String.format(section_list, toCapitlize(section));
		ArrayList<String> valueList = new ArrayList<String>();
		try {
			List<WebElement> typeLableElement = getWebElements(section_list,
					false);
			for (WebElement el : typeLableElement) {
				Logging.info("get " + section + " list as : " + el.getText());
				valueList.add(el.getText());
			}
			Logging.info(valueList.toString());
			return valueList;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 *
	 * Get contact name from detail view -- not ready
	 *
	 * @param area
	 *            area of personal date, like birthday/anniversary
	 * 
	 * @author Fiona Zhang
	 */
	public String getPersonalDateVale(String area) {
		Logging.info("Get the personal date value by js");

		String date_compose = PropertyHelper.coreContactDetail
				.getPropertyValue("date_compose");
		date_compose = String.format(date_compose, area.toLowerCase());

		try {
			Logging.info(date_compose);
			WebElement typeLableElement = getWebElement(date_compose, false);

			JavascriptExecutor setDate = (JavascriptExecutor) Base.base
					.getDriver();
			Logging.info("return document.getElementById('"
					+ typeLableElement.getAttribute("id") + "').value");
			String s = (String) setDate
					.executeScript("return window.document.getElementById('"
							+ typeLableElement.getAttribute("id") + "').value");
			Logging.info(s);
			return s;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 *
	 * Get contact detail view title
	 * 
	 * @author Fiona Zhang
	 */
	public String getContactDetailViewTitle() {
		Logging.info("Get the contact detail title");
		String contact_detail_view_title = PropertyHelper.coreContactDetail
				.getPropertyValue("contact_detail_view_title");
		try {
			WebElement contactDetailTitleElement = getWebElement(
					contact_detail_view_title, false);
			Logging.info("Get text : " + contactDetailTitleElement.getText()
					+ " in contact detail view");

			return contactDetailTitleElement.getText();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

}
