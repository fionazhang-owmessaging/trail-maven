package synchronoss.com.pageobject.core;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class CoreEmailDetail extends PageObjectBase {
	private int threadPageSize = 5; // get "tuiThreadPageSize" value of the count from client.js

  /**
   * Click the tool bar
   *
   * @param iconName The icon on top of the tool bar, such as reply/up/down/delete/create
   *        contact/add to exist contact, etc.
   * 
   * @author Fiona Zhang
   */
  public void clickButtonOnToolBar(String iconName) {

    Logging.info("Click " + iconName + " button on tool bar ");
    String action = null;
    boolean waitLoadingPage = false;
    switch (iconName.toLowerCase()) {
      case ConstantName.BACK:
        if (!Base.deviceName.toLowerCase().contains(ConstantName.IPAD)) 
        action = PropertyHelper.coreEmailDetail.getPropertyValue("tool_bar_back");
        break;
      case ConstantName.UP: {
        action = PropertyHelper.coreEmailDetail.getPropertyValue("tool_bar_up");
        waitLoadingPage = true;
      }
        break;
      case ConstantName.DOWN: {
        action = PropertyHelper.coreEmailDetail.getPropertyValue("tool_bar_down");
        waitLoadingPage = true;
      }
        break;
      case ConstantName.REPLY:
        action = PropertyHelper.coreEmailDetail.getPropertyValue("tool_bar_reply");
        break;
      case ConstantName.DELETE: {
        action = PropertyHelper.coreEmailDetail.getPropertyValue("tool_bar_delete");
        waitLoadingPage = true;
      }
        break;
      case ConstantName.MOVE:
        action = PropertyHelper.coreEmailDetail.getPropertyValue("tool_bar_move");
        break;
      case ConstantName.MORE:
        action = PropertyHelper.coreEmailDetail.getPropertyValue("tool_bar_more");
        break;
      case ConstantName.MAIL: {
        if (Base.deviceName.toLowerCase().contains(ConstantName.IPAD)) {
          action = PropertyHelper.coreEmailDetail.getPropertyValue("ipad_tool_bar_compose");
        } else
          action = PropertyHelper.coreEmailDetail.getPropertyValue("tool_bar_compose");
      }
        break;
    }
    if(action!=null){
      try {
        WebElement toolbarIconElement = getWebElement(action, false);
        this.clickElement(toolbarIconElement);
        if (waitLoadingPage)
          waitForLoadMaskDismissed();
      } catch (WebDriverException ex) {
        ex.printStackTrace();
        throw ex;
    }
    }
  }

	/**
	 * Click the header view
	 *
	 * @param iconName
	 *            The name on header view, such as flag/reply
	 * 
	 * @author Fiona Zhang
	 */
	public void clickButtonOnHeaderView(String iconName) {

		Logging.info("Click " + iconName + " button on header view ");
		boolean waitForLoadingMask = false;
		String action = null;
		switch (iconName.toLowerCase()) {
		case "flag": {
			action = PropertyHelper.coreEmailDetail
					.getPropertyValue("header_view_flag");
			waitForLoadingMask = true;
		}
			break;
		}

		try {
			WebElement toolbarIconElement = getWebElement(action, false);
			 this.clickElement(toolbarIconElement);
			if (waitForLoadingMask)
				this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}
	
	/**
	 * Click the reply button of conversation thread
	 *
	 * @author Fiona Zhang
	 */
	public void clickReplyButtonOfConversationThread() {
		Logging.info("Click the reply button to show up the popup in conversation thread view");
		String mail_conversation_detail_reply_button = PropertyHelper.coreEmailDetail
				.getPropertyValue("mail_conversation_detail_reply_button");
		try {
			WebElement replyButtonElement = getWebElement(
					mail_conversation_detail_reply_button, false);
			this.clickElement(replyButtonElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}

  /**
   * Select a option of pop up menu
   *
   * @param option The name of option, such as
   *        reply/replyall/forward/forwardas/createcontact/addtoexist
   * 
   * @author Fiona Zhang
   */
  public void clickPopupMenuLabel(String option) {

    Logging.info("Click " + option + " option in pop up menu ");
    String action = null;
    String xpath = null;
    String ipadPrefix = "ipad_";
    boolean waitMask = false;
    switch (option.toLowerCase()) {
      case ConstantName.REPLY:
        xpath = "popup_menu_reply";
        break;
      case ConstantName.REPLY_ALL:
        xpath = "popup_menu_reply_all";
        break;
      case ConstantName.FORWARD:
        xpath = "popup_menu_forward";
        break;
      case ConstantName.FORWARD_AS:
        xpath = "popup_menu_forwar_as_attach";
        break;
      case ConstantName.CREATEC_CONTACT:
        xpath = "popup_menu_create_new_contact";
        break;
      case ConstantName.ADD_TO_EXIST:
        xpath = "popup_menu_add_to_exist_contact";
        break;
      case ConstantName.SPAM: {
        xpath = "popup_menu_mark_as_spam";
        waitMask = true;
      }
        break;
      case ConstantName.NOTSPAM: {
        xpath = "popup_menu_mark_as_not_spam";
        waitMask = true;
      }
        break;
      case ConstantName.READ: {
        xpath = "popup_menu_mark_as_read";
        waitMask = true;
      }
        break;
      case ConstantName.UNREAD: {
        xpath = "popup_menu_mark_as_unread";
        waitMask = true;
      }
        break;
      case ConstantName.FLAG: {
        xpath = "popup_menu_flag_to_follow_up";
        waitMask = true;
      }
        break;
      case ConstantName.UNFLAG: {
        xpath = "popup_menu_clear_flag";
        waitMask = true;
      }
        break;
    }
    if (Base.deviceName.toLowerCase().contains(ConstantName.IPAD))
      action = PropertyHelper.coreEmailDetail.getPropertyValue(ipadPrefix + xpath);
    else
      action = PropertyHelper.coreEmailDetail.getPropertyValue(xpath);

    try {
      WebElement toolbarIconElement = getWebElement(action, false);
      this.clickElement(toolbarIconElement);
      if (waitMask)
        waitForLoadMaskDismissed();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;

    }
  }



	/**
	 * Select a option of floating pop up menu
	 *
	 * @param option
	 *            The name of option, such as
	 *            reply/replyall/forward/forwardas/flag/unflag/read/unread
	 * 
	 * @author Fiona Zhang
	 */
	public void clickFloatPopupMenu(String option) {

		Logging.info("Click " + option + " option in floating pop up menu ");
		String action = null;
		boolean waitMask = false;
		switch (option.toLowerCase()) {
		case ConstantName.REPLY:
			action = PropertyHelper.coreEmailDetail
					.getPropertyValue("conversation_popup_reply");
			break;
		case ConstantName.REPLY_ALL:
			action = PropertyHelper.coreEmailDetail
					.getPropertyValue("conversation_popup_reply_all");
			break;
		case ConstantName.FORWARD:
			action = PropertyHelper.coreEmailDetail
					.getPropertyValue("conversation_popup_forward");
			break;
		case ConstantName.FORWARD_AS:
			action = PropertyHelper.coreEmailDetail
					.getPropertyValue("conversation_popup_forward_as_attachment");
			break;
		case ConstantName.READ:
			action = PropertyHelper.coreEmailDetail
					.getPropertyValue("conversation_popup_read");
			waitMask = true;
			break;
		case ConstantName.UNREAD:
			action = PropertyHelper.coreEmailDetail
					.getPropertyValue("conversation_popup_unread");
			waitMask = true;
			break;
		case ConstantName.FLAG:
			action = PropertyHelper.coreEmailDetail
					.getPropertyValue("conversation_popup_flag");
			waitMask = true;
			break;
		case ConstantName.UNFLAG:
			action = PropertyHelper.coreEmailDetail
					.getPropertyValue("conversation_popup_flag");
			waitMask = true;
			break;

		}

		try {
			Logging.info("xpath of action is : --- "+action);
			Tools.scrollElementIntoViewByXpath(action);
		//	Tools.waitFor(2, TimeUnit.SECONDS);
			WebElement toolbarIconElement = getWebElement(action, true);
			 this.clickElement(toolbarIconElement);
			if (waitMask)
				waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}

	
	
	
	
	/**
	 * Click the mail person count in detail view
	 * 
	 * @author Fiona Zhang
	 */
	public void clickMailPersonCount() {
	    Logging.info("Click the person count in mail detail view");
		String header_view_mail_person = PropertyHelper.coreEmailDetail
				.getPropertyValue("header_view_mail_person");
		try {
			WebElement mailPersonCountElement = getWebElement(
					header_view_mail_person, true);
			 this.clickElement(mailPersonCountElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click confim pop up button
	 *
	 * @param option
	 *            action button
	 * 
	 * @author Fiona Zhang
	 */
	public void ClickConfirmMsgButton(String action) {
		try {
			clickButtonOnMessageBox(action);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Select folder name to move email
	 *
	 * @param option
	 *            The name of folder
	 * 
	 * @author Fiona Zhang
	 */
	public void clickFolderName(String folderName) {
		Logging.info("Select " + folderName + " folder name to move email ");
		String tool_bar_move_folder_name = PropertyHelper.coreEmailDetail
				.getPropertyValue("tool_bar_move_folder_name");
		tool_bar_move_folder_name = String.format(tool_bar_move_folder_name,
				folderName);
		try {
			Tools.scrollElementIntoViewByXpath(tool_bar_move_folder_name);
			WebElement folderNameElement = getWebElement(
					tool_bar_move_folder_name, false);
			 this.clickElement(folderNameElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Select msg detail toggle
	 * 
	 * @author Fiona Zhang
	 */
	public void clickMsgDetailToggle() {
		Logging.info("Click the show/hide detail button in msg header view");
		String msg_header_detail_toggle = PropertyHelper.coreEmailDetail
				.getPropertyValue("msg_header_detail_toggle");
		try {
			Tools.scrollElementIntoViewByXpath(msg_header_detail_toggle);
			WebElement detailToggleElement = getWebElement(
					msg_header_detail_toggle, false);
			 this.clickElement(detailToggleElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	

	/**
	 * Click the mail recipient in mail detail view
	 * 
	 * @param contact
	 * 		The contact name in msg detail view
	 * 
	 * @author Fiona Zhang
	 */
	public void clickMailRecipient(String contact) {
		Logging.info("Click receipient " + contact + " in email detail view");
		String header_view_recepient = PropertyHelper.coreEmailDetail
				.getPropertyValue("header_view_recepient");
		header_view_recepient = String.format(header_view_recepient, contact);
		try {
			Tools.scrollElementIntoViewByXpath(header_view_recepient);
			WebElement detailToggleElement = getWebElement(
					header_view_recepient, false);
			 this.clickElement(detailToggleElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the event invitation update button
	 *
	 * @param updateDecision
	 *            event decision in email detail view, like Yes/Maybe/No
	 * 
	 * @author Fiona Zhang
	 */
	public void clickEventInvitationStatusButton(String updateDecision) {
		Logging.info("Click event update button with " + updateDecision);
		String descsion = null;

		switch (updateDecision.toLowerCase()) {
		case "accepted":
			descsion = PropertyHelper.coreEmailDetail
					.getPropertyValue("msg_event_descision_yes");
			break;
		case "declined":
			descsion = PropertyHelper.coreEmailDetail
					.getPropertyValue("msg_event_descision_no");
			break;
		case "tentative":
			descsion = PropertyHelper.coreEmailDetail
					.getPropertyValue("msg_event_descision_maybe");
			break;
		}
		try {
			Tools.scrollElementIntoViewByXpath(descsion);
			WebElement descisionButton = getWebElement(descsion, true);
			 this.clickElement(descisionButton);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}

	/**
	 * Click the name of attachment
	 *
	 * @param name
	 *            The name of the attachment
	 * 
	 * @author Fiona Zhang
	 */
	public void clickAttachmentByName(String name) {
		Logging.info("Click the attachment of name: " + name);
		String msg_attachment_list_item = PropertyHelper.coreEmailDetail
				.getPropertyValue("msg_attachment_list_item");
		msg_attachment_list_item = String
				.format(msg_attachment_list_item, name);
		try {
			Tools.scrollElementIntoViewByXpath(msg_attachment_list_item);
			WebElement attachmentItemElement = getWebElement(
					msg_attachment_list_item, false);
			 this.clickElement(attachmentItemElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the close button in preview container
	 * 
	 * @author Fiona Zhang
	 */
	public void clickCloseButtonInPreviewContainer() {
		Logging.info("Click the close button in preview container: ");
		String attachment_preview_close_button = PropertyHelper.coreEmailDetail
				.getPropertyValue("attachment_preview_close_button");
		try {
			Tools.scrollElementIntoViewByXpath(attachment_preview_close_button);
			WebElement attachmentItemElement = getWebElement(
					attachment_preview_close_button, false);
			 this.clickElement(attachmentItemElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	
	/**
	 * Click the load more button in conversation view
	 * 
	 * @author Fiona Zhang
	 */
	public void clickLoadMoreConversationButton() {
		Logging.info("Click load more conversation view");
		String conversation_msg_load_more = PropertyHelper.coreEmailDetail
				.getPropertyValue("conversation_msg_load_more");
		try {
			Tools.scrollElementIntoViewByXpath(conversation_msg_load_more);
			WebElement loadMoreConversationElement = getWebElement(
					conversation_msg_load_more, false);
			 this.clickElement(loadMoreConversationElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}		
	}

	
  /**
   * Click the edit button in draft detail view
   * 
   * @author Fiona Zhang
   */
  public void clickEditButtonInDraftDetail() {
    String draft_view_edit_button =
        PropertyHelper.coreEmailDetail.getPropertyValue("draft_view_edit_button");
    Logging.info("Click edit button in draft detail view");
    try {
      Tools.scrollElementIntoViewByXpath(draft_view_edit_button);
      WebElement editButtonElement = getWebElement(draft_view_edit_button, true);
      editButtonElement.click();
      waitForLoadMaskDismissed();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }

  }
  
  
  /**
   * Click the mail detail header toggle to display or hide recipient info
   * 
   * @param headerToggle - Show details or hide details
   * 
   * @author Fiona Zhang
   */
  public void clickMailHeaderShowOrHideDetail(String headerToggle) {

    String header_view_show_or_hide_toggle =
        PropertyHelper.coreEmailDetail.getPropertyValue("header_view_show_or_hide_toggle");
    Logging.info("Click mail detail header to " + headerToggle + " recipient detail");
    try {
      Tools.scrollElementIntoViewByXpath(header_view_show_or_hide_toggle);
      WebElement headerButton = getWebElement(header_view_show_or_hide_toggle, true);
      Logging.info("headerToggle value is " + headerButton.getText());
      if (!headerButton.getText().equals(headerToggle))
        headerButton.click();
      waitForLoadMaskDismissed();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

  
	/**
	 * Get email message body context in mail detail view
	 * 
	 * @author Fiona Zhang
	 */
	public String getMailBody() {
		String mailBodyMsg = null;
		String msg_body = PropertyHelper.coreEmailDetail
				.getPropertyValue("msg_body");
		try {
			WebElement msgBodyElement = getWebElement(msg_body, false);
			mailBodyMsg = msgBodyElement.getText();
			Logging.info("Get mail body message as : " + mailBodyMsg);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return mailBodyMsg;
	}

	/**
	 * Get email message subject context in mail detail view
	 * 
	 * @author Fiona Zhang
	 */
	public String getMailSubject() {
		String mailSubject = null;
		String msg_subj = PropertyHelper.coreEmailDetail
				.getPropertyValue("msg_subj");
		try {
			WebElement msgBodyElement = getWebElement(msg_subj, false);
			mailSubject = msgBodyElement.getText();
			Logging.info("Get mail subject message : " + mailSubject);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return mailSubject;
	}

	/**
	 * Get email message from value in mail detail view
	 * 
	 * @author Fiona Zhang
	 */
	public String getMailFromField() {
		String mailFrom = null;
		String header_view_from = PropertyHelper.coreEmailDetail
				.getPropertyValue("header_view_from");
		try {
			WebElement msgBodyElement = getWebElement(header_view_from, false);
			mailFrom = msgBodyElement.getText();
			Logging.info("Get mail from field : " + mailFrom);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return mailFrom;
	}

	/**
	 * Get attachment icon in mail detail view
	 * 
	 * @author Fiona Zhang
	 */
	public boolean getAttachmentIcon() {
		String msg_attachment = PropertyHelper.coreEmailDetail
				.getPropertyValue("header_view_attachment");
		try {
			WebElement msgBodyElement = getWebElement(msg_attachment, false);
			return msgBodyElement.isDisplayed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get status of previous button
	 * 
	 * @author Fiona Zhang
	 * 
	 * @return true - the button is disable, false - the button is enable
	 */
	public boolean isPreviosButtonStatusDisabled() {
		String tool_bar_up = PropertyHelper.coreEmailDetail
				.getPropertyValue("tool_bar_up");
		boolean result = false;
		try {
			WebElement upArrowButton = getWebElement(tool_bar_up, false);
			result = upArrowButton.getAttribute("class").contains("disabled");
			Logging.info("Get preivous button status as " + result);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return result;
	}

	/**
	 * Get mail receive time
	 * 
	 * @author Fiona Zhang
	 * 
	 * @return String - receive time of email
	 */
	public String getMailReceivedTime() {
		String hedar_view_receive_time = PropertyHelper.coreEmailDetail
				.getPropertyValue("hedar_view_receive_time");
		String receiveTime = null;
		try {
			WebElement receivieTimeElement = getWebElement(
					hedar_view_receive_time, false);
			receiveTime = receivieTimeElement.getText();
			Logging.info("Get mail receive time in detail view as "
					+ receiveTime);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return receiveTime;
	}

	/**
	 * Get email message body context in mail detail view
	 * 
	 * @author Fiona Zhang
	 */
	public String getMailBodyInPreviewContainter() {
		String mailBodyMsg = null;
		String attachment_preview_msg_body = PropertyHelper.coreEmailDetail
				.getPropertyValue("attachment_preview_msg_body");
		try {
			WebElement msgBodyElement = getWebElement(
					attachment_preview_msg_body, true);
			mailBodyMsg = msgBodyElement.getText();
			Logging.info("Get mail body message as : " + mailBodyMsg);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return mailBodyMsg;

	}

  /**
   * Get email header message info in mail detail view
   * 
   * @return ArrayList<String> - contains all message header info 
   * 
   * @author Fiona Zhang
   */
  public ArrayList<String> getMessageHeaderInfo() {
    ArrayList<String> mailHeaderInfo = new ArrayList<String>();
    String msg_header_info = PropertyHelper.coreEmailDetail.getPropertyValue("msg_header_info");
    try {
      List<WebElement> msgHeaderInfoElement = getWebElements(msg_header_info, true);
      for (int i = 0; i < msgHeaderInfoElement.size(); i++) {
        mailHeaderInfo.add(msgHeaderInfoElement.get(i).getText());
      }
      Logging.info("Get mail header message as : " + mailHeaderInfo.toString());

    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    return mailHeaderInfo;
  }

	/**
	 * Get email body of conversation view
	 * 
	 * @author Fiona Zhang
	 */
	public String getMailBodyOfConversationView() {
		String conversationViewMsgBody = null;
		String conversation_msg_body = PropertyHelper.coreEmailDetail
				.getPropertyValue("conversation_msg_body");
		try {
			WebElement conversationMsgBodyElement = getWebElement(conversation_msg_body,
					true);
			conversationViewMsgBody = conversationMsgBodyElement.getText();
			Logging.info("Get conversation view mail body message as : " + conversationViewMsgBody);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return conversationViewMsgBody;
	}

	
	/**
	 * Get flag number in conversation view
	 * 
	 * @author Fiona Zhang
	 */
	public int getFlagNumberInConversationView(int threadNumber) {
		String conversation_msg_flag = PropertyHelper.coreEmailDetail
				.getPropertyValue("conversation_msg_flag");
		int result = 0;
		int clickLoadButtonCount = threadNumber / threadPageSize;
		Logging.info("Click load more button : " + clickLoadButtonCount +" times");
		try {
			while (clickLoadButtonCount > 0) {
				this.clickLoadMoreConversationButton();
				clickLoadButtonCount--;
			}
			List<WebElement> conversationMsgBodyElement = getWebElements(
					conversation_msg_flag, true);
			result = conversationMsgBodyElement.size();

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		
		Logging.info("Get flag number as " + result);
		return result;
	}

	   
    /**
     * Get conversation view expand mail number
     * 
     * @author Fiona Zhang
     */ 
	public int getConversationViewExpandedMail() {
		int result = 0;
		String mail_conversation_single_detail = PropertyHelper.coreEmailDetail
				.getPropertyValue("mail_conversation_single_detail");
		String mail_conversation_detail_list = PropertyHelper.coreEmailDetail
				.getPropertyValue("mail_conversation_detail_list");

		try {
			List<WebElement> conversationMsgBodyElement = getWebElements(
					mail_conversation_single_detail, true);
			List<WebElement> conversationMsgListElement = getWebElements(
					mail_conversation_detail_list, true);
			Logging.info("the count for open thread : " + conversationMsgBodyElement.size());
			if (conversationMsgBodyElement.size() != 0) {
				for (int i = 0; i < conversationMsgListElement.size(); i++) {
					if (conversationMsgListElement
							.get(i)
							.getAttribute("id")
							.equals(conversationMsgBodyElement.get(0)
									.getAttribute("id"))) {
						result = i + 1;
						break;
					}
				}

			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

		Logging.info("the result of conversation view number is " + result);
		return result;
	}

	
    /**
     * Click the conversation view mail count
     * @param conversationviewThread - the number of the conversation view, start from 1
     * 
     * @author Fiona Zhang
     */	
	
	public void clickConversationViewMail(int conversationviewThread) {
		String mail_conversation_detail_list_header = PropertyHelper.coreEmailDetail
				.getPropertyValue("mail_conversation_detail_list_header");
        String mail_converstion_detail_photo =
                PropertyHelper.coreEmailDetail.getPropertyValue("mail_converstion_detail_photo");
        
        mail_converstion_detail_photo = String.format(mail_converstion_detail_photo,
            conversationviewThread);
		Logging.info("Click the conversation view mail detail "
				+ conversationviewThread);
		try {

		    List<WebElement> conversationMsgPhotoListElement = getWebElements(mail_converstion_detail_photo,true);
		    if(conversationMsgPhotoListElement.size()==1){
		    conversationMsgPhotoListElement.get(0).click();
		    }
		    else{
	        Tools.scrollElementIntoViewByXpath(String.format(
                    mail_conversation_detail_list_header,
                    conversationviewThread));
			WebElement conversationMsgListElement = getWebElement(
					String.format(mail_conversation_detail_list_header,
							conversationviewThread), true);
			conversationMsgListElement.click();
		    }
			Tools.waitFor(5, TimeUnit.SECONDS);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}
	
	
	/**
	 * Get flag number in conversation view
	 * 
	 * @author Fiona Zhang
	 */
	public int getUnreadCountInConversationView(int threadNumber) {
		String mail_conversation_unread_status = PropertyHelper.coreEmailDetail
				.getPropertyValue("mail_conversation_unread_status");
		int result = 0;
		int clickLoadButtonCount = threadNumber / threadPageSize;
		Logging.info("Click load more button : " + clickLoadButtonCount +" times");
		try {
			while (clickLoadButtonCount > 0) {
				this.clickLoadMoreConversationButton();
				clickLoadButtonCount--;
			}
			Logging.info("mail conversation unread status: "+mail_conversation_unread_status);
			List<WebElement> conversationMsgBodyElement = getWebElements(
					mail_conversation_unread_status, true);
			result = conversationMsgBodyElement.size();

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		
		Logging.info("Get unread email number as " + result);
		return result;
	}

  /**
   * Get calendar event count in email detail view
   * 
   * @author Fiona Zhang
   */
  public int getCalendarEventCount() {
    int result = 0;
    String msg_event_list = PropertyHelper.coreEmailDetail.getPropertyValue("msg_event_list");
    try {
      Tools.scrollElementIntoViewByXpath(msg_event_list);
      List<WebElement> msgEventCountElement = getWebElements(msg_event_list, true);
      result = msgEventCountElement.size();
      Logging.info("Get calendar event count as : " + result);

    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    return result;

  }

  /**
   * Get calendar event name in mail detail view
   * 
   * @author Fiona Zhang
   */
  public ArrayList<String> getCalendarEventName() {
    ArrayList<String> nameList = null;
    String msg_event_all_list =
        PropertyHelper.coreEmailDetail.getPropertyValue("msg_event_all_list");
    try {
      Tools.scrollElementIntoViewByXpath(msg_event_all_list);
      List<WebElement> msgEventCountElement = getWebElements(msg_event_all_list, true);
      nameList = new ArrayList<String>();
      for (int i = 0; i < msgEventCountElement.size(); i++) {
        nameList.add(msgEventCountElement.get(i).getText());
      }
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }

    Logging.info(nameList.toString());
    return nameList;
  }


  /**
   * Get invited event relative top pixel size
   * 
   * @param section - start/end
   * @return int - the relative top pixel
   * @author Fiona Zhang
   */
  public int getInvitedEventRelativeTopPx(String section) {
    int relativeTopPx = 0;
    String top = null;
    String msg_invited_event = PropertyHelper.coreEmailDetail.getPropertyValue("msg_invited_event");
    try {
      Tools.scrollElementIntoViewByXpath(msg_invited_event);
      WebElement msgEventCountElement = getWebElement(msg_invited_event, true);
      String elementStyle = msgEventCountElement.getAttribute("style");
      Logging.info(elementStyle);
      top = elementStyle.substring(elementStyle.indexOf(section));
      Logging.info(top);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    } catch (NumberFormatException ex) {
      ex.printStackTrace();
      throw ex;
    }
    relativeTopPx = Integer.parseInt(top.split(":")[1].split("px")[0].trim());
    Logging.info("The relative pixel to " + section + " is: " + relativeTopPx);
    return relativeTopPx;

  }


  /**
   * Get event shaft show time
   * 
   * @param timeSection - Start/end time
   * @param formatter - date format
   * @param ifTransToLocalDate - if need to transfer the String to LocalTime
   * 
   * @return Object - should be LocalTime or String according to the value you need
   * @author Fiona Zhang
   */
  public Object getEventShaftShowTime(String timeSection, String formatter,
      boolean ifTransToLocalDate) {

    Logging.info("Start to get event shaft show time");
    LocalTime eventTime = null;
    String msg_event_shaft_show_time = null;
    String timeShaft = null;
    if (timeSection.equals(ConstantName.START))
      msg_event_shaft_show_time =
          PropertyHelper.coreEmailDetail.getPropertyValue("msg_event_shaft_start_time");
    else
      msg_event_shaft_show_time =
          PropertyHelper.coreEmailDetail.getPropertyValue("msg_event_shaft_end_time");

    try {
      Tools.scrollElementIntoViewByXpath(msg_event_shaft_show_time);
      WebElement msgEventTimeElement = getWebElement(msg_event_shaft_show_time, true);
      timeShaft = msgEventTimeElement.getText();
      Logging.info(timeShaft);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    if (ifTransToLocalDate) {
      if (formatter.equals(ConstantName.HOUR24FORMAT))
        eventTime = LocalTime.parse(timeShaft);
      else
        eventTime =
            LocalTime.parse(timeShaft,
                DateTimeFormatter.ofPattern("K:mm a").withLocale(Locale.ENGLISH));
      Logging.info("getEventShaftShowTime > as : " + eventTime);
      return eventTime;
    } else
      return timeShaft;
  }



  /**
   * Get event relative pixel value
   * 
   * @param eventName - the name of the event
   * @param section - the section of the event, e.g. ConstantName.TOP, ConstantName.Height
   * 
   * @return int value, the relative pixel of event
   * @author Fiona Zhang
   */
  public int getEventRelativePxValue(String eventName, String section) {
    int relativePixelValue = 0;
    String pixelValue = null;
    String msg_event_new_create_name =
        PropertyHelper.coreEmailDetail.getPropertyValue("msg_event_new_create_name");
    msg_event_new_create_name = String.format(msg_event_new_create_name, eventName);
    try {
      Tools.scrollElementIntoViewByXpath(msg_event_new_create_name);
      WebElement msgEventElement = getWebElement(msg_event_new_create_name, true);
      String elementStyle = msgEventElement.getAttribute("style");
      Logging.info(elementStyle);
      pixelValue = elementStyle.substring(elementStyle.indexOf(section));
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    } catch (NumberFormatException ex) {
      ex.printStackTrace();
      throw ex;
    }
    relativePixelValue = Integer.parseInt(pixelValue.split(":")[1].split("px")[0].trim());
    Logging.info("The relative pixel of the event " + eventName + " to " + section + " is: "
        + relativePixelValue);
    return relativePixelValue;

  }

  /**
   * Get the event invitation date in received email
   * 
   * @return value String, the event invitation date
   * @author Fiona Zhang
   */
  public String getEventInvitationDate() {
    String invitationDate = null;
    String msg_event_invited_date =
        PropertyHelper.coreEmailDetail.getPropertyValue("msg_event_invited_date");
    String msg_event_invited_view_subject =
        PropertyHelper.coreEmailDetail.getPropertyValue("msg_event_invited_view_subject");

    try {
      Tools.scrollElementIntoViewByXpath(msg_event_invited_view_subject);
      WebElement eventInvitationDateElement = getWebElement(msg_event_invited_date, true);
      invitationDate = eventInvitationDateElement.getText();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    Logging.info("Get the event invitation date as : " + invitationDate);
    return invitationDate;
  }

  
  /**
   * Check if edit button displayed or not
   * 
   * @return boolean, if edit button displayed or not
   * @author Fiona Zhang
   */
  public boolean isEditButtonDisplayed() {
    boolean isEditButtonDisplayed = false;
    String draft_view_edit_button =
        PropertyHelper.coreEmailDetail.getPropertyValue("draft_view_edit_button");

    try {
      Tools.scrollElementIntoViewByXpath(draft_view_edit_button);
      WebElement editButtonElement = getWebElement(draft_view_edit_button, true);
      isEditButtonDisplayed = editButtonElement.isDisplayed();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    Logging.info("Check if edit button in draftt view is displayed or not : "
        + isEditButtonDisplayed);

    return isEditButtonDisplayed;
  }




/*	public boolean ifLoadMoreConversation() {
		String conversation_msg_load_more = PropertyHelper.coreEmailDetail
				.getPropertyValue("conversation_msg_load_more");
		boolean result;
		try {
			List<WebElement> conversationMsgBodyElement = getWebElements(conversation_msg_load_more,
					true);
			if(conversationMsgBodyElement.size()==1)
				result = false;
			else
				result = true;

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return false;
	}*/
  
  
  /**
   * Click "Show Image" button in a image
   * 
   * @author Vivian Xie
   */
  public void clickShowImageButton(){
    Logging.info("Starting to click show image button");
    String msg_show_image_button =
        PropertyHelper.coreEmailDetail.getPropertyValue("msg_show_image_button");

    try {
      getWebElement(msg_show_image_button, true).click();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    
    
  }
  

}
