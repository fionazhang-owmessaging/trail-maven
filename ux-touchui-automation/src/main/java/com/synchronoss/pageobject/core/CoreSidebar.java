package synchronoss.com.pageobject.core;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import io.appium.java_client.ios.IOSDriver;
import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class CoreSidebar extends PageObjectBase {

	/**
	 * Empty Trash or Spam folder
	 *
	 * @param folderName
	 *            Spam or Trash
	 * 
	 * @author Jason Song
	 */
	public void clickEmptyFolderIcon(String folderName) {
		Logging.info("Empty the folder: " + folderName);
		String empty_folder_icon = PropertyHelper.coreSidebar
				.getPropertyValue("empty_folder_icon");
		empty_folder_icon = String.format(empty_folder_icon,
				toCapitlize(folderName));
		try {
			WebElement emptyFolderIconElement = this.getWebElement(
					empty_folder_icon, false);
			 this.clickElement(emptyFolderIconElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the system folder name on side bar
	 *
	 * @param folderName
	 *            The system folder name of side bar, Mail/Spam/Trash/Sent, etc.
	 * 
	 * @author Fiona Zhang
	 */
	public void clickSystemFolderName(String folderName) {
		Logging.info("Click the folder " + folderName + " on side bar");
		String folder_name = PropertyHelper.coreSidebar
				.getPropertyValue("folder_name");
		if (folderName.equalsIgnoreCase("My Folders"))
			folder_name = String.format(folder_name, "folder");
		else if (folderName.equalsIgnoreCase("spam"))
			folder_name = String.format(folder_name, "junk");
		else
			folder_name = String.format(folder_name, folderName.toLowerCase());
		try {
			WebElement folderNameElement = this
					.getWebElement(folder_name, true);
			 this.clickElement(folderNameElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the customer folder name on side bar
	 * 
	 * @param folderName
	 *            The customer folder name of side bar
	 * 
	 * @author Fiona Zhang
	 */
	public void clickCustomerFolderName(String customerFolderName) {
		Logging.info("Click the folder " + customerFolderName);
		String customer_folder_name = PropertyHelper.coreSidebar
				.getPropertyValue("customer_folder_name");

		customer_folder_name = String.format(customer_folder_name,
				customerFolderName);

		try {
			WebElement folderNameElement = this.getWebElement(
					customer_folder_name, true);
			 this.clickElement(folderNameElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Trigger add one folder dialog
	 *
	 * @param folderName
	 *            The system folder name of side bar, Mail/Spam/Trash/Sent, etc.
	 * 
	 * @author Jason Song
	 */
	public void clickAddFolderIcon() {
		String add_folder_icon = PropertyHelper.coreSidebar
				.getPropertyValue("add_folder_icon");

		try {
			WebElement addFolderElement = this.getWebElement(add_folder_icon,
					true);
			 this.clickElement(addFolderElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	/**
	 * Click the side bar to dismiss mask
	 *
	 * 
	 * @author Fiona Zhang
	 */
	public void clickMaskOnSidebar() {
		String floating_sidebar = PropertyHelper.coreSidebar
				.getPropertyValue("floating_sidebar");

		try {
			WebElement floatingMask = this.getWebElement(floating_sidebar,
					true);
			this.clickElement(floatingMask);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}		
	}

	/**
	 * Click Cancel or New Folder in edit(move) mail view
	 * 
	 * @param action
	 *            The folder action, cancel or create a new folder
	 * 
	 * @author Fiona Zhang
	 */
	public void clickAddOrCancelFolderButtonInMailEditView(String action) {
		Logging.info(action.toLowerCase() + " mail move to folder action ");
		String folder_Action = null;
		switch (action) {
		case ConstantName.CANCEL:
			folder_Action = PropertyHelper.coreSidebar
					.getPropertyValue("cancel_select_folder_in_edit_mail_view");
			break;
		case ConstantName.NEWFOLDER:
			folder_Action = PropertyHelper.coreSidebar
					.getPropertyValue("add_new_folder_in_edit_mail_view");
			break;
		}

		try {
			WebElement actionElement = this.getWebElement(folder_Action, true);
			 this.clickElement(actionElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Enter a folder name
	 *
	 * @param customFolderName
	 *            Name the folder
	 * 
	 * @author Jason Song
	 */
	public void addFolderName(String customFolderName) {
		Logging.info("Add folder name : " + customFolderName);
		String folder_name_field = PropertyHelper.coreSidebar
				.getPropertyValue("folder_name_field");

		try {
			WebElement folderNameFieldElement = this.getWebElement(
					folder_name_field, false);
			folderNameFieldElement.clear();
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				folderNameFieldElement.sendKeys(customFolderName);
				IOSDriver iosDriver = (IOSDriver) Base.base.getDriver();
				iosDriver.hideKeyboard();
			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				this.elementSendKeys(folderNameFieldElement, customFolderName);
			}
			folderNameFieldElement.sendKeys(Keys.ENTER);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Enter a contact group name
	 *
	 * @param contactGroupName
	 *            Name the contact
	 * 
	 * @author Fiona Zhang
	 */
	public void addContactGroupName(String contactGroupName) {
		Logging.info("Add contact group name : " + contactGroupName);
		String contact_group_name_field = PropertyHelper.coreSidebar
				.getPropertyValue("contact_group_name_field");

		try {
			WebElement contactGroupNameElement = this.getWebElement(
					contact_group_name_field, false);
			contactGroupNameElement.clear();
				if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
					contactGroupNameElement.sendKeys(contactGroupName);
					contactGroupNameElement.sendKeys(Keys.ENTER);
				IOSDriver iosDriver = (IOSDriver) Base.base.getDriver();
				iosDriver.hideKeyboard();
			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				this.elementSendKeys(contactGroupNameElement,contactGroupName);
			}

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Enter a task group name
	 *
	 * @param taskGroupName
	 *            Name the Task group
	 * 
	 * @author Fiona Zhang
	 */
	public void addTaskGroupName(String taskName) {
		Logging.info("Add task group name : " + taskName);
		String task_group_name_input_field = PropertyHelper.coreSidebar
				.getPropertyValue("task_group_name_input_field");

		try {
			WebElement taskGroupNameElement = this.getWebElement(
					task_group_name_input_field, false);
			taskGroupNameElement.clear();
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
				taskGroupNameElement.sendKeys(taskName);
			IOSDriver iosDriver = (IOSDriver) Base.base.getDriver();
			iosDriver.hideKeyboard();
			}
			else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
				this.elementSendKeys(taskGroupNameElement,taskName);
			}
			taskGroupNameElement.sendKeys(Keys.ENTER);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Enter a folder name
	 *
	 * @param customFolderName
	 *            Name the folder
	 * 
	 * @author Fiona Zhang
	 */
	public void clickActionInCreateFolder(String action) {
		String create_folder_action = PropertyHelper.coreSidebar
				.getPropertyValue("create_folder_action");

		if (action.equals(ConstantName.OK))
			create_folder_action = String
					.format(create_folder_action, "action");
		else if (action.equals(ConstantName.CANCEL))
			create_folder_action = String.format(create_folder_action,
					"decline");

		try {
			WebElement actionElement = this.getWebElement(create_folder_action,
					true);
			 this.clickElement(actionElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Switch to folder selection current folder will host
	 *
	 * @param foldername
	 *            Name of the folder
	 * 
	 * @author Fiona Zhang
	 */
	public void selectParentFolder(String foldername) {
		Logging.info("Start to Select Parenet Folder : " + foldername);
		String parent_folder_name = PropertyHelper.coreSidebar
				.getPropertyValue("parent_folder_name");
		String create_folder_form = PropertyHelper.coreSidebar
				.getPropertyValue("create_folder_form");
		parent_folder_name = String.format(parent_folder_name, foldername);

		try {
			WebElement folderCreateForm = getWebElement(create_folder_form,
					true);
			 this.clickElement(folderCreateForm);
			// Tools.scrollElementIntoViewByXpath(parent_folder_name);
			Logging.info("parent_folder_name xpath : " + parent_folder_name);
			Tools.scrollElementIntoViewByXpath(parent_folder_name);
			WebElement fatherFolder = getWebElement(parent_folder_name, true);
			 this.clickElement(fatherFolder); // Find Father folder, and click
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("End to Select Parent Folder");
	}
	
	
	/**
	 * Move folder to parent folder by edit folder
	 *
	 * @param foldername
	 *            Name of the folder
	 * 
	 * @author Fiona Zhang
	 */
	public void moveToParentFolder(String foldername) {
		Logging.info("Start to Select Parenet Folder : " + foldername);
		String parent_folder_name = PropertyHelper.coreSidebar
				.getPropertyValue("parent_folder_name");
		parent_folder_name = String.format(parent_folder_name, foldername);

		try {
			Logging.info("parent_folder_name xpath : " + parent_folder_name);
			Tools.scrollElementIntoViewByXpath(parent_folder_name);
			WebElement fatherFolder = getWebElement(parent_folder_name, true);
			 this.clickElement(fatherFolder); // Find Father folder, and click
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("End to Select Parent Folder");
	}

	/**
	 * Trigger remove and edit form by specified folder name.
	 *
	 * @param foldername
	 *            Name of the folder
	 * 
	 * @author Jason Song
	 */
	public void swipeFolderEditForm(String folderName) {
		Logging.info("swipe to enter edit format of folder");
		String folder_item_name = PropertyHelper.coreSidebar
				.getPropertyValue("folder_item_name");

		folder_item_name = String.format(folder_item_name, folderName);
		try {
			WebElement mailFolderDiv = this.getWebElement(folder_item_name,
					true);
			String folderID = mailFolderDiv.getAttribute("id");

			String folder_edit_js = PropertyHelper.coreSidebar
					.getPropertyValue("folder_edit_form_js");
			folder_edit_js = String.format(folder_edit_js, folderID);
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			executor.executeScript(folder_edit_js);
			Logging.info("Swipe action on current folder and trigger edit/remove pane...");
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the icon in folder name edit view
	 *
	 * @param action
	 *            action of folder name
	 * 
	 * @author Fiona Zhang
	 */
	public void clickFolderNameAction(String action) {
		Logging.info("Start to " + action + " the folder name");
		String actionFolder = null;
		if (action.equalsIgnoreCase("edit")) {
			actionFolder = PropertyHelper.coreSidebar
					.getPropertyValue("folder_edit_icon");
		} else if (action.equalsIgnoreCase("move")) {
			actionFolder = PropertyHelper.coreSidebar
					.getPropertyValue("folder_move_to_icon");

		} else if (action.equalsIgnoreCase("delete")) {
			actionFolder = PropertyHelper.coreSidebar
					.getPropertyValue("folder_delete_icon");

		}
		try {
			WebElement foldereditrm = this.getWebElement(actionFolder, false);
			 this.clickElement(foldereditrm);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the icon to go back to main folder list
	 * 
	 * @author Fiona Zhang
	 */
	public void clickBacktoMainFolderlist() {
		Logging.info("Click back to main folder list button ");
		String switch_mainfolder_list = PropertyHelper.coreSidebar
				.getPropertyValue("switch_mainfolder_list");
		try {
			WebElement switchButton = getWebElement(switch_mainfolder_list,
					true);
			 this.clickElement(switchButton);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the place field of new folder
	 * 
	 * @author Fiona Zhang
	 */
	public void clickPlaceFolder() {
		String place_new_folder_field = PropertyHelper.coreSidebar
				.getPropertyValue("place_new_folder_field");
		try {
			WebElement placeFolderElement = getWebElement(
					place_new_folder_field, false);
			 this.clickElement(placeFolderElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the Yes or No button on pop up dialog
	 *
	 * @param action
	 *            Yes/No to for the confirm dialog
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public void clickInfoConfirm(String action) {
		String folder_delete__confirm_dialog = PropertyHelper.coreSidebar
				.getPropertyValue("folder_delete_confirm_dialog");
		if (action.equals(ConstantName.YES))
			folder_delete__confirm_dialog = String.format(
					folder_delete__confirm_dialog, "Yes");
		else
			folder_delete__confirm_dialog = String.format(
					folder_delete__confirm_dialog, "No");
		try {
			WebElement placeFolderElement = getWebElement(
					folder_delete__confirm_dialog, true);
			 this.clickElement(placeFolderElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}
/*
	*//**
	 * Enter contact group in specified address book
	 * 
	 * @param addressbookName
	 *            Addressbook name want to enter
	 * 
	 * @author Fiona Zhang
	 *//*
	public void clickContactGroupEnterButton(String addressbookName) {
		Logging.info("Click contact group in : " + addressbookName);
		String go_to_contact_group = PropertyHelper.coreSidebar
				.getPropertyValue("go_to_contact_group");
		if (addressbookName.equals(ConstantName.DEFAULT))
			go_to_contact_group = PropertyHelper.coreSidebar
					.getPropertyValue("go_to_contact_group_default");
		else
			go_to_contact_group = String.format(go_to_contact_group,
					addressbookName);

		try {
			WebElement contactGroupEnterElement = this.getWebElement(
					go_to_contact_group, true);
			 this.clickElement(contactGroupEnterElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	*/
	/**
	 * Enter contact group in specified address book
	 * 
	 * @param addressbookName
	 *            Addressbook name want to enter
	 * 
	 * @author Fiona Zhang
	 */
	public void clickContactGroupEnterButton(String addressbookName) {
		Logging.info("Click contact group in : " + addressbookName);
		String go_to_contact_group = PropertyHelper.coreSidebar
				.getPropertyValue("go_to_contact_group");
		if (addressbookName.equals(ConstantName.DEFAULT))
			go_to_contact_group = PropertyHelper.coreSidebar
					.getPropertyValue("go_to_contact_group_default");
		else
			go_to_contact_group = String.format(go_to_contact_group,
					addressbookName);
		String go_to_contact_group_subelement =  PropertyHelper.coreSidebar
				.getPropertyValue("go_to_contact_group_subelement");
		try {
/*			List<WebElement> contactGroupEnterElement = this.getWebElements(
					go_to_contact_group, true);
*///			 this.clickElement(contactGroupEnterElement.get(index));
			 
				List<WebElement> addressbookNameList =  this.getWebElements(go_to_contact_group, true);
				for(int i = 0 ; i<addressbookNameList.size();i++){
					Logging.info("i is " + i);
					Logging.info(addressbookNameList.get(i).findElement(By.xpath(go_to_contact_group_subelement)).getText());
					if(addressbookNameList.get(i).findElement(By.xpath(go_to_contact_group_subelement)).getText().equals(addressbookName))
					{ this.clickElement(addressbookNameList.get(i));
					break;
					}
				}
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Back to contact list view from contact group
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public void clickBackToAddressbookButton() {
		Logging.info("Back to address book from contact group list");
		String back_from_contact_group = PropertyHelper.coreSidebar
				.getPropertyValue("back_from_contact_group");
		try {
			WebElement backButtonContactGroupElement = this.getWebElement(
					back_from_contact_group, false);
			 this.clickElement(backButtonContactGroupElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click add contact group button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickAddContactGroupButton() {
		Logging.info("Click add contact group button");
		String add_contact_group_button = PropertyHelper.coreSidebar
				.getPropertyValue("add_contact_group_button");
		try {
			WebElement addContactGroupElement = this.getWebElement(
					add_contact_group_button, false);
			 this.clickElement(addContactGroupElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the addressbook name
	 * 
	 * @param addressbookName
	 *            Addressbook name want to enter
	 * 
	 * @author Fiona Zhang
	 */
	public void clickAddressbookName(String addressbookName) {
		Logging.info("Click addressbook name:  " + addressbookName);
		String addressbook_name = null;
		if (addressbookName.equals(ConstantName.DEFAULT))
			addressbook_name = PropertyHelper.coreSidebar
					.getPropertyValue("addressbook_name_enter_default");

		else {
			addressbook_name = PropertyHelper.coreSidebar
					.getPropertyValue("addressbook_name_enter");

			addressbook_name = String.format(addressbook_name, addressbookName);
		}
		String addressbook_name_subelement = PropertyHelper.coreSidebar
				.getPropertyValue("addressbook_name_subelement");
		

		try {
			if (addressbookName.equals(ConstantName.DEFAULT)) {
				List<WebElement> addressbookNameList = this.getWebElements(
						addressbook_name, true);
				this.clickElement(addressbookNameList.get(0));

			}
			
			else{
			// for android, cannot click the element, so set all element into a list, iterator the element
			List<WebElement> addressbookNameList =  this.getWebElements(addressbook_name, true);
			for(int i = 0 ; i<addressbookNameList.size();i++){
				Logging.info("i is " + i);
				Logging.info(addressbookNameList.get(i).findElement(By.xpath(addressbook_name_subelement)).getText());
				if(addressbookNameList.get(i).findElement(By.xpath(addressbook_name_subelement)).getText().equals(addressbookName))
				{ this.clickElement(addressbookNameList.get(i));
				break;
				}
			}
			}
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the contactGroup name
	 * 
	 * @param contactGroup
	 *            ContactGroup name want to enter
	 * 
	 * @author Fiona Zhang
	 */
	public void clickContactGroupName(String contactGroup) {
		Logging.info("Click contact group name:  " + contactGroup
				+ " and enter the group");
		String contact_group_name = PropertyHelper.coreSidebar
				.getPropertyValue("contact_group_name");

		contact_group_name = String.format(contact_group_name, contactGroup);

		try {
			WebElement contactGroupElement = this.getWebElement(
					contact_group_name, true);
			 this.clickElement(contactGroupElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 08, 2015<br>
	 * <b>Description</b><br>
	 * Clicks button on message box popup. One of the following options should
	 * be supplied: - Yes - No - Cancel
	 * 
	 * @param button
	 *            button name
	 * @return void
	 */
	public void clickButtonOnMessageBox(String button) {
		if (!button.equals(ConstantName.OK))
			button = button.substring(0, 1).toUpperCase()
					+ button.substring(1).toLowerCase();
		Logging.info("Clicking on message box button: " + button);
		String common_msgbox_button = PropertyHelper.coreSidebar
				.getPropertyValue("common_msgbox_button");
		common_msgbox_button = String.format(common_msgbox_button, button);

		try {
			Tools.setDriverDefaultValues();
			
					 this.clickElement(Base.base.getDriver().findElement(By.xpath(common_msgbox_button)));
			this.waitForLoadMaskDismissed();
		} catch (NoSuchElementException ex) {
			Logging.error("Could not click on message box button: " + button);
			throw ex;
		}
	}

	/**
	 * Create a folder
	 *
	 * @param folderName
	 *            the folder need to created
	 * 
	 * @param parentFolderName
	 *            parent folder name
	 * 
	 * @author Fiona Zhang
	 */
	public void createFolder(String folderName) {
		Logging.info("Start to create a older");
		try {
			clickSystemFolderName(ConstantName.MYFOLDER);
			clickAddFolderIcon();
			addFolderName(folderName);
			clickActionInCreateFolder(ConstantName.OK);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Create a subfolder
	 *
	 * @param folderName
	 *            the folder need to created
	 * 
	 * @param parentFolderName
	 *            parent folder name
	 * 
	 * @author Fiona Zhang
	 */
	public void createSubFolder(String folderName, String parentFolderName) {
		Logging.info("Start to create a sub-folder");
		try {
			clickAddFolderIcon();
			selectParentFolder(parentFolderName);
			addFolderName(folderName);
			clickActionInCreateFolder(ConstantName.OK);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Empty the folder
	 *
	 * @param folderName
	 *            the folder need to be emptied
	 * 
	 * @author Fiona Zhang
	 */
	public void emptyFolder(String folderName) {
		Logging.info("Start to empty the folder " + folderName);
		try {
			this.clickEmptyFolderIcon(folderName);
			String float_confirm_popup = PropertyHelper.coreSidebar
					.getPropertyValue("float_confirm_popup");
			List<WebElement> popElement = getWebElements(float_confirm_popup,
					false);
			if (popElement.size() != 0 && popElement.get(0).isDisplayed())
				clickInfoConfirm(ConstantName.YES);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * click add addressbook icon on sidebar
	 *
	 * @author Fiona Zhang
	 * 
	 */
	public void clickAddAddressbookIcon() {
		Logging.info("Click add addressbook icon");
		String add_address_book_button = PropertyHelper.coreSidebar
				.getPropertyValue("add_address_book_button");

		try {
			WebElement addAddressbookElement = this.getWebElement(
					add_address_book_button, true);
			 this.clickElement(addAddressbookElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * click close button on sidebar to close sidebar
	 *
	 * @author Fiona Zhang
	 * 
	 */
	public void clickCloseButton() {
		Logging.info("Click the close button on sidebar");
		String close_sidebar_button = PropertyHelper.coreSidebar
				.getPropertyValue("close_sidebar_button");

		try {
			WebElement closeButtonElement = this.getWebElement(
					close_sidebar_button, true);
			 this.clickElement(closeButtonElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get contact count(indicator) from user define addressbook
	 * 
	 * @param addressbookName
	 *            The name of addressbook
	 * 
	 * @author Fiona Zhang
	 */
	public String getContactCountInAddressbook(String addressbookName) {
		Logging.info("Get count(indicator) from addressbook : "
				+ addressbookName);
		String address_book_indicator = null;
		if (addressbookName.equals(ConstantName.DEFAULT))
			address_book_indicator = PropertyHelper.coreSidebar
					.getPropertyValue("address_book_indicator_default");
		else {
			address_book_indicator = PropertyHelper.coreSidebar
					.getPropertyValue("address_book_indicator");
			address_book_indicator = String.format(address_book_indicator,
					addressbookName);
		}
		try {
			WebElement indicatorElement = this.getWebElement(
					address_book_indicator, true);
			Logging.info("Totally get : " + indicatorElement.getText()
					+ " contacts");
			return indicatorElement.getText();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	
	/**
	 * Get unread mail count according to the mailbox
	 * 
	 * @param mailbox
	 *            The name of mailbox
	 * 
	 * @author Fiona Zhang
	 */
	public int getUnreadMailCount(String mailbox) {
		Logging.info("Get count(indicator) from of mailbox : " + mailbox);
		String mail_unread_indicator = PropertyHelper.coreSidebar
				.getPropertyValue("mail_unread_indicator");
		mail_unread_indicator = String.format(mail_unread_indicator, this.toCapitlize(mailbox) );

		int result = 0;
		try {
			List<WebElement> indicatorElement = this.getWebElements(
					mail_unread_indicator, true);
            if (indicatorElement.size() != 0) { // if the size is 0, mean in the folder, there's no unread emails 
              result = Integer.valueOf(indicatorElement.get(0).getText());
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
        Logging.info("Totally get : " + result + " unread mails");
		return result;
	}


	/**
	 * Get contact count(indicator) from user define contact group
	 * 
	 * @param contactGroup
	 *            The name of contactGroup
	 * 
	 * @author Fiona Zhang
	 */
	public String getContactCountInContactGroup(String contactGroup) {
		Logging.info("Get count(indicator) from contact group : "
				+ contactGroup);
		String contact_group_indicator = PropertyHelper.coreSidebar
				.getPropertyValue("contact_group_indicator");
		contact_group_indicator = String.format(contact_group_indicator,
				contactGroup);

		try {
			WebElement indicatorElement = this.getWebElement(
					contact_group_indicator, true);
			Logging.info("Totally get : " + indicatorElement.getText()
					+ " contacts");
			return indicatorElement.getText();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Enter a addressbook name
	 *
	 * @param addressbookName
	 *            the name of address book
	 * 
	 * @author Fiona
	 */
	public void addAddressbookName(String addressbookName) {
		Logging.info("Add addressbook name : " + addressbookName);
		String addressbook_name_field = PropertyHelper.coreSidebar
				.getPropertyValue("popup_dialog_name_field");

		try {
			WebElement addressbookElement = this.getWebElement(
					addressbook_name_field, false);
			addressbookElement.clear();
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				addressbookElement.sendKeys(addressbookName);
				addressbookElement.sendKeys(Keys.ENTER);
			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID))
				this.elementSendKeys(addressbookElement, addressbookName);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Enter a addressbook name
	 *
	 * @param addressbookName
	 *            the name of address book
	 * 
	 * @author Fiona
	 */
	public void addNameWithoutClearInPopupDialog(String addressbookName) {
		Logging.info("Add name in popup diaglog : " + addressbookName);
		String task_group_name_input_field = PropertyHelper.coreSidebar
				.getPropertyValue("task_group_name_input_field");

		try {
			WebElement nameElement = this.getWebElement(
					task_group_name_input_field, false);
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS))
				nameElement.sendKeys(addressbookName);

			else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID))
				this.elementSendKeys(nameElement, addressbookName);

			nameElement.sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Create a addressbookName
	 *
	 * @param addressbookName
	 *            the addressbookName need to created
	 * 
	 * @author Fiona Zhang
	 */
	public void createAddressbook(String addressbookName) {
		Logging.info("Start to create a addressbook");
		try {
			clickAddAddressbookIcon();
			addAddressbookName(addressbookName);
			clickButtonOnMessageBox(ConstantName.OK);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/***************************************************************************
	 * @Method Name: triggerFolderEditRMForm
	 * @Author : Jason Song
	 * @Created : 12/26/14
	 * @Description : Trigger remove and edit form by specified folder name.
	 * @parameter1: String represent folder name to be removed or edit.
	 ***************************************************************************/
	public void swipeAddressbookEditRMForm(String addressbookName) {
		Logging.info("Swipe action on current addressbook and trigger edit/remove pane...");

		String address_book_name = PropertyHelper.coreSidebar
				.getPropertyValue("address_book_name");
		address_book_name = String.format(address_book_name, addressbookName);

		try {
			WebElement addressbookElement = getWebElement(address_book_name,
					false);

			String addressbookID = addressbookElement.getAttribute("id");

			String addrbookEdit = PropertyHelper.coreSidebar
					.getPropertyValue("address_book_name_edit_js");
			addrbookEdit = String.format(addrbookEdit, addressbookID);
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			executor.executeScript(addrbookEdit);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the icon in contact group name edit view
	 *
	 * @param action
	 *            action of contact group name
	 * 
	 * @author Fiona Zhang
	 */
	public void swipeContactGroupEditForm(String contactGroupName) {
		Logging.info("Swipe action on current contact group and trigger edit/remove pane...");

		String contact_group_name = PropertyHelper.coreSidebar
				.getPropertyValue("contact_group_name");
		contact_group_name = String
				.format(contact_group_name, contactGroupName);

		try {
			WebElement contactGroupElement = getWebElement(contact_group_name,
					false);

			String addressbookID = contactGroupElement.getAttribute("id");

			String addrbookEdit = PropertyHelper.coreSidebar
					.getPropertyValue("contact_group_name_edit_js");
			addrbookEdit = String.format(addrbookEdit, addressbookID);
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			executor.executeScript(addrbookEdit);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the icon in contact group name edit view
	 *
	 * @param action
	 *            action of contact group name
	 * 
	 * @author Fiona Zhang
	 */
	public void swipeTaskGroupInEditForm(String taskName) {
		Logging.info("Swipe action on current task group " + taskName
				+ " and trigger edit/remove pane...");

		String task_group_name_swipe_item = PropertyHelper.coreSidebar
				.getPropertyValue("task_group_name_swipe_item");
		task_group_name_swipe_item = String.format(task_group_name_swipe_item,
				taskName);

		try {
			WebElement contactGroupElement = getWebElement(
					task_group_name_swipe_item, true);

			String addressbookID = contactGroupElement.getAttribute("id");
			String addrbookEdit = PropertyHelper.coreSidebar
					.getPropertyValue("contact_group_name_edit_js");
			addrbookEdit = String.format(addrbookEdit, addressbookID);
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			executor.executeScript(addrbookEdit);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the icon in addressbook name edit view
	 *
	 * @param action
	 *            action of addressbook name
	 * 
	 * @author Fiona Zhang
	 */
	public void clickAddressbookNameAction(String action) {
		Logging.info("Start to " + action + " the addressbook name");
		String actionAddressbook = null;
		if (action.equalsIgnoreCase("edit")) {
			actionAddressbook = PropertyHelper.coreSidebar
					.getPropertyValue("addressbook_edit_icon");
		} else if (action.equalsIgnoreCase("delete")) {
			actionAddressbook = PropertyHelper.coreSidebar
					.getPropertyValue("addressbook_delete_icon");
		}
		try {
			WebElement addressbookEditrm = this.getWebElement(
					actionAddressbook, false);
			 this.clickElement(addressbookEditrm);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the icon in addressbook name edit view
	 *
	 * @param action
	 *            action of addressbook name
	 * 
	 * @author Fiona Zhang
	 */
	public void clickContactGroupAction(String action) {
		Logging.info("Start to " + action + " the contact group name");
		String actionContactGroup = null;
		if (action.equalsIgnoreCase("edit")) {
			actionContactGroup = PropertyHelper.coreSidebar
					.getPropertyValue("contact_group_edit_icon");
		} else if (action.equalsIgnoreCase("delete")) {
			actionContactGroup = PropertyHelper.coreSidebar
					.getPropertyValue("contact_group_delete_icon");
		} else if (action.equalsIgnoreCase("mail")) {
			actionContactGroup = PropertyHelper.coreSidebar
					.getPropertyValue("contact_group_mail_icon");
		}
		try {
			WebElement contactGroupEditrm = this.getWebElement(
					actionContactGroup, false);
			 this.clickElement(contactGroupEditrm);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click auto compelete folder, enter the folder
	 *
	 * 
	 * @author Fiona Zhang
	 */
	public void clickAutoCompleteFolder() {
		Logging.info("Click auto complete folder");
		String auto_complete_contact_folder_enter = PropertyHelper.coreSidebar
				.getPropertyValue("auto_complete_contact_folder_enter");

		try {
			WebElement autoCompleteFolderEnterElement = this.getWebElement(
					auto_complete_contact_folder_enter, false);
			 this.clickElement(autoCompleteFolderEnterElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the calendar visible option
	 * 
	 * @param calendarName
	 *            the calendar name
	 *
	 * @param visible
	 *            if true , set the calendar as visible, if false, set it as
	 *            invisible
	 * 
	 * @author Fiona Zhang
	 */
	public void clickCalendarVisbleOption(String calendarName, boolean visible) {
		if (visible)
			Logging.info("Set the calendar " + calendarName + " as : visible");
		else
			Logging.info("Set the calendar " + calendarName + " as : unvisible");

		String visible_button = PropertyHelper.coreSidebar
				.getPropertyValue("visible_button");

		visible_button = String.format(visible_button, calendarName);

		try {
			WebElement visibleButton = this
					.getWebElement(visible_button, false);
			if (visibleButton.getAttribute("class").contains("check")
					&& !visible
					|| !visibleButton.getAttribute("class").contains("check")
					&& visible)
				 this.clickElement(visibleButton);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the Alltask or Task groups in side bar
	 *
	 * @param group
	 *            The name of the group
	 * 
	 * @author Fiona Zhang
	 */
	public void clickTaskCatelog(String group) {
		Logging.info("Click task catelog " + group);
		String taskGroup = null;
		if (group.equals(ConstantName.GROUP))
			taskGroup = PropertyHelper.coreSidebar
					.getPropertyValue("task_group_catelog_group");

		else if (group.equals(ConstantName.ALLTASK))
			taskGroup = PropertyHelper.coreSidebar
					.getPropertyValue("task_group_catelog_task");
		try {
			WebElement taskGroupElement = this.getWebElement(taskGroup, false);
			 this.clickElement(taskGroupElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the task group name action
	 *
	 * @param action
	 *            delete or edit task group name
	 * 
	 * @author Fiona Zhang
	 */
	public void clickTaskGroupNameAction(String action) {
		Logging.info("Click task edit action :  " + action);
		String taskGroupAction = null;
		if (action.equals(ConstantName.DELETE))
			taskGroupAction = PropertyHelper.coreSidebar
					.getPropertyValue("task_group_delete_action");

		else if (action.equals(ConstantName.EDIT))
			taskGroupAction = PropertyHelper.coreSidebar
					.getPropertyValue("task_group_edit_action");
		try {
			WebElement taskGroupElement = this.getWebElement(taskGroupAction,
					false);
			 this.clickElement(taskGroupElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the add task group button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickAddTaskGroupButton() {
		Logging.info("Click add task group button");
		String task_group_add_button = PropertyHelper.coreSidebar
				.getPropertyValue("task_group_add_button");
		try {
			WebElement taskGroupAddButtonElement = this.getWebElement(
					task_group_add_button, false);
			 this.clickElement(taskGroupAddButtonElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click task group by name
	 * 
	 * @author Fiona Zhang
	 */
	public void clickTaskGroupByName(String groupName) {
		Logging.info("Click add task group button");
		String task_group_enter_by_name = PropertyHelper.coreSidebar
				.getPropertyValue("task_group_enter_by_name");
		task_group_enter_by_name = String.format(task_group_enter_by_name,
				groupName);
		try {
			WebElement taskGroupEnterElement = this.getWebElement(
					task_group_enter_by_name, false);
			 this.clickElement(taskGroupEnterElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click back to task catelog button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickBackToTaskCatelogButton() {
		Logging.info("Click back to task catalog button");
		String task_group_back_to_catelog_button = PropertyHelper.coreSidebar
				.getPropertyValue("task_group_back_to_catelog_button");
		try {
			WebElement taskGroupBackButtonElement = this.getWebElement(
					task_group_back_to_catelog_button, true);
			 this.clickElement(taskGroupBackButtonElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get a addressbookName list
	 *
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getAddressbookNameList() {
		Logging.info("Get addressbook name list");
		String address_book_name_list = PropertyHelper.coreSidebar
				.getPropertyValue("address_book_name_list");
		ArrayList<String> nameList = new ArrayList<String>();
		try {
			List<WebElement> addressbookElement = this.getWebElements(
					address_book_name_list, false);
			for (WebElement el : addressbookElement)
				nameList.add(el.getText());
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return nameList;
	}

	/**
	 * Get status of address book in edit mode
	 *
	 * @param action
	 *            action of addressbook name
	 * 
	 * @return boolean true - the status is disabled false - the status is
	 *         enabled
	 * 
	 * @author Fiona Zhang
	 */
	public boolean getEditStatusOfAddrbook(String action) {
		Logging.info("Start to check " + action
				+ " button status in addressbook name");
		String actionAddressbook = null;
		if (action.equalsIgnoreCase("edit")) {
			actionAddressbook = PropertyHelper.coreSidebar
					.getPropertyValue("addressbook_edit_icon_status");
		} else if (action.equalsIgnoreCase("delete")) {
			actionAddressbook = PropertyHelper.coreSidebar
					.getPropertyValue("addressbook_delete_icon_status");
		}
		try {
			WebElement addressbookEditrm = this.getWebElement(
					actionAddressbook, false);
			return addressbookEditrm.getAttribute("class").contains("disable");
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get contact group name list
	 *
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getContactGroupList() {
		Logging.info("Get contact group name list");
		String contact_group_name_list = PropertyHelper.coreSidebar
				.getPropertyValue("contact_group_name_list");
		ArrayList<String> nameList = new ArrayList<String>();
		try {
			List<WebElement> contactGroupListElement = this.getWebElements(
					contact_group_name_list, false);
			for (WebElement el : contactGroupListElement)
				nameList.add(el.getText());
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info(nameList.toString());
		return nameList;
	}

	/**
	 * Get contact count(indicator) in auto complete folder
	 * 
	 * @author Fiona Zhang
	 */
	public int getContactAutoCompleteCount() {
		int autoCompleteCount = 100;
		String auto_complete_contact_folder_indicator = PropertyHelper.coreSidebar
				.getPropertyValue("auto_complete_contact_folder_indicator");
		try {
			WebElement autoCompleteContactElement = this.getWebElement(
					auto_complete_contact_folder_indicator, true);
			Logging.info(autoCompleteContactElement.getText());
			autoCompleteCount = Integer.parseInt(autoCompleteContactElement
					.getText());
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Get " + autoCompleteCount
				+ " contact in auto complete folder");
		return autoCompleteCount;
	}

	/**
	 * Get user name on top of the side bar
	 *
	 * @author Fiona Zhang
	 */
	public String getUserName() {
		String user_name = PropertyHelper.coreSidebar
				.getPropertyValue("user_name");
		try {
			WebElement userNameElement = this.getWebElement(user_name, false);
			Logging.info("Get user name " + userNameElement.getText()
					+ " on top of the side bar");
			return userNameElement.getText();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get the folder level in my Folder list
	 * 
	 * @param folderName
	 *            The name of the folder. If return 1, means, the folder is
	 *            under My Folder list, return 2, means, its under 1
	 *
	 * @author Fiona Zhang
	 */
	public int getFolderLevel(String folderName) {
		String folder_level = PropertyHelper.coreSidebar
				.getPropertyValue("folder_level");
		folder_level = String.format(folder_level, folderName);
		int result = 0;
		try {
			WebElement folderLevelElement = this.getWebElement(folder_level,
					false);
			String folderClassName = folderLevelElement.getAttribute("class");
			result = Integer.parseInt(folderClassName.substring(folderClassName
					.lastIndexOf("-") + 1));
			Logging.info("Get the folder level under My Folder as : " + result);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return result;
	}

	/**
	 * Get all folder names under My Folders
	 * 
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getFolderNames() {
		Logging.info("Get folders under my folders");
		String folder_list_under_my_folder = PropertyHelper.coreSidebar
				.getPropertyValue("folder_list_under_my_folder");
		ArrayList<String> nameList = new ArrayList<String>();
		try {
			List<WebElement> folderListElement = this.getWebElements(
					folder_list_under_my_folder, false);
			for (WebElement el : folderListElement)
				nameList.add(el.getText());
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Get folders under 'My Folders' : " + nameList.toString());
		return nameList;
	}

	/**
	 * Get task group names under group list
	 * 
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getTaskGroupList() {
		Logging.info("Get task under task group");
		String task_group_list_item = PropertyHelper.coreSidebar
				.getPropertyValue("task_group_list_item");
		ArrayList<String> nameList = new ArrayList<String>();
		try {
			List<WebElement> taskListElement = this.getWebElements(
					task_group_list_item, false);
			for (WebElement el : taskListElement)
				nameList.add(el.getText());
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Get task under 'Task Groups' : " + nameList.toString());
		return nameList;

	}

	


}
