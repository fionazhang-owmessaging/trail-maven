package synchronoss.com.pageobject.core;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import io.appium.java_client.ios.IOSDriver;
import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class CoreEmailComposer extends PageObjectBase {
	/***************************************************************************
	 * @Method Name: addToReceipent
	 * @Author : Fiona Zhang
	 * @Created : 06 Jan 2016
	 * @Description : This method to click write email button
	 ***************************************************************************/
	public void addToReceipent(String recepient) {
		Logging.info("Add receipent " + recepient + " to To the compose field");
		String to_field_ebox = PropertyHelper.coreEmailComposer
				.getPropertyValue("to_field_ebox");
		String field_ebox_parent = PropertyHelper.coreEmailComposer
				.getPropertyValue("field_ebox_parent");

		try {
			Tools.scrollElementIntoViewByXpath(to_field_ebox);
			WebElement to_field = this.getWebElement(to_field_ebox, false);
			to_field.clear();
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				to_field.sendKeys(recepient);
			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID))
				this.elementSendKeys(
						to_field.findElement(By.xpath(field_ebox_parent)),
						recepient);
			to_field.sendKeys(Keys.ENTER);

		}
		catch (WebDriverException ex) {
			Logging.error("add To Receipent failed");
			throw ex;
		}
	}
	
	public void addToReceipent(String[] toRecepit) {
		Logging.info("Add multiple receipent to To the compose field");
		String to_field_ebox = PropertyHelper.coreEmailComposer
				.getPropertyValue("to_field_ebox");
		String field_ebox_parent = PropertyHelper.coreEmailComposer
				.getPropertyValue("field_ebox_parent");

		try {
			Tools.scrollElementIntoViewByXpath(to_field_ebox);
			WebElement to_field = this.getWebElement(to_field_ebox, false);
			to_field.clear();
				for (String s : toRecepit) {
					to_field.sendKeys(s);
					to_field.sendKeys(Keys.ENTER);
				}

		} catch (WebDriverException ex) {
			Logging.error("add To Receipent failed");
			throw ex;
		}
	}


	/***************************************************************************
	 * @Method Name: addToReceipent
	 * @Author : Fiona Zhang
	 * @Created : 06 Jan 2016
	 * @Description : This method to click write email button
	 ***************************************************************************/
	public void addToReceipentWithoutEnter(String recepient) {
		String to_field_ebox = PropertyHelper.coreEmailComposer
				.getPropertyValue("to_field_ebox");
		try {
			Tools.scrollElementIntoViewByXpath(to_field_ebox);
			WebElement to_field = this.getWebElement(to_field_ebox, false);
			to_field.clear();
		//	if (Base.platformName.equalsIgnoreCase(ConstantName.IOS))
				to_field.sendKeys(recepient);
	//		else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
			//	this.elementSendKeys(to_field, recepient);
	//		}
		} catch (WebDriverException ex) {
			Logging.error("addToReceipent failed");
			throw ex;
		}
	}

	/***************************************************************************
	 * @Method Name: addCcReceipent
	 * @Author : Fiona Zhang
	 * @Created : 08 Jan 2016
	 * @Description : This method will add cc receipient
	 ***************************************************************************/
	public void addCcReceipent(String ccRecepient) {
		String cc_field_ebox = PropertyHelper.coreEmailComposer
				.getPropertyValue("cc_field_ebox");
		String field_ebox_parent = PropertyHelper.coreEmailComposer
				.getPropertyValue("field_ebox_parent");
		try {
			Tools.scrollElementIntoViewByXpath(cc_field_ebox);
			WebElement cc_field = this.getWebElement(cc_field_ebox, false);
			cc_field.clear();
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				cc_field.sendKeys(Keys.ENTER);
				cc_field.sendKeys(ccRecepient);

			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				this.elementSendKeys(
						cc_field.findElement(By.xpath(field_ebox_parent)),
						ccRecepient);
			}
		} catch (WebDriverException ex) {
			Logging.error("addCcReceipent failed");
			throw ex;
		}
	}

	/***************************************************************************
	 * @Method Name: addBccReceipent
	 * @Author : Fiona Zhang
	 * @Created : 08 Jan 2016
	 * @Description : This method will add bcc receipient
	 ***************************************************************************/
	public void addBccReceipent(String bccRecepient) {
		String bcc_field_ebox = PropertyHelper.coreEmailComposer
				.getPropertyValue("bcc_field_ebox");
		String field_ebox_parent = PropertyHelper.coreEmailComposer
				.getPropertyValue("field_ebox_parent");

		try {
			WebElement bcc_field = this.getWebElement(bcc_field_ebox, false);
			bcc_field.clear();
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				bcc_field.sendKeys(bccRecepient);
				bcc_field.sendKeys(Keys.ENTER);
			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				this.elementSendKeys(
						bcc_field.findElement(By.xpath(field_ebox_parent)),
						bccRecepient);
			}
		} catch (WebDriverException ex) {
			Logging.error("addBccReceipent failed");
			throw ex;
		}
	}

	/***************************************************************************
	 * @Method Name: addSubject
	 * @Author : Fiona Zhang
	 * @Created : 06 Jan 2016
	 * @Description : This method to add a subject to a email
	 ***************************************************************************/
	public void addSubject(String subject) {
		String subject_ebox = PropertyHelper.coreEmailComposer
				.getPropertyValue("subject_ebox");
		try {
			Tools.scrollElementIntoViewByXpath(subject_ebox);
			WebElement subject_field = this.getWebElement(subject_ebox, false);
			subject_field.clear();
			subject_field.sendKeys(subject);
			subject_field.sendKeys(Keys.ENTER);
			Logging.info("Add subject as : " + subject);
		} catch (WebDriverException ex) {
			Logging.error("addSubject failed");
			throw ex;
		}
	}



	
	/**
	 * Input some contact name into the search field
	 * 
	 * @param contact
	 *            The contact first name need to searched
	 *
	 * @author Fiona Zhang
	 */
	public void addContactNameInContactPickerSearch(String contact) {
		String contact_picker_search_input_field = PropertyHelper.coreEmailComposer
				.getPropertyValue("contact_picker_search_input_field");
		try {
			Tools.scrollElementIntoViewByXpath(contact_picker_search_input_field);
			WebElement contactSearchFieldElement = this.getWebElement(
					contact_picker_search_input_field, false);
			contactSearchFieldElement.clear();
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				contactSearchFieldElement.sendKeys(contact);
				IOSDriver driver = (IOSDriver) Base.base.getDriver();
				driver.hideKeyboard();
			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				this.elementSendKeys(contactSearchFieldElement, contact);
			}
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click the plus button to open contact picker
	 *
	 * @author Fiona Zhang
	 */
	public void clickPlusContactPickerButton(String field) {
		String contact_picker_plus_button = null;
		switch (field) {
		case ConstantName.CCFIELD:
			contact_picker_plus_button = PropertyHelper.coreEmailComposer
					.getPropertyValue("contact_picker_plus_button_cc_field");
			break;
		case ConstantName.BCCFIELD:
			contact_picker_plus_button = PropertyHelper.coreEmailComposer
					.getPropertyValue("contact_picker_plus_button_bcc_field");
			break;
		case ConstantName.TOFIELD:
			contact_picker_plus_button = PropertyHelper.coreEmailComposer
					.getPropertyValue("contact_picker_plus_button_to_field");
			break;
		}
		try {
			Logging.info("Click add contact from contact picker button (the plus button) in "
					+ field + " field");
			Tools.scrollElementIntoViewByXpath(contact_picker_plus_button);
			WebElement plusContactPickerElement = getWebElement(
					contact_picker_plus_button, false);
			 this.clickElement(plusContactPickerElement);
			 if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
			IOSDriver iosDriver = (IOSDriver) Base.base.getDriver();
			iosDriver.hideKeyboard();
			 }
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click Contact in contact picker
	 *
	 * @author Fiona Zhang
	 */
	public void clickContactInContactPicker(String contact1, boolean isSelect) {
		String contact_picker_item = PropertyHelper.coreEmailComposer
				.getPropertyValue("contact_picker_item");
		contact_picker_item = String.format(contact_picker_item, contact1);
		Logging.info(contact_picker_item);
		try {
			Logging.info("Click the item of : " + contact1
					+ " in contact picker");
			Tools.scrollElementIntoViewByXpath(contact_picker_item);
			WebElement contactItemElement = getWebElement(contact_picker_item,
					true);
			Logging.info("contact item element xpath : "
					+ contactItemElement.getAttribute("class"));
			if (isSelect
					&& !contactItemElement.getAttribute("class").contains(
							"selected")
					|| !isSelect
					&& contactItemElement.getAttribute("class").contains(
							"selected"))
				 this.clickElement(contactItemElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click done button in contact picker view
	 *
	 * @author Fiona Zhang
	 */
	public void clickDoneInContactPicker() {
		String contact_picker_done_button = PropertyHelper.coreEmailComposer
				.getPropertyValue("contact_picker_done_button");
		try {
			Logging.info("Click done button in contact picker view");
			Tools.scrollElementIntoViewByXpath(contact_picker_done_button);
			WebElement contactPickerDoneElement = getWebElement(
					contact_picker_done_button, false);
			 this.clickElement(contactPickerDoneElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/***************************************************************************
	 * @Method Name: clickSendEmailButton
	 * @Author : Fiona Zhang
	 * @Created : 06 Jan 2016
	 * @Description : This method to click send email button
	 ***************************************************************************/
	public void clickSendEmailButton() {
		String send_Button = PropertyHelper.coreEmailComposer
				.getPropertyValue("sendmail_button");
		try {
			Logging.info("Click send email button in compose view");
			Tools.scrollElementIntoViewByXpath(send_Button);
			WebElement sendButton = getWebElement(send_Button, false);
			 this.clickElement(sendButton);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("clickSendEmailButton failed");
			throw ex;
		}
	}

	/**
	 * Click cancel button in compose view
	 *
	 * @author Fiona Zhang
	 */
	public void clickCancelComposeButton() {
		String cancel_sendmail_button = PropertyHelper.coreEmailComposer
				.getPropertyValue("cancel_sendmail_button");
		Logging.info("Click cancel button in compose view");

		try {
			Tools.scrollElementIntoViewByXpath(cancel_sendmail_button);
			WebElement cancelSendButtonElement = getWebElement(
					cancel_sendmail_button, true);
			 this.clickElement(cancelSendButtonElement);
		} catch (WebDriverException ex) {
			Logging.error("click Send Email Button failed");
			throw ex;
		}
	}

	/**
	 * Click priority button in compose view
	 *
	 * @author Fiona Zhang
	 */
	public void clickPriorityButton() {
		String priority_button = PropertyHelper.coreEmailComposer
				.getPropertyValue("priority_button");
		try {
			Logging.info("Click priority button in compose view");
			Tools.scrollElementIntoViewByXpath(priority_button);
			WebElement priorityElement = getWebElement(priority_button, false);
			 this.clickElement(priorityElement);
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click insert signature button in compose view
	 *
	 * @author Fiona Zhang
	 */
	public void clickInsertSigatureButton() {

		String insert_signature_button = PropertyHelper.coreEmailComposer
				.getPropertyValue("insert_signature_button");
		try {
			Logging.info("Click insert signature button in compose view");
			Tools.scrollElementIntoViewByXpath(insert_signature_button);
			WebElement cancelSendButtonElement = getWebElement(
					insert_signature_button, false);
			 this.clickElement(cancelSendButtonElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	
  /**
   * Click the auto-suggest items
   * 
   * @param autoSuggestEmail the auto suggest email need to click
   *
   * @author Fiona Zhang
   */
  public void clickAutoSuggestContact(String autoSuggestEmail) {
    String auto_suggest_contact_item =
        PropertyHelper.pageObjectBaseFile.getPropertyValue("auto_suggest_contact_item");
    auto_suggest_contact_item = String.format(auto_suggest_contact_item, autoSuggestEmail);
    Logging.info("click auto suggest contact : " + auto_suggest_contact_item);
    try {
      Tools.scrollElementIntoViewByXpath(auto_suggest_contact_item);
      WebElement autoSuggestElement = getWebElement(auto_suggest_contact_item, true);
      this.clickElement(autoSuggestElement);
    } catch (WebDriverException ex) {
      Logging.error("click auto suggest contact failed");
      throw ex;
    }
  }

	/***************************************************************************
	 * @Method Name: expandToButton
	 * @Author : Fiona Zhang
	 * @Created : 08 Jan 2016
	 * @param : boolean ifExpand true - expected to expand , false - expected to
	 *        collapse
	 * @Description : This method will expand or collapse the cc/bcc part
	 ***************************************************************************/
	public void expandOrCollapseOtherField(boolean ifExpand) {

		if (ifExpand == true) {
			if (verifyCCFiledNotDisplay() && verifyBCCFiledNotDisplay()) {
				String expand_arrow_user_box = PropertyHelper.coreEmailComposer
						.getPropertyValue("expand_arrow_user_box");
				try {
					WebElement actionButton = this.getWebElement(
							expand_arrow_user_box, false);
					 this.clickElement(actionButton);

				} catch (WebDriverException ex) {
					ex.printStackTrace();
					throw ex;
				}
			}
		}

		else {
			if (!(verifyCCFiledNotDisplay() && verifyBCCFiledNotDisplay())) {
				String collapse_arrow_user_box = PropertyHelper.coreEmailComposer
						.getPropertyValue("collapse_arrow_user_box");
				try {
					WebElement actioButton = this.getWebElement(
							collapse_arrow_user_box, false);
					 this.clickElement(actioButton);

				} catch (WebDriverException ex) {
					ex.printStackTrace();
					throw ex;
				}
			}
		}

	}

	/***************************************************************************
	 * @Method Name: addMessageBody
	 * @Author : Fiona Zhang
	 * @Created : 08 Jan 2016
	 * @param : String messageBody
	 * @Description : This method add email body to the email
	 ***************************************************************************/
	public void addMessageBody(String messageBody) {
		String mailbody = PropertyHelper.coreEmailComposer
				.getPropertyValue("mailbody_ebox");
		try {
			WebElement mail_field = this.getWebElement(mailbody, true);
				mail_field.clear();
				if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
					this.setValueByJs(mail_field, messageBody);
				}
				if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
					mail_field.sendKeys(messageBody);
					mail_field.sendKeys(Keys.RETURN);
				}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * select the priority of the email
	 * 
	 * @param low
	 *            The priority of the mail
	 *
	 * @author Fiona Zhang
	 */
	public void selectPriority(String low) {
		String priority = null;
		switch (low) {
		case ConstantName.LOW:
			priority = PropertyHelper.coreEmailComposer
					.getPropertyValue("priority_low_option");
			break;
		case ConstantName.NORMAL:
			priority = PropertyHelper.coreEmailComposer
					.getPropertyValue("priority_normal_option");
			break;
		case ConstantName.HIGH:
			priority = PropertyHelper.coreEmailComposer
					.getPropertyValue("priority_high_option");
			break;
		}
		Logging.info("Click the priority option " + low
				+ " in priority popup menu");
		try {
			WebElement actioButton = this.getWebElement(priority, false);
			 this.clickElement(actioButton);

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/***************************************************************************
	 * @Method Name: verifyCCFiledDisplayOrNot
	 * @Author : Fiona Zhang
	 * @Created : 08 Jan 2016
	 * @return : true - the cc field is not displayed , false - the cc field is
	 *         displayed
	 * @Description : verifyCCFiledDisplayOrNot
	 ***************************************************************************/
	public boolean verifyCCFiledNotDisplay() {
		boolean display = false;
		String cc_field_compomnent = PropertyHelper.coreEmailComposer
				.getPropertyValue("cc_field_compomnent");
		try {
			WebElement cc_compomnent = this.getWebElement(cc_field_compomnent,
					false);
			if (cc_compomnent.getAttribute("style").contains("display"))
				display = true;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return display;
	}

	/***************************************************************************
	 * @Method Name: verifyBCCFiledDisplayOrNot
	 * @Author : Fiona Zhang
	 * @Created : 08 Jan 2016
	 * @return : true - the bcc field is not displayed , false - the bcc field
	 *         is displayed
	 * @Description : verifyBCCFiledDisplayOrNot
	 ***************************************************************************/
	public boolean verifyBCCFiledNotDisplay() {
		boolean display = false;
		String bcc_field_compomnent = PropertyHelper.coreEmailComposer
				.getPropertyValue("bcc_field_compomnent");
		try {
			WebElement bcc_compomnent = this.getWebElement(
					bcc_field_compomnent, false);
			if (bcc_compomnent.getAttribute("style").contains("display"))
				display = true;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return display;
	}

	/**
	 * Get contact in TO field
	 *
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getTextInToRecepient() {
		Logging.info("Get list in To recepient field");
		String to_field_text = PropertyHelper.coreEmailComposer
				.getPropertyValue("to_field_text");
		ArrayList<String> nameList = new ArrayList<String>();
		try {
			List<WebElement> toFieldElement = this.getWebElements(
					to_field_text, true);
			for (WebElement el : toFieldElement)
				nameList.add(el.getText());
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info(nameList.toString());
		return nameList;
	}

	/**
	 * Get contact in CC field
	 *
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getTextInCCRecepient() {
		Logging.info("Get list in CC recepient field");
		String cc_field_text = PropertyHelper.coreEmailComposer
				.getPropertyValue("cc_field_text");
		ArrayList<String> nameList = new ArrayList<String>();
		try {
			List<WebElement> ccFieldElement = this.getWebElements(
					cc_field_text, true);
			for (WebElement el : ccFieldElement)
				nameList.add(el.getText());
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info(nameList.toString());
		return nameList;
	}

	/**
	 * Get contact in BCC field
	 *
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getTextInBCCRecepient() {
		Logging.info("Get list in BCC recepient field");
		String bcc_field_text = PropertyHelper.coreEmailComposer
				.getPropertyValue("bcc_field_text");
		ArrayList<String> nameList = new ArrayList<String>();
		try {
			List<WebElement> bccFieldElement = this.getWebElements(
					bcc_field_text, true);
			for (WebElement el : bccFieldElement)
				nameList.add(el.getText());
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info(nameList.toString());
		return nameList;
	}

	/**
	 * Get text in message body in compose view
	 *
	 * @author Fiona Zhang
	 */
	public String getTextInMessageBody() {
		String mailBodyText = null;
		String mailbody_ebox = PropertyHelper.coreEmailComposer
				.getPropertyValue("mailbody_ebox");
		try {
			mailBodyText = getItemValueById(mailbody_ebox);
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
			IOSDriver iosDriver = (IOSDriver) Base.base.getDriver();
			iosDriver.hideKeyboard();
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Get text in message body in compose view : "
				+ mailBodyText);
		return mailBodyText;
	}


  /**
   * Get the priority of email in compose view
   *
   * @author Fiona Zhang
   */
  public String getPriority() {
    String priority_button = PropertyHelper.coreEmailComposer.getPropertyValue("priority_button");
    String auto_save_draft_toast =
        PropertyHelper.coreEmailComposer.getPropertyValue("auto_save_draft_toast");

    String result = null;
    try {
      Tools.scrollElementIntoViewByXpath(priority_button);
      WebElement priorityElement = getWebElement(priority_button, false);
      String tmp = priorityElement.getAttribute("class");
      if (tmp.contains(ConstantName.LOW))
        result = ConstantName.LOW;
      else if (tmp.contains(ConstantName.NORMAL))
        result = ConstantName.NORMAL;
      else if (tmp.contains(ConstantName.HIGH))
        result = ConstantName.HIGH;
      Tools.waitUntilElementNotDisplayedByXpath(auto_save_draft_toast, 10);
      Logging.info("Get the priority place class name as " + tmp);
    } catch (WebDriverException ex) {
      throw ex;
    }
    return result;
  }

  /**
   * Check if save draft bar exists or not
   *
   * @author Fiona Zhang
   */
  public boolean isSaveDraftBarDisplayed() {
    String auto_save_draft_toast =
        PropertyHelper.coreEmailComposer.getPropertyValue("auto_save_draft_toast");
    boolean result = false;
    try {
      Base.base.getDriver().manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
      WebElement el = getWebElement(auto_save_draft_toast, true);
      result = el.isDisplayed();
      Logging.info("Check if save draft bar displayed : " + result);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    } finally {
      Tools.setDriverDefaultValues();
    }
    return result;
  }


}
