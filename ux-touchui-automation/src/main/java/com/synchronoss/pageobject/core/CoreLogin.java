package synchronoss.com.pageobject.core;

import java.time.ZoneId;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class CoreLogin extends PageObjectBase {

	/**
	 * Check if reply quote the orgiginal message
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isReplyQuoteMessage() {
		String reply_quoting_orginal_msg_js = PropertyHelper.coreLoginFile
				.getPropertyValue("reply_quoting_orginal_msg_js");
		boolean isReplyQuoteMessage = false;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			isReplyQuoteMessage = (boolean) js
					.executeScript(reply_quoting_orginal_msg_js);
			Logging.info("Is reply quote the original message : "
					+ isReplyQuoteMessage);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return isReplyQuoteMessage;
	}

	/**
	 * Check if empty the trash when logout
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isEmptyTrashWhenLogout() {
		String empty_trash_when_logout_js = PropertyHelper.coreLoginFile
				.getPropertyValue("empty_trash_when_logout_js");
		boolean isEmptyTrash = false;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			isEmptyTrash = (boolean) js
					.executeScript(empty_trash_when_logout_js);
			Logging.info("Is empty the trash when logout : " + isEmptyTrash);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return isEmptyTrash;
	}

	/**
	 * Check if show times in message list
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isTimeDisplayedInMsgList() {
		String show_times_in_msg_list_js = PropertyHelper.coreLoginFile
				.getPropertyValue("show_times_in_msg_list_js");
		boolean isTimeDisplayedInMsgList = false;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			String result = (String) js
					.executeScript(show_times_in_msg_list_js);
			if (result.equals("long"))
				isTimeDisplayedInMsgList = true;
			else if (result.equals("short"))
				isTimeDisplayedInMsgList = false;
			Logging.info("Is show times(date) in message list : "
					+ isTimeDisplayedInMsgList);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return isTimeDisplayedInMsgList;
	}

	/**
	 * Check if auto save out going msg
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isAutoSaveOutGoingMsg() {
		String auto_save_out_going_msg_js = PropertyHelper.coreLoginFile
				.getPropertyValue("auto_save_out_going_msg_js");
		boolean isAutoSaveOutGoingMsg = false;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			isAutoSaveOutGoingMsg = (boolean) js
					.executeScript(auto_save_out_going_msg_js);
			Logging.info("Is auto save out going msg : "
					+ isAutoSaveOutGoingMsg);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return isAutoSaveOutGoingMsg;
	}

	/**
	 * Check if auto save new contact
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isAutoSaveNewContact() {
		String auto_save_new_contact_js = PropertyHelper.coreLoginFile
				.getPropertyValue("auto_save_new_contact_js");
		boolean isAutoSaveOutGoingMsg = false;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			String result = (String) js.executeScript(auto_save_new_contact_js);
			if (result.equals("true"))
				isAutoSaveOutGoingMsg = true;
			Logging.info("Is auto save new contact : " + isAutoSaveOutGoingMsg);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return isAutoSaveOutGoingMsg;
	}

	/**
	 * Check if use device time zone
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isUseDeviceTimeZone() {
		String use_device_time_zone_js = PropertyHelper.coreLoginFile
				.getPropertyValue("use_device_time_zone_js");
		boolean isDeviceTimeZone = false;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			String result = (String) js.executeScript(use_device_time_zone_js);
			if (result.equals("true"))
				isDeviceTimeZone = true;
			else
				isDeviceTimeZone = false;
			Logging.info("Is use device time zone : " + isDeviceTimeZone);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return isDeviceTimeZone;
	}

	/**
	 * Check if it is auto suggest contact
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isAutoSuggestContact() {
		String auto_suggest_js = PropertyHelper.coreLoginFile
				.getPropertyValue("auto_suggest_js");
		boolean isAutoSuggest = false;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			String result = (String) js.executeScript(auto_suggest_js);
			if (result.equals("true"))
				isAutoSuggest = true;
			Logging.info("Is use auto-suggest : " + isAutoSuggest);
		} catch (WebDriverException ex) {
			Logging.error("Can not check if auto suggest");
			ex.printStackTrace();
			throw ex;
		}
		return isAutoSuggest;

	}

	/**
	 * Check if use conversation view
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isConversionViewPattern() {
		String conversition_view_enable = PropertyHelper.coreLoginFile
				.getPropertyValue("conversition_view_enable");
		boolean isConversationViewEnable = false;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			String result = (String) js.executeScript(conversition_view_enable);
			if (result.equals("true"))
				isConversationViewEnable = true;
			Logging.info("Is use conversation-view : "
					+ isConversationViewEnable);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return isConversationViewEnable;
	}
	/**
	 * Check if use auto reply message
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isAutoReplyMessage() {
		String auto_reply_enable_js = PropertyHelper.coreLoginFile
				.getPropertyValue("auto_reply_enable_js");
		boolean isAutoreplyEnable = false;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			isAutoreplyEnable = (Boolean) js.executeScript(auto_reply_enable_js);
			Logging.info("Is use auto reply disabled : "
					+ isAutoreplyEnable);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return isAutoreplyEnable;
	}


	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: 12-30-2015<br>
	 * <b>Description</b> Get user dateFormat throw JS after login<br>
	 * 
	 * @param
	 * @return
	 */
	public String getUserDateFormat() {
		String date_format_js = PropertyHelper.coreLoginFile
				.getPropertyValue("date_format_js");
		String userDateFormat = null;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			userDateFormat = (String) js.executeScript(date_format_js);
			if (userDateFormat == null)
				throw new NullPointerException();
		} catch (WebDriverException ex) {
			Logging.info("Can not get user date format");
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Get user Date format as : " + userDateFormat);

		return userDateFormat;
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: 12-30-2015<br>
	 * <b>Description</b> Get user timeZone throw JS after login<br>
	 * 
	 * @param
	 * @return
	 */
	public ZoneId getUserTimeZone() {
		String time_zone_js = PropertyHelper.coreLoginFile
				.getPropertyValue("time_zone_js");
		Object scriptResult = null;
		String jsonTimeZone = null;
		ZoneId zoneid = null;
		try {
			if (!isUseDeviceTimeZone()) {
				Tools.setDriverDefaultValues();
				JavascriptExecutor js = (JavascriptExecutor) Base.base
						.getDriver();
				scriptResult = (Object) js.executeScript(time_zone_js);
				if (scriptResult == null)
					throw new NullPointerException();
				jsonTimeZone = scriptResult.toString();
				Logging.info("get time zone as : " + jsonTimeZone);
				jsonTimeZone = jsonTimeZone.substring(
						jsonTimeZone.lastIndexOf("=") + 1,
						jsonTimeZone.lastIndexOf("}"));
				zoneid = ZoneId.of(jsonTimeZone);
			} else {
				String time_zone_local_js = PropertyHelper.coreLoginFile
						.getPropertyValue("time_zone_local_js");
				JavascriptExecutor js = (JavascriptExecutor) Base.base
						.getDriver();
				scriptResult = (String) js.executeScript(time_zone_local_js);
				if (scriptResult == null)
					throw new NullPointerException();
				zoneid = ZoneId.of((String) scriptResult);

			}
			Logging.info("Get user Time zone id as " + zoneid.toString());
		} catch (WebDriverException ex) {
			Logging.info("Can not get user time zone");
			throw ex;
		}
		return zoneid;
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: 12-30-2015<br>
	 * <b>Description</b> Get user timeFormat throw JS after login<br>
	 * 
	 * @param
	 * @return
	 */
	public String getUserTimeFormat() {
		String time_format_js = PropertyHelper.coreLoginFile
				.getPropertyValue("time_format_js");
		String usertimeFormat = null;
		String checkBox = "false";
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			checkBox = (String) js.executeScript(time_format_js);
		} catch (WebDriverException ex) {
			Logging.info("Can not get user time format");
			throw ex;
		}
		if (checkBox.equalsIgnoreCase("true"))
			usertimeFormat = ConstantName.HOUR24FORMAT;
		else
			usertimeFormat = ConstantName.HOUR12FORMAT;
		Logging.info("Get user Time format as : " + usertimeFormat);

		return usertimeFormat;
	}
	
  /**
   * Get the block sender max count
   * 
   * @author Fiona Zhang
   */
  public int getBlockSenderMaxCount() {
    String block_sender_max_count =
        PropertyHelper.coreLoginFile.getPropertyValue("block_sender_max_count");
    Long maxCount = null;
    try {
      JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
      maxCount = (Long) js.executeScript(block_sender_max_count);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    Logging.info("block sender max count is : " + maxCount);
    return maxCount.intValue();
  }

  /**
   * Get the allow sender max count
   * 
   * @author Fiona Zhang
   */
  public int getAllowSenderMaxCount() {
    String allow_sender_max_count =
        PropertyHelper.coreLoginFile.getPropertyValue("allow_sender_max_count");
    Long maxCount = null;
    try {
      JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
      maxCount = (Long) js.executeScript(allow_sender_max_count);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    Logging.info("safe sender max count is : " + maxCount);
    return maxCount.intValue();
  }

	/**
	 * <b>Author</b>: <br>
	 * <b>Date</b>: <br>
	 * <b>Description</b><br>
	 * 
	 * @param
	 * @return
	 */
	public void typeUsername(String username) {
		Logging.info("Typing login user name: " + username);
		String login_username_field = PropertyHelper.coreLoginFile
				.getPropertyValue("username_field");
		try {
			Tools.setDriverDefaultValues();
			WebElement userNameElement = this.getWebElement(login_username_field, true);
			userNameElement.sendKeys(username.trim());
		} catch (WebDriverException ex) {
			Logging.error("Could not type login username: " + username);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: <br>
	 * <b>Date</b>: <br>
	 * <b>Description</b><br>
	 * 
	 * @param
	 * @return
	 */
	public void typePassword(String password) {
		Logging.info("Typing login password: " + password);
		String login_password_field = PropertyHelper.coreLoginFile
				.getPropertyValue("password_field");
		try {
			Tools.setDriverDefaultValues();
            WebElement passwdElement = this.getWebElement(login_password_field, true);
            passwdElement.sendKeys(password.trim());
		} catch (WebDriverException ex) {
			Logging.error("Could not type login password: " + password);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: <br>
	 * <b>Date</b>: <br>
	 * <b>Description</b><br>
	 * 
	 * @param
	 * @return
	 */
	public void clickLoginButton() {
		Logging.info("Clicking on Login Button");
		String login_login_button = PropertyHelper.coreLoginFile
				.getPropertyValue("login_button");
		try {
			Tools.setDriverDefaultValues();
			WebElement loginButtonElement = this.getWebElement(login_login_button, true);
			this.clickElement(loginButtonElement);
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on login button");
			throw ex;
		}
	}

	/*************************************************************************
	 * @Method Name : waitForLoginPageToLoad
	 * @author : Jerry Zhang
	 * @Created on : 09/15/2014
	 * @Description : This method is used to make the automation wait until the
	 *              Login page has completely loaded before executing next
	 *              action and then select default language to be English.
	 * @Parameter 1: int timeoutInSeconds - value specified in seconds to wait
	 **************************************************************************/
	public void waitForLoginPageToLoad(int timeoutInSeconds) {
		Logging.info("Waiting for login page to load, timeout seconds: "
				+ timeoutInSeconds);
		// Verify login page loaded then change language.
		boolean loginPageLoaded = this.verifyLoginPageLoaded(timeoutInSeconds);
		// if (loginPageLoaded) {
		// this.selectLanguageAsEnglishOnLoginPage();
		// Verify login page loaded again after language changing.
		// this.verifyLoginPageLoaded(timeoutInSeconds);
		// }
		Logging.info("load login page :  "+loginPageLoaded);
		if (!loginPageLoaded) {
			Logging.info("Loading login page failed");
			throw new WebDriverException();
		} else
			Logging.info("Loading finished");
	}

	/*************************************************************************
	 * @Method Name : verifyLoginPageLoaded
	 * @author : Jerry Zhang
	 * @Created on : 09/15/2014
	 * @Description : This method will verify whether login page loaded
	 *              correctly.
	 * @Parameter 1: int timeoutInSeconds - value specified in seconds to wait
	 * @Return: boolean - true: page loaded / false: page not loaded
	 **************************************************************************/
	public boolean verifyLoginPageLoaded(int timeoutInSeconds) {
		Logging.info("Start to verify if the login page loadded");
		boolean result = false;
		String login_username_field = PropertyHelper.coreLoginFile
				.getPropertyValue("username_field");
		String login_password_field = PropertyHelper.coreLoginFile
				.getPropertyValue("password_field");
		String login_login_button = PropertyHelper.coreLoginFile
				.getPropertyValue("login_button");

		try {
			boolean flag1 = Tools.waitUntilElementDisplayedByXpath(
					login_username_field, timeoutInSeconds);
/*			if (!flag1) {
				throw new NoSuchElementException("");
			}

			boolean flag2 = Tools.waitUntilElementDisplayedByXpath(
					login_password_field, timeoutInSeconds);
			if (!flag2) {
				throw new NoSuchElementException("");
			}

			boolean flag3 = Tools.waitUntilElementDisplayedByXpath(
					login_login_button, timeoutInSeconds);
			if (!flag3) {
				throw new NoSuchElementException("");
			}
*/
			// boolean flag4 =
			// Tools.waitUntilElementDisplayedByXpath(login_language_box,
			// timeoutInSeconds);
			// if (!flag4) { throw new NoSuchElementException(""); }

			// All core elements loaded.
			result = flag1 ;
		} catch (WebDriverException ex) {
			Logging.warn("Login page could not be confirmed as loaded");
			throw new WebDriverException();
		}

		if (result) {
			Logging.info("Login page loaded");
		}
		return result;
	}


}
