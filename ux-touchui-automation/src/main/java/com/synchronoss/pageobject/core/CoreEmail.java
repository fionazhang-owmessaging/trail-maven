package synchronoss.com.pageobject.core;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import io.appium.java_client.ios.IOSDriver;
import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class CoreEmail extends PageObjectBase {

  /**
   * Click write email button
   * 
   * @author Fiona Zhang
   */
  public void clickWriteEmailButton() {
    Logging.info("Click compose email button");
    String mail_compose_button = null;

    if (Base.deviceName.toLowerCase().contains(ConstantName.IPAD))
      mail_compose_button =
          PropertyHelper.coreEmailFile.getPropertyValue("ipad_mail_compose_button");
    else
      mail_compose_button = PropertyHelper.coreEmailFile.getPropertyValue("mail_compose_button");

    try {
      WebElement compose_button = this.getWebElement(mail_compose_button, true);
      this.clickElement(compose_button);
      if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
        IOSDriver iosDriver = (IOSDriver) Base.base.getDriver();
        iosDriver.hideKeyboard();
      }
    } catch (WebDriverException ex) {
      Logging.info("Cannot click write email button");
      throw ex;
    }
  }

	/**
	 * Click search button on email list view
	 * 
	 * @author Fiona Zhang
	 */
	public void clickSearchEmailButton() {
		Logging.info("Click search email button");

		String mail_search_button = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_search_button");
		try {
			WebElement searchButtonElement = this.getWebElement(
					mail_search_button, true);
			 this.clickElement(searchButtonElement);
		} catch (WebDriverException ex) {
			Logging.info("Cannot click search email button");
			throw ex;
		}
	}

	/**
	 * Click the icon on bottom of the email list
	 * 
	 * @author Fiona Zhang
	 */
	public void clickBottomButton(String iconName) {
		Logging.info("Click " + iconName + " button on bottom bar ");
		String action = null;
		switch (iconName) {
		case ConstantName.MORE:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("mail_bottom_more_button");
			break;
		case ConstantName.FLAG:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("mail_bottom_flag_button");
			break;
		case ConstantName.DELETE:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("mail_bottom_trash_button");
			break;
		}
		try {
			WebElement toolbarIconElement = getWebElement(action, false);
			 this.clickElement(toolbarIconElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}

	/**
	 * Click mail control bar beyond mail item, like
	 * read/unread/more/flag/delete, etc.
	 * 
	 * @param option
	 *            the option beyond mail item after swipe, like
	 *            read/unread/delete/flag/more
	 * 
	 * @author Fiona Zhang
	 */
	public void clickMailControlBar(String option) {
		Logging.info("Click the button " + option + " on mail list item");
		String action = null;
		switch (option) {
		case ConstantName.READ:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("mail_simplist_item_control_option_read");
			break;
		case ConstantName.UNREAD:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("mail_simplist_item_control_option_unread");
			break;
		case ConstantName.DELETE:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("mail_simplist_item_control_option_delete");
			break;
		case ConstantName.FLAG:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("mail_simplist_item_control_option_flag");
			break;
		case ConstantName.UNFLAG:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("mail_simplist_item_control_option_unflag");
			break;
		case ConstantName.MORE:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("mail_simplist_item_control_option_more");
			break;

		}
		try {
			Tools.scrollElementIntoViewByXpath(action);
			WebElement actionButtonElement = this.getWebElement(action, true);
			 this.clickElement(actionButtonElement);
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click get a new message button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickMailRefershBar() {
		Logging.info("Click new message bar on top of mail list view");

		String mail_refresh_bar = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_refresh_bar");
		try {
			WebElement searchButtonElement = this.getWebElement(
					mail_refresh_bar, true);
			 this.clickElement(searchButtonElement);
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click edit mail list button, trigger/untrigger the mail list to multiple
	 * choice mode
	 * 
	 * @author Fiona Zhang
	 */
	public void clickEditMailListButton() {
		Logging.info("Click edit mail list button");

		String multiple_mail_edit_button = PropertyHelper.coreEmailFile
				.getPropertyValue("multiple_mail_edit_button");
		try {
			WebElement editMailListElement = this.getWebElement(
					multiple_mail_edit_button, true);
			 this.clickElement(editMailListElement);
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Input search keyword
	 * 
	 * @param keyword
	 *            Input the keyword of search
	 * 
	 * @author Fiona Zhang
	 */
	public void addSearchKeyword(String keyword) {
		Logging.info("Input search keyword " + keyword + " in search field");

		String keyword_in_search_field = PropertyHelper.coreEmailFile
				.getPropertyValue("keyword_in_search_field");
		try {
			WebElement searchFieldElement = this.getWebElement(
					keyword_in_search_field, true);
			searchFieldElement.clear();
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
			searchFieldElement.sendKeys(keyword);
			}
			else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
				this.elementSendKeys(searchFieldElement,keyword);
			}
			searchFieldElement.sendKeys(Keys.ENTER);

		} catch (WebDriverException ex) {
			Logging.info("Cannot input the keyword");
			throw ex;
		}
	}

	/**
	 * Input search keyword, and click search button, if the search bar is not
	 * display, click search button first
	 * 
	 * @param keyword
	 *            Input the keyword of search
	 * 
	 * @author Fiona Zhang
	 */
	public void searchKeyword(String keyword) {
		Logging.info("Search keyword " + keyword);

		String keyword_in_search_field_component = PropertyHelper.coreEmailFile
				.getPropertyValue("keyword_in_search_field_component");
		try {
			WebElement searchFieldElement = this.getWebElement(
					keyword_in_search_field_component, false);
			if (!searchFieldElement.getAttribute("style").equals("")) {
				clickSearchEmailButton();
			}
			addSearchKeyword(keyword);
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.info("Cannot search keyword");
			throw ex;
		}
	}

	/***************************************************************************
	 * @Method Name: ListentoNewEmail
	 * @Author : Jason Song
	 * @Created : 09/18/14
	 * @Description : This method is used for receive email with special subject
	 *              and return boolean value for found or not found
	 * @parameter 1 : represent trying to receive email times
	 * @parameter 2 : represent latency time for each receiving email
	 * @parameter 2 : represent email subject for searching
	 ***************************************************************************/
	public boolean listentoNewEmail(int tryTimes, int interval, String subject) {
		boolean is_found = false;
		int interation = 0;
		String mailSubject = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_item_bysubject");
		mailSubject = String.format(mailSubject, subject.trim());
		Logging.info("Expected email subject is:" + subject);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().manage().timeouts()
					.implicitlyWait(Base.timeoutMedium, TimeUnit.SECONDS);
			refresh_maillist();

			List<WebElement> targetList = getWebElements(mailSubject, false);
			while (tryTimes-- > 0) {
				Logging.info("Detecting email iteration:"
						+ Integer.toString(++interation));
				Logging.info("emal subject>> :" + mailSubject);
				if (0 == targetList.size()) {
					Tools.waitFor(Base.timeoutMedium, TimeUnit.MILLISECONDS);
				} else {
					is_found = true;
					break;
				}

				refresh_maillist();
				Tools.waitFor(Base.timeoutMedium, TimeUnit.MILLISECONDS);

				targetList = getWebElements(mailSubject, false);
				Logging.info("Get email list as " + targetList.toString());
			}
			Tools.setDriverDefaultValues();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

		return is_found;
	}

	/***************************************************************************
	 * @Method Name: refresh_maillist
	 * @Author : Jason Song
	 * @Created : 09/23/14
	 * @Description : Refresh mail list, to callback update status of mail
	 *              status, or recieve new mails.
	 * 
	 ***************************************************************************/

	public void refresh_maillist() {
		/*
		 * String refreshbtElement = PropertyHelper.coreEmailFile
		 * .getPropertyValue("mail_refresh_button"); String uinterface =
		 * PropertyHelper.getUserInterface();
		 */String touch_refresh_div = PropertyHelper.coreEmailFile
				.getPropertyValue("touch_refresh_div");
		try {
			Tools.setDriverDefaultValues();
			WebElement refreshdiv = getWebElement(touch_refresh_div, false);
			String refreshjs_statement = PropertyHelper.coreEmailFile
					.getPropertyValue("refreshjs");
			refreshjs_statement = String.format(refreshjs_statement,
					refreshdiv.getAttribute("id"));
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			Logging.info("Refresh mail list.");
			executor.executeScript(refreshjs_statement);

/*			String newmessagebar_div = PropertyHelper.coreEmailFile
					.getPropertyValue("newmessage_bar");
			List<WebElement> newmessagebar = getWebElements(newmessagebar_div,
					false);
			if (newmessagebar.size() > 0) {
				if (newmessagebar.get(0).isDisplayed()) {
					Logging.info("Click new message bar.");
					newmessagebar.get(0) this.clickElement();
				}

			}*/
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click subject name in mail list view
	 *
	 * @param subject
	 *            The email subject
	 * 
	 * @author Fiona Zhang
	 */
	public void clickEmailBySubject(String subject) {
		Logging.info("Click the email by subject: " + subject
				+ " in mail list view");
		String mail_subject = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_subject");
		mail_subject = String.format(mail_subject, subject);
		try {
			Tools.scrollElementIntoViewByXpath(mail_subject);
			WebElement mailSubElement = this.getWebElement(mail_subject, true);
			 this.clickElement(mailSubElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.info("Cannot click write email button");
			throw ex;
		}
	}

	/**
	 * Click subject name in mail search result list view
	 *
	 * @param subject
	 *            The email subject
	 * 
	 * @author Fiona Zhang
	 */
	public void clickEmailBySubjectInSearchResult(String subject) {
		String search_result_subj = PropertyHelper.coreEmailFile
				.getPropertyValue("search_result_subj");
		search_result_subj = String.format(search_result_subj, subject);
		try {
			Tools.scrollElementIntoViewByXpath(search_result_subj);
			WebElement mailSubElement = this.getWebElement(search_result_subj,
					true);
			 this.clickElement(mailSubElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.info("Cannot click write email button");
			throw ex;
		}
	}

	/**
	 * Click the check box by subject
	 *
	 * @param subject
	 *            The email subject
	 * 
	 * @author Fiona Zhang
	 */
	public void clickEmailCheckBox(String subject) {
		Logging.info("Click the check box of : " + subject);
		String mail_check_button = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_check_button");
		mail_check_button = String.format(mail_check_button, subject);
		try {
			Tools.scrollElementIntoViewByXpath(mail_check_button);
			WebElement mailSubElement = this.getWebElement(mail_check_button,
					true);
			 this.clickElement(mailSubElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click popup menu in mail list view like reply,flag,move,etc.
	 *
	 * @param option
	 *            The option of popup menu
	 * 
	 * @param ifFromMore
	 *            true - its from more popup false - its not from popup
	 * 
	 * @author Fiona Zhang
	 */
	public void clickPopupMenuLabel(String option, boolean ifFromMore) {
		Logging.info("Click the button " + option + " on poup list");
		String action = null;
		switch (option) {
		case ConstantName.REPLY:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("more_popup_reply_button");
			break;
		case ConstantName.FLAG:
			if (ifFromMore)
				action = PropertyHelper.coreEmailFile
						.getPropertyValue("more_popup_flag_button");
			else
				action = PropertyHelper.coreEmailFile
						.getPropertyValue("popup_flag_button");
			break;
		case ConstantName.UNFLAG:
			if (ifFromMore)
				action = PropertyHelper.coreEmailFile
						.getPropertyValue("more_popup_unflag_button");
			else
				action = PropertyHelper.coreEmailFile
						.getPropertyValue("popup_clear_flag_button");
			break;
		case ConstantName.READ:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("more_popup_read_button");
			break;
		case ConstantName.UNREAD:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("more_popup_unread_button");
			break;
		case ConstantName.MOVE:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("more_popup_move_button");
			break;
		case ConstantName.SPAM:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("more_popup_junk_button");
			break;
		case ConstantName.NOTSPAM:
			action = PropertyHelper.coreEmailFile
					.getPropertyValue("more_popup_not_junk_button");
			break;
		case ConstantName.CANCEL:
			if (ifFromMore)
				action = PropertyHelper.coreEmailFile
						.getPropertyValue("more_popup_cancel_button");
			else
				action = PropertyHelper.coreEmailFile
						.getPropertyValue("popup_cancel_button");
			break;

		}
		try {
			Tools.scrollElementIntoViewByXpath(action);
			WebElement actionButtonElement = this.getWebElement(action, true);
			 this.clickElement(actionButtonElement);
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Get email receive time
	 *
	 * @param subject
	 *            The email subject
	 * 
	 * @author Fiona Zhang
	 */
	public String getEmailReceiveTimeInMailList(String subject) {
		String mail_receive_in_mail_list = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_receive_in_mail_list");
		mail_receive_in_mail_list = String.format(mail_receive_in_mail_list,
				subject);
		String result = null;
		try {
			Tools.scrollElementIntoViewByXpath(mail_receive_in_mail_list);
			WebElement mailSubElement = this.getWebElement(
					mail_receive_in_mail_list, true);
			result = mailSubElement.getText();
			Logging.info("Get email receive time in mail list view as : "
					+ result);
		} catch (WebDriverException ex) {
			throw ex;
		}
		return result;
	}

	/**
	 * The function will return current status of the email, including
	 * read/unread/fwd/reply
	 *
	 * @param subject
	 *            The subject of the email
	 * 
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getEmailStatus(String subject) {
		ArrayList<String> status = new ArrayList<String>();
		String mail_status = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_status");
		String mail_subject = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_subject");
		mail_status = String.format(mail_status, subject);
		mail_subject = String.format(mail_subject, subject);

		try {
			Tools.scrollElementIntoViewByXpath(mail_subject);
			List<WebElement> mailSubElement = this.getWebElements(mail_status,
					true);
			if (mailSubElement.size() == 0)
				status.add("read");
			else {
				for (WebElement el : mailSubElement) {
					Logging.info(el.getAttribute("class"));
					if (el.getAttribute("class").contains("reply"))
						status.add("reply");
					else if (el.getAttribute("class").contains("unread"))
						status.add("unread");
					else if (el.getAttribute("class").contains("forward"))
						status.add("forward");
					else if (el.getAttribute("class").contains("re-fwd")) {
						status.add("reply");
						status.add("forward");
					}
				}
				if (!status.contains("unread"))
					status.add("read");
			}
		} catch (WebDriverException ex) {
			Logging.info("Cannot click write email button");
			throw ex;
		}
		for (String s : status)
			Logging.info("subject of : " + subject + " is " + s);
		return status;
	}

	/**
	 * Get email subject list in search result list
	 * 
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getEmailSubjectsInSearchList() {
		Logging.info("Get email subject list in search result list");
		String search_result_subj_list = PropertyHelper.coreEmailFile
				.getPropertyValue("search_result_subj_list");
		ArrayList<String> subjList = new ArrayList<String>();
		try {

			List<WebElement> subjListElement = this.getWebElements(
					search_result_subj_list, false);
			for (WebElement el : subjListElement) {
				subjList.add(el.getText());
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info(subjList.toString());
		return subjList;

	}

	/**
	 * Get conversation view thread number
	 * 
	 * @author Fiona Zhang
	 */
	public int getConversionViewThreadCount(String subject) {
		String mail_conversion_view_thread_count = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_conversion_view_thread_count");
		mail_conversion_view_thread_count = String.format(
				mail_conversion_view_thread_count, subject);
		try {
			Tools.scrollElementIntoViewByXpath(mail_conversion_view_thread_count);
			WebElement conversionViewCount = this.getWebElement(
					mail_conversion_view_thread_count, true);
			int count = Integer.parseInt(conversionViewCount.getText());
			Logging.info("Get conversation view thread count number as : "
					+ count);
			return count;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get email subject list in email list
	 * 
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getEmailSubjectsInMailList() {
		Logging.info("Get email subject list in mail list view");
		String mail_list_items = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_list_items");
		ArrayList<String> subjList = new ArrayList<String>();
		try {

			List<WebElement> subjListElement = this.getWebElements(
					mail_list_items, true);
			for (WebElement el : subjListElement) {
				subjList.add(el.getText());
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info(subjList.toString());
		return subjList;

	}

	/**
	 * Check if the new message bar is displayed or not
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isNewMessageWarnAppear() {
		String mail_refresh_bar = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_refresh_bar");
		try {
			Tools.scrollElementIntoViewByXpath(mail_refresh_bar);
			WebElement mailSubElement = this.getWebElement(mail_refresh_bar,
					true);
			Logging.info("New message notification bar displayed : "
					+ mailSubElement.isDisplayed());
			return mailSubElement.isDisplayed();

		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * check if current mail list displayed as conversion view pattern
	 * 
	 * @param subject
	 *            The subject of the checked items
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isConversionViewDisplayed(String subject) {
		String mail_conversion_view_thread_count = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_conversion_view_thread_count");
		mail_conversion_view_thread_count = String.format(
				mail_conversion_view_thread_count, subject);
		boolean result = false;
		try {
			Tools.scrollElementIntoViewByXpath(mail_conversion_view_thread_count);
			List<WebElement> conversionViewCount = this.getWebElements(
					mail_conversion_view_thread_count, true);
			if (conversionViewCount.size() == 0)
				result = false;
			else
				result = conversionViewCount.get(0).isDisplayed();

			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Check if the mail is flagged or not
	 * 
	 * @param subject
	 *            The subject of the checked items
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isMailFlagged(String subject) {
		String mail_flag_icon = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_flag_icon");
		mail_flag_icon = String.format(mail_flag_icon, subject);
		boolean result = false;
		try {
			Tools.scrollElementIntoViewByXpath(mail_flag_icon);
			List<WebElement> mailFlagCount = this.getWebElements(
					mail_flag_icon, true);
			if (mailFlagCount.size() == 0)
				result = false;
			else
				result = mailFlagCount.get(0).isDisplayed();

			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Get the mail list title
	 * 
	 * @author Fiona Zhang
	 */
	public String getMailListTitle() {
		String mail_list_title = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_list_title");
		try {
			WebElement mailSubElement = this.getWebElement(mail_list_title,
					true);
			String result = mailSubElement.getText();
			Logging.info("Get the mail list title as : " + result);
			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}
	
	
  /**
   * Get mail empty detail info for tablet UI
   * 
   * @Description: In mail view, click edit button, and select 2 msgs, on the right of the
   *           view,"2 message(s) selected" displayed
   * 
   * @author Fiona Zhang
   */
  public String getEmailEmptyDetailForTablet() {
    String ipad_mail_empty_detail =
        PropertyHelper.coreEmailFile.getPropertyValue("ipad_mail_empty_detail");
    try {
      WebElement mailEmptyDetail = this.getWebElement(ipad_mail_empty_detail, true);
      String result = mailEmptyDetail.getText();
      Logging.info("Get the mail empty detail as : " + result);
      return result;
    } catch (WebDriverException ex) {
      throw ex;
    }
  }

  
	/**
	 * Get the total count of email number in top bar
	 * 
	 * @author Fiona Zhang
	 */
	public int getTotalCountOfEmailNumber() {
		int totalCount = 0;
		String mail_count_number = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_count_number");
		try {
			WebElement mailCountElement = this.getWebElement(mail_count_number,
					true);
			String mailCount = mailCountElement.getText();
			mailCount = mailCount.substring(mailCount.indexOf('(') + 1,
					mailCount.lastIndexOf(')'));
			totalCount = Integer.valueOf(mailCount);
			Logging.info("Get the mail list title as : " + totalCount);
		} catch (WebDriverException ex) {
			throw ex;
		}
		return totalCount;
	}

	/**
	 * Get the priority of subject
	 * 
	 * @param subject
	 *            The subject of the checked items
	 * 
	 * @author Fiona Zhang
	 */
	public String getEmailPriority(String subject) {
		String mail_priority_icon = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_priority_icon");
		mail_priority_icon = String.format(mail_priority_icon, subject);
		String result = null;
		try {
			Tools.scrollElementIntoViewByXpath(mail_priority_icon);
			WebElement priorityElement = getWebElement(mail_priority_icon,
					false);
			String tmp = priorityElement.getAttribute("class");
			if (tmp.contains(ConstantName.LOW))
				result = ConstantName.LOW;
			else if (tmp.contains(ConstantName.NORMAL))
				result = ConstantName.NORMAL;
			else if (tmp.contains(ConstantName.HIGH))
				result = ConstantName.HIGH;
			Logging.info("Get the priority place class name as " + tmp);
		} catch (WebDriverException ex) {
			throw ex;
		}
		return result;
	}

	public void logoutSessionByJS() {
		String logout_js = PropertyHelper.coreEmailFile
				.getPropertyValue("logout_js");
		try {
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			js.executeScript(logout_js);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get the priority of subject
	 * 
	 * @param subject
	 *            The subject of the mail
	 * @param direction
	 *            The swipe direction, should be value of right or left
	 * 
	 * @author Fiona Zhang
	 */
	public void swipeEmailByName(String subject, String direction) {
		Logging.info("Swipe " + direction + " on " + subject
				+ " mail item and trigger slide bar...");

		String mail_simplist_item = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_simplist_item");
		mail_simplist_item = String.format(mail_simplist_item, subject);

		try {
			WebElement contactItemElement = getWebElement(mail_simplist_item,
					false);
			String mailItemID = contactItemElement.getAttribute("id");

			String swipe_mail_js = PropertyHelper.coreEmailFile
					.getPropertyValue("swipe_mail_js");
			swipe_mail_js = String.format(swipe_mail_js, mailItemID, direction);
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			executor.executeScript(swipe_mail_js);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}


  /**
   * Click on account collapse menu
   * 
   * @Method Name: clickAccountCollapseMenu
   * @Author Vivian Xie
   * @Created Feb/10/2016
   * 
   */
  public void clickAccountCollapseMenu() {
    Logging.info("Starting to click on account collapse menu");
    String mail_menu_account_collapse =
        PropertyHelper.coreEmailFile.getPropertyValue("mail_menu_account_collapse");
    try {
      Tools.scrollElementIntoViewByXpath(mail_menu_account_collapse);
      getWebElement(mail_menu_account_collapse, false).click();

    } catch (WebDriverException ex) {
      ex.printStackTrace();
      Logging.info("Failed to click on account collapse menu");
      throw ex;
    }
  }


  /**
   * Click on specific account on mail menu page
   * 
   * @Author Vivian Xie
   * @Created Feb/10/2016
   * @param accountName -- the account Name to be click on
   */
  public void clickAccountByNameOnMailMenuPage(String accountName) {
    Logging.info("Starting to click on account " + accountName);
    String mail_menu_account_by_name = PropertyHelper.coreEmailFile
        .getPropertyValue("mail_menu_account_by_name").replace("+variable+", accountName);
    try {
      Logging.info(mail_menu_account_by_name);
      Tools.scrollElementIntoViewByXpath(mail_menu_account_by_name);
      getWebElement(mail_menu_account_by_name, false).click();
      waitForLoadMaskDismissed(); 

    } catch (WebDriverException ex) {
      ex.printStackTrace();
      Logging.info("Failed to click account " + accountName);
      throw ex;
    }
  }
  
  
  /**
   * return the src of the fist image in the mail body
   * 
   * @Author Vivian Xie
   * @Created Mar/3rd/2017
   */
  public String getFirstImageSrcInMailContent() {
    Logging.info("Starting to get the src of the first img in the mail body.");
    String mail_img = PropertyHelper.coreEmailDetail.getPropertyValue("msg_attachment_img");
    String src = null;
    try {
      src = getWebElements(mail_img, true).get(0).getAttribute("src");
      waitForLoadMaskDismissed();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      Logging.info("Failed to get image src ");
      throw ex;
    } 
    Logging.info("The src of the first image is "+src );
    return src;
  }

}
