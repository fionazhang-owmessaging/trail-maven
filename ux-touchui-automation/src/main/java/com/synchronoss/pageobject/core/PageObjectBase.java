package synchronoss.com.pageobject.core;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.interactions.touch.TouchActions;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class PageObjectBase {

	// Definitions for timeout value that will be used mainly in page
	// objects class for waiting actions.
	protected final int timeoutShort = Base.timeoutShort;
	protected final int timeoutMedium = Base.timeoutMedium;
	protected final int timeoutLong = Base.timeoutLong;

	protected final String YES = "yes";
	protected final String NO = "no";

	protected final String WORK = "work";
	protected final String HOME = "home";
	protected final String OTHER = "other";

	public enum ContactItemType {
		WORK, HOME, OTHER
	};

	public enum Action {
		SAVE, CANCEL
	};

	public enum ActionPop {
		OK, CANCEL
	};

	/*
	 * public enum ContactArea{
	 * Email,Mobile,Phone,Address,Personal,Fax,Website,Other,Chat };
	 */
	// Definition for emoji value
	public static final String emoji_263a = "sprite U-_263a";

	/**
	 * @Method_Name: getWebElement
	 * @Author Fiona Zhang
	 * @Created_Date: 12/18/2015
     * @Description This method find the element, and return the element
	 * @Param xpath : xpath of the elements waitLoadMask : true - Will wait
	 *        until the loading mask disappear, false - Do NOT wait loading mask
	 *        =
	 */
	WebElement getWebElement(String xpath, boolean waitLoadMask) {
		WebElement el = null;
		try {
			Tools.setDriverDefaultValues();
			if (waitLoadMask) {
				waitForLoadMaskDismissed();
			}
			el = Base.base.getDriver().findElement(By.xpath(xpath));
		} catch (WebDriverException ex) {
			Logging.info("The element is not found");
			throw ex;
		}
		return el;
	}
	
	MobileElement getMobileElement(String xpath, boolean waitLoadMask) {
		MobileElement el = null;
		try {
			Tools.setDriverDefaultValues();
			if (waitLoadMask) {
				waitForLoadMaskDismissed();
			}
			el = (MobileElement)Base.base.getDriver().findElement(By.xpath(xpath));
		} catch (WebDriverException ex) {
			Logging.info("The element is not found");
			throw ex;
		}
		return el;
	}


	/**
	 * @Method_Name: getWebElements
	 * @Author Fiona Zhang
	 * @Created_Date: 12/18/2015
     * @Description This method find the element, and return the element
	 * @Param xpath : xpath of the elements waitLoadMask : true - Will wait
	 *        until the loading mask disappear, false - Do NOT wait loading mask
	 */
	List<WebElement> getWebElements(String xpath, boolean waitLoadMask) {
		List<WebElement> el = null;
		try {
			Tools.setDriverDefaultValues();
			if (waitLoadMask) {
				waitForLoadMaskDismissed();
			}
			el = Base.base.getDriver().findElements(By.xpath(xpath));
		} catch (WebDriverException ex) {
			Logging.info("The elements are not found");
			throw ex;
		}
		return el;
	}

	/**
	 * @Method_Name: waitForLoadMaskDismissed
	 * @Author Jerry Zhang
	 * @Created_Date: 03/17/2014
     * @Description This method will wait until the loading mask disappear
	 */
	public void waitForLoadMaskDismissed() {
		String common_load_mask = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("loading_mask");
		try {
			Tools.setDriverDefaultValues();
			Tools.waitUntilElementNotDisplayedByXpath(common_load_mask,
					this.timeoutLong);
		} catch (WebDriverException ex) {
			Logging.error("Fail to wait for load mask dismissed");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 08, 2015<br>
	 * <b>Description</b><br>
	 * Clicks button on message box popup. One of the following options should
	 * be supplied: - Yes - No - Cancel
	 * 
	 * @param button
	 *            button name
	 * @return void
	 */
	public void clickButtonOnMessageBox(String button) {
		Logging.info("click button " + button + " in pop up message box");
		if (!button.equals(ConstantName.OK))
			button = button.substring(0, 1).toUpperCase()
					+ button.substring(1).toLowerCase();
		Logging.info("Clicking on message box button: " + button);
		String common_msgbox_button = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("common_msgbox_button");
		common_msgbox_button = String.format(common_msgbox_button, button);

		try {
			Tools.setDriverDefaultValues();
			WebElement el = this.getWebElement(common_msgbox_button, true);
			this.clickElement(el);
			
			waitForLoadMaskDismissed();
		} catch (NoSuchElementException ex) {
			Logging.error("Could not click on message box button: " + button);
			throw ex;
		}
	}

	/**
	 * click 'x' button in popup input field to clear text
	 *
	 * @author Fiona Zhang
	 * 
	 */
	public void clickClearButtonOnMessageBox() {
		Logging.info("Click 'x' to clear text");
		String msgbox_field_clear_button = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("msgbox_field_clear_button");

		String msgbox_input_field = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("msgbox_input_field");

		try {

			WebElement clearAddrbookNameElement = this.getWebElement(
					msgbox_field_clear_button, false);
			this.clickElement(clearAddrbookNameElement);
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
			IOSDriver driver = (IOSDriver) Base.base.getDriver();
			driver.hideKeyboard();
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Get the message on pop up message box
	 *
	 * @author Fiona Zhang
	 * 
	 */
	public String getTextOnMessageBox() {
		Logging.info("Get message from popup message box");
		String msgbox_warning_msg = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("msgbox_warning_msg");

		try {
			WebElement textElement = this.getWebElement(msgbox_warning_msg,
					false);
			Logging.info("Get text on message pop up box as : " + textElement.getText() );
			return textElement.getText();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * get the boolean for auto suggest
	 *
	 * @author Fiona Zhang
	 */
	public boolean isAutoSuggestListDisplayed() {
		boolean isAutoSuggest = true;
		String auto_suggest_field = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("auto_suggest_field");
		try {
		    Tools.waitFor(2, TimeUnit.SECONDS);
			// get auto suggest element first, if get the element, check if it
			// is displayed
			List<WebElement> toFieldElement = this.getWebElements(
					auto_suggest_field, true);
			
			if (toFieldElement.size() != 0) {
			  Logging.info("auto suggest list " +  toFieldElement.get(0).getText());
				isAutoSuggest = toFieldElement.get(0).isDisplayed();
				Logging.info("The auto suggest list displayed : "
						+ isAutoSuggest);
			} else
				// or the element is not displayed
				isAutoSuggest = false;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return isAutoSuggest;
	}


	
	
  /**
   * Click the auto-suggest items
   * 
   * @param autoSuggestEmail the auto suggest email need to click
   *
   * @author Fiona Zhang
   */
  public void clickAutoSuggestContact(String autoSuggestEmail) {
    String auto_suggest_contact_item =
        PropertyHelper.pageObjectBaseFile.getPropertyValue("auto_suggest_contact_item");
    auto_suggest_contact_item = String.format(auto_suggest_contact_item, autoSuggestEmail);
    Logging.info("click auto suggest contact : " + auto_suggest_contact_item);
    try {
      Tools.scrollElementIntoViewByXpath(auto_suggest_contact_item);
      WebElement autoSuggestElement = getWebElement(auto_suggest_contact_item, true);
      this.clickElement(autoSuggestElement);
      if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
        IOSDriver iosDriver = (IOSDriver) Base.base.getDriver();
        iosDriver.hideKeyboard();
      }
    } catch (WebDriverException ex) {
      Logging.error("click auto suggest contact failed");
      throw ex;
    }
  }

	/**
	 * Inject a date time into the Date component of UI
	 *
	 * @param date
	 *            The date you want to input
	 * 
	 * @param dateXpath
	 *            The xpath for the component you want to set the value
	 * 
	 * @author Fiona Zhang
	 */
	protected void selectInDatePicker_ByJS(LocalDate date, String dateXpath) {
		Logging.info("select a date");

		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String expectDate = date.format(format);
		try {
			Tools.scrollElementIntoViewByXpath(dateXpath);
			WebElement datecompose = getWebElement(dateXpath, false);
			String elementId = datecompose.getAttribute("id");
			JavascriptExecutor setDate = (JavascriptExecutor) Base.base
					.getDriver();
			setDate.executeScript("Ext.getCmp('" + elementId
					+ "').setValue(new Date('" + expectDate + "'));");
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Inject a date time into the Date component of UI
	 *
	 * @param date
	 *            The date you want to input
	 * 
	 * @param dateXpath
	 *            The xpath for the component you want to set the value
	 * 
	 * @author Fiona Zhang
	 */
	protected void selectInDatePicker_ByJS(LocalDateTime datetime,
			String dateXpath) {
		Logging.info("select a date");

		DateTimeFormatter format = DateTimeFormatter
				.ofPattern("yyyy,MM,dd,HH,mm,ss,SS");
		String expectDate = datetime.format(format);
		Logging.info("will set date as : " + expectDate);
		try {
			Tools.scrollElementIntoViewByXpath(dateXpath);
			WebElement datecompose = getWebElement(dateXpath, false);
			String elementId = datecompose.getAttribute("id");
			JavascriptExecutor setDate = (JavascriptExecutor) Base.base
					.getDriver();
			setDate.executeScript("Ext.getCmp('" + elementId
					+ "').setValue(new Date(" + expectDate + "));");
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Set a value to the JS component
	 *
	 * @param xpath
	 *            The xpath for the component you want to set the value
	 * 
	 * @param componentValue
	 *            The value of the component
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	protected void selectItem_ByJS(String xpath, String componentValue) {
		String set_item_js = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("set_item_js");
		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(xpath);
			WebElement el = getWebElement(xpath, false);
			String itemId = el.getAttribute("id");
			set_item_js = String.format(set_item_js, itemId, componentValue);
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			Logging.info(set_item_js);
			js.executeScript(set_item_js);
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get system date by js
	 * 
	 * @author Fiona Zhang
	 */
	public LocalDate getSystemDate() {
		Logging.info("Get system date time");
		LocalDate sysDate = null;
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy/MM/dd");

		String get_sys_time_js = PropertyHelper.coreCalendar
				.getPropertyValue("get_sys_time_js");
		String scriptResult = null;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			scriptResult = (String) js.executeScript(get_sys_time_js);
			Logging.info("system time is : " + scriptResult);
			if (scriptResult == null)
				throw new NullPointerException();
			sysDate = LocalDate.parse(scriptResult, format);
			Logging.info("get system date as : " + sysDate);
		} catch (WebDriverException ex) {
			Logging.info("Can not get system date");
			throw ex;
		}
		return sysDate;
	}
	
	/**
	 * Get system date by js
	 * 
	 * @author Fiona Zhang
	 */
	public LocalDateTime getSystemDate_2() {
		Logging.info("Get system date time");
		LocalDateTime sysDate = null;
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");

		String get_sys_time_js = PropertyHelper.coreCalendar
				.getPropertyValue("get_sys_time_js_2");
		String scriptResult = null;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			scriptResult = (String) js.executeScript(get_sys_time_js);
			Logging.info("system time is : " + scriptResult);
			if (scriptResult == null)
				throw new NullPointerException();
			sysDate = LocalDateTime.parse(scriptResult, format);
			Logging.info("get system date time as : " + sysDate);
		} catch (WebDriverException ex) {
			Logging.info("Can not get system date");
			throw ex;
		}
		return sysDate;
	}

	/**
	 * Get a item value by js
	 *
	 * @param xpath
	 *            The xpath of the destination elements
	 * 
	 * @return String The value of the element
	 * 
	 * @author Fiona Zhang
	 */
	public String getItemValueById(String xpath) {
		String get_item_js = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("get_item_js");
		try {
			Tools.scrollElementIntoViewByXpath(xpath);
			WebElement itemElement = getWebElement(xpath, false);

			get_item_js = String.format(get_item_js,
					itemElement.getAttribute("id"));
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			String result = (String) js.executeScript(get_item_js);
			Logging.info("Get item value as : " + result);
			return result;

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}
	
	/**
	 * Get a item value by js
	 *
	 * @param xpath
	 *            The xpath of the destination elements
	 * 
	 * @return String The value of the element
	 * 
	 * @author Fiona Zhang
	 */
	public String getItemValueByIdWithoutScroll(String xpath) {
		String get_item_js = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("get_item_js");
		try {
			WebElement itemElement = getWebElement(xpath, false);

			get_item_js = String.format(get_item_js,
					itemElement.getAttribute("id"));
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			String result = (String) js.executeScript(get_item_js);
			Logging.info("Get item value as : " + result);
			return result;

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}


	/**
	 * Get the checkbox value
	 *
	 * @param xpath
	 *            The xpath of checkbox
	 * 
	 * @author Fiona Zhang
	 */
	protected boolean getCheckFieldValue(String xpath, String area) {
		String get_check_box_value_js = PropertyHelper.pageObjectBaseFile
				.getPropertyValue("get_check_box_value_js");
		boolean result = false;
		try {
			Tools.scrollElementIntoViewByXpath(xpath);
			WebElement datecompose = getWebElement(xpath, false);
			get_check_box_value_js = String.format(get_check_box_value_js,
					datecompose.getAttribute("id"));
			JavascriptExecutor getCheckBoxValue = (JavascriptExecutor) Base.base
					.getDriver();
			result = (boolean) getCheckBoxValue
					.executeScript(get_check_box_value_js);
			Logging.info("The " + area + " value of check field is : " + result);
		} catch (WebDriverException ex) {
			throw ex;
		}
		return result;
	}

	/**
	 * Refresh current page
	 * 
	 * @author Fiona Zhang
	 */
	public void refreshCurrentPage() {
		Logging.info("Refresh the page");
		try {
			this.waitForLoadMaskDismissed();
			Base.base.getDriver().navigate().refresh();
			if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
				Alert alert = Base.base.getDriver().switchTo().alert();
				Logging.info(alert.getText());
				alert.accept();
			}
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}		
	}
	/**
	 * Swipe the element by flick operation
	 * 
	 * @param xpath
	 *            The xpath of the flickked elements
	 * 
	 * @author Fiona Zhang
	 */
	protected void swipeView(String xpath, int offsetX, int offsetY) {
		Logging.info("Start to flick the element , the offset of x is: "
				+ offsetX + " the offset of y is " + offsetY);
		int speed = 0;
		if(Base.platformName.equalsIgnoreCase(ConstantName.IOS))
			speed = 5;
		else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID))
			speed = 300;
		try {
			WebElement el = Base.base.getDriver().findElement(By.xpath(xpath));
			Logging.info("Get location of the element x as : "
					+ el.getLocation().getX() + " , get y as : "
					+ el.getLocation().getY());
			TouchActions ta = new TouchActions(Base.base.getDriver());
			Tools.waitFor(2, TimeUnit.SECONDS);				
			ta.flick(el, offsetX, offsetY, speed).perform();
			Tools.waitFor(5, TimeUnit.SECONDS);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Inject a date time into the Date component of UI
	 *
	 * @param youString
	 *            The string need to capitalized
	 * 
	 * @return String The capitalized String
	 * 
	 * @author Fiona Zhang
	 */
	public String toCapitlize(String youString) {
		StringBuffer stringbf = new StringBuffer();
		Matcher m = Pattern
				.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(
						youString);
		while (m.find()) {
			m.appendReplacement(stringbf, m.group(1).toUpperCase()
					+ m.group(2).toLowerCase());
		} 
		System.out.println(m.appendTail(stringbf).toString());
		return m.appendTail(stringbf).toString();
	}
	
	/**
	 * click the element in android
	 *
	 * @param el
	 * 		click the webelement in android
	 * 
	 * @author Fiona Zhang
	 */
	public void clickElement(WebElement el) {
		try {
			if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				((JavascriptExecutor) Base.base.getDriver())
						.executeScript("window.scrollTo(0,"
								+ el.getLocation().x + ")");
				TouchActions ta = new TouchActions(Base.base.getDriver());
				ta.singleTap(el).perform();
			} else if(Base.platformName.equals(ConstantName.IOS)){
              el.click();
			}
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Set element value by js
	 *
	 * @param element
	 * 		The element need to set value
	 * 
	 * @param value
	 * 		The exactly value need to be set
	 * 
	 * @author Fiona Zhang
	 */
	public void setValueByJs(WebElement element, String value){
		String execSetValue = PropertyHelper.pageObjectBaseFile.getPropertyValue("execSetValue");
		String elementId = element.getAttribute("id");
		JavascriptExecutor setValue = (JavascriptExecutor) Base.base
				.getDriver();
		setValue.executeScript(String.format(execSetValue,elementId), value);
	}

	/**
	 * Input text in android
	 *
	 * @param eventElement
	 * 		the webelement in android
	 * 
	 * @param keyword
	 * 		the input text
	 * 
	 * @author Fiona Zhang
	 */
	public void elementSendKeys(WebElement eventElement, String keyword) {
		Tools.waitFor(5000, TimeUnit.MILLISECONDS);
		Logging.info("input text : " + keyword);

		try {
			eventElement.click();
			for (char ch : keyword.toCharArray()) {
				String cmdLine = this.adbCommand(ch);

				Runtime.getRuntime().exec(cmdLine);
				Thread.sleep(280);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
/*	String cmdstr = "adb shell input text %s";
	String cmdstr_semicolon = "adb shell input keyevent 'KEYCODE_SEMICOLON'";
	String cmdstr_space = "adb shell input keyevent 'KEYCODE_SPACE'";
*/
		/*try {
			eventElement.click();
	
			if (keyword.contains(";"))
				for (char ch : keyword.toCharArray()) {
					if (ch == ';')
						Runtime.getRuntime().exec(cmdstr_semicolon);
					else
						Runtime.getRuntime().exec(String.format(cmdstr, ch));
					Thread.sleep(280);
				}
			else if(keyword.contains(" "))
				for (char ch : keyword.toCharArray()) {
					if (ch == ' ')
						Runtime.getRuntime().exec(cmdstr_space);
					else
						Runtime.getRuntime().exec(String.format(cmdstr, ch));
					Thread.sleep(280);
				}
			else
				for (char ch : keyword.toCharArray()) {
					Runtime.getRuntime().exec(String.format(cmdstr, ch));
						Thread.sleep(280);
				}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	*/
	
	/**
	 * return ADB command line
	 * 
	 * @param ch
	 * 		The input character
	 * 
	 * @author Fiona Zhang
	 */
	protected String adbCommand(char ch) {
		String execCmd = null;
		String cmdstr = "adb shell input text %s";
		HashMap<Character, String> specialKeyword = new HashMap<Character, String>();
		specialKeyword.put(';', "adb shell input keyevent 'KEYCODE_SEMICOLON'");
		specialKeyword.put(' ', "adb shell input keyevent 'KEYCODE_SPACE'");
		if (specialKeyword.containsKey(ch))
			execCmd = specialKeyword.get(ch);
		else
			execCmd = String.format(cmdstr, ch);
		return execCmd;
	}
	
	/**
	 * Check if error popup displayed
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isErrorPopDisplayed() {
		Logging.info("check if the error of conversation view is displayed");
		String error_popup = PropertyHelper.coreLoginFile
				.getPropertyValue("error_popup");
		try {
			Tools.setDriverDefaultValues();
			List<WebElement> popupError = Base.base.getDriver().findElements(
					By.xpath(error_popup));
			if (popupError.size() == 0)
				return false;
			else
				return true;
		} catch (WebDriverException ex) {
			Logging.error("Could not click on login button");
			throw ex;
		}
	}

}
