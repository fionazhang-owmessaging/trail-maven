package synchronoss.com.pageobject.core;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class CoreSettings extends PageObjectBase {

	/**
	 * Click setting option in setting list
	 *
	 * @param settingOption
	 *            setting option in setting list
	 * 
	 * @author Fiona Zhang
	 */
	public void clickSettingOptionList(String settingOption) {
		Logging.info("Click " + settingOption + " option in setting list");
		String setting_option_item = PropertyHelper.coreSettings
				.getPropertyValue("setting_option_item");

		setting_option_item = String.format(setting_option_item,
				toCapitlize(settingOption));
		try {
			WebElement settingOptionElement = getWebElement(
					setting_option_item, false);
			 this.clickElement(settingOptionElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

  /**
   * Click setting option in setting list
   *
   * @param settingOption(varargs) , the var args, first parameter should be setting option in
   *        setting list, like save, cancel, back etc. The second parameter is just to differentiate
   *        if need to click back button or not, any strings is fine.
   * 
   * @author Fiona Zhang
   */
  public void clickHeadBarOfSettingDetail(String... option) {
    Logging.info("Click " + option[0] + " button in setting view");
    String action = null;
    switch (option[0]) {
      case ConstantName.BACK:
        if (!Base.deviceName.toLowerCase().contains(ConstantName.IPAD) || option.length != 1) {
          // there's no back button in most situations of settings view when device is tablet, but
          // in some case need to click back button, so use varargs to control it
          action = PropertyHelper.coreSettings.getPropertyValue("setting_head_bar_go_back_button");
        }
        break;
      case ConstantName.CANCEL:
        action = PropertyHelper.coreSettings.getPropertyValue("setting_head_bar_go_back_button");
        break;
      case ConstantName.SAVE:
        action = PropertyHelper.coreSettings.getPropertyValue("setting_head_bar_save_button");
        break;
    }
    try {
      if (action == null || action.isEmpty()) {
        Logging.info("Back button don't need to be clicked in tablet device");
      } else {
        Tools.scrollElementIntoViewByXpath(action);
        WebElement settingOptionElement = getWebElement(action, false);
        this.clickElement(settingOptionElement);
        waitForLoadMaskDismissed();

      }
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }
	
  /**
   * Click setting option in mail setting list
   *
   * @param options - the options for mail settings, the value could be ConstantName.ACCOUNT,
   *        ConstantName.BLOCKIMG, etc.
   * 
   * @author Fiona Zhang
   */
  public void clickMailSettingOption(String options) {
    Logging.info("Click " + options + " button in settings view");
    String buttonOption = null;
    switch (options) {
      case ConstantName.ACCOUNT:
        buttonOption = PropertyHelper.coreSettings.getPropertyValue("mail_ext_account");
        break;
      case ConstantName.AUTOREPLY:
        buttonOption = PropertyHelper.coreSettings.getPropertyValue("mail_auto_reply");
        break;
      case ConstantName.AUTOFWD:
        buttonOption = PropertyHelper.coreSettings.getPropertyValue("mail_auto_fwd");
        break;
      case ConstantName.BLOCKSENDER:
        buttonOption = PropertyHelper.coreSettings.getPropertyValue("mail_block_sender");
        break;
      case ConstantName.SAFESENDER:
        buttonOption = PropertyHelper.coreSettings.getPropertyValue("mail_safe_sender");
        break;
      case ConstantName.BLOCKIMG:
        buttonOption = PropertyHelper.coreSettings.getPropertyValue("mail_block_image");
        break;
      case ConstantName.SIGNATURE:
        buttonOption = PropertyHelper.coreSettings.getPropertyValue("mail_signature");
        break;

    }
    try {
      Tools.scrollElementIntoViewByXpath(buttonOption);
      WebElement blockSenderElement = getWebElement(buttonOption, false);
      this.clickElement(blockSenderElement);
      waitForLoadMaskDismissed();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }

  }
  
  
  /**
   * Click block or safe sender add button
   *
   * @param buttonField - safe or block sender
   * 
   * @author Fiona Zhang
   */
  public void clickBlockOrSafeSenderAddButton(String buttonField) {
    Logging.info("Click add button in " + buttonField + " page");
    String buttonXpath = null;
    if (buttonField.equals(ConstantName.BLOCKSENDER))
      buttonXpath = PropertyHelper.coreSettings.getPropertyValue("mail_block_sender_add_btn");
    else if (buttonField.equals(ConstantName.SAFESENDER))
      buttonXpath = PropertyHelper.coreSettings.getPropertyValue("mail_safe_sender_add_btn");

    try {
      WebElement addButtonElement = getWebElement(buttonXpath, false);
      this.clickElement(addButtonElement);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

  
  /**
   * Click block or safe sender remove button
   *
   * @param buttonField - safe or block sender
   * 
   * @author Fiona Zhang
   */
  public void clickDeleteButtonOfBlockOrSafeSenderButton(String senderSection, String senderMail) {
    Logging.info("Click " + senderMail + "delete button in " + senderSection + " page");
    String buttonXpath = null;
    if (senderSection.equals(ConstantName.BLOCKSENDER))
      buttonXpath = PropertyHelper.coreSettings.getPropertyValue("mail_block_sender_remove_btn");
    else if (senderSection.equals(ConstantName.SAFESENDER))
      buttonXpath = PropertyHelper.coreSettings.getPropertyValue("mail_safe_sender_remove_btn");
    buttonXpath = String.format(buttonXpath, senderMail);
    try {
      WebElement removeButtonElement = getWebElement(buttonXpath, false);
      this.clickElement(removeButtonElement);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }

  }

	/**
	 * Edit first name in setting view
	 *
	 * @param updateName
	 *            the name need to be edit.
	 * 
	 * @author Fiona Zhang
	 */
	public void editNameInProfile(String updateName, String area) {
		Logging.info("Edit " + area + " in setting view as  " + updateName);
		String nameArea = null;
		if (area.equalsIgnoreCase("firstname"))
			nameArea = PropertyHelper.coreSettings
					.getPropertyValue("first_name_area");
		else if (area.equalsIgnoreCase("lastname"))
			nameArea = PropertyHelper.coreSettings
					.getPropertyValue("last_name_area");

		try {
			WebElement firstNameElement = getWebElement(nameArea, false);
			firstNameElement.clear();
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
				firstNameElement.sendKeys(updateName);
				firstNameElement.sendKeys(Keys.ENTER);
			}
			else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID))
				this.elementSendKeys(firstNameElement, updateName);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Add event duration
	 *
	 * @param eventDuration
	 *            event duration
	 * 
	 * @author Fiona Zhang
	 */
	public void addEventDuration(int eventDuration) {
		Logging.info("Set event duration as : " + eventDuration);
		String event_duration = PropertyHelper.coreSettings
				.getPropertyValue("event_duration");

		try {
			Tools.scrollElementIntoViewByXpath(event_duration);
			WebElement eventDurationElement = getWebElement(event_duration,
					false);
			eventDurationElement.clear();
			eventDurationElement.sendKeys(String.valueOf(eventDuration));
			eventDurationElement.sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Add auto reply message
	 *
	 * @param autoReplyMessage
	 *            auto reply message
	 * 
	 * @author Fiona Zhang
	 */
	public void addAutoReplyMessage(String autoReplyMessage) {
		Logging.info("Add auto reply message as  : " + autoReplyMessage);
		String auto_reply_message = PropertyHelper.coreSettings
				.getPropertyValue("auto_reply_message");

		try {
			Tools.scrollElementIntoViewByXpath(auto_reply_message);
			WebElement signatureElement = getWebElement(auto_reply_message,
					false);
			signatureElement.clear();
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
			signatureElement.sendKeys(autoReplyMessage);
			signatureElement.sendKeys(Keys.ENTER);
			}
			else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID))
				this.setValueByJs(signatureElement, autoReplyMessage);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Add signature
	 *
	 * @param signature
	 *            the signature need to add to the email
	 * 
	 * @author Fiona Zhang
	 */
	public void addSignature(String signature) {
		Logging.info("Add email signature as  : " + signature);
		String reply_signature_message_input = PropertyHelper.coreSettings
				.getPropertyValue("reply_signature_message_input");

		try {
			Tools.scrollElementIntoViewByXpath(reply_signature_message_input);
			WebElement signatureElement = getWebElement(
					reply_signature_message_input, false);
			signatureElement.clear();
			signatureElement.sendKeys(signature);
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS))
				signatureElement.sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
  /**
   * Add block or safe sender value
   *
   * @param options - block or safe sender
   * @param fieldValue - the field value you need to add
   * 
   * @author Fiona Zhang
   */
  public void addBlockOrSafeSenderValueInField(String options, String fieldValue) {
    Logging.info("Add " + options + " value as  : " + fieldValue);
    String inputValue = null;
    if (options.equals(ConstantName.BLOCKSENDER))
      inputValue = PropertyHelper.coreSettings.getPropertyValue("mail_block_sender_value_field");
    else if (options.equals(ConstantName.SAFESENDER))
      inputValue = PropertyHelper.coreSettings.getPropertyValue("mail_safe_sender_value_field");

    try {
      Tools.scrollElementIntoViewByXpath(inputValue);
      WebElement signatureElement = getWebElement(inputValue, true);
      signatureElement.clear();
      signatureElement.sendKeys(fieldValue);
      if (Base.platformName.equalsIgnoreCase(ConstantName.IOS))
        signatureElement.sendKeys(Keys.ENTER);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

  
  /**
   * Add auto forward email address
   *
   * @param emailIds - email addresses
   * 
   * @author Fiona Zhang
   */
  public void addAutoForwardDestinationEmail(String[] emailIds) {
    String auto_forward_input = PropertyHelper.coreSettings.getPropertyValue("auto_forward_input");

    try {
      Tools.scrollElementIntoViewByXpath(auto_forward_input);
      WebElement autoFwdElement = getWebElement(auto_forward_input, false);
      autoFwdElement.clear();
      for (int i = 0; i < emailIds.length; i++) {
        Logging.info("Add email auto forward email as  : " + emailIds[i]);
        autoFwdElement.sendKeys(emailIds[i]);
        autoFwdElement.sendKeys(";");
      }
      if (Base.platformName.equalsIgnoreCase(ConstantName.IOS))
        autoFwdElement.sendKeys(Keys.ENTER);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }
  

  /**
   * Add auto forward email address
   *
   * @param emailIds - email addresses
   * @param isAutoSuggest - true will not use auto suggest , false will use auto suggest
   * 
   * @author Fiona Zhang
   */
  public void addAutoForwardDestinationEmail(String emailIds, boolean isAutoSuggest) {
    String auto_forward_input = PropertyHelper.coreSettings.getPropertyValue("auto_forward_input");

    try {
      Tools.scrollElementIntoViewByXpath(auto_forward_input);
      WebElement autoFwdElement = getWebElement(auto_forward_input, false);
      autoFwdElement.clear();
      autoFwdElement.sendKeys(emailIds);
      if (isAutoSuggest) {
        Tools.waitFor(5,TimeUnit.SECONDS);
        autoFwdElement.sendKeys(Keys.ENTER);

      }
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }



	/**
	 * Set date format for the user
	 *
	 * @param format
	 *            The format of the date, like yy-dd-ll
	 * 
	 * @author Fiona Zhang
	 */
	public void selectDateFormat_ByJS(String format) {
		Logging.info("Selecting date format in settings view by JS: " + format);

		// find the option area
		String date_format = PropertyHelper.coreSettings
				.getPropertyValue("date_format");

		try {
			WebElement formatElement = this.getWebElement(date_format, false);
			String itemId = formatElement.getAttribute("id");
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			js.executeScript("Ext.getCmp('" + itemId + "').setValue('" + format
					+ "')");
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set if include the signature
	 *
	 * @param isIncludeSignature
	 *            true - will reply with quoting false - reply without quoting
	 * 
	 * @author Fiona Zhang
	 */
	public void useIncludeSignature(boolean isIncludeSignature) {
		String include_signature = PropertyHelper.coreSettings
				.getPropertyValue("include_signature");
		try {
			Tools.scrollElementIntoViewByXpath(include_signature);
			if (!getCheckFieldValue(include_signature, "include the signature")
					&& isIncludeSignature
					|| getCheckFieldValue(include_signature,
							"include the signature") && !isIncludeSignature) {
				WebElement signatureElement = getWebElement(include_signature,
						false);
				 this.clickElement(signatureElement);
				Logging.info("Is include the signature when write a email : "
						+ isIncludeSignature);

			} else
				Logging.info("Don't need to change the include signature value");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set if auto reply the email
	 *
	 * @param isAutoReply
	 *            true - will auto reply the email false - do not auto reply the
	 *            email
	 * 
	 * @author Fiona Zhang
	 */
	public void useAutoReply(boolean isAutoReply) {
		String enable_auto_reply = PropertyHelper.coreSettings
				.getPropertyValue("enable_auto_reply");
		try {
			Tools.scrollElementIntoViewByXpath(enable_auto_reply);
			if (!getCheckFieldValue(enable_auto_reply, "reply with quoting")
					&& isAutoReply
					|| getCheckFieldValue(enable_auto_reply,
							"reply with quoting") && !isAutoReply) {
				WebElement replyQuoteElement = getWebElement(enable_auto_reply,
						true);
				 this.clickElement(replyQuoteElement);
				Logging.info("Is reply with quoting : " + isAutoReply);

			} else
				Logging.info("Don't need to change the reply with quote option");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set if reply with quoting
	 *
	 * @param isUseQuoting
	 *            true - will reply with quoting false - reply without quoting
	 * 
	 * @author Fiona Zhang
	 */
	public void useReplyQuoting(boolean isUseQuoting) {
		String reply_quote_original_message = PropertyHelper.coreSettings
				.getPropertyValue("reply_quote_original_message");
		try {
			Tools.scrollElementIntoViewByXpath(reply_quote_original_message);
			if (!getCheckFieldValue(reply_quote_original_message,
					"reply with quoting")
					&& isUseQuoting
					|| getCheckFieldValue(reply_quote_original_message,
							"reply with quoting") && !isUseQuoting) {
				WebElement replyQuoteElement = getWebElement(
						reply_quote_original_message, false);
				 this.clickElement(replyQuoteElement);
				Logging.info("Is reply with quoting : " + isUseQuoting);

			} else
				Logging.info("Don't need to change the reply with quote option");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set use my device time zone value according to the parameter
	 *
	 * @param checkOrNot
	 *            true - will use my device time zone false - will not use my
	 *            device time zone
	 * 
	 * @author Fiona Zhang
	 */
	public void useDeviceTimeZone(boolean checkOrNot) {
		Logging.info("set use my device time zone as " + checkOrNot);
		String use_my_device_time_zone_field = PropertyHelper.coreSettings
				.getPropertyValue("use_my_device_time_zone_field");
		try {
			if (!getCheckFieldValue(use_my_device_time_zone_field, "time zone")
					&& checkOrNot
					|| getCheckFieldValue(use_my_device_time_zone_field,
							"time zone") && !checkOrNot) {
				WebElement deviceTimeElement = getWebElement(
						use_my_device_time_zone_field, false);
				 this.clickElement(deviceTimeElement);
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set time format
	 *
	 * @param is24Hour
	 *            true - will use 24 hour time format false - will use 12 hour
	 *            time format
	 * 
	 * @author Fiona Zhang
	 */
	public void useTimeFormat24Hours(boolean is24Hour) {
		String time_format_24_hour = PropertyHelper.coreSettings
				.getPropertyValue("time_format_24_hour");
		try {
			if (!getCheckFieldValue(time_format_24_hour, "use 24 hours")
					&& is24Hour
					|| getCheckFieldValue(time_format_24_hour, "use 24 hours")
					&& !is24Hour) {
				WebElement deviceTimeElement = getWebElement(
						time_format_24_hour, false);
				 this.clickElement(deviceTimeElement);
				Logging.info("set use 24-hour time format as : " + is24Hour);

			} else
				Logging.info("Dont need to change time format");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set auto-suggest value
	 *
	 * @param isAutoSuggest
	 *            true - will use auto-suggest contact function false - will not
	 *            use auto-suggest contact function
	 * 
	 * @author Fiona Zhang
	 */
	public void useAutoSuggestContact(boolean isAutoSuggest) {
		String contact_auto_suggest = PropertyHelper.coreSettings
				.getPropertyValue("contact_auto_suggest");
		try {
			if (!getCheckFieldValue(contact_auto_suggest, "use auto suggest")
					&& isAutoSuggest
					|| getCheckFieldValue(contact_auto_suggest,
							"use auto suggest") && !isAutoSuggest) {
				WebElement deviceTimeElement = getWebElement(
						contact_auto_suggest, false);
				 this.clickElement(deviceTimeElement);
				Logging.info("set auto-suggest as : " + isAutoSuggest);
			} else
				Logging.info("Dont need to change auto suggest contact");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set auto-save out going value
	 *
	 * @param isAutoSaveOutGoing
	 *            true - will use auto-save out going msg false - will not save
	 *            out going msg contact function
	 * 
	 * @author Fiona Zhang
	 */
	public void useAutoSaveOutGoingMsg(boolean isAutoSaveOutGoing) {
		String mail_auto_save_outgoing = PropertyHelper.coreSettings
				.getPropertyValue("mail_auto_save_outgoing");
		try {
			if (!getCheckFieldValue(mail_auto_save_outgoing,
					"use auto save out going msg")
					&& isAutoSaveOutGoing
					|| getCheckFieldValue(mail_auto_save_outgoing,
							"use auto save out going msg")
					&& !isAutoSaveOutGoing) {
				WebElement deviceTimeElement = getWebElement(
						mail_auto_save_outgoing, false);
				 this.clickElement(deviceTimeElement);
				Logging.info("set auto save out going msg : "
						+ mail_auto_save_outgoing);
			} else
				Logging.info("Dont need to change auto save out going msg");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set show time value
	 *
	 * @param showTime
	 *            true - will display the date in mail list false - will not
	 *            display the date in mail list
	 * 
	 * @author Fiona Zhang
	 */
	public void useShowTimeInMsgList(boolean showTime) {
		String show_times_in_msg_list = PropertyHelper.coreSettings
				.getPropertyValue("show_times_in_msg_list");
		try {
			if (!getCheckFieldValue(show_times_in_msg_list,
					"Show times(date) in message list")
					&& showTime
					|| getCheckFieldValue(show_times_in_msg_list,
							"Show times(date) in message list") && !showTime) {
				WebElement deviceTimeElement = getWebElement(
						show_times_in_msg_list, false);
				 this.clickElement(deviceTimeElement);
				Logging.info("Show times(date) in message list : " + showTime);
			} else
				Logging.info("Dont need to change use show time msg list");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set if empty trash folder when log out
	 *
	 * @param isEmptyTrash
	 *            true - will empty trash folder when logout false - will not
	 *            empty trash folder when logout contact function
	 * 
	 * @author Fiona Zhang
	 */
	public void useEmptyTrashWhenLogout(boolean isEmptyTrash) {
		String empty_trash_when_log_out = PropertyHelper.coreSettings
				.getPropertyValue("empty_trash_when_log_out");
		try {
			if (!getCheckFieldValue(empty_trash_when_log_out,
					"set empty trash when logout")
					&& isEmptyTrash
					|| getCheckFieldValue(empty_trash_when_log_out,
							"set empty trash when logout") && !isEmptyTrash) {
				WebElement deviceTimeElement = getWebElement(
						empty_trash_when_log_out, false);
				 this.clickElement(deviceTimeElement);
				Logging.info("set empty trash when logout as : " + isEmptyTrash);
			} else
				Logging.info("Dont need to change empty trash when log out value");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set if auto reply with quoting
	 *
	 * @param isUseQuoting
	 *            true - will auto reply with quoting false - auto reply without
	 *            quoting
	 * 
	 * @author Fiona Zhang
	 */
	public void useOriginalEmailInAutoReply(boolean isUseQuoting) {
		String auto_reply_with_origianl_quote = PropertyHelper.coreSettings
				.getPropertyValue("auto_reply_with_origianl_quote");
		try {
			Tools.scrollElementIntoViewByXpath(auto_reply_with_origianl_quote);
			if (!getCheckFieldValue(auto_reply_with_origianl_quote,
					"reply with quoting in auto reply")
					&& isUseQuoting
					|| getCheckFieldValue(auto_reply_with_origianl_quote,
							"reply with quoting in auto reply")
					&& !isUseQuoting) {
				WebElement replyQuoteElement = getWebElement(
						auto_reply_with_origianl_quote, false);
				 this.clickElement(replyQuoteElement);
				Logging.info("Is reply with quoting in auto reply : "
						+ isUseQuoting);

			} else
				Logging.info("Don't need to change the reply with quote with auto reply option");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set if use conversion view
	 *
	 * @param isConversionView
	 *            true - use conversion view false - not use conversion view
	 *            quoting
	 * 
	 * @author Fiona Zhang
	 */
	public void useConversionView(boolean isConversionView) {
		String conversation_view = PropertyHelper.coreSettings
				.getPropertyValue("conversation_view");
		try {
			Tools.scrollElementIntoViewByXpath(conversation_view);
			if (!getCheckFieldValue(conversation_view, "conversation view")
					&& isConversionView
					|| getCheckFieldValue(conversation_view,
							"conversation view") && !isConversionView) {
				WebElement replyQuoteElement = getWebElement(conversation_view,
						false);
				 this.clickElement(replyQuoteElement);
				Logging.info("Is conversation view is enable : "
						+ isConversionView);

			} else
				Logging.info("Don't need to change the conversion view option");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Set if auto reply with quoting
	 *
	 * @param isSaveNewContact
	 *            true - will save new contact 
	 *            false - will not save contact
	 * 
	 * @author Fiona Zhang
	 */
	public void useAutoSaveNewContacts(boolean isSaveNewContact) {
		String auto_save_contact = PropertyHelper.coreSettings
				.getPropertyValue("auto_save_contact");
		try {
			Tools.scrollElementIntoViewByXpath(auto_save_contact);
			if (!getCheckFieldValue(auto_save_contact, "auto save new contact")
					&& isSaveNewContact
					|| getCheckFieldValue(auto_save_contact,
							"auto save new contact") && !isSaveNewContact) {
				WebElement replyQuoteElement = getWebElement(auto_save_contact,
						false);
				 this.clickElement(replyQuoteElement);
				Logging.info("Is auto save new contact : " + isSaveNewContact);

			} else
				Logging.info("Don't need to change auto save new contact");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
  /**
   * Set if auto forward
   *
   * @param isAutoForward 
   * true - will auto reply with quoting false - auto reply without quoting
   * 
   * @author Fiona Zhang
   */
  public void useAutoForward(boolean isAutoForward) {
    String auto_forward_option = PropertyHelper.coreSettings.getPropertyValue("auto_forward_option");
    try {
      Tools.scrollElementIntoViewByXpath(auto_forward_option);
      if (!getCheckFieldValue(auto_forward_option, "auto forward") && isAutoForward
          || getCheckFieldValue(auto_forward_option, "auto forward") && !isAutoForward) {
        WebElement replyQuoteElement = getWebElement(auto_forward_option, false);
        this.clickElement(replyQuoteElement);
        Logging.info("Set auto forward option as : " + isAutoForward);

      } else
        Logging.info("Don't need to change auto forward");

    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

  /**
   * Set if keep a copy in inbox 
   *
   * @param isKeepCopy 
   * true - keep a copy
   * false - do not keep a copy
   * 
   * @author Fiona Zhang
   */
  public void useKeepCopyInInbox(boolean isKeepCopy) {
    Logging.info("Keep a copy of auto forward in inbox");
    String auto_forward_keep_copy = PropertyHelper.coreSettings.getPropertyValue("auto_forward_keep_copy");
    try {
      if (!getCheckFieldValue(auto_forward_keep_copy, "keep copy") && isKeepCopy
          || getCheckFieldValue(auto_forward_keep_copy, "keep copy") && !isKeepCopy) {
        Tools.scrollElementIntoViewByXpath(auto_forward_keep_copy);
        WebElement replyQuoteElement = getWebElement(auto_forward_keep_copy, true);
        this.clickElement(replyQuoteElement);
        Logging.info("Set keep copy option as : " + isKeepCopy);

      } else
        Logging.info("Don't need to change keep copy option");

    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }


	/**
	 * Select time zone with zone id
	 *
	 * @param zoneID
	 *            The zone id of time zone
	 * 
	 * @author Fiona Zhang
	 */
	public void selectTimeZone(ZoneId zoneID) {
		String time_zone_format = PropertyHelper.coreSettings
				.getPropertyValue("time_zone_format");
		Logging.info("Set time zone as " + zoneID);
		try {
			super.selectItem_ByJS(time_zone_format, zoneID.getId());
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Select start week in calendar setting view
	 *
	 * @param startWeek
	 *            The start week value
	 * 
	 * @author Fiona Zhang
	 */
	public void selectStartWeek(String startWeek) {
		String start_week = PropertyHelper.coreSettings
				.getPropertyValue("start_week");
		Logging.info("Set start week as : " + startWeek);
		try {
			super.selectItem_ByJS(start_week, startWeek);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Select event duration unit
	 *
	 * @param durationUnit
	 *            The duration unit of the event
	 * 
	 * @author Fiona Zhang
	 */
	public void selectEventDurationUnit(String durationUnit) {
		String event_duration_unit = PropertyHelper.coreSettings
				.getPropertyValue("event_duration_unit");
		Logging.info("Set event duration unit as " + durationUnit);
		try {
			super.selectItem_ByJS(event_duration_unit, durationUnit);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Select auto save draft time interval
	 *
	 * @param timeInterval
	 *            Time interval for save draft time
	 * 
	 * @author Fiona Zhang
	 */
	public void selectAutoSaveDraftTime(String timeInterval) {
		String mail_auto_save_draft = PropertyHelper.coreSettings
				.getPropertyValue("mail_auto_save_draft");
		Logging.info("Set auto save draft time unit as " + timeInterval);
		try {
			super.selectItem_ByJS(mail_auto_save_draft, timeInterval);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Select fetch new message time
	 *
	 * @param fetchMsgTime
	 *            Fetch msg time
	 * 
	 * @author Fiona Zhang
	 */
	public void selectFetchNewMsgAutomatically(String fetchMsgTime) {
		String mail_autocheck = PropertyHelper.coreSettings
				.getPropertyValue("mail_autocheck");
		Logging.info("Set fetch mail automatically as " + fetchMsgTime);
		try {
			super.selectItem_ByJS(mail_autocheck, fetchMsgTime);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	
  /**
   * Select signature postion
   *
   * @param signaturePostion
   * 
   * @author Fiona Zhang
   */
  public void selectSingaturePosition(String signaturePostion) {
    String mail_signature_position =
        PropertyHelper.coreSettings.getPropertyValue("mail_signature_position");
    Logging.info("Select signature postion");
    try {
      super.selectItem_ByJS(mail_signature_position, signaturePostion);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }

  }

	/**
	 * Get the profile settings of each field
	 *
	 * @param area
	 *            The profile area, like firstname/lastname
	 * 
	 * @author Fiona Zhang
	 */
	public String getProfileSettingValue(String area) {
		Logging.info("Get the " + area + " value in profile view");
		String field = null;
		String result = null;

		switch (area) {
		case ConstantName.FIRSTNAME:
			field = PropertyHelper.coreSettings
					.getPropertyValue("first_name_area");
			break;
		case ConstantName.LASTNAME:
			field = PropertyHelper.coreSettings
					.getPropertyValue("last_name_area");
			break;
		}
		Logging.info(field);
		try {
			result = getItemValueById(field);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return result;
	}

	/**
	 * Get the start week value of calendar
	 * 
	 * @author Fiona Zhang
	 * 
	 */
	public String getStartWeekValue() {
		Logging.info("Get the start week value in calendar setting view");
		String start_week_value = PropertyHelper.coreSettings
				.getPropertyValue("start_week_value");
		String result = null;
		try {
			result = getItemValueById(start_week_value);
			Logging.info("Get start week value as " + result);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return result;
	}

	/**
	 * Get the validation value message
	 * 
	 * @author Fiona Zhang
	 * 
	 */
	public String getValidationText() {
		Logging.info("Get the validation message in current settings view");
		String validation_msg = PropertyHelper.coreSettings
				.getPropertyValue("validation_msg");
		String result = null;
		try {
			WebElement validationTextElement = getWebElement(validation_msg,
					false);
			result = validationTextElement.getText();
			Logging.info("Get validation msg as :  "
					+ validationTextElement.getText());
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return result;
	}

	/**
	 * Get the expected validation value message
	 * 
	 * @author Fiona Zhang
	 * 
	 */
	public String getExpectedValidationOfNumberExceeding(int numberEdgeStart,
			int numberEdgeEnd) {
		Logging.info("Get user Time format");
		String get_validation_reminder_number_js = PropertyHelper.coreSettings
				.getPropertyValue("get_validation_reminder_number_js");
		String validationMsg = null;
		try {
			Tools.setDriverDefaultValues();
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			validationMsg = (String) js
					.executeScript(get_validation_reminder_number_js);
			validationMsg = validationMsg.replace("{0}",
					String.valueOf(numberEdgeStart));
			validationMsg = validationMsg.replace("{1}",
					String.valueOf(numberEdgeEnd));
			Logging.info("Get validation msg :  " + validationMsg);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return validationMsg;

	}

	/**
	 * Get quota info of mail in settings view
	 * 
	 * @author Fiona Zhang
	 * 
	 */
	public String getQuotaInfo() {
		String result = null;
		String mail_quota_info = PropertyHelper.coreSettings
				.getPropertyValue("mail_quota_info");
		try {
		  Tools.scrollElementIntoViewByXpath(mail_quota_info);
          WebElement mailQuotaInfoElement = getWebElement(mail_quota_info,true);
          result = mailQuotaInfoElement.getText();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Get the quota value " + result + "in mail setting view");
		return result;
	}

  /**
   * Get block or safe sender list
   * 
   * @param buttonField - the location of the list
   * 
   * @author Fiona Zhang
   * 
   */
  public ArrayList<String> getBlockOrSafeSenderList(String listField) {
    Logging.info("Get " + listField + " field sender list");
    ArrayList<String> result = new ArrayList<String>();
    String buttonXpath = null;
    if (listField.equals(ConstantName.BLOCKSENDER))
      buttonXpath = PropertyHelper.coreSettings.getPropertyValue("mail_block_sender_list");
    else if (listField.equals(ConstantName.SAFESENDER))
      buttonXpath = PropertyHelper.coreSettings.getPropertyValue("mail_safe_sender_list");
    try {
      Tools.scrollElementIntoViewByXpath(buttonXpath);
      List<WebElement> senderListElement = getWebElements(buttonXpath, true);
      for (WebElement el : senderListElement) {
        result.add(el.getText());
      }
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    Logging.info("Result of get " + listField + " sender list is " + result.toString());

    return result;

  }

  
  /**
   * Delete all senders in the list
   * 
   * @param senderSection - the sender section
   * 
   * @author Fiona Zhang
   * 
   */
  public void deleteAllSenders(String senderSection) {
    Logging.info("Delete all senders in the field of " + senderSection);
    String buttonXpath = null;

    if (senderSection.equals(ConstantName.BLOCKSENDER))
      buttonXpath =
          PropertyHelper.coreSettings
              .getPropertyValue("mail_block_sender_remove_btn_collection_last_item");
    else if (senderSection.equals(ConstantName.SAFESENDER))
      buttonXpath =
          PropertyHelper.coreSettings
              .getPropertyValue("mail_safe_sender_remove_btn_collection_last_item");
    try {

      List<WebElement> senderListElement = getWebElements(buttonXpath, true);
      while (senderListElement.size() == 1) {
        senderListElement.get(0).click();
        this.waitForLoadMaskDismissed();
        senderListElement = getWebElements(buttonXpath, true);
      }
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }
  
  
  
  /**
   * Get toast message
   * 
   * @author Fiona Zhang
   * 
   */
  public String getToastMessage() {
    String result = null;
    String toast_msg = PropertyHelper.coreSettings.getPropertyValue("toast_msg");
    try {
      WebElement toastMsgElement = getWebElement(toast_msg, false);
      result = toastMsgElement.getText();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    Logging.info("Get toast message as: " + result);
    return result;

  }

  
  /**
   * Remove all auto forward mails in the list
   * 
   * @author Fiona Zhang
   * 
   */
  public void removeAllAutoForwardEmailDestinations() {
    Logging.info("Remove all emails in auto forward mail list");
    String auto_forward_mail_list_delete_button =
        PropertyHelper.coreSettings.getPropertyValue("auto_forward_mail_list_delete_button");
    try {
      List<WebElement> deleteButtonsElement =
          getWebElements(auto_forward_mail_list_delete_button, false);
      for (int i = deleteButtonsElement.size() - 1; i >= 0; i--) {
        deleteButtonsElement.get(i).click();
      }
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }

  }
  
  /**
   * Remove specific auto forward email by parameter
   * 
   * @param name
   * @author Fiona Zhang
   * 
   */
  public void removeAutoForwardMail(String name) {
    Logging.info("Remove email " + name + " in auto forward mail list");
    String auto_forward_mail_list_specail_user_delete_button =
        PropertyHelper.coreSettings
            .getPropertyValue("auto_forward_mail_list_specail_user_delete_button");
    auto_forward_mail_list_specail_user_delete_button =
        String.format(auto_forward_mail_list_specail_user_delete_button, name);
    try {
      WebElement deleteButtonsElement =
          getWebElement(auto_forward_mail_list_specail_user_delete_button, false);
      deleteButtonsElement.click();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }


  
  /**
   * Get all auto forward mails in the list
   * 
   * @author Fiona Zhang
   * 
   */
  public ArrayList<String> getAutoForwardDestinationEmailList() {
    ArrayList<String> destinationMailList = new ArrayList<String>();
    String auto_forward_mail_list = PropertyHelper.coreSettings.getPropertyValue("auto_forward_mail_list");
    try {
      Tools.scrollElementIntoViewByXpath(auto_forward_mail_list);
      List<WebElement> autoFwdMailListElements =
          getWebElements(auto_forward_mail_list, false);
      for (int i = 0; i< autoFwdMailListElements.size() ; i++) {
        destinationMailList.add(autoFwdMailListElements.get(i).getText());
      }
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    Logging.info("Get auto forward destination email list as " + destinationMailList);

    return destinationMailList;
  }

  /**
   * Click on Add account button on settings page
   * 
   * @Author Vivian Xie
   * @Created Feb/10/2016
   */
  public void clickAddAcountButton() {
    Logging.info("Starting to click on Add account button");
    String settings_mail_add_account_button =
        PropertyHelper.coreSettings.getPropertyValue("settings_mail_add_account_button");
    try {
      Tools.scrollElementIntoViewByXpath(settings_mail_add_account_button);
      getWebElement(settings_mail_add_account_button, false).click();

    } catch (WebDriverException ex) {
      ex.printStackTrace();
      Logging.info("Failed to click on Add account button");
      throw ex;
    }
  }

  /**
   * Click on Add account button on settings page
   * 
   * @Author Vivian Xie
   * @Created Feb/10/2016
   * @param: fieldName -- the field to input, value can be "desc","fromname","username","pwd"
   * @param: value -- the string that will be input
   */
  public void inputAccountInfoOnNewExternalAccountPage(String fieldName, String value) {
    Logging.info("Starting to input external account " + fieldName + ", value:" + value);
    String settings_mail_external_account_info_input =
        PropertyHelper.coreSettings.getPropertyValue("settings_mail_external_account_info_input")
            .replace("+variable+", fieldName);
    Logging.info(settings_mail_external_account_info_input);
    try {
      Tools.scrollElementIntoViewByXpath(settings_mail_external_account_info_input);
      WebElement ele = getWebElement(settings_mail_external_account_info_input, false);
      ele.clear();
      ele.sendKeys(value);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      Logging.info("Failed to input " + fieldName);
      throw ex;
    }
  }

  /**
   * Select protocol on new external account page
   * 
   * @param protocolName value can be IMAP or POP
   * @Author Vivian Xie
   * @Created Feb/10/2016
   */
  public void setOnExternalAccountProtocolValue(String protocolName) {
    Logging.info("Starting to select protocol ");
    String settings_mail_external_account_protocol_input_component = PropertyHelper.coreSettings
        .getPropertyValue("settings_mail_external_account_protocol_input_component");
    Logging.info(settings_mail_external_account_protocol_input_component);
    try {
      this.selectItem_ByJS(settings_mail_external_account_protocol_input_component, protocolName);
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      Logging.info("Failed to set protocol value:" + protocolName);
      throw ex;
    }
  }


  /**
   * Click head button include Save, Cancel, Delete button
   * 
   * @param buttonName -- Save, Cancel, Delete
   * @Author Vivian Xie
   * @Created Feb/10/2016
   */
  public void clickCreateExternalAccountHeadButton(String buttonName) {
    Logging.info("Starting to click on " + buttonName + " button");
    String setting_head_button =
        PropertyHelper.coreSettings.getPropertyValue("settings_external_account_head_button").replace("+variable+",
            buttonName);
    try {
      Tools.scrollElementIntoViewByXpath(setting_head_button);
      getWebElement(setting_head_button, true).click();
      waitForLoadMaskDismissed();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      Logging.info("Failed to click " + buttonName + " button");
      throw ex;
    }
  }

  /**
   * Get all the email account in Settings
   * 
   * @return all the email account name.
   * @Author Vivian Xie
   * @Created Feb/10/2016
   */
  public ArrayList<String> getAllMailAccountInSettings() {
    Logging.info("Starting to get all mail account in settings.");
    String settings_mail_external_account_button =
        PropertyHelper.coreSettings.getPropertyValue("settings_mail_account");
    ArrayList<String> subjList = null;
    try { 
      List<WebElement> account = getWebElements(settings_mail_external_account_button, false);
      subjList = new ArrayList<String>();
      for (int i = 1; i < account.size(); i++) {
        subjList.add(account.get(i).getText());
      }
     
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      Logging.info("Failed to get all the external account");
      throw ex;
    }
   
    Logging.info("All the exteral account are:"+subjList.toString());
    return subjList;
  }



  /**
   * Remove all external account;
   * 
   * @Author Vivian Xie
   * @Created Feb/10/2016
   */
  public void removeAllExternalAccount() {
    Logging.info("Starting to remove all external accounts");
    String settings_mail_external_account_button =
        PropertyHelper.coreSettings.getPropertyValue("settings_mail_account");
    try {
      List<WebElement> account = getWebElements(settings_mail_external_account_button, true);
      for (int i = 1; i < account.size(); i++) {
        account.get(i).click();
        clickCreateExternalAccountHeadButton(ConstantName.DELETE);
        clickButtonOnMessageBox(ConstantName.YES);
      }
      waitForLoadMaskDismissed();
    } catch (WebDriverException ex) {
      ex.printStackTrace();
      Logging.info("Failed to delete all the external account");
      throw ex;
    }
  }

  /**
   * Click on Image Block option
   * 
   * @Author Vivian Xie
   * @param  OptionText -- Value can be "Allow all images","Allow from my contacts only"," Allow all except spam","Block all images"
   * @Created Feb/21/2016
   */
public void clickImageBlockOption(String OptionTitle){
  
  Logging.info("Starting to click on image block image option:"+OptionTitle);
  String mail_block_image_option =
      PropertyHelper.coreSettings.getPropertyValue("mail_block_image_option").replace("+variable+", OptionTitle);
  Logging.info(mail_block_image_option);
  try {
    getWebElement(mail_block_image_option, false).click();
 
  } catch (WebDriverException ex) {
    ex.printStackTrace();
    Logging.info("Failed to click on image block image option");
    throw ex;
  }
  
  
}

}
  