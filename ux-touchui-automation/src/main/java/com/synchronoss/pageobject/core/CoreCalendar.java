package synchronoss.com.pageobject.core;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class CoreCalendar extends PageObjectBase {
	private String reminderSubject = "Reminder: Calendar reminder at ";

	/**
	 * Input event search keyword
	 * 
	 * @param keyword
	 *            The keyword for searching
	 * 
	 * @author Fiona Zhang
	 */
	public void addSearchKeyword(String keyword) {
		Logging.info("Start to search keyword : " + keyword);
		String search_keyword_field = PropertyHelper.coreCalendar
				.getPropertyValue("search_keyword_field");
		try {
			WebElement searchEventElement = this.getWebElement(
					search_keyword_field, true);

			searchEventElement.clear();
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
				searchEventElement.sendKeys(keyword);
			}
			else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
				this.elementSendKeys(searchEventElement,keyword);
			}
			searchEventElement.sendKeys(Keys.ENTER);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click search event button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickSearchEventButton() {
		Logging.info("Click search event button");

		String search_event_button = PropertyHelper.coreCalendar
				.getPropertyValue("search_event_button");
		try {
			WebElement searchEventElement = getWebElement(search_event_button,
					true);
			this.clickElement(searchEventElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click clear search field button 'x'
	 * 
	 * @author Fiona Zhang
	 */
	public void clickClearSearchKeyword() {
		Logging.info("Click clear search field button 'x'");

		String clear_search_field = PropertyHelper.coreCalendar
				.getPropertyValue("clear_search_field");
		try {
			WebElement clearKeywordButton = getWebElement(clear_search_field,
					false);
			this.clickElement(clearKeywordButton);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click search event button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickArrowDownButton() {
		Logging.info("Click the down arrow in calenar view");

		String event_date_arrow_down_button = PropertyHelper.coreCalendar
				.getPropertyValue("event_date_arrow_down_button");
		try {
			WebElement downArrowElement = getWebElement(
					event_date_arrow_down_button, true);
			this.clickElement(downArrowElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click add event button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickAddEventButton() {
		Logging.info("Click add event button");

		String add_event_button = PropertyHelper.coreCalendar
				.getPropertyValue("add_event_button");
		try {
			WebElement addEventElement = getWebElement(add_event_button, true);
			this.clickElement(addEventElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click specific event in event list
	 * 
	 * @param eventTitle
	 *            The title of the event
	 * 
	 * @author Fiona Zhang
	 */
	public void clickEventTitle(String eventTitle) {
		Logging.info("Click event title : " + eventTitle + " in calendar view");

		String event_title = PropertyHelper.coreCalendar
				.getPropertyValue("event_title");
		event_title = String.format(event_title, eventTitle);
		try {
			Tools.scrollElementIntoViewByXpath(event_title);
			WebElement addEventElement = getWebElement(event_title, true);
			this.clickElement(addEventElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the view model to see the calendar
	 * 
	 * @param viewModel
	 *            the model of the event
	 * 
	 * @author Fiona Zhang
	 */
	public void clickViewModel(String viewModel) {
		Logging.info("Click view model : " + viewModel + " in calendar view");
		String viewModelXpath = null;

		switch (viewModel.toLowerCase()) {
		case "week":
			viewModelXpath = PropertyHelper.coreCalendar
					.getPropertyValue("week_view_button");
			break;
		case "month":
			viewModelXpath = PropertyHelper.coreCalendar
					.getPropertyValue("month_view_button");
			break;
		case "day":
			viewModelXpath = PropertyHelper.coreCalendar
					.getPropertyValue("day_view_button");
			break;
		case "agenda":
			viewModelXpath = PropertyHelper.coreCalendar
					.getPropertyValue("list_view_button");
			break;

		}
		try {
			WebElement addEventElement = getWebElement(viewModelXpath, true);
			this.clickElement(addEventElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click Cancel or Go button in date time select bar
	 * 
	 * @author Fiona Zhang
	 */
	public void clickButtonOnTimePicker(String action) {
		Logging.info("Click go to today button in view mode panel");

		String date_select_head_bar = PropertyHelper.coreCalendar
				.getPropertyValue("date_select_head_bar");
		date_select_head_bar = String.format(date_select_head_bar,
				toCapitlize(action));
		try {
			WebElement goToTodayElement = getWebElement(date_select_head_bar,
					false);
			this.clickElement(goToTodayElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click go to today button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickGoToTodayButton() {
		Logging.info("Click go to today button in view mode panel");

		String go_to_today_button = PropertyHelper.coreCalendar
				.getPropertyValue("go_to_today_button");
		try {
			WebElement goToTodayElement = getWebElement(go_to_today_button,
					false);
			this.clickElement(goToTodayElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click go to date button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickGoToDateButton() {
		Logging.info("Click go to date button in view mode panel");

		String go_to_date_button = PropertyHelper.coreCalendar
				.getPropertyValue("go_to_date_button");
		try {
			WebElement gotoDateElement = getWebElement(go_to_date_button, false);
			this.clickElement(gotoDateElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click refresh button in view model panel
	 * 
	 * @author Fiona Zhang
	 */
	public void clickRefreshButton() {
		Logging.info("Click refresh button in view mode panel");

		String refresh_button = PropertyHelper.coreCalendar
				.getPropertyValue("refresh_button");
		try {
			WebElement refreshElement = getWebElement(refresh_button, false);
			this.clickElement(refreshElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Go to the date directely by executive javascript
	 * 
	 * @param LocalDate
	 *            date expected date
	 * 
	 * @author Fiona Zhang
	 */
	public void goToDate_ByJS(LocalDate date) {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("YYYY-LL-dd");
		Logging.info("Go to date by js: " + date.format(format));
		try {
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			js.executeScript("Touch.controllerRef.calendar.main.pickDateToSelect(undefined, new Date('"
					+ date.format(format) + "'))");
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Go to the date directely by executive javascript
	 * 
	 * @param LocalDate
	 *            date expected date
	 * 
	 * @author Fiona Zhang
	 */
	public void goToDate_ByJS(LocalDateTime date) {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("YYYY-LL-dd");
		Logging.info("Go to date by js: " + date.format(format));
		try {
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			js.executeScript("Touch.controllerRef.calendar.main.pickDateToSelect(undefined, new Date('"
					+ date.format(format) + "'))");
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}
	
	
	/**
	 * Get event title bar date of current day in calendar view
	 * 
	 * @author Fiona Zhang
	 */
	public String getEventTitleBar() {
		Logging.info("Get event title bar text in calendar view");

		String event_title_bar = PropertyHelper.coreCalendar
				.getPropertyValue("event_title_bar");
		try {
			Tools.scrollElementIntoViewByXpath(event_title_bar);
			WebElement eventTimeElement = getWebElement(event_title_bar, false);
			Logging.info(eventTimeElement.getText());
			return eventTimeElement.getText();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}	}

	/**
	 * Get event time in current day
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public String getEventTime(String eventTitle) {
		Logging.info("Get event time in current event item");

		String event_time = PropertyHelper.coreCalendar
				.getPropertyValue("event_time");
		event_time = String.format(event_time, eventTitle);
		try {
			Tools.scrollElementIntoViewByXpath(event_time);
			WebElement eventTimeElement = getWebElement(event_time, true);
			Logging.info(eventTimeElement.getText());
			return eventTimeElement.getText();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get event category in current day
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public String getEventCategory(String title) {
		Logging.info("Get event category in current event item");

		String event_category = PropertyHelper.coreCalendar
				.getPropertyValue("event_category");
		event_category = String.format(event_category, title);
		try {
			Tools.scrollElementIntoViewByXpath(event_category);
			WebElement eventTimeElement = getWebElement(event_category, false);
			Logging.info(eventTimeElement.getAttribute("class"));
			return eventTimeElement.getAttribute("class");
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get event space icon info in current day
	 * 
	 * it may include recurrence, reminder, shared icon info
	 * 
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getEventSpaceIconInfo(String title) {
		Logging.info("Get event space icon info in current event item");

		String event_icon_space = PropertyHelper.coreCalendar
				.getPropertyValue("event_icon_space");
		event_icon_space = String.format(event_icon_space, title);
		ArrayList<String> eventList = new ArrayList<String>();

		try {
			Tools.scrollElementIntoViewByXpath(event_icon_space);
			List<WebElement> eventTimeElement = getWebElements(
					event_icon_space, true);
			for (WebElement el : eventTimeElement)
				eventList.add(el.getAttribute("class"));
			Logging.info(eventList.toString());
			return eventList;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get event title in current day
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getEventListInCurrentDay() {
		Logging.info("Get event list in current calendar day");

		String event_list = PropertyHelper.coreCalendar
				.getPropertyValue("event_list");
		String event_list_container = PropertyHelper.coreCalendar
				.getPropertyValue("event_list_container");
		ArrayList<String> eventList = new ArrayList<String>();

		try {
			Tools.scrollElementIntoViewByXpath(event_list_container);
			List<WebElement> eventItemsElement = getWebElements(event_list,
					true);
			for (WebElement el : eventItemsElement) {
				Logging.info("get event list as : " + el.getText());
				eventList.add(el.getText());
			}
			Logging.info(eventList.toString());
			return eventList;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get event search result in event list view
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getSearchResultEventsList() {

		Logging.info("Get event list in search result event list");

		String event_search_list = PropertyHelper.coreCalendar
				.getPropertyValue("event_search_result_list");
		ArrayList<String> eventList = new ArrayList<String>();

		try {
			List<WebElement> eventItemsElement = getWebElements(
					event_search_list, true);
			for (WebElement el : eventItemsElement) {
				Logging.info("get event list as : " + el.getText());
				eventList.add(el.getText());
			}
			Logging.info(eventList.toString());
			return eventList;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get no event list
	 * 
	 * @return true - no event list display false - no event list not display
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public boolean getNoEventsInList() {
		Logging.info("Check if the event list is empty");

		boolean result = false;

		String no_event_list = PropertyHelper.coreCalendar
				.getPropertyValue("no_event_list");

		try {
			WebElement noEventElement = getWebElement(no_event_list, false);
			if (noEventElement.getAttribute("class").contains("display: none"))
				result = false;
			else
				result = true;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return result;
	}

	/**
	 * Get expected reminder of the subject
	 * 
	 * @param startDateTime
	 *            The start date time
	 * 
	 * @param endDateTime
	 *            The end date time
	 * 
	 * @param timeZone
	 *            The system time zone , get from login set up part
	 * 
	 * 
	 * @author Fiona Zhang
	 */

	public String getExpectedReminderSubject(LocalDateTime startDateTime,
			LocalDateTime endDateTime, String timeZone) {
		StringBuilder sb = new StringBuilder();
		DateTimeFormatter format = DateTimeFormatter
				.ofPattern("EEE dd MMM yyyy h:mma");
		DateTimeFormatter hoursOnlyFormat = DateTimeFormatter
				.ofPattern("h:mma");

		startDateTime.format(format);
		endDateTime.format(format);
		sb.append(reminderSubject);
		sb.append(startDateTime.format(format));
		sb.append(" - ");
		/*
		 * if(notSameDay) sb.append(endDateTime.format(format)); else
		 */sb.append(endDateTime.format(hoursOnlyFormat));
		sb.append(" ");
		sb.append(timeZone);

		Logging.info("get expected reminder subject as "
				+ sb.toString().replace("AM", "am").replace("PM", "pm"));
		return sb.toString().replace("AM", "am").replace("PM", "pm");
	}

	/**
	 * Get search bar status in current day
	 * 
	 * @return true - the bar is displayed
	 *  false - the bar is not displayed
	 * 
	 * @author Fiona Zhang
	 */
	public boolean getStatusOfSearchBar() {
		Logging.info("Get search bar status");
		boolean result = false;
		String search_keyword_field = PropertyHelper.coreCalendar
				.getPropertyValue("search_keyword_field");
		try {
			WebElement searchKeywordField = getWebElement(search_keyword_field,
					false);
			result= searchKeywordField.isDisplayed();
			Logging.info("Is search bar displayed: " + result);
			return result;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	

	/*	*//**
	 * Get current view model, including week/month views.
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	/*
	 * public String getEventViewModelOfWeekAndMonth() {
	 * 
	 * Logging.info("Get current view model"); String resultViewModel = null;
	 * String current_cell_view_model = PropertyHelper.coreCalendar
	 * .getPropertyValue("current_cell_view_model"); try { WebElement
	 * eventTimeElement = getWebElement( current_cell_view_model, false);
	 * Logging.info(eventTimeElement.getAttribute("class")); resultViewModel =
	 * eventTimeElement.getAttribute("class").trim(); if
	 * (resultViewModel.equals("month")) resultViewModel = "monthview"; if
	 * (resultViewModel.equals("week")) resultViewModel = "weekview"; return
	 * resultViewModel; } catch (WebDriverException ex) { ex.printStackTrace();
	 * throw ex; } }
	 *//**
	 * Get current view model, including day/week/month/list views.
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	/*
	 * public String getEventViewModelOfList() {
	 * 
	 * Logging.info("Get current view model"); String resultViewModel = null;
	 * String current_list_view_model = PropertyHelper.coreCalendar
	 * .getPropertyValue("current_list_view_model"); try { WebElement
	 * eventTimeElement = getWebElement( current_list_view_model, false);
	 * Logging.info(eventTimeElement.getAttribute("class")); resultViewModel =
	 * eventTimeElement.getAttribute("class").trim(); if
	 * (resultViewModel.contains("automation-calendar-listview"))
	 * resultViewModel = "listview"; else if
	 * (resultViewModel.contains("automation-calendar-dayview")) resultViewModel
	 * = "dayview";
	 * 
	 * return resultViewModel; } catch (WebDriverException ex) {
	 * ex.printStackTrace(); throw ex; } }
	 */

	/**
	 * Get current view model, including day/week/month/list views.
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public String getCurrentViewModel() {

		try {
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			Logging.info("Get view  model of current view as "
					+ (String) js
							.executeScript("return CalendarManager.currentView"));
			return (String) js
					.executeScript("return CalendarManager.currentView");
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Get start date value
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public String getStartDate() {
		String start_week_day = PropertyHelper.coreCalendar
				.getPropertyValue("start_week_day");
		try {
			List<WebElement> startWeekDayElement = getWebElements(
					start_week_day, true);
			Logging.info("get element number : "+startWeekDayElement.size());
			Logging.info("Get start week day as : "
					+ startWeekDayElement.get(0).getText());
			Logging.info("Get start week day as : "
					+ startWeekDayElement.get(1).getText());
			Logging.info("Get start week day as : "
					+ startWeekDayElement.get(2).getText());

			return startWeekDayElement.get(1).getText();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	/**
	 * swipe the cell view ,like month view , week view
	 * 
	 * @param backOrForth
	 *            - true, the page will go to previous view - false, the page
	 *            will go to next view
	 * @author Fiona Zhang
	 **/
	public void swipeCellView(boolean backOrForth) {
		Logging.info("swipe the cell view of calendar");

		String xpath = PropertyHelper.coreCalendar
				.getPropertyValue("swipe_calendar_cell_view");
		try {
			if (backOrForth)
				super.swipeView(xpath, 80, 0);
			else
				super.swipeView(xpath, -80, 0);
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * swipe the list view ,like day view
	 * 
	 * @param backOrForth
	 *            - true, the page will go to previous view - false, the page
	 *            will go to next view
	 * @author Fiona Zhang
	 **/
	public void swipeListView(boolean backOrForth) {
		Logging.info("swipe the list view of calendar");
		String xpath = PropertyHelper.coreCalendar
				.getPropertyValue("swipe_calendar_list_view");
		Logging.info(xpath);
		int x = 80;
		if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID))
			x = 400;
		try {
			if (backOrForth)
				super.swipeView(xpath, x, 0);
			else
				super.swipeView(xpath, -x, 0);
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Select a date by scroll down to a random date
	 * 
	 * @author Fiona Zhang
	 * 
	 **/
	public void scrollDateInTimeSelect() {
		Logging.info("Scroll the date time picker");
		String date_picker_year = PropertyHelper.coreCalendar
				.getPropertyValue("date_picker_year");
		String date_picker_month = PropertyHelper.coreCalendar
				.getPropertyValue("date_picker_month");
		String date_picker_day = PropertyHelper.coreCalendar
				.getPropertyValue("date_picker_day");
		try {
			super.swipeView(date_picker_year, 0, -50);
			super.swipeView(date_picker_month, 0, -20);
			super.swipeView(date_picker_day, 0, -30);

			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}



	

}
