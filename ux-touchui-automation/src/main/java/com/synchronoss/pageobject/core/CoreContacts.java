package synchronoss.com.pageobject.core;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class CoreContacts extends PageObjectBase {
	/**
	 * Trigger add contact page
	 * 
	 * @author Jason Song
	 */
	public void clickAddContactButton() {
		Logging.info("Click add contact button");

		String add_Contact_Plus = PropertyHelper.coreContacts
				.getPropertyValue("add_Contact_Plus");
		try {
			WebElement addContactElement = getWebElement(add_Contact_Plus, true);
			this.clickElement(addContactElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Clicking add contact button.");
	}

	/**
	 * Click user define option in contact action tool bar
	 *
	 * @param option
	 * 
	 * 
	 * @author Fiona Zhang
	 */

	public void clickActionButtonOnSlideToolbar(String option) {
		Logging.info("Click " + option + " option in contact action tool bar ");
		String action = null;
		switch (option.toLowerCase()) {
		case "group":
			action = PropertyHelper.coreContacts
					.getPropertyValue("tool_bar_add_to_group");
			break;
		case "mail":
			action = PropertyHelper.coreContacts
					.getPropertyValue("tool_bar_send_mail");
			break;
		case "event":
			action = PropertyHelper.coreContacts
					.getPropertyValue("tool_bar_invite_event");
			break;
		case "move":
			action = PropertyHelper.coreContacts
					.getPropertyValue("tool_bar_move_to_addr_book");
			break;
		case "delete":
			action = PropertyHelper.coreContacts
					.getPropertyValue("tool_bar_delete");
			break;
		}

		try {
			WebElement toolbarIconElement = getWebElement(action, false);
			this.clickElement(toolbarIconElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}

	/**
	 * Click clear button in search bar field
	 *
	 * @author Fiona Zhang
	 */
	public void clickClearButtonInSearchBar() {
		Logging.info("Click 'x' button in search bar field");

		String contact_search_clear_icon = PropertyHelper.coreContacts
				.getPropertyValue("contact_search_clear_icon");
		try {
			WebElement clearButtonElement = getWebElement(
					contact_search_clear_icon, false);
			this.clickElement(clearButtonElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Search keyword in contact view
	 * 
	 * @param keywords
	 *            The search keyword
	 * 
	 * @author Jason Song
	 */
	public void searchContactbyKeywords(String keywords) {
		Logging.info("Searching contact by keyword: " + keywords);
		String contact_search_input = PropertyHelper.coreContacts
				.getPropertyValue("contact_search_input");
		try {
			clickSearchBar();
			WebElement contactSearchElement = getWebElement(
					contact_search_input, true);
			contactSearchElement.clear();
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				contactSearchElement.sendKeys(keywords);
			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				this.elementSendKeys(contactSearchElement, keywords);
			}
			contactSearchElement.sendKeys(Keys.ENTER);

			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Check if search bar displays, if yes, other function will input the
	 * keyword directly, \n if no, will click search icon to display the search
	 * bar
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public void clickSearchBar() {
		String contact_search_field = PropertyHelper.coreContacts
				.getPropertyValue("contact_search_field");
		String contact_search_button = PropertyHelper.coreContacts
				.getPropertyValue("contact_search_button");
		try {
		    waitForLoadMaskDismissed();
			WebElement contactSearchField = getWebElement(contact_search_field,
					false);
//			if (contactSearchField.getAttribute("style").contains("display")) {
			if(!contactSearchField.isDisplayed()){
				WebElement contactSearchButton = getWebElement(
						contact_search_button, false);
				this.clickElement(contactSearchButton);
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click one contact by name and switch to detail page
	 * 
	 * @param contactName
	 *            The contact name in the list, so could go to detail view by
	 *            click the name
	 * @author Fiona Zhang
	 */
	public void clickContactbyName(String contactName) {
		Logging.info("Enter contact detail view of " + contactName);

		String contacter = PropertyHelper.coreContacts
				.getPropertyValue("contact_item");
		contacter = String.format(contacter, contactName);

		try {

			Tools.scrollElementIntoViewByXpath(contacter);
			WebElement contacter_item = getWebElement(contacter, true);
			this.clickElement(contacter_item);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	/**
	 * Click one contact by name and switch to detail page
	 * 
	 * @param emailAddress
	 *            The email address in the list, so could go to detail view by
	 *            click the address
	 * @author Fiona Zhang
	 */
	public void clickContactbyEmail(String emailAddress) {
		Logging.info("Enter contact detail view of " + emailAddress);

		String contact_item_in_list_email = PropertyHelper.coreContacts
				.getPropertyValue("contact_item_in_list_email");
		contact_item_in_list_email = String.format(contact_item_in_list_email, emailAddress);

		try {

			Tools.scrollElementIntoViewByXpath(contact_item_in_list_email);
			WebElement contacter_item = getWebElement(contact_item_in_list_email, true);
			this.clickElement(contacter_item);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Swipe the contact item in contact list
	 * 
	 * @param firstName
	 *            The first name of contact
	 * @author Fiona Zhang
	 */
	public void swipeContactByName(String firstName) {
		Logging.info("Swipe action on current contact item and trigger tool bar...");

		String contact_item_name = PropertyHelper.coreContacts
				.getPropertyValue("contact_item_name");
		contact_item_name = String.format(contact_item_name, firstName);

		try {
			WebElement contactItemElement = getWebElement(contact_item_name,
					true);
			String addressbookID = contactItemElement.getAttribute("id");

			String addrbookEdit = PropertyHelper.coreContacts
					.getPropertyValue("contact_item_tool_bar_js");
			addrbookEdit = String.format(addrbookEdit, addressbookID);
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			executor.executeScript(addrbookEdit);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}
	
	
  /**
   * Swipe the first contact item in contact list and delete
   * 
   * @author Fiona Zhang
   */
  public void swipeContactAndDelete() {
    Logging.info("Swipe action on the first contact item and trigger tool bar then delete the contact...");

    String contact_item_first_item =
        PropertyHelper.coreContacts.getPropertyValue("contact_item_first_item");

    try {
      WebElement contactItemElement = getWebElement(contact_item_first_item, true);
      String contactId = contactItemElement.getAttribute("id");
      String contactEdit = PropertyHelper.coreContacts.getPropertyValue("contact_item_tool_bar_js");
      contactEdit = String.format(contactEdit, contactId);
      JavascriptExecutor executor = (JavascriptExecutor) Base.base.getDriver();
      executor.executeScript(contactEdit);
      this.clickActionButtonOnSlideToolbar(ConstantName.DELETE);
      this.clickButtonOnMessageBox(ConstantName.YES);
      this.waitForLoadMaskDismissed();

    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }

  }

	/**
	 * Swipe the contact item in auto suggest list
	 * 
	 * @param autoSuggestName
	 *            The auto suggest name of contact
	 * @author Fiona Zhang
	 */
	public void swipeAutoSuggestContactByName(String autoSuggestName) {
		Logging.info("Swipe action on current contact item and trigger tool bar...");

		String auto_complete_contact_item = PropertyHelper.coreContacts
				.getPropertyValue("auto_complete_contact_item");
		auto_complete_contact_item = String.format(auto_complete_contact_item,
				autoSuggestName);

		try {
			WebElement contactItemElement = getWebElement(
					auto_complete_contact_item, false);
			String addressbookID = contactItemElement.getAttribute("id");

			String addrbookEdit = PropertyHelper.coreContacts
					.getPropertyValue("contact_item_tool_bar_js");
			addrbookEdit = String.format(addrbookEdit, addressbookID);
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			executor.executeScript(addrbookEdit);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Get the auto contact list items
	 * 
	 * @author Fiona Zhang
	 */
	public List<WebElement> getAutoContactList() {
		List<WebElement> autoCompleteItemsElement = null;
		String auto_complete_contact_item_list = PropertyHelper.coreContacts
				.getPropertyValue("auto_complete_contact_item_list");
		try {
			autoCompleteItemsElement = getWebElements(
					auto_complete_contact_item_list, true);
			Logging.info("Get count : " + autoCompleteItemsElement.size()
					+ " auto contact item list web element");

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return autoCompleteItemsElement;
	}
	
  /**
   * Get contact list in current page
   * 
   * @author Fiona Zhang
   */
  public ArrayList<String> getCurrentContactList() {
    Logging.info("Start to get current contact list");

    ArrayList<String> contactList = new ArrayList<String>();
    String contact_item_search_result_list =
        PropertyHelper.coreContacts.getPropertyValue("contact_item_search_result_list");
    try {
      List<WebElement> contactElements = getWebElements(contact_item_search_result_list, true);
      for (WebElement el : contactElements) {
        contactList.add(el.getText());
      }

    } catch (WebDriverException ex) {
      ex.printStackTrace();
      throw ex;
    }
    Logging.info("Get current contact list as : " + contactList.toString());
    return contactList;
  }


	/**
	 * Get if the contact list is empty result
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isContactListEmpty() {

		String contact_empty_list = PropertyHelper.coreContacts
				.getPropertyValue("contact_empty_list");
		boolean result = false;
		try {
			WebElement emptyListElement = getWebElement(contact_empty_list,
					true);
			Logging.info(emptyListElement.getAttribute("style"));
			result = emptyListElement.isDisplayed();
			Logging.info("Is contact list empty : " + result);
			return result;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	public boolean is_webURLCanVisit(String value) {
		// TODO Auto-generated method stub
		return false;
	}

}
