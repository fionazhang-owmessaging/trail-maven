package synchronoss.com.pageobject.core;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;


public class CoreNavigation extends PageObjectBase {
	private static String MAILPAGE = "mail";
	private static String CONTACTPAGE = "concat";
	private static String CALENDARPAGE = "cal";
	private static String TASKPAGE = "task";
	private static String SETTINGPAGE = "set";
	
	
	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Jan 01, 2011 <br>
	 * <b>Description</b><br>
	 * This method simulates the user clicking tab page
	 * Navigation pane.
	 * 
	 * @return void
	 */
	public void clickNavigationButton(String pageName) {
		Logging.info("Click navigation button , and navigate to : " + pageName + " page.");
		String tab_path = PropertyHelper.coreNavigationFile
				.getPropertyValue(String.format("tab_icon"));
		try {
			WebElement el = getWebElement(String.format(tab_path,pageName),true);
			 this.clickElement(el);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Dec 21, 2015 <br>
	 * <b>Description</b><br>
	 * This method simulates the user clicking on the "menu" button on
	 * Navigation pane.
	 * 
	 * @return void
	 */
	public void clickMenuButton() {
		String xpathParam = null;
		String currentWebPage = Base.base.getDriver().getCurrentUrl();
		String currentPage = currentWebPage.substring(currentWebPage
				.lastIndexOf("/") + 1);

		switch (currentPage) {
		case "mail":
			xpathParam = MAILPAGE;
			break;
		case "contacts":
			xpathParam = CONTACTPAGE;
			break;
		case "calendar":
			xpathParam = CALENDARPAGE;
			break;
		case "tasks":
			xpathParam = TASKPAGE;
			break;
		case "settings":
			xpathParam = SETTINGPAGE;
			break;
		}

		Logging.info("Click menu button in "+xpathParam+" on left side bar");

		String menu_button = PropertyHelper.coreNavigationFile
				.getPropertyValue("menu_button");

		try {
			WebElement el = getWebElement(String.format(menu_button, xpathParam),true);
			 this.clickElement(el);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Jan 01, 2011 <br>
	 * <b>Description</b><br>
	 * This method simulates the user clicking on the "Logout" button on
	 * Navigation pane.
	 * 
	 * @return void
	 */
	public void clickLogoutButton() {
		Logging.info("Click log out button");
		String logout_button = PropertyHelper.coreNavigationFile
				.getPropertyValue("logout_button");
		try {
			WebElement el = getWebElement(logout_button, false);
			 this.clickElement(el);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}


}
