package synchronoss.com.pageobject.core;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import io.appium.java_client.ios.IOSDriver;
import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class CoreEventDetail extends PageObjectBase {

	/**
	 * Add event title
	 * 
	 * @param eventTitle
	 *            Title of the event
	 * 
	 * @author Fiona
	 */
	public void addEventTitle(String eventTitle) {
		Logging.info("add event title as " + eventTitle);

		String event_title = PropertyHelper.coreEventDetail
				.getPropertyValue("event_title");
		try {
			WebElement eventTitleElement = getWebElement(event_title, true);
			eventTitleElement.clear();
		 	if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				eventTitleElement.sendKeys(eventTitle.trim());
			 } else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
		 		this.elementSendKeys(eventTitleElement, eventTitle);
		 	}
			eventTitleElement.sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Add event description
	 * 
	 * @param eventDescription
	 *            Description of the event
	 * 
	 * @author Fiona
	 */
	public void addEventDescription(String eventDescription) {
		Logging.info("add event description as " + eventDescription);

		String event_description = PropertyHelper.coreEventDetail
				.getPropertyValue("event_description");
		try {
			Tools.scrollElementIntoViewByXpath(event_description);
			WebElement eventDesciptionElement = getWebElement(
					event_description, false);
			eventDesciptionElement.sendKeys(eventDescription);
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS))
				eventDesciptionElement.sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Add event repeate interval
	 * 
	 * @param repeateIntervalDay
	 *            interval days for the event
	 * 
	 * @author Fiona
	 */
	public void addEventRepeatInterval(int repeateIntervalDay) {
		Logging.info("add event repeat interval as " + repeateIntervalDay);

		String event_repeat_interval = PropertyHelper.coreEventDetail
				.getPropertyValue("event_repeat_interval");
		try {
			Tools.scrollElementIntoViewByXpath(event_repeat_interval);
			WebElement repeateIntervalDayElement = getWebElement(
					event_repeat_interval, false);
			repeateIntervalDayElement.clear();
			repeateIntervalDayElement.sendKeys(String
					.valueOf(repeateIntervalDay));
			repeateIntervalDayElement.sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Add event repeate occurrences
	 * 
	 * @param occurrences
	 *            occurrences days for the event
	 * 
	 * @author Fiona
	 */
	public void addEventRepeatOccurrences(int occurrences) {
		Logging.info("add event repeat occurrences as " + occurrences);

		String event_repeat_occurrence = PropertyHelper.coreEventDetail
				.getPropertyValue("event_repeat_occurrence");
		try {
			Tools.scrollElementIntoViewByXpath(event_repeat_occurrence);
			WebElement repeateOccurenceElement = getWebElement(
					event_repeat_occurrence, false);
			repeateOccurenceElement.clear();
			repeateOccurenceElement.sendKeys(String.valueOf(occurrences));
			repeateOccurenceElement.sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	
	/**
	 * Add event attendenee
	 * 
	 * @param attendenee
	 *            invite attendenee for the event
	 * @param isClickEnterKey
	 *             true - will press enter key
	 *             false - will not press enter key
	 * @author Fiona
	 */
	public void addEventAttendees(String attendenee, boolean isClickEnterKey) {
		Logging.info("add event attendenee as " + attendenee);

		String event_attendee_input = PropertyHelper.coreEventDetail
				.getPropertyValue("event_attendee_input");
		try {
			Tools.scrollElementIntoViewByXpath(event_attendee_input);
			WebElement attendeeElement = getWebElement(event_attendee_input,
					false);
			attendeeElement.clear();
			attendeeElement.sendKeys(attendenee);
			if(isClickEnterKey){
			    attendeeElement.sendKeys(Keys.ENTER);
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	/**
	 * Add event location
	 * 
	 * @param locate
	 *            location for the event
	 * 
	 * @author Fiona
	 */
	public void addEventLocation(String locate) {
		Logging.info("add event location as " + locate);

		String event_location = PropertyHelper.coreEventDetail
				.getPropertyValue("event_location");
		try {
			Tools.scrollElementIntoViewByXpath(event_location);
			WebElement locationElement = getWebElement(event_location, false);
			locationElement.clear();
			locationElement.sendKeys(locate);
			locationElement.sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Add event attendee search keyword
	 * 
	 * @param contactGroup
	 *            contact group for the event
	 * 
	 * @author Fiona
	 */
	public void addAttendeeSearchKeyword(String contactGroup) {
		Logging.info("add event attendee search keyword as " + contactGroup);

		String ateendee_search_keyword = PropertyHelper.coreEventDetail
				.getPropertyValue("ateendee_search_keyword");
		try {
			WebElement locationElement = getWebElement(ateendee_search_keyword,
					false);
			locationElement.clear();
			locationElement.sendKeys(contactGroup);
			locationElement.sendKeys(Keys.RETURN);
			if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
			IOSDriver driver = (IOSDriver) Base.base.getDriver();
			driver.hideKeyboard();
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * According to the parameter to click the all day event check box
	 * 
	 * @param checkOrNot
	 *            Will check the all day option first, then according to the
	 *            parameter, to click the all day option. if True, it will
	 *            trigger it as all day event, if false, it will be common event
	 * 
	 * @author Fiona
	 */
	public void clickAllDayCheckbox(boolean checkOrNot) {
		if(checkOrNot)
		Logging.info("Set the event as all day event");
		else
			Logging.info("Set the event not as all day event");

		String event_allday = PropertyHelper.coreEventDetail
				.getPropertyValue("event_allday");
		
		String event_startTime = PropertyHelper.coreEventDetail
				.getPropertyValue("event_startTime");
		try {
			Tools.scrollElementIntoViewByXpath(event_allday);
			WebElement allDayEventElement = getWebElement(event_allday, false);
			WebElement EventStartTimeElement = getWebElement(event_startTime, false);

			if (checkOrNot && EventStartTimeElement.isDisplayed() || !checkOrNot
					&& !EventStartTimeElement.isDisplayed())
				 this.clickElement(allDayEventElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the back to calendar button
	 * 
	 * @author Fiona
	 */
	public void clickBackToCalendarButton() {
		Logging.info("Click back to calendar button");

		String back_to_calendar = PropertyHelper.coreEventDetail
				.getPropertyValue("back_to_calendar");
		try {
			WebElement backToCalendarButtonElement = getWebElement(
					back_to_calendar, false);
			 this.clickElement(backToCalendarButtonElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click add attendee button
	 * 
	 * @author Fiona
	 */
	public void clickAddAttendeeButton() {
		Logging.info("Click add attendee button");

		String event_attendee_add = PropertyHelper.coreEventDetail
				.getPropertyValue("event_attendee_add");
		try {
			Tools.scrollElementIntoViewByXpath(event_attendee_add);
			WebElement addAttendeeButtonElement = getWebElement(
					event_attendee_add, false);
			 if(Base.platformName.equalsIgnoreCase(ConstantName.IOS))
			 this.clickElement(addAttendeeButtonElement);
			 else if(Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)){
					JavascriptExecutor addAttendeeButton = (JavascriptExecutor) Base.base
							.getDriver();
					addAttendeeButton.executeScript("arguments[0].click();", addAttendeeButtonElement);
			 }
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click delete attendee button according to the parameter
	 * 
	 * @param attendee
	 *            The attendee need to be deleted from event detail view
	 * 
	 * @author Fiona
	 */
	public void clickDeleteAttendeeButton(String attendee) {
		Logging.info("Get list in attendee field in event view");
		String attendee_list = PropertyHelper.coreEventDetail
				.getPropertyValue("attendee_list");
		String attendee_delete_button = PropertyHelper.coreEventDetail
				.getPropertyValue("attendee_delete_button");

		try {
			Tools.scrollElementIntoViewByXpath(attendee_list);
			List<WebElement> attendeeElement = this.getWebElements(
					attendee_list, false);
			List<WebElement> deleteAttendeeButtonElement = this.getWebElements(
					attendee_delete_button, false);

			for (int i = 0; i < attendeeElement.size(); i++) {
				String getValuejs = "return document.getElementById('"
						+ attendeeElement.get(i).getAttribute("id")
						+ "').value";
				JavascriptExecutor executor = (JavascriptExecutor) Base.base
						.getDriver();
				String result = (String) executor.executeScript(getValuejs);
				Logging.info("The current attendee is : " + result);
				if (result.contains(attendee)) {
					Logging.info("loops at : " + i);

					 this.clickElement(deleteAttendeeButtonElement.get(i));
				}
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click search attendee button
	 * 
	 * @author Fiona
	 */
	public void clickSearchAttendeeButton() {
		Logging.info("Click search attendee button");

		String event_attendee_search = PropertyHelper.coreEventDetail
				.getPropertyValue("event_attendee_search");
		try {
			Tools.scrollElementIntoViewByXpath(event_attendee_search);
			WebElement searchAttendeeButtonElement = getWebElement(
					event_attendee_search, false);
			 this.clickElement(searchAttendeeButtonElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the save button in event view
	 * 
	 * @author Fiona
	 */
	public void clickSaveButton() {
		Logging.info("Click save button");

		String event_save = PropertyHelper.coreEventDetail
				.getPropertyValue("event_save");
		try {
			Tools.scrollElementIntoViewByXpath(event_save);
			WebElement eventSaveButtonElement = getWebElement(event_save, false);
			 this.clickElement(eventSaveButtonElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click the save button in event view
	 * 
	 * @author Fiona
	 */
	public void clickSaveButtonWithValidation() {
		Logging.info("Click save with validation button");
		try {
			if (getValidationErrorOfEvent() == null)
				clickSaveButton();
			else {
				repairEvent(getValidationErrorOfEvent());
				clickSaveButton();
			}
			waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click on days button
	 * 
	 * @author Fiona
	 */
	public void clickOnDaysButton() {
		Logging.info("Click on days button");

		String event_repeat_on_day_button = PropertyHelper.coreEventDetail
				.getPropertyValue("event_repeat_on_day_button");
		try {
			Tools.scrollElementIntoViewByXpath(event_repeat_on_day_button);
			WebElement onDaysButtonElement = getWebElement(
					event_repeat_on_day_button, false);
			 this.clickElement(onDaysButtonElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Select the day
	 * 
	 * @author Fiona
	 */
	public void clickOnDaysPicker(DayOfWeek day) {
		Logging.info("Click on days button");

		String event_on_day_select = PropertyHelper.coreEventDetail
				.getPropertyValue("event_on_day_select");
		event_on_day_select = String.format(event_on_day_select, day);
		try {
			if (!this.getSelectedOnDays().toString().contains(day.toString())) {
				WebElement onDaysButtonElement = getWebElement(
						event_on_day_select, false);
				 this.clickElement(onDaysButtonElement);
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Repair the event
	 * 
	 * @author Fiona
	 */
	public void repairEvent(String validationOfEvent) {
		LocalDateTime repair = getEventDateTime(getSystemDate(),
				getEventTime(ConstantName.START)).plusMinutes(5);
		Logging.info("the repair time is : " + repair);
		if (validationOfEvent.contains("future event")) {
			selectItems(ConstantName.REMINDER, ConstantName.NONE);
			selectItems(ConstantName.REMINDER, ConstantName.ATTIME);
			selectEventTime(repair, ConstantName.START);
		}
	}

	/**
	 * Click the delete button in event view
	 * 
	 * @author Fiona
	 */
	public void clickDeleteButton() {
		Logging.info("Click delete button in event detail view");

		String event_delete = PropertyHelper.coreEventDetail
				.getPropertyValue("event_delete");
		try {
			WebElement eventDeleteButtonElement = getWebElement(event_delete,
					true);
			 this.clickElement(eventDeleteButtonElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Click Cancel or done button on top bar of day select view
	 * 
	 * @param action
	 *            cancel or done
	 * 
	 * @author Fiona
	 */
	public void clickOnDaysTopBarButton(String action) {
		Logging.info("Click " + action + " on top bar of on days");
		String on_day_action = null;
		switch (action.toLowerCase()) {
		case "cancel":
			on_day_action = PropertyHelper.coreEventDetail
					.getPropertyValue("event_on_day_cancel_action");
			break;
		case "done":
			on_day_action = PropertyHelper.coreEventDetail
					.getPropertyValue("event_on_day_done_action");
			break;
		}
		try {
			WebElement onDaySelectElement = getWebElement(on_day_action, false);
			 this.clickElement(onDaySelectElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click the attendee result in the list
	 * 
	 * @param contactGroup
	 *            the contact group need to be clicked
	 * 
	 * @author Fiona
	 */
	public void clickAttendeeSearchResult(String contactGroup) {
		Logging.info("Click the attendee " + contactGroup + "in search result");
		String attendee_search_result = PropertyHelper.coreEventDetail
				.getPropertyValue("attendee_search_result");
		attendee_search_result = String.format(attendee_search_result,
				contactGroup);
		try {
			Tools.scrollElementIntoViewByXpath(attendee_search_result);
			WebElement attendeeSelectElement = getWebElement(
					attendee_search_result, true);
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				Logging.info(attendeeSelectElement.getAttribute("class"));
				this.clickElement(attendeeSelectElement);
			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID))
				this.clickElement(attendeeSelectElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}

	/**
	 * Click the button on top bar of attendee select view
	 * 
	 * @param action
	 *            Done or cancel
	 * 
	 * @author Fiona
	 */
	public void clickButtonOnContactSelect(String action) {
		Logging.info("Click " + action
				+ " on top bar of attendee select pop up");
		String attendeAction = null;
		if (action.equalsIgnoreCase("done"))
			attendeAction = PropertyHelper.coreEventDetail
					.getPropertyValue("attendee_search_title_bar_done");
		else
			attendeAction = PropertyHelper.coreEventDetail
					.getPropertyValue("attendee_search_title_bar_cancel");
		try {
			WebElement onDaySelectElement = getWebElement(attendeAction, false);
			 this.clickElement(onDaySelectElement);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Click date time in event detail view
	 *
	 * @param date
	 *            The expected start/end/repeatend date
	 * 
	 * @param position
	 *            Set value in start/end/repeatend date
	 * 
	 * @author Fiona Zhang
	 */
	public void selectEventDate(LocalDate date, String position) {
		Logging.info("select event start/end date " + position
				+ " , and the date time is : " + date.toString());
		String date_js = null;
		switch (position.toLowerCase()) {

		case "startdate":
			date_js = PropertyHelper.coreEventDetail
					.getPropertyValue("event_startDate");
			break;
		case "enddate":
			date_js = PropertyHelper.coreEventDetail
					.getPropertyValue("event_endDate");
			break;
		case "endrepeatdate":
			date_js = PropertyHelper.coreEventDetail
					.getPropertyValue("event_repeat_end_date");
			break;

		}
		try {
			selectInDatePicker_ByJS(date, date_js);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("add date of " +position+ "failed...");
			throw ex;
		}

	}

	/**
	 * Click time in event detail view
	 *
	 * @param changeTime
	 *            The expected start/end time
	 * 
	 * @param area
	 *            Set value in start/end time
	 * 
	 * @author Fiona Zhang
	 */
	public void selectEventTime(LocalDateTime changeTime, String area) {
		Logging.info("Select the event time of " + area + " value as "
				+ changeTime);
		String event_time = null;
		if (area.equals("start"))
			event_time = PropertyHelper.coreEventDetail
					.getPropertyValue("event_startTime");
		else
			event_time = PropertyHelper.coreEventDetail
					.getPropertyValue("event_endTime");

		try {
			super.selectInDatePicker_ByJS(changeTime, event_time);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get attendee text in event view
	 *
	 * @param field
	 *            The field of drop down list, like category/repeat
	 * 
	 * @param fieldValue
	 *            The field value, like daily/weekly/green/pet, this is
	 *            according to your field
	 *
	 * @author Fiona Zhang
	 */
	public void selectItems(String field, String fieldValue) {
		Logging.info("select field " + field + " , value : " + fieldValue);
		String itemField = null;
		switch (field.toLowerCase()) {

		case "category":
			itemField = PropertyHelper.coreEventDetail
					.getPropertyValue("event_category");
			break;
		case "repeat":
			itemField = PropertyHelper.coreEventDetail
					.getPropertyValue("event_repeat");
			break;
		case "reminder":
			itemField = PropertyHelper.coreEventDetail
					.getPropertyValue("event_reminder");
			break;
		case "repeatendtype":
			itemField = PropertyHelper.coreEventDetail
					.getPropertyValue("event_repeat_end_type");
			break;
		case "ondays":
			itemField = PropertyHelper.coreEventDetail
					.getPropertyValue("event_repeat_monthly_on_days");
			break;

		}
		try {
			super.selectItem_ByJS(itemField, fieldValue);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			Logging.error("add item failed...");
			throw ex;
		}
	}

	/**
	 * Get top tool bar of the event title
	 *
	 * @author Fiona Zhang
	 */
	public String getTopToolBarTitle() {
		String event_tool_bar_title = null;
		if(Base.deviceName.toLowerCase().contains(ConstantName.IPAD)){
		  event_tool_bar_title = PropertyHelper.coreEventDetail
                .getPropertyValue("ipad_event_tool_bar_title");
		}
		else{
		  event_tool_bar_title = PropertyHelper.coreEventDetail
				.getPropertyValue("event_tool_bar_title");
		}
		try {
			WebElement toolBarTitleElement = getWebElement(
					event_tool_bar_title, false);
			Logging.info("Get top tool bar title as "
					+ toolBarTitleElement.getText());
			return toolBarTitleElement.getText();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get location text in event view
	 *
	 * @author Fiona Zhang
	 */
	public String getLocation() {
		String event_location = PropertyHelper.coreEventDetail
				.getPropertyValue("event_location");
		try {
			Logging.info("Get the Location of the event as : "
					+ getItemValueById(event_location));
			return getItemValueById(event_location);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get description text in event view
	 *
	 * @author Fiona Zhang
	 */
	public String getDescription() {

		String event_description = PropertyHelper.coreEventDetail
				.getPropertyValue("event_description");
		try {
			Logging.info("Get the description of the event as : "
					+ getItemValueById(event_description));
			return getItemValueById(event_description);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	/**
	 * Get start time exactly value of event detail view
	 *
	 * @author Fiona Zhang
	 */
	public String getStartTime() {

		String event_startTime_input_value = PropertyHelper.coreEventDetail
				.getPropertyValue("event_startTime_input_value");
		try {
			Logging.info("Get the start time of the event as : "
					+ getItemValueById(event_startTime_input_value));
			return getItemValueById(event_startTime_input_value);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	
	
	/**
	 * Get end time exactly value of event detail view
	 *
	 * @author Fiona Zhang
	 */
	public String getEndTime() {

		String event_endTime_input_value = PropertyHelper.coreEventDetail
				.getPropertyValue("event_endTime_input_value");
		try {
			Logging.info("Get the end time of the event as : "
					+ getItemValueById(event_endTime_input_value));
			return getItemValueById(event_endTime_input_value);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	
	/**
	 * Get end date exactly value of event detail view
	 *
	 * @author Fiona Zhang
	 */
	public String getEndDate() {

		String event_endDate_input_value = PropertyHelper.coreEventDetail
				.getPropertyValue("event_endDate_input_value");
		try {
			Logging.info("Get the end date of the event as : "
					+ getItemValueById(event_endDate_input_value));
			return getItemValueById(event_endDate_input_value);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	/**
	 * Get attendee text in event view
	 *
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getAttendee() {
		Logging.info("Get list in attendee field in event view");
		String attendee_list = PropertyHelper.coreEventDetail
				.getPropertyValue("attendee_list");
		ArrayList<String> nameList = new ArrayList<String>();
		try {
		    waitForLoadMaskDismissed();
			List<WebElement> attendeeElement = this.getWebElements(
					attendee_list, false);
			for (WebElement el : attendeeElement) {
				String getValuejs = "return document.getElementById('"
						+ el.getAttribute("id") + "').value";
				JavascriptExecutor executor = (JavascriptExecutor) Base.base
						.getDriver();
				String result = (String) executor.executeScript(getValuejs);
				nameList.add(result);
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info(nameList.toString());
		return nameList;
	}

	/**
	 * Get attendee invitation status in event view
	 * 
	 * @param attendee
	 *            The attendee account to check invitation status
	 *
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getAttendeeInvitation(String attendee) {
		Logging.info("Get list in attendee field in event view");
		String attendee_list = PropertyHelper.coreEventDetail
				.getPropertyValue("attendee_list");
		String attendee_invite_status = PropertyHelper.coreEventDetail
				.getPropertyValue("attendee_invite_status");
		ArrayList<String> attendeeInviteStatus = new ArrayList<String>();
		try {
			Tools.scrollElementIntoViewByXpath(attendee_list);
			List<WebElement> attendeeElement = this.getWebElements(
					attendee_list, false);
			List<WebElement> inviteStatusElement = this.getWebElements(
					attendee_invite_status, false);

			for (int i = 0; i < attendeeElement.size(); i++) {
				String getValuejs = "return document.getElementById('"
						+ attendeeElement.get(i).getAttribute("id")
						+ "').value";
				JavascriptExecutor executor = (JavascriptExecutor) Base.base
						.getDriver();
				String result = (String) executor.executeScript(getValuejs);
				Logging.info("The current attendee is : " + result);
				if (result.contains(attendee)) {
					Logging.info("loops at : " + i);
					attendeeInviteStatus.add(inviteStatusElement.get(i)
							.getAttribute("class"));
				}
			}
			Logging.info("get css as :" + attendeeInviteStatus.toString());
			return attendeeInviteStatus;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Get start or end time in event detail view
	 * 
	 * @param timeArea
	 *            the time area of event detail, like start/end
	 *
	 * @author Fiona Zhang
	 */
	public LocalTime getEventTime(String timeArea) {
		Logging.info("Get start time in event detail view");
		String event_time_area = null;
		String get_event_time_js = PropertyHelper.coreEventDetail
				.getPropertyValue("get_event_time_js");

		if (timeArea.equals(ConstantName.START))
			event_time_area = PropertyHelper.coreEventDetail
					.getPropertyValue("event_startTime");
		else if (timeArea.equals(ConstantName.END))
			event_time_area = PropertyHelper.coreEventDetail
					.getPropertyValue("event_endTime");

		LocalTime eventTime;
		try {

			WebElement startTimeElement = this.getWebElement(event_time_area,
					false);
			String getTimeJs = String.format(get_event_time_js,
					startTimeElement.getAttribute("id"),
					startTimeElement.getAttribute("id"));
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			String result = (String) executor.executeScript(getTimeJs);
			eventTime = LocalTime.parse(result);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Get " + timeArea + " time as  : " + eventTime.toString());
		return eventTime;
	}

	/**
	 * Get bottom validation event view
	 *
	 * @author Fiona Zhang
	 */
	public String getValidationErrorOfEvent() {
		Logging.info("Get the bottom validation of event view");
		String event_validation = PropertyHelper.coreEventDetail
				.getPropertyValue("event_validation");
		String result = null;

		try {
			WebElement validationElement = getWebElement(event_validation,
					false);
			result = validationElement.getText();
		} catch (NoSuchElementException ex) {
			result = null;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			return result;
		}
	}

	public LocalDateTime getEventDateTime(LocalDate date, LocalTime time) {
		return LocalDateTime.of(date, time);
	}

	/**
	 * Get selected day in week day select view
	 *
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getSelectedOnDays() {
		Logging.info("Get selected on days list");
		String event_on_day_select_list = PropertyHelper.coreEventDetail
				.getPropertyValue("event_on_day_select_list");
		ArrayList<String> dayList = new ArrayList<String>();
		try {

			List<WebElement> dayListElement = this.getWebElements(
					event_on_day_select_list, false);
			for (WebElement el : dayListElement) {
				if (el.getAttribute("class").contains("selected"))
					dayList.add(el.getText());
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info(dayList.toString());
		return dayList;
	}
	
  /**
   * Click the auto-suggest items
   * 
   * @param autoSuggestEmail the auto suggest email need to click
   *
   * @author Fiona Zhang
   */
  public void clickAutoSuggestContact(String autoSuggestEmail) {
    String auto_suggest_contact_item =
        PropertyHelper.pageObjectBaseFile.getPropertyValue("auto_suggest_contact_item");
    auto_suggest_contact_item = String.format(auto_suggest_contact_item, autoSuggestEmail);
    Logging.info("click auto suggest contact : " + auto_suggest_contact_item);
    try {
      Tools.scrollElementIntoViewByXpath(auto_suggest_contact_item);
      WebElement autoSuggestElement = getWebElement(auto_suggest_contact_item, true);
      this.clickElement(autoSuggestElement);
      // if the device is tablet, will hide keyboard, more info check UIA-1154
      if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)
          && Base.deviceName.toLowerCase().contains(ConstantName.IPAD)) {
        IOSDriver iosDriver = (IOSDriver) Base.base.getDriver();
        iosDriver.hideKeyboard();
      }
    } catch (WebDriverException ex) {
      Logging.error("click auto suggest contact failed");
      throw ex;
    }
  }

}
