package synchronoss.com.pageobject.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.springframework.util.StopWatch.TaskInfo;

import io.appium.java_client.ios.IOSDriver;
import synchronoss.com.component.TaskDueDateDescComparator;
import synchronoss.com.component.TaskPriorityDescComparator;
import synchronoss.com.component.TaskTitleDescComparator;
import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class CoreTask extends PageObjectBase {

	/**
	 * Click add task button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickAddTaskButton() {
		Logging.info("Click add task button");
		String task_add_button = PropertyHelper.coreTask
				.getPropertyValue("task_add_button");
		try {
			WebElement addTaskButton = this.getWebElement(task_add_button,
					false);
			 this.clickElement(addTaskButton);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click sort task button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickSortTaskButton() {
		Logging.info("Click sort task button");
		String task_sort_button = PropertyHelper.coreTask
				.getPropertyValue("task_sort_button");
		try {
			WebElement sortTaskButton = this.getWebElement(task_sort_button,
					false);
			 this.clickElement(sortTaskButton);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click task contains the name
	 * 
	 * @param taskName
	 *            The name of the task
	 * 
	 * @author Fiona Zhang
	 */
	public void clickTaskByName(String taskName) {
		Logging.info("Click the task by name : " + taskName);
		String task_name_item_summary = PropertyHelper.coreTask
				.getPropertyValue("task_name_item_summary");
		task_name_item_summary = String
				.format(task_name_item_summary, taskName);
		try {
			Tools.scrollElementIntoViewByXpath(task_name_item_summary);
			WebElement nameTaskButton = this.getWebElement(
					task_name_item_summary, false);
			 this.clickElement(nameTaskButton);
			 if(Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
			IOSDriver iosDriver = (IOSDriver) Base.base.getDriver();
			iosDriver.hideKeyboard();
			 }
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click task check box
	 * 
	 * @param taskTitle
	 *            The name of the task
	 * 
	 * @author Fiona Zhang
	 */
	public void clickCheckBoxOfTask(String taskTitle) {
		Logging.info("Click the check box of task by name : " + taskTitle);
		String task_check_box_name = PropertyHelper.coreTask
				.getPropertyValue("task_check_box_name");
		task_check_box_name = String.format(task_check_box_name, taskTitle);
		try {
			Tools.scrollElementIntoViewByXpath(task_check_box_name);
			WebElement nameTaskButton = this.getWebElement(task_check_box_name,
					false);
			 this.clickElement(nameTaskButton);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click task priority
	 * 
	 * @param option
	 *            The priority of the task
	 * 
	 * @author Fiona Zhang
	 */
	public void clickPriorityOfPopupMenu(String option) {
		Logging.info("Click " + option + "  in task pop up priority selector ");
		String action = null;
		switch (option) {
		case ConstantName.LOW:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_priority_low_bottom");
			break;
		case ConstantName.HIGH:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_priority_high_bottom");
			break;
		case ConstantName.NORMAL:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_priority_normal_bottom");
			break;
		case ConstantName.CANCEL:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_priority_cancel_bottom");
			break;
		}
		try {
			WebElement toolbarIconElement = getWebElement(action, false);
			 this.clickElement(toolbarIconElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}

	/**
	 * Click task action on tool bar
	 *
	 * @param option
	 *            The option action
	 * 
	 * @author Fiona Zhang
	 */
	public void clickActionButtonOnSlideToolbar(String option) {
		Logging.info("Click " + option
				+ " option in task action slide tool bar ");
		String action = null;
		switch (option) {
		case ConstantName.FINISHED:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_complete_button");
			break;
		case ConstantName.UNFINISHED:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_uncomplete_button");
			break;
		case ConstantName.NEEDACTION:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_need_action_button");
			break;
		case ConstantName.DELETE:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_delete_button");
			break;
		}
		try {
			WebElement toolbarIconElement = getWebElement(action, false);
			 this.clickElement(toolbarIconElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}

	/**
	 * Click the sort condition
	 *
	 * @param nameOfCondition
	 *            The name of condition, like Due Date,Priority,Title
	 * 
	 * @author Fiona Zhang
	 */
	public void clickSortTaskByCondition(String nameOfCondition) {
		Logging.info("Click " + nameOfCondition + " option in task sort menu ");
		String task_sort_condition = PropertyHelper.coreTask
				.getPropertyValue("task_sort_condition");
		task_sort_condition = task_sort_condition.format(task_sort_condition,
				this.toCapitlize(nameOfCondition));

		try {
			WebElement nameConditionElement = getWebElement(
					task_sort_condition, false);
			 this.clickElement(nameConditionElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}

	/**
	 * Click task action on bottom bar
	 *
	 * @param option
	 *            The option action
	 * 
	 * @author Fiona Zhang
	 */
	public void clickButtomButtonOfTask(String option) {
		Logging.info("Click " + option + " option in task action bottom bar ");
		String action = null;
		switch (option) {
		case ConstantName.FINISHED:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_complete_button_bottom");
			break;
		case ConstantName.UNFINISHED:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_uncomplete_button_bottom");
			break;
		case ConstantName.NEEDACTION:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_need_action_button_bottom");
			break;
		case ConstantName.PRIORITY:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_priority_button_bottom");
			break;

		case ConstantName.DELETE:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_delete_button_bottom");
			break;
		case ConstantName.CANCEL:
			action = PropertyHelper.coreTask
					.getPropertyValue("task_close_button_bottom");
			break;

		}
		try {
			WebElement toolbarIconElement = getWebElement(action, false);
			 this.clickElement(toolbarIconElement);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;

		}
	}

	/**
	 * Get the task priority
	 * 
	 * @param taskTitle
	 *            The task title
	 * 
	 * @author Fiona Zhang
	 */
	public int getTaskPrirorityByName(String taskTitle) {
		String task_item_priority = PropertyHelper.coreTask
				.getPropertyValue("task_item_priority");
		String task_name_item = PropertyHelper.coreTask
				.getPropertyValue("task_name_item");

		List<WebElement> taskPriorityElement;
		String priority = null;
		int result;

		task_name_item = String.format(task_name_item, taskTitle);
		task_item_priority = String.format(task_item_priority, taskTitle);
		try {
			Tools.scrollElementIntoViewByXpath(task_name_item);
			taskPriorityElement = this
					.getWebElements(task_item_priority, false);
			if (taskPriorityElement.size() == 0)
				result = 1;
			else {
				priority = taskPriorityElement.get(0).getAttribute("class");
				if (priority.contains(ConstantName.LOW.toLowerCase()))
					result = 0;
				else
					result = 2;

			}
			Logging.info("Get task - " + taskTitle + " priority as : " + result);
			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Get the task url
	 * 
	 * @param taskTitle
	 *            The task title
	 * 
	 * @author Fiona Zhang
	 */
	public String getTaskURLByName(String taskTitle) {
		String task_item_link = PropertyHelper.coreTask
				.getPropertyValue("task_item_link");
		String result = null;
		task_item_link = String.format(task_item_link, taskTitle);
		try {
			Tools.scrollElementIntoViewByXpath(task_item_link);
			WebElement taskLinkElement = this.getWebElement(task_item_link,
					false);
			result = taskLinkElement.getText();
			Logging.info("Get the task url in task list view as : "
					+ task_item_link);
			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Get the task due time
	 * 
	 * @param taskTitle
	 *            The task title
	 * 
	 * @author Fiona Zhang
	 */
	public String getTaskDueTimeByName(String taskTitle) {
		String task_item_time = PropertyHelper.coreTask
				.getPropertyValue("task_item_time");
		String result = null;
		task_item_time = String.format(task_item_time, taskTitle);
		try {
			Tools.scrollElementIntoViewByXpath(task_item_time);
			WebElement taskLinkElement = this.getWebElement(task_item_time,
					true);
			result = taskLinkElement.getText();
			Logging.info("Get the task due time in task list view as : "
					+ taskLinkElement.getText());
			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Get the task sort method
	 * 
	 * @param taskTitle
	 *            The task title
	 * 
	 * @author Fiona Zhang
	 */
	public String getCurrentTaskSortMethod(String nameOfConditon) {
		String task_sort_method = PropertyHelper.coreTask
				.getPropertyValue("task_sort_method");
		task_sort_method = task_sort_method.format(task_sort_method,
				this.toCapitlize(nameOfConditon));
		String result = null;
		try {
			WebElement sortMethodElement = getWebElement(task_sort_method,
					false);
			result = sortMethodElement.getAttribute("class");
			Logging.info(result);
			result = result.substring(result.lastIndexOf("-") + 1);
			Logging.info(result);
			Logging.info("Get the sort " + nameOfConditon + " method as "
					+ result);
			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Get the task status
	 * 
	 * @param taskTitle
	 *            The task title
	 * 
	 * @author Fiona Zhang
	 */
	public String getTaskStatusByName(String taskTitle) {
		String task_item_status_check_complete = PropertyHelper.coreTask
				.getPropertyValue("task_item_status_check_complete");
		String task_item_status_need_action = PropertyHelper.coreTask
				.getPropertyValue("task_item_status_need_action");
		String task_name_item = PropertyHelper.coreTask
				.getPropertyValue("task_name_item");

		List<WebElement> taskStatusElement;
		String result = null;
		task_item_status_check_complete = String.format(
				task_item_status_check_complete, taskTitle);
		task_item_status_need_action = String.format(
				task_item_status_need_action, taskTitle);
		task_name_item = String.format(task_name_item, taskTitle);
		try {
			Tools.scrollElementIntoViewByXpath(task_name_item);
			WebElement completeStatus = this.getWebElement(
					task_item_status_check_complete, false);
			if (completeStatus.getAttribute("class").contains("complete"))
				result = ConstantName.FINISHED;
			else {
				taskStatusElement = this.getWebElements(
						task_item_status_need_action, false);
				if (taskStatusElement.size() == 0)
					result = ConstantName.UNFINISHED;
				else
					result = ConstantName.NEEDACTION;
			}
			Logging.info("Get task - " + taskTitle + " status as : " + result);
			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Get current task list of the view
	 * 
	 * @author Fiona Zhang
	 */
	public ArrayList<String> getCurrentTaskList() {
		String task_name_item_list = PropertyHelper.coreTask
				.getPropertyValue("task_name_item_list");
		ArrayList<String> taskList = new ArrayList<String>();
		try {

			List<WebElement> taskListElement = this.getWebElements(
					task_name_item_list, true);
			for (WebElement el : taskListElement) {
				taskList.add(el.getText());
			}
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Get current task list as : " + taskList.toString());
		return taskList;
	}

	/**
	 * Get current task tool bar title
	 * 
	 * @author Fiona Zhang
	 */
	public String getCurrentTaskToolbarTitle() {
		String task_toolbar_title = PropertyHelper.coreTask
				.getPropertyValue("task_toolbar_title");
		String result = null;
		try {
			WebElement taskToolbarTitleElement = getWebElement(
					task_toolbar_title, false);
			result = taskToolbarTitleElement.getText();
			Logging.info("Get current task tool bar title as " + result);
			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Swipe the task item in task list
	 * 
	 * @param taskName
	 *            The task name
	 * 
	 * @author Fiona Zhang
	 */
	public void swipeTaskByTaskName(String taskName) {
		Logging.info("Swipe action on current task item " + taskName
				+ "and trigger tool bar...");

		String task_name_item = PropertyHelper.coreTask
				.getPropertyValue("task_name_item");
		task_name_item = String.format(task_name_item, taskName);

		try {
			Tools.scrollElementIntoViewByXpath(task_name_item);

			WebElement taskItemElement = getWebElement(task_name_item, true);
			String taskID = taskItemElement.getAttribute("id");

			String taskEdit = PropertyHelper.coreTask
					.getPropertyValue("task_item_tool_bar_js");
			taskEdit = String.format(taskEdit, taskID);
			JavascriptExecutor executor = (JavascriptExecutor) Base.base
					.getDriver();
			executor.executeScript(taskEdit);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Check if the task has reminder
	 * 
	 * @param taskName
	 *            The task name
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isTaskSetReminder(String taskName) {
		String task_item_reminder = PropertyHelper.coreTask
				.getPropertyValue("task_item_reminder");
		boolean result = false;
		task_item_reminder = String.format(task_item_reminder, taskName);
		try {
			Tools.scrollElementIntoViewByXpath(task_item_reminder);
			List<WebElement> taskReminderElement = this.getWebElements(
					task_item_reminder, false);
			if (taskReminderElement.size() == 0)
				result = false;
			else
				result = true;
			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Check if the task list is empty
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isEmptyTaskList() {

		String task_empty_list = PropertyHelper.coreTask
				.getPropertyValue("task_empty_list");
		boolean result = false;
		try {
			WebElement emptyListElement = getWebElement(task_empty_list, true);
			Logging.info(emptyListElement.getAttribute("style"));
			result = emptyListElement.isDisplayed();
			Logging.info("Is task list empty : " + result);
			return result;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Check if the task list is sorted by condition
	 * 
	 * @param taskList
	 *            The original task info array list
	 * 
	 * @param currentTaskList
	 *            Get current task list title
	 * 
	 * @param sortAttribute
	 *            The attribute of sort, like date, priority, title
	 * 
	 * @param sortMethod
	 *            The sort method, like desc or asc
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isTaskListIsSortedByCondition(ArrayList<TaskInfo> taskList,
			ArrayList<String> currentTaskList, String sortAttribute,
			String sortMethod) {
		boolean result = false;
/*		if (sortAttribute.equals(ConstantName.DUEDATE))
			Collections.sort(taskList, new TaskDueDateDescComparator());
		else if (sortAttribute.equals(ConstantName.PRIORITY))
			Collections.sort(taskList, new TaskPriorityDescComparator());
		else if (sortAttribute.equals(ConstantName.TITLE))
			Collections.sort(taskList, new TaskTitleDescComparator());

*/		// verify the sorted task list
		if (sortMethod.equals("asc"))
			Collections.reverse(taskList);
		
		for (int i = 0; i < taskList.size(); i++)
			Logging.info("Expected sort is: " + i + ":"
					+ taskList.get(i).getTaskName());
		
		for (int i = 0; i < taskList.size(); i++)
			if (taskList.get(i).getTaskName().equals(currentTaskList.get(i)))
				result = true;
			else {
				result = false;
				break;
			}
		return result;
	}

	
	/**
	 * Check if the task list action tool bar is displayed or not
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isTaskListActionIsDisplayed() {
		String task_list_action_too_bar = PropertyHelper.coreTask
				.getPropertyValue("task_list_action_too_bar");
		boolean result = false;
		try {
			WebElement listActionBarElement = getWebElement(task_list_action_too_bar, true);
			result = listActionBarElement.isDisplayed();
			Logging.info("Is task list tool bar displayed : " + result);
			return result;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}	}

	
	/**
	 * Check if the task item is selected
	 * 
	 * @param taskTitle
	 *            The task title of checked item
	 * 
	 * @author Fiona Zhang
	 */
	public boolean isTaskItemIsSelected(String taskTitle) {
		String task_check_box_name = PropertyHelper.coreTask
				.getPropertyValue("task_check_box_name");
		task_check_box_name = task_check_box_name.format(task_check_box_name,
				taskTitle);
		boolean result = false;
		try {
			WebElement listActionBarElement = getWebElement(
					task_check_box_name, true);
			if (listActionBarElement.getAttribute("class").contains("selected"))
				result = true;
			else
				result = false;
			Logging.info("Is task item " + taskTitle + " is selected : "
					+ result);
			return result;
		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

}
