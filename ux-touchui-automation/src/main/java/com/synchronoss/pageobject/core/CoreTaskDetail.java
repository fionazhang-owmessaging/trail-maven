package synchronoss.com.pageobject.core;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import io.appium.java_client.ios.IOSDriver;
import synchronoss.com.core.Base;
import synchronoss.com.core.Logging;
import synchronoss.com.core.PropertyHelper;
import synchronoss.com.core.Tools;
import synchronoss.com.testcase.utils.ConstantName;


public class CoreTaskDetail extends PageObjectBase {
	/**
	 * Add task title to the task detail view
	 * 
	 * @param title
	 *            the title of the task
	 * 
	 * @author Fiona Zhang
	 */
	public void addTaskTitle(String title) {
		Logging.info("Add task title as " + title);
		String task_title_field = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_title_field");
		try {
			Tools.scrollElementIntoViewByXpath(task_title_field);
			WebElement taskTitleField = this.getWebElement(task_title_field,
					true);
			taskTitleField.clear();
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				taskTitleField.sendKeys(title);
				taskTitleField.sendKeys(Keys.ENTER);
			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				this.elementSendKeys(taskTitleField, title);
			}
		} catch (WebDriverException ex) {
			throw ex;
		}

	}

	/**
	 * Add task url to the task detail view
	 * 
	 * @param url
	 *            the url of the task
	 * 
	 * @author Fiona Zhang
	 */
	public void addTaskURL(String url) {
		Logging.info("Add task URL as " + url);
		String task_url_field = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_url_field");
		try {
			Tools.scrollElementIntoViewByXpath(task_url_field);
			WebElement taskURLField = this.getWebElement(task_url_field, true);
			taskURLField.clear();
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)){
				taskURLField.sendKeys(url);
				taskURLField.sendKeys(Keys.ENTER);
			}
			else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				this.elementSendKeys(taskURLField, url);
			}
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Add task descripiton to the task detail view
	 * 
	 * @param descripiton
	 *            the descripiton of the task
	 * 
	 * @author Fiona Zhang
	 */
	public void addTaskDescription(String descripiton) {
		Logging.info("Add task title as " + descripiton);
		String task_description_field = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_description_field");
		try {
			Tools.scrollElementIntoViewByXpath(task_description_field);
			WebElement taskDescriptionField = this.getWebElement(
					task_description_field, true);
			taskDescriptionField.clear();
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS))
				taskDescriptionField.sendKeys(descripiton);
			else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				this.elementSendKeys(taskDescriptionField, descripiton);
			}
			taskDescriptionField.sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Click the button on top bar of task
	 * 
	 * @param actionButton
	 *            Save or Cancel button
	 * 
	 * @author Fiona Zhang
	 */
	public void clickButtonOnTopBar(String actionButton) {
		Logging.info("Click " + actionButton
				+ " on top bar in task detail view");
		String top_bar_button = null;
		if (actionButton.equals(ConstantName.SAVE))
			top_bar_button = PropertyHelper.coreTaskDetail
					.getPropertyValue("task_save_button");
		else
			top_bar_button = PropertyHelper.coreTaskDetail
					.getPropertyValue("task_cancel_button");
		try {
			WebElement topBarButton = this.getWebElement(top_bar_button, true);
			 this.clickElement(topBarButton);
			waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Select the priority of the task
	 * 
	 * @param priority
	 *            The priority number of the task 0 - low 1 - normal 2 - high
	 * 
	 * @author Fiona Zhang
	 */
	public void selectTaskPriority(int priorityNumber) {
		String task_priority = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_priority");
		String priority = null;
		switch (priorityNumber) {
		case 0:
			priority = ConstantName.LOW;
			break;
		case 1:
			priority = ConstantName.NORMAL;
			break;
		case 2:
			priority = ConstantName.HIGH;
			break;
		}
		Logging.info("Select task priority as : " + priority);

		try {
			this.selectItem_ByJS(task_priority, priority);
		} catch (WebDriverException ex) {
			throw ex;
		}

	}

	/**
	 * Select the status of the task
	 * 
	 * @param status
	 *            The status of the task
	 * 
	 * @author Fiona Zhang
	 */
	public void selectTaskStatus(String status) {
		Logging.info("Select task status as : " + status);
		String task_priority = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_status");
		try {
			this.selectItem_ByJS(task_priority, status);
		} catch (WebDriverException ex) {
			throw ex;
		}

	}

	/**
	 * Select the group of the task
	 * 
	 * @param group
	 *            The group of the task
	 * 
	 * @author Fiona Zhang
	 */
	public void selectTaskGroup(String groupName) {
		Logging.info("Select task group as : " + groupName);
		String task_group = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_group");
		try {
			this.selectItem_ByJS(task_group, groupName);
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Select task due date
	 * 
	 * @param setDueDate
	 *            The options of set due date
	 * 
	 * @author Fiona Zhang
	 */
	public void selectTaskSetDueDate(String setDueDate) {
		Logging.info("Set due date as : " + setDueDate);
		String task_set_due_date = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_set_due_date");
		try {
			this.selectItem_ByJS(task_set_due_date, setDueDate);
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Select task reminder
	 * 
	 * @param reminderOption
	 *            The reminder options
	 * 
	 * @author Fiona Zhang
	 */
	public void selectTaskReminder(String reminderOption) {
		Logging.info("Set task reminder of : " + reminderOption);
		String task_reminder_option = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_reminder_option");
		try {
			this.selectItem_ByJS(task_reminder_option, reminderOption);
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Select task due date
	 * 
	 * @param localDate
	 *            the local date of task reminder
	 * 
	 * @author Fiona Zhang
	 */
	public void selectTaskDueDate(LocalDate localDate) {
		Logging.info("select task of due date: " + localDate);
		String task_due_date = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_due_date");
		try {
			this.selectInDatePicker_ByJS(localDate, task_due_date);
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Select task due time
	 * 
	 * @param localTime
	 *            the local time of task reminder
	 * 
	 * @author Fiona Zhang
	 */
	public void selectTaskDueTime(LocalDateTime localdateTime) {
		Logging.info("select task of due date: " + localdateTime);
		String task_due_time = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_due_time");
		localdateTime.getMinute();
		try {
			this.selectInDatePicker_ByJS(localdateTime, task_due_time);
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Get task description
	 * 
	 * @author Fiona Zhang
	 */
	public String getTaskDescription() {
		String task_description_field = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_description_field");
		
		String task_group_selector = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_group_selector");
		
		String task_group_selector_cancel_button = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_group_selector_cancel_button");


		String result = null;
		try {
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {

				// check if the select group picker pop up or not, if yes, click cancel button
				List<WebElement> el = this.getWebElements(task_group_selector,
						true);
				if (el.size() == 1) {
					WebElement cancelButtonOfSelector = this.getWebElement(
							task_group_selector_cancel_button, false);
					cancelButtonOfSelector.click();
				}
				result = this.getItemValueById(task_description_field);
				IOSDriver iosDriver = (IOSDriver) Base.base.getDriver();
				iosDriver.hideKeyboard();
			}
			else
				result = this.getItemValueById(task_description_field);
			Logging.info("Get task description as : " + result);
			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}

	}



	/**
	 * Get task group
	 * 
	 * @author Fiona Zhang
	 */
	public String getTaskGroup() {
		String task_group_field_value = PropertyHelper.coreTaskDetail
				.getPropertyValue("task_group_field_value");
		String result = null;
		try {
			result = this.getItemValueById(task_group_field_value);
			Logging.info("Get task group name as : " + result);
			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * Get task due date
	 * 
	 * @author Fiona Zhang
	 */
	public String getTaskDueDate(){
		String task_due_date_value = PropertyHelper.coreTaskDetail.getPropertyValue("task_due_date_value");
		String result = null;
		try{
			result = this.getItemValueById(task_due_date_value);
		} catch (WebDriverException ex) {
			throw ex;
		}
		
		Logging.info("Get task due date in task detail view as : " + result);

		return result;
	}
	
	/**
	 * Get task due time
	 * 
	 * @author Fiona Zhang
	 */
	public String getTaskDueTime() {
		String task_due_time_value = PropertyHelper.coreTaskDetail.getPropertyValue("task_due_time_value");
		String result = null;
		try{
			result = this.getItemValueById(task_due_time_value);
		} catch (WebDriverException ex) {
			throw ex;
		}
		
		Logging.info("Get task due time in task detail view as : " + result);

		return result;
	}

	
	/**
	 * Get page validation text
	 * 
	 * @param validationSection
	 *            The validation select part
	 * 
	 * @author Fiona Zhang
	 */
	public String getTaskValidationText(String validationSection) {
		String validation_xpath = null;
		String result = null;

		switch (validationSection) {
		case ConstantName.DUEDATE:
			validation_xpath = PropertyHelper.coreTaskDetail
					.getPropertyValue("task_date_time_validation");
			break;
		case ConstantName.DUETIME:
			validation_xpath = PropertyHelper.coreTaskDetail
					.getPropertyValue("task_date_time_validation");
			break;
		case ConstantName.REMINDER:
			validation_xpath = PropertyHelper.coreTaskDetail
					.getPropertyValue("task_reminder_validation");
			break;
		case ConstantName.URL:
			validation_xpath = PropertyHelper.coreTaskDetail
					.getPropertyValue("task_url_validation");
			break;
		case ConstantName.TITLE:
			validation_xpath = PropertyHelper.coreTaskDetail
					.getPropertyValue("task_title_validation");
			break;

		}
		validation_xpath = validation_xpath.format(validation_xpath,
				validationSection);
		try {
			Tools.scrollElementIntoViewByXpath(validation_xpath);
			WebElement validationTextElement = this.getWebElement(
					validation_xpath, false);
			result = validationTextElement.getText().trim();
			Logging.info("Get validation text in " + validationSection
					+ " as : " + result);
			return result;
		} catch (WebDriverException ex) {
			throw ex;
		}
	}


}
