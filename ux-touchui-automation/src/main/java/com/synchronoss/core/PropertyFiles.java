/*************************************************************************
@Class Name  : Property Files
@Author		 : Ravid Te
@Created On  : 08/21/2012
@Description : This class is for reading values from properties or config files.
@Modified on : 	09/27/2013
@Referred Test Case Number : NA 
@Target Application  : AppSuite 1.4.x
@Revised By     :Ravid Te
@Changes     : Rewrote functions to support localization and overriding
'*************************************************************************/

package synchronoss.com.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class PropertyFiles {
    private String filename;
    public static String userInterface = "";
    public static String url="";
    
    public PropertyFiles(String file) {
        setFilename(file);
    }
    
    public String getFilename() {
        return filename;
    }
    
    public void setFilename(String filename) {
        this.filename = filename;
    }
    
    public static String getUserInterface() {
        return userInterface;
    }
    
    public static void setUserInterface(String userInterface) {
        PropertyFiles.userInterface = userInterface;
    }
    
    //SSO workaround
    public static String getURL() {
        return url;
    }
    
    //SSO workaround
    public static void setURL(String url) {
        PropertyFiles.url = url;
    }
    
    public String getPropertyValue(String key) {
    	
        Properties coreProperties = new Properties();
        Properties overridingProperties = new Properties();
        Properties merged = new Properties();
        
        String coreFileName = this.getFilename();
        String overridingFileName = "";
        
        if (userInterface.equals("???")) {
            overridingFileName = "";
        } else {
            overridingFileName = this.getFilename();
        }
        
        try {
            final String dir = System.getProperty("user.dir");
            System.out.print("current dir is : ------------ "+dir);
          String resourceDir = "/ux-touchui-automation/src/main/java/resource/properties/";
//          String resourceDir = "\\openwave-automation\\resource\\";
            
            File coreFile = new File(dir + resourceDir + coreFileName + ".properties");
            FileInputStream coreFileInput = new FileInputStream(coreFile);
            coreProperties.load(coreFileInput);
            coreFileInput.close();
            
            File overridingFile = new File(dir + resourceDir + overridingFileName + ".properties");
            FileInputStream overridingFileInput = new FileInputStream(overridingFile);
            overridingProperties.load(overridingFileInput);
            overridingFileInput.close();
            
            // Merge and replace values in core properties with values from 
            // overriding properties that has identical keys.
            merged.putAll(coreProperties);
            merged.putAll(overridingProperties);
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return merged.getProperty(key);
    }
    
}