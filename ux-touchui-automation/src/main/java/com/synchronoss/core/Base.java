package synchronoss.com.core;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;

import synchronoss.com.pageobject.core.CoreCalendar;
import synchronoss.com.pageobject.core.CoreContactDetail;
import synchronoss.com.pageobject.core.CoreContacts;
import synchronoss.com.pageobject.core.CoreEmail;
import synchronoss.com.pageobject.core.CoreEmailComposer;
import synchronoss.com.pageobject.core.CoreEmailDetail;
import synchronoss.com.pageobject.core.CoreEventDetail;
import synchronoss.com.pageobject.core.CoreLogin;
import synchronoss.com.pageobject.core.CoreNavigation;
import synchronoss.com.pageobject.core.CoreSettings;
import synchronoss.com.pageobject.core.CoreSidebar;
import synchronoss.com.pageobject.core.CoreTask;
import synchronoss.com.pageobject.core.CoreTaskDetail;


public class Base {

	public static Base base = null;
	public WebDriver driver = null;

	public static CssClassTracker cssTracker = new CssClassTracker();

	/*************************************************************************
	 * @Method Name : getDriver
	 * @Author : Alkesh Dagade
	 * @Created On : 08/29/2012
	 * @Description : This method is an abstract method to get webdriver
	 *              instance.
	 * @parameter 1 : NA
	 *************************************************************************/
	public WebDriver getDriver() {
		return null;
	}

  /*************************************************************************
   * @Method Name : setBaseInstance
   * @Author : Alkesh Dagade
   * @Created On : 08/29/2012
   * @Description : This method is to create instance of child class based on SuiteExecutionType
   *              variable value.
   * @parameter 1 : SuiteExecutionType->its value should be "Grid"->if suite to be executed using
   *            Grid on remote machine "Local"->If suite to be executed on local machine using
   *            webdriver
   * @parameter 2 : hub->URL of hub where Jenkins is installed
   * @parameter 3 : platform->IOS - safari, Andorid - chrome
   * @parameter 4 : enableTracker: Trace for the css or not
   * @parameter 5 : platformVersion: The version of platform, like 9.3, 10.0 etc.
   * @parameter 6 : deviceName: The name of device, like iPad Air 2, ipad simulator, iphone
   *            simulator, 02157df2da6c3c2a etc.
   *************************************************************************/
  public void setBaseInstance(String SuiteExecutionType, String hub, String platform,
      boolean enableTracker, String platformVersion, String deviceName)
      throws MalformedURLException {
    if (SuiteExecutionType.equalsIgnoreCase("grid")) {
      base = new SeleniumGrid(hub, platform, enableTracker, platformVersion, deviceName);
    } else {
      base = new SeleniumLocal(platform);
    }
  }

	/*
	 * Starts a WebDriver session. Should be overridden in driver instantiation
	 * classes.
	 */
	public void startSession(String url) {
		// To be overridden.
	}

	// #########################################################################
	// Utility Fields and Methods
	// #########################################################################

	// Logging Duty
	public Logging frameworkLog = new Logging();

	// Tools Helper
	public Tools tools = new Tools();

	// User pool utilities
	public static boolean usingUserPool;
	public static String userPoolFileName;
	public static boolean ifCreateDynamicUser = false;
	private static UserPool pool;

	// Fields and method records of login user/pass for reference in test
	protected String loginUser = "";
	protected String loginPass = "";

	// Login Latency always captured
	public static long startTime;
	public static long endTime;
	public static long totalTime;
	// Configurable Timeout
	public static String timeout;
	// PlatformName
	public static String platformName;
	public static String deviceName;

	// Timeout value definitions in seconds
	public static final int timeoutShort = 15;
	public static final int timeoutMedium = 30;
	public static final int timeoutLong = 60;

	// #########################################################################
	// Core Page Objects
	// #########################################################################

	public CoreLogin login = new CoreLogin();
	public CoreNavigation navigation = new CoreNavigation();
	public CoreEmail email = new CoreEmail();
	public CoreEmailComposer emailComposer = new CoreEmailComposer();
	public CoreEmailDetail emailDetail = new CoreEmailDetail();
	public CoreContacts contact = new CoreContacts();
	public CoreContactDetail contactDetail = new CoreContactDetail();
	public CoreCalendar calendar = new CoreCalendar();
	public CoreEventDetail eventDetail = new CoreEventDetail();
	public CoreSettings settings = new CoreSettings();
	public CoreSidebar sidebar = new CoreSidebar();
	public CoreTask task = new CoreTask();
	public CoreTaskDetail taskDetail = new CoreTaskDetail();

	// Getters and setters for user pool manipulation.
	public static UserPool getPool() {
		System.out.println("[Debug: ] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> From base you can get base.pool as : "+Base.pool);
		return Base.pool;
	}

	public static void setPool(UserPool userPool) {
		Logging.info("start to set userPool");
		Base.pool = userPool;
	}

}
