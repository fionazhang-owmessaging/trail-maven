package synchronoss.com.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentException;
import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlSuite.ParallelMode;
import org.testng.xml.XmlTest;

/* 
 * This class aims to read arguments from command line and generate a
 * virtual TestNG configuration XML file.
 */
public class RunFromJenkins {
	
	public static void main(String args[]) {

        try {        	
            String url = args[0];
            String typeOfExecution = args[1];
            String userInterface = args[2];
            String groupName = args[3];
            String excludeGroupName = args[4];
            String packagesToLoad = args[5];
            
            String seleniumHub = args[6];
            String plaform = args[7];
            String deviceName = args[8];
            String version = args[9];
            String userPool = args[10];
            String userPoolFile = args[11];
            
            String username1 = args[12];
            String password1 = args[13];
            String emailid1 = args[14];
            
            String username2 = args[15];
            String password2 = args[16];
            String emailid2 = args[17];
            
            String username3 = args[18];
            String password3 = args[19];
            String emailid3 = args[20];
            
            String username4 = args[21];
            String password4 = args[22];
            String emailid4 = args[23];
            
            String enableCssTracker = args[24];
            
            String ifCreateDynamicUser = args[25];
            String mxosIp = args[26];
            String mxosPort = args[27];
            
            String executeAsRetry = args[28];
            String maxRetryCount = args[29];

            System.out.println("--------------------------------------------");
            System.out.println("Testing URL: " + url);
            System.out.println("Type of execution: " + typeOfExecution);
            System.out.println("User interface: " + userInterface);
            System.out.println("Group name: " + groupName);
            System.out.println("Exclude group name: " + excludeGroupName);
            System.out.println("Packages to load: " + packagesToLoad);
            System.out.println();
            // default to userPool.xlsx if none are specified
            if (userPoolFile.equals("${userPoolFile}")) {
            	userPoolFile = "userPool.xlsx";
            }
            
            // check if a predefined Selenium hub name is selected
            String hub = seleniumHub.trim().toLowerCase();
            System.out.println("will run script on this hub: -- "+hub);
            if (hub.equals("bj")) {
            	// Beijing Selenium hub address.
            	seleniumHub = "http://10.13.16.37:4444/wd/hub";
            } else if (hub.equals("rwc")) {
            	// RWC Selenium hub address.
            	seleniumHub = "http://rwc-jenkins-prd1.openwave.com:4444/wd/hub";
            } else if (hub.equals("bjdev")) {
            	// Beijing DEV Selenium hub address.
            	seleniumHub = "http://10.37.10.37:44442/wd/hub";
            }
            else if (hub.equals("bjdev2")) {
            	// Beijing DEV Selenium hub address.
            	seleniumHub = "http://10.37.10.37:44441/wd/hub";
            }
            else if(hub.equals("bjdev3")){
              // Beijing DEV Selenium hub address.
              seleniumHub = "http://10.37.10.37:44445/wd/hub";
            }
            
            // Here we use java print because testNG is not involved yet, and it will print null-session if use Logging.info()
            System.out.println("--------------------------------------------");
            System.out.println("Testing URL: " + url);
            System.out.println("Type of execution: " + typeOfExecution);
            System.out.println("User interface: " + userInterface);
            System.out.println("Group name: " + groupName);
            System.out.println("Exclude group name: " + excludeGroupName);
            System.out.println("Packages to load: " + packagesToLoad);
            System.out.println();
            
            System.out.println("Selenium hub: " + seleniumHub);
            System.out.println("Plaform: " + plaform);
            System.out.println("Plaform version: " + version);
            System.out.println("Device name: " + deviceName);
            System.out.println("Using user pool: " + userPool);
            System.out.println("User pool file: " + userPoolFile);
            System.out.println();
            
            System.out.println("username1: " + username1);
            System.out.println("password1: " + password1);
            System.out.println("emailid1: " + emailid1);
            
            System.out.println("username2: " + username2);
            System.out.println("password2: " + password2);
            System.out.println("emailid2: " + emailid2);
            System.out.println();
            
            System.out.println("username3: " + username3);
            System.out.println("password3: " + password3);
            System.out.println("emailid3: " + emailid3);
            
            System.out.println("username4: " + username4);
            System.out.println("password4: " + password4);
            System.out.println("emailid4: " + emailid4);
            System.out.println("enableCssTracker: " + enableCssTracker);

            System.out.println("ifCreateDynamicUser: " + ifCreateDynamicUser);
            System.out.println("mxosIp: " + mxosIp);
            System.out.println("mxosPort: " + mxosPort);
            
            System.out.println("executeAsRetry: " + executeAsRetry);
            System.out.println("maxRetryCount: " + maxRetryCount);

            System.out.println();
            
            // Variables
            String[] platforms = new String[0];
            
            // Instantiations
            TestNG testng = new TestNG();
            XmlSuite suite = new XmlSuite();
            List<XmlTest> tests = new ArrayList<XmlTest>();
            List<XmlSuite> suites = new ArrayList<XmlSuite>();
            Map<String, String> suiteParameters = new HashMap<String, String>();
            List<XmlClass> retryClasses = new ArrayList<XmlClass>();

            // Suite Parameters, some of them are hard-coded for now.
            suiteParameters.put("selenium.hub", seleniumHub);
            suiteParameters.put("SuiteExecutionType", "Grid");
            suiteParameters.put("browser.url", url);
            suiteParameters.put("platform.version", version);
            suiteParameters.put("userPool", userPool);
            suiteParameters.put("userPoolFile", userPoolFile);
            suiteParameters.put("userInterface", userInterface);
            suiteParameters.put("ifCreateDynamicUser", ifCreateDynamicUser);
            suiteParameters.put("timelimit", "80"); // hard-coded for now

            // Configure our suite
            suite.setName("Ux2.1 TouchUI Test Suite");
            suite.setParameters(suiteParameters);
            suite.setConfigFailurePolicy(XmlSuite.CONTINUE);
            
/*            if (typeOfExecution.equals("parallel")) {
                suite.setThreadCount(2);
                suite.setParallel(ParallelMode.TESTS);
                platforms = new String[] { "IOS", "Android" };
            } if the mode is test, need to re-write the xmlSuite*/
            if (typeOfExecution.equals("sequential")) {
                System.out.println("Enabling sequential execution");
                suite.setPreserveOrder("true");
                suite.setThreadCount(1);
                suite.setParallel(ParallelMode.NONE);
                platforms = new String[] { plaform };
            }
            if (typeOfExecution.equals("distributed")) {
                suite.setThreadCount(4);
                suite.setParallel(ParallelMode.CLASSES);
               // suite.setTimeOut("90000000000");
                platforms = new String[] { plaform };
            }
            
            
            for (int i = 0; i < platforms.length; i++) {
                Map<String, String> testParameters = new HashMap<String, String>();
                
                // Test Parameters
                testParameters.put("username1", username1);
                testParameters.put("password1", password1);
                testParameters.put("emailid1", emailid1);
                testParameters.put("username2", username2);
                testParameters.put("password2", password2);
                testParameters.put("emailid2", emailid2);
                testParameters.put("username3", username3);
                testParameters.put("password3", password3);
                testParameters.put("emailid3", emailid3);
                testParameters.put("username4", username4);
                testParameters.put("password4", password4);
                testParameters.put("emailid4", emailid4);
                testParameters.put("selenium.platform", platforms[i].toString());
                testParameters.put("enableCssTracker", enableCssTracker);
                testParameters.put("platformVersion", version);
                testParameters.put("deviceName", deviceName);
                testParameters.put("mxosIp", mxosIp);
                testParameters.put("mxosPort", mxosPort);

                // Loop instantiations
                Map<String, XmlTest> test = new HashMap<String, XmlTest>();
                List<String> groups = new ArrayList<String>();
                List<XmlPackage> packages = new ArrayList<XmlPackage>();
                test.put("test" + i, new XmlTest());
                test.get("test" + i).setSuite(suite);
                
                // Set those parameters into each of our tests
                test.get("test" + i).setParameters(testParameters);
                
                // Add the group to our test
                groups.add(groupName);
                test.get("test" + i).setIncludedGroups(groups);
                
        // add multiple exclude group name
        if (excludeGroupName.contains(",")) {
          String[] excludeGroupNames = excludeGroupName.split(",");
          for (int j = 0; j < excludeGroupNames.length; j++) {
            test.get("test" + i).addExcludedGroup(excludeGroupNames[j]);
          }
        } else {
          test.get("test" + i).addExcludedGroup(excludeGroupName);
        }

        
        /*
         * Since retry mechanism is introduced to framework, a TestNG test suite will be formed
         * conditionally:
         * 1) When a suite is executed as retry, we extract skipped and failed
         * classes from previous test result, then form the suite only with those classes.
         * 2) When a suite is executed as normal execution or as the initial round of multiple
         * retries, we form the suite with all test packages included.
         */
        System.out.println("Get start to check retry");
        try {
          Logging.info("*************************************This is a retry execution.*******************");

            if (Boolean.valueOf(executeAsRetry)) {
                int currentRetryNumber = Integer.valueOf(RetryHelper.getCurrentRetryCount());
                int maxRetryNumber = Integer.valueOf(RetryHelper.getMaxRetryCount());
                
                Logging.info("Current retry count (execution finished): " + currentRetryNumber);
                Logging.info("Max retry count: " + maxRetryNumber);
                
                if (currentRetryNumber < maxRetryNumber) {
                    
                    // Parse previous test results to extract skipped and failed names of test class.
                    // Combine both skipped and failed tests as the new retry test set.
                    try {
                        
                        // Parse previous test results
                        TestResultsParser parser = new TestResultsParser();
                        List<XmlClass> skippedClass = parser.getSkippedXmlClasses();
                        List<XmlClass> failedClass = parser.getFailedXmlClasses();
                        
                        Logging.info("Skipped tests in previous execution: ");
                        for (XmlClass skipped : skippedClass) {
                            Logging.info(skipped.getName());
                        }
                        
                        Logging.info("Failed tests in previous execution: ");
                        for (XmlClass failed : failedClass) {
                            Logging.info(failed.getName());
                        }
                        
                        // Combine both skipped and failed tests as the new retry test set.
                        retryClasses.addAll(skippedClass);
                        retryClasses.addAll(failedClass);
                        
                        // Add retry test set to TestNG test suite.
                        test.get("test" + i).setXmlClasses(retryClasses);
                    } catch (DocumentException e) {
                        Logging.error("Could not find existing testng-results.xml. Retry cancelled.");
                    }
                    
                    // Increase current retry count.
                    RetryHelper.updateRetryProperties(executeAsRetry, maxRetryCount, 
                            String.valueOf(currentRetryNumber + 1));
                }
                
                // Move previous test results to backup folder.
                RetryHelper.backupTestResults(String.valueOf(currentRetryNumber));
                
            } else {
                
                // Normal execution of first run of retry rounds, update retry properties.
                RetryHelper.updateRetryProperties(executeAsRetry, maxRetryCount, String.valueOf(1));
                
                // Add all packages
                packages.add(new XmlPackage(packagesToLoad));
                test.get("test" + i).setXmlPackages(packages);
            }
            
        } catch (IOException e) {
            Logging.error("Error occurred when accessing retry properties file: " 
                    + e.getMessage());
        }
        Logging.info("*************************************test NG*******************");

                // Preserve the order of tests
                test.get("test" + i).setPreserveOrder("true");
                
                // Set Generic Test Name
                test.get("test" + i).setName("Test on " + platforms[i]);
                
                // Add all our tests
                tests.add(test.get("test" + i));
            }
            
            suite.setTests(tests);
            suites.add(suite);
            testng.setXmlSuites(suites);
            
            // Start tests.
            testng.run();
            System.out.println("finish running");

        } catch (ArrayIndexOutOfBoundsException ex) {
        	System.out.println(ex.getMessage());
        }
        System.exit(0);
    }
	
}


