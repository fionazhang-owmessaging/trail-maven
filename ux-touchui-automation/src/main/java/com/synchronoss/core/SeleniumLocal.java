/*************************************************************************
@Class Name  : SeleniumLocal
@Author		 : Alkesh Dagade
@Created On  : 08/29/2012
@Description : This class is for creating Selenium/WebDriver object to execute suite on local machine i.e. without grid.
@Modified on : 	
@Referred Test Case Number : NA 
@Target Application  : Rich Mail 4.x
@Revised By     :Alkesh
@Changes     : NA
'*************************************************************************/
package synchronoss.com.core;

import io.appium.java_client.ios.IOSDriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumLocal extends Base {

	public static WebDriver driver = null;

	/*************************************************************************
	 * @Method Name : getDriver
	 * @Author : Alkesh Dagade
	 * @Created On : 08/29/2012
	 * @Description : This method is to get WebDriver instance.
	 * @parameter 1 : NA
	 *************************************************************************/
	public WebDriver getDriver() {
		return SeleniumLocal.driver;
	}

	/*************************************************************************
	 * @Method Name : SeleniumLocal (constructor)
	 * @Author : Alkesh Dagade
	 * @Created On : 08/29/2012
	 * @Description : This method is to create WebDriver and Selenium instance
	 *              as per browser type.
	 * @parameter 1 : browser: Browser name on which suite to be possible values
	 *            are: Firefox, Chrome and internet explorer
	 * @parameter 2 : url: application URL on which suite to be executed
	 *************************************************************************/
	public SeleniumLocal(String platform) {

	}

	@Override
	public void startSession(String url) {
		Logging.info("Navigating to: " + url);
		// Navigating to test site.
		SeleniumLocal.driver.get(url);
	}

}
