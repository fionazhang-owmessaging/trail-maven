/*************************************************************************
@Class Name  : PropertyHelper
@Author		 : Sravani Akkineni
@Created On  : 07/03/2013
@Description : This class is for creating Property file instance for all property files.
@Modified on : 	
@Referred Test Case Number : NA 
@Target Application  : App Suite
@Revision Date     :
@Revised By     :
@Changes     :
'*************************************************************************/
package synchronoss.com.core;


public class PropertyHelper {
	
    private static String userInterface;
    
    public static String getUserInterface() {
        return userInterface;
    }
    
    public static void setUserInterface(String ui) {
        userInterface = ui;
    }
    
    public static PropertyFiles pageObjectBaseFile = new PropertyFiles("PageObjectBase");
    public static PropertyFiles coreLoginFile = new PropertyFiles("CoreLogin");
    public static PropertyFiles coreNavigationFile = new PropertyFiles("CoreNavigation");
    public static PropertyFiles coreEmailFile = new PropertyFiles("CoreEmail");
    public static PropertyFiles coreEmailComposer = new PropertyFiles("CoreEmailComposer");
    public static PropertyFiles coreEmailDetail = new PropertyFiles("CoreEmailDetail");
    public static PropertyFiles coreContacts = new PropertyFiles("CoreContacts");
    public static PropertyFiles coreContactDetail = new PropertyFiles("CoreContactDetail");
    public static PropertyFiles coreCalendar = new PropertyFiles("CoreCalendar");
    public static PropertyFiles coreEventDetail = new PropertyFiles("CoreEventDetail");
    public static PropertyFiles coreSettings = new PropertyFiles("CoreSettings");
    public static PropertyFiles CoreExternal = new PropertyFiles("CoreExternal");
    public static PropertyFiles coreSidebar = new PropertyFiles("CoreSidebar");
    public static PropertyFiles coreTask = new PropertyFiles("CoreTask");
    public static PropertyFiles coreTaskDetail = new PropertyFiles("CoreTaskDetail");


}
