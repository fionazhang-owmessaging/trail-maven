package synchronoss.com.core;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import synchronoss.com.testcase.utils.ConstantName;


public class Tools {

	/**
	 * <b>Date</b>: Dec 21, 2015 <br>
	 * <b>Description</b><br>
	 * Get a screen shot for current situation<br>
	 * 
	 * @param String
	 *            imageName - saved image name
	 * @return void
	 */
	public static void captureScreen(String imageName) {

		final String dir = System.getProperty("user.dir");
		final String SCREENSHOTDIR = "/screenshots/";
		String directory = dir + SCREENSHOTDIR;

		Logging.info("[SCREENSHOT] Saving image to: " + directory);
		Logging.info("[SCREENSHOT] Captured with name: " + imageName + ".png");
		WebDriver augmentedDriver = new Augmenter().augment(Base.base
				.getDriver());
		File srcFile = ((TakesScreenshot) augmentedDriver)
				.getScreenshotAs(OutputType.FILE);
		try {
			// Remove existing pictures
			FileUtils.copyFile(srcFile,
					new File(directory + imageName + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <b>Author</b>: Jerry Zhang <br>
	 * <b>Date</b>: Jun 02, 2015 <br>
	 * <b>Description</b><br>
	 * set the following WebDriver values to be default:<br>
	 * - focused content of the locator<br>
	 * - implicit wait timeout<br>
	 * 
	 * @return void
	 */
	public static void setDriverDefaultValues() {
		try {
//			Base.base.getDriver().switchTo().defaultContent();
			Base.base
					.getDriver()
					.manage()
					.timeouts()
					.implicitlyWait(Long.parseLong(Base.timeout),
							TimeUnit.SECONDS);
		} catch (WebDriverException ex) {
			Logging.warn("Fail to set WebDriver to default values");
		}
	}

	/**
	 * Waits until the given period of time has been expired and then continue
	 * normal execution. The method will explicitly keep Selenium to wait
	 * without any kind of shortcut to continue if some element becomes
	 * available. In addition for that behavior Thread.sleep() is explicitly not
	 * used due to timeout issues for longer periods.
	 */
	public static void waitFor(long period, TimeUnit unit) {
		Logging.info("Time before driver wait: " + (new Date()).toString());

		long timeout = unit.toSeconds(period);

		Base.base.getDriver().manage().timeouts().implicitlyWait(timeout, unit);

		try {
			Base.base.getDriver()
					.findElement(
							By.xpath("//not-existing-element-"
									+ (new Date()).getTime()));
		} catch (NoSuchElementException nsee) {
			Logging.info("Time after driver wait: " + (new Date()).toString());
		} finally {
			Base.base
					.getDriver()
					.manage()
					.timeouts()
					.implicitlyWait(Long.parseLong(Base.timeout),
							TimeUnit.SECONDS);
		}
	}

	/*************************************************************************
	 * @Method Name : waitUntilElementDisplayedByXpath
	 * @Author : Jerry Zhang
	 * @Created on : 09/03/2014
	 * @Description : This method will detect and wait for the expected element
	 *              to be displayed in view with the specified timeout value.
	 * @Parameter1 : String element - XPath expression of the element
	 * @Parameter2 : int timeoutInSeconds - timeout value in seconds
	 * @Return: boolean - true: element displayed / false: element not displayed
	 **************************************************************************/
	public static boolean waitUntilElementDisplayedByXpath(String element,
			int timeoutInSeconds) {
		// Set WebDriver's wait time to 1 second. So each iteration will wait
		// for at most 1 second.
		Base.base.getDriver().manage().timeouts()
				.implicitlyWait(1, TimeUnit.SECONDS);

		boolean isDisplayed = false;

		try {
			WebElement visibleElement = (new WebDriverWait(
					Base.base.getDriver(), timeoutInSeconds))
					.until(ExpectedConditions.visibilityOfElementLocated(By
							.xpath(element)));

			isDisplayed = visibleElement.isDisplayed();
			if (!isDisplayed) {
				Logging.warn("Element still not displayed after "
						+ timeoutInSeconds + " seconds, XPath: " + element);
			}

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			throw ex;
		}

		finally {
			// Reset WebDriver's wait time to default.
			Base.base
					.getDriver()
					.manage()
					.timeouts()
					.implicitlyWait(Long.parseLong(Base.timeout),
							TimeUnit.SECONDS);
		}

		return isDisplayed;
	}

	// /*************************************************************************
	// * @Method Name : waitUntilElementNotDisplayedByXpath
	// * @Author : Jerry Zhang
	// * @Created on : 07/28/2014
	// * @Description : This method will detect and wait for the expected
	// element
	// * to be NOT displayed in view (for at most 30 seconds).
	// * @Parameter : String element - XPath expression of the element
	// **************************************************************************/
	// public void waitUntilElementNotDisplayedByXpath(String element) {
	// // Set WebDriver's wait time to 2 seconds.
	// Base.base.getDriver().manage().timeouts()
	// .implicitlyWait(2, TimeUnit.SECONDS);
	//
	// boolean isDisplayed = true;
	//
	// try {
	// for (int i = 0; i < 30; i++) {
	// Thread.sleep(1000);
	//
	// try {
	// if (!Base.base.getDriver().findElement(By.xpath(element)).isDisplayed())
	// {
	// isDisplayed = false;
	// break;
	// }
	// } catch (StaleElementReferenceException ex) {
	// // Element not attached to DOM any more, leave for checking in next
	// iteration.
	// }
	// }
	//
	// if (isDisplayed) {
	// Logging.warn("Element still displayed after 30 seconds waiting, XPath: "
	// + element);
	// }
	//
	// } catch (NoSuchElementException ex) {
	// // Element not found.
	// } catch (InterruptedException e) {
	// e.printStackTrace();
	// } finally {
	// // Reset WebDriver's wait time to default.
	// Base.base.getDriver().manage().timeouts()
	// .implicitlyWait(Long.parseLong(Base.timeout), TimeUnit.SECONDS);
	// }
	// }

	/*************************************************************************
	 * @Method Name : waitUntilElementNotDisplayedByXpath
	 * @Author : Jerry Zhang
	 * @Created on : 09/03/2014
	 * @Description : This method will detect and wait for the expected element
	 *              to be NOT displayed in view for a specific timeout value.
	 * @Parameter1 : String element - XPath expression of the element
	 * @Parameter2 : int timeoutInSeconds - timeout value in seconds
	 * @Return: boolean - true: element displayed / false: element not displayed
	 **************************************************************************/
	public static boolean waitUntilElementNotDisplayedByXpath(String element,
			int timeoutInSeconds) {
		// Set WebDriver's wait time to 3 seconds.
		Base.base.getDriver().manage().timeouts()
				.implicitlyWait(3, TimeUnit.SECONDS);

		boolean isDisplayed = true;

		try {
			isDisplayed = new WebDriverWait(Base.base.getDriver(),
					timeoutInSeconds).until(ExpectedConditions
					.invisibilityOfElementLocated(By.xpath(element)));
			if (!isDisplayed) {
        Logging.warn("The element " + element + " is still displayed after " + timeoutInSeconds
            + " seconds.");
      }
		}catch(StaleElementReferenceException ex){
		  Logging.warn("The element is stale");
			  
		} catch (NoSuchElementException ex) {
			throw ex;
		}
		// Element not found.
		finally {
			// Reset WebDriver's wait time to default.
			Base.base
					.getDriver()
					.manage()
					.timeouts()
					.implicitlyWait(Long.parseLong(Base.timeout),
							TimeUnit.SECONDS);
		}

		return isDisplayed;
	}

	/*************************************************************************
	 * @Method Name : scrollElementIntoViewByXpath
	 * @Author : Jerry Zhang
	 * @Created on : 07/28/2014
	 * @Description : This method will scroll the specified web element into
	 *              view by executing javascript code.
	 * @Parameter : String element - XPath expression of the element
	 **************************************************************************/
	public static void scrollElementIntoViewByXpath(String element) {
		try {
			if (Base.platformName.equalsIgnoreCase(ConstantName.IOS)) {
				int i = 0;
				while (i < 5) {
					try {
						WebElement destElement = Base.base.getDriver()
								.findElement(By.xpath(element));
						((JavascriptExecutor) Base.base.getDriver())
								.executeScript(
										"arguments[0].scrollIntoView(true);",
										destElement);
						// To quit loop.
						i = 5;
					} catch (StaleElementReferenceException ex) {
						i += 1;
					}
					// Wait for 500 milliseconds to let the view update.
					Thread.sleep(500);
				}
			} else if (Base.platformName.equalsIgnoreCase(ConstantName.ANDROID)) {
				int i = 0;
				while (i < 5) {
					try {
						WebElement destElement = Base.base.getDriver()
								.findElement(By.xpath(element));
						
							destElement.getAttribute("class");

						((JavascriptExecutor) Base.base.getDriver())
								.executeScript(
										"arguments[0].scrollIntoView(true);",
										destElement);

					} catch (StaleElementReferenceException ex) {
						Logging.info("Let's continue scroll" + ex.getMessage());
						i += 1;
					}
					catch (ElementNotVisibleException ex) {
						Logging.info("Let's continue scroll" + ex.getMessage());
						i += 1;
					}

					i = i + 5;
					// Wait for 1000 milliseconds to let the view update.
					Thread.sleep(1000);
				}

			}
		} catch (NoSuchElementException ex) {
			Logging.error("Could not scroll element into view; element not found with xpath: "
					+ element);
			// Do not throw exception here.
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 08, 2015<br>
	 * <b>Description</b><br>
	 * Moves WebDriver focus to the specified web element.
	 * 
	 * @param elementXpath
	 *            Xpath value of the element
	 * @return void
	 */
	public static void moveFocusToElementByXpath(String elementXpath) {
		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(elementXpath);
			WebElement element = Base.base.getDriver().findElement(
					By.xpath(elementXpath));
			Action moveTo = new Actions(Base.base.getDriver()).moveToElement(
					element).build();
			moveTo.perform();
		} catch (WebDriverException ex) {
			Logging.warn("Could not move focus to element: " + elementXpath);
		}
	}

	/***************************************************************************
	 * @Method Name: captureFullStackTracek
	 * @Author : Ravid Te
	 * @Created : 11/17/13
	 * @Description : This method will help provide full stacktrace report
	 *              within the testlink report
	 * @parameter 1 : NoSuchElementException object
	 *
	 ***************************************************************************/
	public static void captureFullStackTrace(WebDriverException ex) {
		StringWriter errors = new StringWriter();
		ex.printStackTrace(new PrintWriter(errors));
		Assert.fail(errors.toString());
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Aug 07, 2015<br>
	 * <b>Description</b><br>
	 * transfer color to RGB string <b>Parameters</b><br>
	 * Color color
	 */
	public static String color2RgbString(Color color) {
		String r = String.valueOf(color.getRed());
		String g = String.valueOf(color.getGreen());
		String b = String.valueOf(color.getBlue());
		return r + ", " + g + ", " + b;
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Aug 07, 2015<br>
	 * <b>Description</b><br>
	 * transfer color to hex string <b>Parameters</b><br>
	 * Color color
	 */
	public static String Color2HexString(Color color) {
		String R = Integer.toHexString(color.getRed());
		R = R.length() < 2 ? ('0' + R) : R;
		String B = Integer.toHexString(color.getBlue());
		B = B.length() < 2 ? ('0' + B) : B;
		String G = Integer.toHexString(color.getGreen());
		G = G.length() < 2 ? ('0' + G) : G;
		return '#' + R + B + G;
	}

	/**
	 * Get a ZoneDateTime value from a String
	 *
	 * @param time
	 *            The string need to squeez time
	 * 
	 * @param zoneID
	 *            The Zone id of the current time
	 * 
	 * @param date
	 *            The date of the split time
	 * 
	 * @author Fiona Zhang
	 */
	public static ZonedDateTime splitTimeFromString(String time, ZoneId zoneID,
			LocalDate date) {
		DateTimeFormatter format1 = DateTimeFormatter.ofPattern("HH:mm");

		String originalTime = time;
		String[] s = originalTime.split(" ");
		String temp = s[s.length - 1];
		Logging.info("Get the time from the string as : " + temp);
		ZonedDateTime zoneDateTime = ZonedDateTime.of(date,
				LocalTime.parse(temp, format1), zoneID);
		Logging.info("The zone time is " + zoneDateTime);
		return zoneDateTime;
	}

	/**
	 * Get a ZoneDateTime value from a String
	 *
	 * @param localTime
	 *            The localTime
	 * 
	 * @param zoneID
	 *            The Zone id of the current time
	 * 
	 * @param date
	 *            The date of the split time
	 * 
	 * @author Fiona Zhang
	 */
	public static ZonedDateTime splitTimeFromString(LocalTime localTime,
			ZoneId zoneID, LocalDate date) {
		ZonedDateTime zoneDateTime = ZonedDateTime.of(date, localTime, zoneID);
		Logging.info("The zone time is " + zoneDateTime);
		return zoneDateTime;
	}

	/**
	 * Return a time converted by time zone
	 *
	 * @param zoneDateTime
	 *            The zone time you need to convert
	 * 
	 * @param zoneID
	 *            The zone id need to changed
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public static LocalDateTime getTimeFromConvertTimeZone(
			ZonedDateTime zoneDateTime, ZoneId zoneID) {
		Logging.info("Original time is : " + zoneDateTime.toLocalDateTime()
				+ " and time zone is " + zoneID);
		ZoneId changedZoneID = zoneID;
		ZonedDateTime newZoneTime = zoneDateTime
				.withZoneSameInstant(changedZoneID);
		Logging.info("Get new time for time zone " + zoneID.getId()
				+ " and time as : " + newZoneTime.toLocalDateTime());
		return newZoneTime.toLocalDateTime();
	}

	/**
	 * Return boolean type to check if the test data matches the regex
	 *
	 * @param regex
	 *            the regex of
	 * 
	 * @param testData
	 *            the test data
	 * 
	 * 
	 * @author Fiona Zhang
	 */
	public static boolean isMatchString(String regex, String testData) {
		boolean isFound = false;
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(testData);
		isFound = m.find();
		Logging.info("the matching result of regex is : " + isFound);
		return isFound;
	}

	
	}


