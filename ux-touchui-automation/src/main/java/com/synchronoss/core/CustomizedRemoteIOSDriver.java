package synchronoss.com.core;

import io.appium.java_client.ios.IOSDriver;

import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.HasTouchScreen;
import org.openqa.selenium.interactions.TouchScreen;
import org.openqa.selenium.remote.RemoteTouchScreen;

/**
 * @author Jerry Zhang
 * Customized version of Remote Webdriver. Only use this version to track CSS class values.
 * A parameter from the build environment (Ant) should determine whether to track CSS class values.
 */
public class CustomizedRemoteIOSDriver extends SwipeableIOSWebDriver{

	public CustomizedRemoteIOSDriver(URL remoteAddress, Capabilities desiredCapabilities) {
		super(remoteAddress, desiredCapabilities);

	}

	@Override
	public WebElement findElement(By by) {		
		String xpath = by.toString().replaceFirst("By.xpath: ", "");
		String cssClass;
		try {
			// Get css class value
			cssClass = super.findElement(by).getAttribute("class");
		} catch (NoSuchElementException e) {
			cssClass = CssClassTracker.NULL_SIGN;
		}
		
		// Add this record to CSS tracker
		Base.cssTracker.add(xpath, cssClass);
		
		// Do the real job
		return super.findElement(by);
	}

}
