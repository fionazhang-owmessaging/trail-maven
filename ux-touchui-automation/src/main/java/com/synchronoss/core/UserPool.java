package synchronoss.com.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;

public class UserPool {

	// Fields and getters.
	private JSONArray users;
	private Map<String, Integer> sessionData = new HashMap<String, Integer>();
	private List<String> columnHeaders = new ArrayList<String>();
	private List<String> userTracker = new ArrayList<String>();
	
    private String testUser = "touch%s@openwave.com" ;
    private String cosID = "sender";
    private String mxosURL = null;
    private String mxosPort = null;
    private String userName = null;
    private String userPassword = "p";
    private int countOfRetryCreateUser = 30;
    private int resultOfCreateMxos = 1;
    private JSONObject jsonThread = new JSONObject();
    private JSONObject json = new JSONObject();

	private JSONArray getUsers() {
		return this.users;
	}

	/*************************************************************************
	 * @Method Name : UserPool
	 * @Author : Ravid Te
	 * @Created On : 10/30/2013
	 * @Description : Constructor to generate userpool from excel
	 *************************************************************************/
	public UserPool(String userPoolFile) {
		final String dir = System.getProperty("user.dir");
		String sourceFile = dir + "//ux-touchui-automation///src//main//java//resource//userpool//" + userPoolFile;
//		String sourceFile = dir + "\\openwave-automation\\test-datasheets\\" + userPoolFile;
//		properties/userpool/Fusion_userPool.xlsx
		System.out.println("user pool is in dir: " + sourceFile);
		FileInputStream inp;
		Workbook workbook = null;
///D:\SourceCode\Workspace1\fiona-ux2.1-touchui-automation\openwave-automation\test-datasheets\Fusion_userPool.xlsx
		try {
			File file = new File(sourceFile);
			inp = new FileInputStream(file);
			workbook = WorkbookFactory.create(inp);
			// Get the first Sheet.
			Sheet sheet = workbook.getSheetAt(0);
			// Start constructing JSON.
			this.users = new JSONArray();

			// Iterate through the rows.
			for (Iterator<Row> rowsIT = sheet.rowIterator(); rowsIT.hasNext();) {
				JSONObject userData = new JSONObject();
				Row row = rowsIT.next();
				int index = 0;
				// Tests if the row is empty (no cells available).
				boolean emptyRow = true;

				// Iterate through the cells.
				for (Iterator<Cell> cellsIT = row.cellIterator(); cellsIT
						.hasNext();) {
					emptyRow = false;
					Cell cell = cellsIT.next();
					cell.setCellType(1);
					// add in our column headers
					if (row.getRowNum() == 0) {
						columnHeaders.add(cell.getStringCellValue());

					} else {
						userData.put(columnHeaders.get(index),
								cell.getStringCellValue());
					}
					index++;
				}
				// Add our user data but exclude the column headers
				if ((row.getRowNum() != 0) && (!emptyRow)) {
					this.users.put(userData);
					this.userTracker.add("free");
				}
			}
			System.out.println("Finished initialize user pool");
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}catch(NullPointerException ex){
			ex.printStackTrace();
		}
	}

  /**
   *************************************************************************
   * @Method Name UserPool
   * @Description Constructor to generate userpool from mxos
   * @param mxosURL the ip address of mxos
   * @param mxosPort the port of mxos
   *************************************************************************/

  public UserPool(String mxosURL, String mxosPort) {
    Logging.info("Initialize user pool by creating dynamic mxos url");
    this.mxosURL = mxosURL;
    this.mxosPort = mxosPort;
    int i = 1;
    try {
      while (i <= 4) { // hard code for 4 is due to user map we defined in test case is 4 users in each case
        userName = this.generateRandomUser(testUser);
        Logging.info("get random user: " + userName);
        // if the user created sucessfully, then put it in json object
        if (this.createUserByMxos(this.getCreateUserURL(userName, mxosURL, mxosPort), testUser) == 0) {
          json.put("username_" + i, userName);
          json.put("password_" + i, userPassword);
          json.put("emailid_" + i, userName);
          jsonThread.put(Base.base.getDriver().toString(), json);
          json = jsonThread.getJSONObject(Base.base.getDriver().toString());
          Logging.info("base get driver and put json object with user into the thread"
              + json.toString());
          i++;
        }
      }
    } catch (JSONException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    Logging.info("Finsih inintailze user pool......");
  }

  /**
	 * <b>Author</b>: <br>
	 * <b>Date</b>: <br>
	 * <b>Description</b><br>
	 * Obtains a free user from the pool and attachs it to a specific WebDriver
	 * session.
	 *
	 * @return index number
	 */
	public boolean obtainUserByDriver(WebDriver webDriver) {
		int freeUserIndex = this.findFreeUserIndex();
		if (freeUserIndex == -1) {
			return false;
		} else {
            this.sessionData.put(webDriver.toString(), freeUserIndex);
			this.userTracker.set(freeUserIndex, "taken");
			return true;
		}
	}

	/**
	 * <b>Author</b>: <br>
	 * <b>Date</b>: <br>
	 * <b>Description</b><br>
	 * Finds the index number of the first free user in pool.
	 *
	 * @return index number
	 */
	private int findFreeUserIndex() {
		int freeUserIndex = -1;
		int count = 0;

		while (count < 10) {
			freeUserIndex = this.userTracker.indexOf("free");

			if (freeUserIndex == -1) {
				Logging.info("NO FREE USERS IN USERPOOL!  Waiting until one free's up ...");
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			count++;
		}
		return freeUserIndex;
	}

	/**
	 * <b>Author</b>: <br>
	 * <b>Date</b>: <br>
	 * <b>Description</b><br>
	 * Frees the user which has been attached to a WebDriver session.
	 *
	 * @return void
	 */
	public void setUserFree(WebDriver webDriver) {
		System.out.println("The user is freen " + webDriver.toString());

		int index = this.sessionData.get(webDriver.toString());
		System.out.println("The user is freen " + index);

		this.userTracker.set(index, "free");
	}

	/**
	 * <b>Author</b>: <br>
	 * <b>Date</b>: <br>
	 * <b>Description</b><br>
	 * Extracts data from a cell with the specified header name.
	 *
	 * @return cell data content
	 */
	private String getCellData(String headerName) {
		String data = "";
		try {
		  if(Base.ifCreateDynamicUser){
            // use mxos to get user
            JSONObject json = (JSONObject) this.getUsersByMxos();
            data = json.getString(headerName);
		  }
		  else{
	          int index = this.sessionData.get(Base.base.getDriver().toString());
	          JSONObject json = (JSONObject) this.getUsers().get(index);
	          data = json.getString(headerName);
		  }
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return data;
	}

	// Encapsulated cell data getter methods.
	public String getEmailId1() {
		return this.getCellData("emailid_1");
	}

	public String getUserName1() {
		return this.getCellData("username_1");
	}

	public String getPassword1() {
		return this.getCellData("password_1");
	}

	public String getEmailId2() {
		return this.getCellData("emailid_2");
	}

	public String getUserName2() {
		return this.getCellData("username_2");
	}

	public String getPassword2() {
		return this.getCellData("password_2");
	}

	public String getEmailId3() {
		return this.getCellData("emailid_3");
	}

	public String getUserName3() {
		return this.getCellData("username_3");
	}

	public String getPassword3() {
		return this.getCellData("password_3");
	}

	public String getEmailId4() {
		return this.getCellData("emailid_4");
	}

	public String getUserName4() {
		return this.getCellData("username_4");
	}

	public String getPassword4() {
		return this.getCellData("password_4");
	}
	
	

  /**
   * Generate random users according to the prefix
   * 
   * @param prefix , the prefix of the user account
   * @return String - generated user
   */
  public String generateRandomUser(String prefix) {
    Random random = new Random();
    String user = String.format(prefix, String.valueOf(random.nextInt(1000000000)));
    return user;
  }

  /**
   * Generate the create user URL
   * 
   * @param user - that need to create
   * @param mxosIp - mxos IP address
   * @param mxosPort - mxos port
   * 
   * @return return the MXOS URL of create user
   */
  public String getCreateUserURL(String user, String mxosIp, String mxosPort) {
    String url = "http://%s:%s/mxos/mailbox/v2/" + user + "/";
    url = url.format(url, mxosIp, mxosPort, user);

    Logging.info("Create user url is : " + url);

    return url;
  }

  /**
   * Create user by Mxos put request
   * 
   * @param postUrl - mxos create user url
   * @param user - user prefix
   * @return 0 is successful, 1 is not successful
   * @throws ClientProtocolException
   * @throws IOException
   */
  public int createUserByMxos(String postUrl, String user) {

    HttpClient client = new DefaultHttpClient();
    HttpPut putRequest = new HttpPut(postUrl);
    HttpResponse response = null;

    // add http header to create mxos request string
    putRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");

    // set name parameter to the post action
    List<NameValuePair> nvps = new ArrayList<NameValuePair>();
    nvps.add(new BasicNameValuePair("cosId", cosID));
    nvps.add(new BasicNameValuePair("password", userPassword));

    // send the request to mxos
    try {
      putRequest.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
      response = client.execute(putRequest);
    } catch (UnsupportedEncodingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (ClientProtocolException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    // if the response return 200, then the user is created successfully
    if (response.getStatusLine().toString().contains("200"))
      resultOfCreateMxos = 0;
    else
      resultOfCreateMxos = 1;
    return resultOfCreateMxos;
  }

  /**
   * Create a user cell to generate a json object for the user
   * 
   * @param userName - login user name
   * @param password - login user password
   * @param email - email address
   * @param count - the count for user created
   * @return a json object
   */
  public JSONObject createUserCell(String userName, String password, String email, String count) {

    JSONObject json = new JSONObject();
    try {

      json.put("username_" + count, userName);
      json.put("password_" + count, password);
      json.put("emailid_" + count, email);

    } catch (JSONException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return json;

  }
    
  /**
   * get user from the json thread according to the get driver string
   * 
   * @return a json object
   */
  private JSONObject getUsersByMxos() {
    try {
      json = jsonThread.getJSONObject(Base.base.getDriver().toString());
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return this.json;
  }
}
