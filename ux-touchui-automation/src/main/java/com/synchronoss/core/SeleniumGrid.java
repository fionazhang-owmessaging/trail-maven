package synchronoss.com.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SeleniumGrid extends Base {
	DesiredCapabilities capability;
	/*
	 * Using "ThreadLocal" typed WebDriver static field to ensure that each
	 * TestNG test thread handles only 1 driver session, which is bound to a
	 * specific browser on Selenium Gird node machines.
	 */
	public static InheritableThreadLocal<RemoteWebDriver> driver = new InheritableThreadLocal<RemoteWebDriver>();

	/*************************************************************************
	 * @Method Name : getDriver
	 * @Author : Alkesh Dagade
	 * @Created On : 08/29/2012
	 * @Description : This method is to get WebDriver instance.
	 * @parameter 1 : NA
	 *************************************************************************/
	public RemoteWebDriver getDriver() {
		return SeleniumGrid.driver.get();
	}

  /*************************************************************************
   * @param platformVersion
   * @Method SeleniumGrid (constructor)
   * @Author Alkesh Dagade
   * @Created On 08/29/2012
   * @Description This method is to create RemoteWebDriver and Selenium instance.
   * @parameter hub: The hub ip
   * @parameter platform: Android or IOS
   * @parameter enableTracker: Trace for the css or not
   * @parameter platformVersion: The version of platform, like 9.3, 10.0 etc.
   * @parameter deviceName: The name of device, like iPad Air 2, ipad simulator, iphone
   *            simulator, 02157df2da6c3c2a etc.
   *************************************************************************/
  public SeleniumGrid(String hub, String platform, boolean enableTracker, String platformVersion,
      String deviceName) throws MalformedURLException {

    if (platform.equalsIgnoreCase("Android")) {
      Logging.info("Start android test :>>>> .............." + hub + " platform: " + platform);
      capability = new DesiredCapabilities();
      // DesiredCapabilities.android();
      capability.setCapability("platformName", "Android");
      capability.setCapability("deviceName", deviceName); // here need to check the adb devices to
                                                          // set the deviceName
      // capability.setCapability("nativeWebTap", true);
      capability.setCapability(CapabilityType.VERSION, platformVersion);
      // capability.setJavascriptEnabled(true);
      // capability.setCapability("autoWebview", "true");
      capability.setCapability("androidPackage", "com.android.chrome");
      // capability.setCapability("unicodeKeyboard", "true");
      capability.setCapability("app", "/Users/UIA/Downloads/com.android.chrome_52.apk");

      try {
        if (enableTracker)
          SeleniumGrid.driver.set(new CustomizedRemoteAndroidDriver(new URL(hub), capability));

        else {
          SeleniumGrid.driver.set(new SwipeableAndroidWebDriver(
          // new URL(hub), capability));

              new URL("http://10.37.2.202:4723/wd/hub"), capability));
        }
      } catch (MalformedURLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } else if (platform.equalsIgnoreCase("IOS")) {
      capability = new DesiredCapabilities();
      capability.setCapability(CapabilityType.BROWSER_NAME, "Safari");
      capability.setCapability("platformName", "iOS");
      capability.setCapability("platformVersion", platformVersion);
      if (!platformVersion.equals("9.3"))
        capability.setCapability("automationName", "XCUITest");
      capability.setCapability("nativeWebTap", true);
      capability.setCapability("deviceName", deviceName);
      capability.setJavascriptEnabled(true);
      capability.setCapability("app", "Safari");
      try {
        if (enableTracker)
          SeleniumGrid.driver.set(new CustomizedRemoteIOSDriver(new URL(hub), capability));
        else
          SeleniumGrid.driver.set(new SwipeableIOSWebDriver(new URL(hub), capability));

      } catch (MalformedURLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    }

  }

	@Override
	public void startSession(String url) {
		// Get and print out WebDriver session info.
		String ipOfNode = getIPOfNode(SeleniumGrid.driver.get());
		String sessionData = getDriver().toString();
		Logging.info("STARTED SESSION ON: " + ipOfNode + " using "
				+ sessionData);

		// Navigating to test site.
		Logging.info("Navigating to: " + url);
		SeleniumGrid.driver.get().get(url);
	}

	/*************************************************************************
	 * @Method Name : getIPOfNode
	 * @Author : Ravid Te
	 * @Created On : 10/30/2013
	 * @Description : This method will extract the node data from the hub
	 * @parameter 1 : remoteDriver: this is the remoteDriver instance
	 *************************************************************************/
	private static String getIPOfNode(RemoteWebDriver remoteDriver) {
		String hostFound = null;
		try {
			HttpCommandExecutor ce = (HttpCommandExecutor) remoteDriver
					.getCommandExecutor();
			String hostName = ce.getAddressOfRemoteServer().getHost();
			int port = ce.getAddressOfRemoteServer().getPort();
			HttpHost host = new HttpHost(hostName, port);
			DefaultHttpClient client = new DefaultHttpClient();
			URL sessionURL = new URL("http://" + hostName + ":" + port
					+ "/grid/api/testsession?session="
					+ remoteDriver.getSessionId());
			BasicHttpEntityEnclosingRequest r = new BasicHttpEntityEnclosingRequest(
					"POST", sessionURL.toExternalForm());
			HttpResponse response = client.execute(host, r);
			JSONObject object = extractObject(response);
			URL myURL = new URL(object.getString("proxyId"));
			Logging.info(myURL.toString());
			if ((myURL.getHost() != null) && (myURL.getPort() != -1)) {
				hostFound = myURL.getHost();
			}
		} catch (Exception e) {
			System.err.println(e);
	         throw new RuntimeException("Failed to aquire remote webdriver node and port, root cause: ", e);
		}
		return hostFound;
	}

	/***************************************************************************************
	 * @Method Name : extractObject
	 * @Author : Ravid Te
	 * @Created On : 10/30/2013
	 * @Description : This method will get the http response into json format
	 *              for consumption
	 * @parameter 1 : httpResponse: the return value from the http request to
	 *            hub
	 ***************************************************************************************/
	private static JSONObject extractObject(HttpResponse resp)
			throws IOException, JSONException {
		InputStream contents = resp.getEntity().getContent();
		StringWriter writer = new StringWriter();
		IOUtils.copy(contents, writer, "UTF8");
		JSONObject objToReturn = new JSONObject(writer.toString());
		writer.close();
		contents.close();
		return objToReturn;
	}

}
