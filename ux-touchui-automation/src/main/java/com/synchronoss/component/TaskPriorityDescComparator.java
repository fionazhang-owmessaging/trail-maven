package synchronoss.com.component;

import java.util.Comparator;

import synchronoss.com.testcase.utils.TaskInfo;


public class TaskPriorityDescComparator implements Comparator<TaskInfo> {

	public int compare(TaskInfo task1, TaskInfo task2) {
		if (task1.getTaskPriority() > task2.getTaskPriority())
			return -1;
		else if (task1.getTaskPriority() < task2.getTaskPriority())
			return 1;
		else
			return 0;
	}
}