package synchronoss.com.component;

import java.util.Comparator;

import synchronoss.com.testcase.utils.TaskInfo;


public class TaskDueDateDescComparator implements Comparator<TaskInfo> {

	public int compare(TaskInfo task1, TaskInfo task2) {
		if (task1.getDueDateTime() != null && task2.getDueDateTime() != null) {
			if (task1.getDueDateTime().isBefore(task2.getDueDateTime()))
				return 1;
			else if (task1.getDueDateTime().isAfter(task2.getDueDateTime()))
				return -1;
			else
				return 0;
		} else if (task1.getDueDateTime() != null
				&& task2.getDueDateTime() == null)
			return -1;
		else if (task1.getDueDateTime() == null
				&& task2.getDueDateTime() != null)
			return 1;
		else
			return 0;
	}
}