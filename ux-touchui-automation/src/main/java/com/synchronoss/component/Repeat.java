package synchronoss.com.component;

public class Repeat {
	private String repeatInterval;	
	private String repeatEnding;
	private String repeatOccurrences;
	private String repeatEndDate;
	private String repeateDescription;

	public String getRepeatInterval() {
		return repeatInterval;
	}
	public void setRepeatInterval(String repeatInterval) {
		this.repeatInterval = repeatInterval;
	}
	public String getRepeatEnding() {
		return repeatEnding;
	}
	public void setRepeatEnding(String repeatEnding) {
		this.repeatEnding = repeatEnding;
	}
	public String getRepeatOccurrences() {
		return repeatOccurrences;
	}
	public void setRepeatOccurrences(String repeatOccurrences) {
		this.repeatOccurrences = repeatOccurrences;
	}
	public String getRepeatEndDate() {
		return repeatEndDate;
	}
	public void setRepeatEndDate(String repeatEndDate) {
		this.repeatEndDate = repeatEndDate;
	}
	public String getRepeateDescription() {
		return repeateDescription;
	}
	public void setRepeateDescription(String repeateDescription) {
		this.repeateDescription = repeateDescription;
	}

}
