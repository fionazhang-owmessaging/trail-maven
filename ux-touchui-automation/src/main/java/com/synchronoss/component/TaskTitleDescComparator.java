package synchronoss.com.component;

import java.util.Comparator;

import synchronoss.com.testcase.utils.TaskInfo;


public class TaskTitleDescComparator implements Comparator<TaskInfo> {

	public int compare(TaskInfo task1, TaskInfo task2) {
		String taskName1 = task1.getTaskTitle();
		String taskName2 = task2.getTaskTitle();
		return taskName2.compareTo(taskName1);

	}
}